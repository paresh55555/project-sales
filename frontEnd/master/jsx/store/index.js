import {compose, createStore, applyMiddleware } from 'redux';
import { routerMiddleware, push } from 'react-router-redux';
import { browserHistory } from 'react-router';
import storageMiddleware from '../middlewares/storageMiddleware';

import reducers from '../reducers';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

const logger = createLogger();
const middlewarer = routerMiddleware(browserHistory)
export default (initialState = {}) => {
  let middleware = applyMiddleware(thunk, middlewarer, storageMiddleware, logger);

  const store = createStore(reducers, middleware);

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      store.replaceReducer(require('../reducers').default);
    });
  }

  return store;
};






