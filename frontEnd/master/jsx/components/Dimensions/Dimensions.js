import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {setSize} from '../../actions/component'

class Dimensions extends React.Component {
    constructor() {
        super();
        this.state = {
          width: '',
          height: ''
        };
        this.setComponentSize = this.setComponentSize.bind(this);
        this.onChangeSize = this.onChangeSize.bind(this);
    }
    componentWillMount() {
        const cart = JSON.parse(localStorage.getItem('cart'));
        if(cart) {
            const width = cart.width ? cart.width : '';
            const height = cart.height ? cart.height : '';
            this.setState({width, height});
            this.props.setSize(this.props.systemID, cart.width, cart.height)
        }
        console.log(cart)
    }
    onChangeSize(value) {
        function changeSize(e) {
            if(value === 'width') {
                this.setState({width: +e.target.value})
            }
            if(value === 'height') {
                this.setState({height: +e.target.value})
            }
        }
        return changeSize.bind(this)
    }
    setComponentSize() {
        this.props.setSize(this.props.systemID, this.state.width, this.state. height)
    }
    render(){
        const {component} = this.props;
        const title =!!component && component.traits.find((trait)=>trait.name ==='title');
        const notes = !!component && component.traits.filter((trait)=>trait.name ==='note');
        const widthInfo =!!component && component.traits.find((trait)=>trait.name ==='widthInfo');
        const heightInfo =!!component && component.traits.find((trait)=>trait.name ==='heightInfo');
        const widthUnitOfMeasure =!!component && component.traits.find((trait)=>trait.name ==='widthUnitOfMeasure');
        const heightUnitOfMeasure =!!component && component.traits.find((trait)=>trait.name ==='heightUnitOfMeasure');
        return(
            <div>
            <div className="bg-info-dark defaultWidth">
                <div className="optionTitle">{!!title && title.value}</div>
                {!!notes.length && notes.map((note)=>{
                    return(<span className="optionNote clearfix" key={note.id}>{note.value}</span>)
                })}
            </div>
            <div className="form-group dimensionFrom optionsBox">
                <div className="col-sm-5">
                    <label className="control-label">
                        <span>{widthInfo && widthInfo.value || 'No content'}</span>
                    </label>
                    <div className="input-group m-b">
                        <input className="form-control" type="text" value={this.state.width} onChange={this.onChangeSize('width')} onBlur={this.setComponentSize} />
                        <span className="input-group-addon">
                            <span>{widthUnitOfMeasure && widthUnitOfMeasure.value || 'no unit'}</span>
                        </span>
                    </div>
                    <br />
                    <label className="control-label">
                        <span>{heightInfo && heightInfo.value || 'No content'}</span>
                    </label>
                    <div className="input-group m-b">
                        <input className="form-control" type="text" value={this.state.height} onChange={this.onChangeSize('height')} onBlur={this.setComponentSize} />
                        <span className="input-group-addon">
                            <span>{heightUnitOfMeasure && heightUnitOfMeasure.value || 'no unit'}</span>
                        </span>
                    </div>
                </div>
            </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    cart: state.component.cart
});
const actions = {
    setSize
};

export default connect(mapStateToProps, actions)(Dimensions);