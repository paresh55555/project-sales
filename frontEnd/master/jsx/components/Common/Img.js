import React, { Component, PropTypes } from 'react';
import {getImageUrl} from '../../components/Common/helpers'

export default function({className,width,src,style}){
	return(
		<img 
		onError={onError} 
		src={getImageUrl(src)} 
		className={className? className : ''} 
		width={width ? width : ''} 
		style={style ? style : {}}/>
		)
}

function onError(ev){
	ev.target.src = '/img/No_Image.png';
}