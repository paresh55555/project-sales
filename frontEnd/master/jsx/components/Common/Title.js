import React, {Component, PropTypes} from 'react';
import {getNoteText} from '../../components/Common/helpers'

class Title extends Component{
    render(){
        const {component} = this.props
        const title =!!component && component.traits && component.traits.find((trait)=>trait.name ==='title');
        const notes = !!component && component.traits && component.traits.filter((trait)=>trait.name ==='note');
        return(
            <div className="bg-info-dark defaultWidth">
                <div className="optionTitle">{!!title && title.value}</div>
                {notes && !!notes.length && notes.map((note)=>{
                    return(<span className="optionNote clearfix" key={note.id}>{getNoteText(note)}</span>)
                })}
            </div>
        );
    }
}

Title.propTypes = {
    component: PropTypes.object.isRequired
}

export default Title;