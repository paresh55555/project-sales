export function getImageUrl(imageObj){
    return '/'+imageObj.value;
}
export function getNoteText(note){
    var emptyText = '',
        note = note && note.value
    if(note && note.length){
        emptyText = note
        if(!note.replace(/\s/g, '').length){
            emptyText = note.replace(/\s/g, '&nbsp;')
        }
    }
    return emptyText
}
