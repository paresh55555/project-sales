import React, { Component, PropTypes } from 'react';
import { Grid, Row, Col, Panel, Button, Form, ControlLabel, FormControl, Radio, FormGroup } from 'react-bootstrap';
import { Link } from 'react-router';
import {getNoteText} from '../../components/Common/helpers'
import Img from '../../components/Common/Img'
import request from 'axios'
import axios from 'axios'
import when from 'when'
export default class SystemGrid extends React.Component {
	render(){
		const {system} = this.props;  
		const title =!!system && system.traits.find((trait)=>trait.name ==='title');
		const notes = !!system && system.traits.filter((trait)=>trait.name ==='note');
		const imgUrl = !!system && system.traits.find((trait)=>trait.name ==='imgUrl');
        return (
			<Col sm={4} >
				<Link to={'/systems/'+system.id}>
					<div className="panel panel-primary system">
						<div className="panel-content">
							{imgUrl ? <Img src={imgUrl} width="277" className="img-responsive center-block" /> : null}
						</div>
						<div className="panel-footer bg-info-dark bt0 clearfix btn-block">
							<div className="container-fluid">
									<span className="pull-left item-1090 editable editable-click"
										  tabIndex="-1">
										{!!title && title.value}
									</span>
								<span className="pull-right">
										<em className="fa fa-chevron-circle-right"></em>
									</span>
							</div>
							<div className="container-fluid">
								{!!notes.length && notes.map((note) => {
									return (
										<span key={note.id} className="optionNote clearfix editable editable-click" dangerouslySetInnerHTML={{__html: getNoteText(note)}}></span>
									)
								})}
							</div>
						</div>
					</div>
				</Link>
			</Col>
        )
	}
}