import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {componentAction} from '../../actions';

import DoorImage from '../../components/Common/DoorImage'
import Title from '../../components/Common/Title'

class SwingDirection extends React.Component {

    constructor() {
        super();
        this.state = {
            swingDoorDirection: ''
        };
        this.setSwingDoorDirection = this.setSwingDoorDirection.bind(this);
    }

    componentWillMount(){
        const cart = JSON.parse(localStorage.getItem('cart'));
        if(cart) {
            this.setState({swingDoorDirection: cart.swingDoorDirection})
            this.props.setSwingDoorDirection(cart.swingDoorDirection);
        }
    }

    setSwingDoorDirection(direction){
        this.setState({swingDoorDirection: direction})
        this.props.setSwingDoorDirection(direction);
    }

    getPattern(type){
        let panels = []
        const {cart, component} = this.props
        const panelImg = component.traits.find(function(el,i){return el.name === 'panelImg'});
        const leftImg = component.traits.find(function(el,i){return el.name === 'leftImg'});
        const rightImg = component.traits.find(function(el,i){return el.name === 'rightImg'});
        let funcCap = function(){return false},
            panel = (i)=>{return <DoorImage key={i} doorType={'panel'} imgUrl={panelImg && panelImg.value} onClick={funcCap} />},
            left = (i)=>{return <DoorImage key={i} doorType={'left'} imgUrl={leftImg && leftImg.value} onClick={funcCap} />},
            right = (i)=>{return <DoorImage key={i} doorType={'right'} imgUrl={rightImg && rightImg.value} onClick={funcCap} />};
        switch (type){
            case 'left':
                panels = Array.apply(null, Array(cart.panels - 1)).map(function(){return panel})
                return [left].concat(panels).map((func, i)=>{ return func(i) })
                break;
            case 'right':
                panels = Array.apply(null, Array(cart.panels - 1)).map(function(){return panel})
                return panels.concat(right).map((func, i)=>{ return func(i) })
                break;
            case 'both':
                panels = Array.apply(null, Array(cart.panels - 2)).map(function(){return panel})
                return [left].concat(panels,right).map((func, i)=>{ return func(i) })
                break;
            default:
                return ''
        }
    }

    render(){
        const {traits, options} = this.props.component
        const panelImg = traits.find(function(el,i){return el.name === 'panelImg'});
        const leftImg = traits.find(function(el,i){return el.name === 'leftImg'});
        const rightImg = traits.find(function(el,i){return el.name === 'rightImg'});
        return(
            <div>
                <Title component={this.props.component} />
                <div className="optionsBox">
                    {options.map((option, i)=>{
                        const title = !!option && option.traits.find((trait)=>trait.name ==='title');
                        return <div key={option.id} className="panelOptions">
                            <div className="horizontalOption"  onClick={this.setSwingDoorDirection.bind(this,option.name)}>
                                <div className="horizontalPanelImage">
                                    {this.state.swingDoorDirection === option.name ? <div className="selectedPanels"></div> : null}
                                    {this.getPattern(option.name)}
                                </div>
                                <div className="thumbnailTitle">
                                    <span>{!!title && title.value}</span>
                                </div>
                            </div>
                        </div>
                    })}
                </div>
            </div>
        )
    }
}

export default connect(state => ({
    cart: state.component.cart
}), Object.assign({}, componentAction))(SwingDirection);