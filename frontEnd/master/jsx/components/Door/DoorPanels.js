import React, { Component, PropTypes } from 'react';
import Title from '../../components/Common/Title'
import { connect } from 'react-redux';
import PanelsExample from './PanelsExample'
import {setPanelsAmount} from '../../actions/component'

class DoorPanels extends React.Component {
    constructor() {
        super();
        this.state = {
            selectedId: null
        }
        this.onSelect = this.onSelect.bind(this);
        this.checkSelection = this.checkSelection.bind(this);
    }
    onSelect(id) {
        return () => {
            this.props.setPanelsAmount(this.props.data[id]);
            this.setState({selectedId: id});
        }
    }
    checkSelection(id) {
        if(id === this.state.selectedId) {
            return true;
        }
        return false;
    }
    render(){
        console.log(29,this.props)
        const {data} = this.props;
        var image = this.props.component && this.props.component.traits && this.props.component.traits.find(function(el,i){return el.name === 'imgUrl'});
        var imageSrc = '/img/slide-panel.gif';
        if(image && image.value && image.value !== 'tmp'){
            imageSrc = '/'+image.value
        }
        return(
            <div>
                <Title component={this.props.component} />
                <div className="optionsBox">
                    {data ? data.map((number,index) => <PanelsExample
                            key={index}
                            id={index}
                            numberOfPanels={number}
                            imageSrc={imageSrc}
                            onSelect={this.onSelect}
                            selected={this.checkSelection(index)} />)
                        :null}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    data: state.component.data,
    cart: state.component.cart
});
const actions = {
    setPanelsAmount
};

export default connect(mapStateToProps, actions)(DoorPanels);