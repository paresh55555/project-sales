import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col, Panel, Button } from 'react-bootstrap';
import {cartAction} from '../../actions';


class Cart extends React.Component {
		componentWillMount(){
			this.props.getCartItems()
		}
    render() {
        return (
            <div className="cart">
              CART ITEMS = {JSON.stringify(this.props.cart.cartItems)}
            </div>
            );
    }

}

export default connect(state => ({
  cart: state.cart
}), Object.assign({}, cartAction))(Cart);


