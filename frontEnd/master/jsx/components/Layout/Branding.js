import React, { Component, PropTypes } from 'react';
import { Grid, Row, Col, Panel, Button, Form, ControlLabel, FormControl, Radio, FormGroup } from 'react-bootstrap';

export default class Branding extends React.Component {
	render(){
		
		const {banner_url, 
        background_color,
        first_link_text,
        first_link_url,
        first_link_color,
        second_link_text,
        second_link_url,
        second_link_color
        } = this.props.branding
        return (
          <div className="demoBanner" style={{backgroundColor:background_color}}>
          <Grid>
          <Row>
            <Col xs={8}>
              {!!banner_url && banner_url!== '' ?
                <img src={`/${banner_url}`} alt=""/>
                :''
              }
            </Col>
            <Col xs={4} className={'text-right'}>
              {!!first_link_text && first_link_text !== ''?
                <a target="blank" href={first_link_url} style={{color:first_link_color}}>{first_link_text}</a>:''
              }
              {!!second_link_text && second_link_text !== ''?
                <a target="blank" href={second_link_url} style={{color:second_link_color}}>{second_link_text}</a>:''
              }
            </Col>

          </Row>
          </Grid>
          </div>
            );
	}
}