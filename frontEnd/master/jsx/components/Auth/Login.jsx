import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col, Panel, Button } from 'react-bootstrap';
//import {Auth} from '../../services/authService'
import {authAction} from '../../actions';


class Login extends React.Component {
    
    constructor() {
        super()
        self = this;
        this.state = {auth:{username:'',password:''},disabled:false, error:'Wrong Email or Password',showError:''};
        this._onChange = this._onChange.bind(this);
        this._login = this._login.bind(this)
        this.gotoGuest = this.gotoGuest.bind(this)
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.auth.isLoading)return
        if(nextProps.auth != this.props.auth && nextProps.auth.error){
            this.setState({showError:nextProps.auth.error})
        }
        if(nextProps.auth != this.props.auth && !nextProps.auth.error){
            self.context.router.push('/systemGroups');
        }
     }
    _onChange(value, e){
        let {auth} = this.state;
        auth[value] = e.target.value;
        this.setState({auth:auth, showError:false,disabled:false})
        
    }
    gotoGuest(e){
        e.preventDefault()
        this.context.router.push('/guest')
    }
    _login(e){
        e.preventDefault()
        console.log('authOptions')
        this.setState({disabled:true})
        const {auth} = this.state;
        this.props.Authenticate(auth);
    }
    render() {
        console.log(this.state,'this.state')
        return (
            <div className="block-center mt-xl wd-xl">
                { /* START panel */ }
                <div className="panel panel-dark panel-flat">
                    <div className="panel-heading text-center">
                        <a href="#">
                            Sales Quoter  
                        </a>
                    </div>
                    <div className="panel-body">
                        <p className="text-center pv">SIGN IN TO CONTINUE.</p>
                        <form role="form" data-parsley-validate="" noValidate className="mb-lg">
                            <div className="form-group has-feedback">
                                <input id="exampleInputEmail1" type="text" placeholder="Username" autoComplete="off" required="required" className="form-control" onChange={this._onChange.bind(this,'username')} />
                                <span className="fa fa-envelope form-control-feedback text-muted"></span>
                            </div>
                            <div className="form-group has-feedback">
                                <input id="exampleInputPassword1" type="password" placeholder="Password" required="required" className="form-control" onChange={this._onChange.bind(this,'password')} />
                                <span className="fa fa-lock form-control-feedback text-muted"></span>
                            </div>
                            {this.state.showError ? <p className="alert alert-danger">{this.state.error}</p>:null}
                            <button type="submit" className="btn btn-block btn-primary mt-lg" onClick={this._login} disabled={this.state.disabled}>Login</button>
                            <button type="submit" className="btn btn-block btn-primary mt-lg" onClick={this.gotoGuest}>Proceed As a Guest</button>
                        </form>
                    </div>
                </div>
                { /* END panel */ }
            </div>
            );
    }

}
Login.contextTypes = {
        router: React.PropTypes.object.isRequired
      };
Login.propTypes = {
    Authenticate: PropTypes.func.isRequired
  };
export default connect(state => ({
  auth: state.auth
}), Object.assign({}, authAction))(Login);


