import React, { Component, PropTypes } from 'react';
import {getNoteText} from '../../components/Common/helpers'
import Img from '../../components/Common/Img'

export default class HalfLineOption extends React.Component {
    render(){
        const {option, addSelected, id} = this.props
        const title = !!option && option.traits.find((trait)=>trait.name ==='title');
        const image = !!option && option.traits.find((trait)=>trait.name ==='imgUrl');
        const notes = !!option && option.traits.filter((trait)=>trait.name ==='note');
        return(
            <div className="halfOption">
                <div className="horizontalOption">
                    <div className="horizontalImage" onClick={addSelected(id)}>
                        {this.props.selected ? <div className="demoSelected"></div> : null}
                        { image && image.value ?
                            <Img style={{width:121,height:121}} src={image} />
                            :
                            <div className="img_placeholder" style={{width:121,height:121}}></div>
                        }
                    </div>
                    <div className="horizontalHalfText">
                        <span className="horizontalTitle">{!!title && title.value}</span>
                        {!!notes.length && notes.map((note, i)=>{
                            return(<span key={i} className="horizontalNote clearfix" dangerouslySetInnerHTML={{__html: getNoteText(note)}}></span>)
                        })}
                    </div>
                </div>
            </div>
        )
    }
}