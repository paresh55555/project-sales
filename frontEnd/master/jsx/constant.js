import {API_URL} from './api/ApiServerConfigs.js'
const AUTH_URL = API_URL+'/token'
const GUEST_URL = API_URL+'/guest'
const SYSTEM_LIST_URL = API_URL+'/systems';

export {
	API_URL,
	AUTH_URL,
	GUEST_URL,
    SYSTEM_LIST_URL
}