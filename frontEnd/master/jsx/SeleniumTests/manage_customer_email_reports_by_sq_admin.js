const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sidebar = require('./FT/sqAdmin/sideBar/sideBar');
const orderPage = require('./FT/sqAdmin/OrderReports/orderReports');
const moment = require('moment');
let _u = undefined;

function customer_email_reports_by_sq_admin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US:Customer Email Reports by SQ-admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "SalesManager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sidebar.customerEmailReportClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));

    // DATE WISE FILTER
    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));

    let currentMonthDate1 = moment().date(1).format('MM/DD/YYYY');
    let currentMonthDate2 = moment().endOf('month').format('MM/DD/YYYY');;

    pc = pc.then(() => orderPage.dateInput(d, wd,'toDatePicker',currentMonthDate1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.dateInput(d, wd,'fromDatePicker',currentMonthDate1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.goButton(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [currentMonthDate1,currentMonthDate2]));

    // SALES PERSON WISE FIlTER
    pc = pc.then(() => orderPage.filterClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.searchSalesPersonInput(d, wd,'a')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,0,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,0,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,0,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.clearButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,0,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.filterButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.filterClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.searchSalesPersonInput(d, wd,'a')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,0,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.closeIconClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    // pc = pc.then(() => cf.testContentByArray(d, ['Filter by Sales Person']));
    pc = pc.then(() => orderPage.filterClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.searchSalesPersonInput(d, wd,'a')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,0,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.filterButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Sales Person - ']));
    pc = pc.then(() => orderPage.clearIconClick(d, wd,'reportFilterInnerCon',1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));

    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.groupBySelect(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Sales Person']));
    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.groupBySelect(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Group by']));

    pc = pc.then(() => orderPage.paginationEntryClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.paginationEntryClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));

    pc = pc.then(() => orderPage.paginationClick(d, wd,'activeNext')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.paginationClick(d, wd,'activePrevious')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));

    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));

    pc = pc.then(() => orderPage.search(d, wd,'a')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));

    pc = pc.then(() => orderPage.exportClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/customeremailsreport', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Customer Email Reports by SQ-admin');
    return pc;
}

module.exports = {customer_email_reports_by_sq_admin};