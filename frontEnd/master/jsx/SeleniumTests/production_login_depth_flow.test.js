const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const quote_ViewOrder = require('./FT/afterLogin/order/viewOrder');
const order_actions = require('./FT/afterLogin/accountingActions/accountingActionFlow');
const accounting = require('./FT/afterLogin/accountingActions/accountingActionFlow');

let _u = undefined;

function production_login_depth_flow(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;

    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Production login depth flow'));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "Production")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewAllProduction', true, _u));

    let orderNo;
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => orderNo = o.replace('-S',''));
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,orderNo)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewAllProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.orderNumberClick(d, wd,orderNo)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));

    // CHECK ALL THE BUTTONS
    pc = pc.then(() => cf.testContentByArray(d, ['Print Cut Sheet','Print Labels','Print Job','Close Job','Manifest','Delivery Note']));

    // pc = pc.then(() => order_actions.onHoldSelection(d, wd, 1)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    // pc = pc.then(() => order_actions.onHoldSelection(d, wd, 1)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
      pc = pc.then(() => order_actions.quoteNotesInput(d, wd, "Some notes")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));

    pc = pc.then(() => order_actions.pickupDeliverySelection(d, wd, 2)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.pickupDeliverySelection(d, wd, 1)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.pickupDeliverySelection(d, wd, 0)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.productionDetailNotesInput(d, wd, "Some note")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    // pc = pc.then(() => order_actions.poInput(d, wd, "Po notes here")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.commentInput(d, wd, "comments here")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));

    pc = pc.then(() => order_actions.billingContactInput(d, wd, "test name")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.billingAddress1Input(d, wd, "some address")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.billingAddress2Input(d, wd, "test")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.billingCityInput(d, wd, "San Mateo")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.billingStateInput(d, wd, "CA")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.billingZipInput(d, wd, "94402")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.billingPhoneInput(d, wd, "012-345-6789")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.billingEmailInput(d, wd, "test@test.com")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.shippingContactInput(d, wd, "test name")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.shippingAddress1Input(d, wd, "some address")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.shippingAddress2Input(d, wd, "test")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.shippingCityInput(d, wd, "San Mateo")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.shippingStateInput(d, wd, "CA")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.shippingZipInput(d, wd, "94402")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.shippingPhoneInput(d, wd, "012-345-6789")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => order_actions.shippingEmailInput(d, wd, "test@test.com")).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));

    pc = pc.then(() => order_actions.scroll(d, wd,100)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));

    // PAYMENT INFO
    let balance,boolData;
    pc = pc.then(() => order_actions.getBalanceDue(d, wd)).then((b) => balance = b);
    pc = pc.then(() => order_actions.checkEnterPaymentInfoLink(d, wd)).then((bd) => boolData = bd);

    if(boolData) {
        pc = pc.then(() => order_actions.paymentInfoClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.paymentOptionClick(d, wd, 0)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.last4DigitsOfCreditCardInput(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.paymentAmountInput(d, wd, balance)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        // pc = pc.then(() => order_actions.paymentDateSelect(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => cf.testContentByArray(d, ['Check 1111']));
        pc = pc.then(() => order_actions.deletePaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));

        pc = pc.then(() => order_actions.paymentOptionClick(d, wd, 1)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.last4DigitsOfCreditCardInput(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.paymentAmountInput(d, wd, balance)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        // pc = pc.then(() => order_actions.paymentDateSelect(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => cf.testContentByArray(d, ['Visa 1111']));
        pc = pc.then(() => order_actions.deletePaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));

        pc = pc.then(() => order_actions.paymentOptionClick(d, wd, 2)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.last4DigitsOfCreditCardInput(d, wd, '1000')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.paymentAmountInput(d, wd, balance)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        // pc = pc.then(() => order_actions.paymentDateSelect(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => cf.testContentByArray(d, ['MasterCard 1000']));
        pc = pc.then(() => order_actions.deletePaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));

        pc = pc.then(() => order_actions.paymentOptionClick(d, wd, 3)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.last4DigitsOfCreditCardInput(d, wd, '9999')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.paymentAmountInput(d, wd, balance)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        // pc = pc.then(() => order_actions.paymentDateSelect(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => cf.testContentByArray(d, ['Amex 9999']));
        pc = pc.then(() => order_actions.deletePaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));

        pc = pc.then(() => order_actions.paymentOptionClick(d, wd, 4)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.last4DigitsOfCreditCardInput(d, wd, '3333')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.paymentAmountInput(d, wd, balance)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        // pc = pc.then(() => order_actions.paymentDateSelect(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => cf.testContentByArray(d, ['Cash 3333']));
        pc = pc.then(() => order_actions.deletePaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));

        pc = pc.then(() => order_actions.paymentOptionClick(d, wd, 5)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.last4DigitsOfCreditCardInput(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.paymentAmountInput(d, wd, balance)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        // pc = pc.then(() => order_actions.paymentDateSelect(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => cf.testContentByArray(d, ['Wire 1111']));
        pc = pc.then(() => order_actions.deletePaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));

        pc = pc.then(() => order_actions.paymentOrderNotesInput(d, wd, 'Payment Order Notes test')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
        pc = pc.then(() => order_actions.closePaymentInfo(d, wd)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    }
    pc = pc.then(() => accounting.chooseActionClick(d,wd,3)).then(() => cf.test(d, 'viewAllProduction', Config.BASE_URL + '?tab=viewAllProduction&', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewPreProductionClick(d, wd)).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => orderNo = o);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,orderNo)).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction', true, _u));
    pc = pc.then(() => accounting.changeProductionStatus(d, wd,1)).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewProductionClick(d, wd)).then(() => cf.test(d, 'viewProduction', Config.BASE_URL + '?tab=viewProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => orderNo = o);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,orderNo)).then(() => cf.test(d, 'viewProduction', Config.BASE_URL + '?tab=viewProduction', true, _u));
    pc = pc.then(() => accounting.changeProductionStatus(d, wd,2)).then(() => cf.test(d, 'viewProduction', Config.BASE_URL + '?tab=viewProduction', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewCompletedClick(d, wd)).then(() => cf.test(d, 'viewCompleted', Config.BASE_URL + '?tab=viewCompleted', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => orderNo = o);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,orderNo)).then(() => cf.test(d, 'viewCompleted', Config.BASE_URL + '?tab=viewCompleted', true, _u));
    pc = pc.then(() => accounting.changeProductionStatus(d, wd,3)).then(() => cf.test(d, 'viewCompleted', Config.BASE_URL + '?tab=viewCompleted', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewDeliveredClick(d, wd)).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => orderNo = o);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,orderNo)).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));
    pc = pc.then(() => accounting.changeProductionStatus(d, wd,4)).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));

    // pc = pc.then(() => quote_ViewOrder.viewAllProductionClick(d, wd,orderNo)).then(() => cf.test(d, 'viewAllProduction', Config.BASE_URL + '?tab=viewAllProduction', true, _u));

    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd, "a")).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));
    pc = pc.then(() => quote_ViewOrder.itemPerPageSelect(d, wd)).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));
    // pc = pc.then(() => quote_ViewOrder.sortOrderByIdClick(d, wd)).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));
    // pc = pc.then(() => quote_ViewOrder.sortOrderByIdClick(d, wd)).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));

    pc = pc.then(() => quote_ViewOrder.productionMenuClick(d, wd)).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuItemClick(d, wd, 0)).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuClick(d, wd)).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuItemClick(d, wd, 1)).then(() => cf.test(d, 'viewProduction', Config.BASE_URL + '?tab=viewProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuClick(d, wd)).then(() => cf.test(d, 'viewProduction', Config.BASE_URL + '?tab=viewProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuItemClick(d, wd, 2)).then(() => cf.test(d, 'viewCompleted', Config.BASE_URL + '?tab=viewCompleted', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuClick(d, wd)).then(() => cf.test(d, 'viewCompleted', Config.BASE_URL + '?tab=viewCompleted', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuItemClick(d, wd, 3)).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuClick(d, wd)).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuItemClick(d, wd, 4)).then(() => cf.test(d, 'viewHold', Config.BASE_URL + '?tab=viewHold', true, _u));

    pc = pc.then(() => order_actions.generateProductionReport(d, wd,'generateProdReportButton')).then(() => cf.test(d, 'viewHold',  Config.BASE_URL + '?tab=viewHold', true, _u));
    // pc = pc.then(() => quote_ViewOrder.productionMenuClick(d, wd)).then(() => cf.test(d, 'viewHold', Config.BASE_URL + '?tab=viewHold', true, _u));
    // pc = pc.then(() => quote_ViewOrder.productionMenuItemClick(d, wd, 5)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Production login depth flow');
    return pc;
}

module.exports = {production_login_depth_flow};