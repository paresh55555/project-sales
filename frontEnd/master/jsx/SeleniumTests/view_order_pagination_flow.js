const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const l1 = require('./FT/l1');
const viewOrder = require('./FT/afterLogin/order/viewOrder');
const pagination = require('./FT/afterLogin/order/pagination_fn');
const viewQuote = require('./FT/afterLogin/quote/viewQuotePage');

const moment = require('moment');
let _u = undefined;

function viewOrderPaginationBySalesPerson(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Pagination Flow in View order'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    pc = pc.then(() => viewOrder.viewOrderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));

    let itemsPerPage, totalPages ;
    pc = pc.then(() => pagination.getPerPageItems(d, wd)).then((items) => itemsPerPage = items);
    pc = pc.then(() => pagination.getTotalPages(d, wd)).then((pages) => totalPages = pages);

    let itemsArray = [1,2,3,4,5,6,7,8,9,0];
    // Items Per Page
    for(let i=0;i<itemsArray.length;i++){
        pc = pc.then(() => pagination.itemPerPageSelect(d, wd,i+1)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders', true, _u));
        pc = pc.then(() => pagination.getPerPageItems(d, wd)).then((items) => itemsPerPage = items);
        pc = pc.then(() => pagination.checkPerPageRecords(d, wd,itemsPerPage)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders', true, _u));
    }

    // Next pagination
    pc = pc.then(() => pagination.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders', true, _u));
    // Previous pagination
    pc = pc.then(() => pagination.prevButtonClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders', true, _u));

    // SEARCH WITH PAGINATION
    let orderNo;
    pc = pc.then(() => viewOrder.getFirstRowOrderNo(d, wd,'orderRow ')).then((o) => orderNo = o);
    pc = pc.then(() => viewOrder.searchOrderInput(d, wd, orderNo)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders', true, _u));
    pc = pc.then(() => pagination.getPerPageItems(d, wd)).then((items) => itemsPerPage = items);
    pc = pc.then(() => pagination.checkPerPageRecords(d, wd,itemsPerPage)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders', true, _u));
    pc = pc.then(() => pagination.checkTotalPagesInSearch(d, wd,1)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Pagination Flow in View order');
    return pc;
}

function viewQuotePaginationBySalesPerson(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Pagination Flow in View Quote'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&', true, _u));

    let itemsPerPage, totalPages ;
    pc = pc.then(() => pagination.getPerPageItems(d, wd)).then((items) => itemsPerPage = items);
    pc = pc.then(() => pagination.getTotalPages(d, wd)).then((pages) => totalPages = pages);

    let itemsArray = [1,2,3,4,5,6,7,8,9,0];
    // Items Per Page
    for(let i=0;i<itemsArray.length;i++){
        pc = pc.then(() => pagination.itemPerPageSelect(d, wd,i+1)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));
        pc = pc.then(() => pagination.getPerPageItems(d, wd)).then((items) => itemsPerPage = items);
        pc = pc.then(() => pagination.checkPerPageRecords(d, wd,itemsPerPage)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    }

    // Next pagination
    pc = pc.then(() => pagination.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    // Previous pagination
    pc = pc.then(() => pagination.prevButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));

    // SEARCH WITH PAGINATION
    let orderNo;
    pc = pc.then(() => viewOrder.getFirstRowOrderNo(d, wd,'quoteRow  ')).then((o) => orderNo = o);
    pc = pc.then(() => viewQuote.searchInput(d, wd, orderNo)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    pc = pc.then(() => pagination.getPerPageItems(d, wd)).then((items) => itemsPerPage = items);
    pc = pc.then(() => pagination.checkPerPageRecords(d, wd,itemsPerPage)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    pc = pc.then(() => pagination.checkTotalPagesInSearch(d, wd,1)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Pagination Flow in View Quote');
    return pc;
}

module.exports = {viewOrderPaginationBySalesPerson, viewQuotePaginationBySalesPerson};