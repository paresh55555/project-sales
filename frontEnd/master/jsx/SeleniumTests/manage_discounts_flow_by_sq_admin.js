const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const l1 = require('./FT/l1');
const discountsPage = require('./FT/sqAdmin/discounts/discountsPage');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const quote_viewQuote= require('./FT/afterLogin/quote/viewQuotePage');
const admin_action_flow = require('./FT/afterLogin/adminActions/adminActionFlow');

let _u = undefined;

function discounts_by_SQ_admin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let url = Config.BASE_URL + 'SQ-admin/sales/discounts';
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Discount by SQ-admin & Sales Person'));
    pc = sqAdminLogin(pc,wd,d);
    pc = pc.then(() => sideBar.toggleSidebar(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sideBar.discountsClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    //ADD DISCOUNT
    pc = pc.then(() => discountsPage.addDiscountsClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.discountNameInput(d, wd, '50rsOFF')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.selectType(d, wd,'inlineradio2')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.discountAmountInput(d, wd, '50')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.selectCommissionGroupDropDown(d, wd,1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.datePickerClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.nextNavClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.dateSelectClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.selectSalesPersonDropDown(d, wd, "1")).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.noEndClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.addDiscountsButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['50rsOFF','$  50.00','Main Commission Group']));
    pc = pc.then(() => l1.adminLogout(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', true, _u));

    // LOGIN INTO SALES PERSON AND CHECKING DISCOUNT TYPE
    pc = pc.then (() => d.get(Config.BASE_URL));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = salesPersonLogin(pc ,wd, d);
    pc = pc.then(() => quote_viewQuote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    // EDIT PROJECT INFO
    pc = pc.then(() => admin_action_flow.scroll(d, wd,800)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.editQuoteClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Discount Name','Amount','Show On Quote']));
    // Discount
    pc = pc.then(() => admin_action_flow.addDiscountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.deleteDiscountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.addDiscountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.discountSelect(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.checkDiscountTypeData(d, wd, '50rsOFF')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$50.00']));
    pc = pc.then(() => admin_action_flow.yesClick(d, 'discountsList')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.saveClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // EDIT DISCOUNT AT SQ ADMIN SITE
    pc = pc.then(() => l1.salesPersonLogout(d,wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));
    pc = pc.then(() => d.get(Config.BASE_URL + "SQ-admin/login"));
    pc = sqAdminLogin(pc,wd,d);
    pc = pc.then(() => sideBar.discountsClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    //EDIT DISCOUNT
    pc = pc.then(() => discountsPage.searchInput(d, wd, '50rsOFF')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.discountRowClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.discountNameInput(d, wd, '10rsOFF')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.selectType(d, wd,'inlineradio1')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.discountAmountInput(d, wd, '10')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.addDiscountsButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['10rsOFF','10%']));

    // CHECK EDITED DISCOUNT AT SALES PERSON SITE
    pc = pc.then (() => d.get(Config.BASE_URL)).then(() => cf.delayBySecond(cf.delaySecond.thirty));;
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc= salesPersonLogin(pc, wd, d);
    pc = pc.then(() => quote_viewQuote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    // EDIT PROJECT INFO
    pc = pc.then(() => admin_action_flow.scroll(d, wd,800)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.editQuoteClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Discount Name','Amount','Show On Quote']));
    // Discount
    pc = pc.then(() => admin_action_flow.deleteDiscountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.addDiscountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.discountSelect(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.checkDiscountTypeData(d, wd, '10rsOFF')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.yesClick(d, 'discountsList')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.saveClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // SALES PERSON LOGOUT
    pc = pc.then(() => l1.salesPersonLogout(d,wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));
    pc = pc.then(() => d.get(Config.BASE_URL + "SQ-admin/login"));
    pc = sqAdminLogin(pc,wd,d);
    pc = pc.then(() => sideBar.discountsClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    //SEARCH DISCOUNT
    pc = pc.then(() => discountsPage.searchInput(d, wd, '10rsOFF')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.deleteDiscountClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.confirmClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    //ADD ONE MORE DISCOUNT
    pc = pc.then(() => discountsPage.addDiscountsClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.discountNameInput(d, wd, '20off')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.selectType(d, wd,'inlineradio2')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.discountAmountInput(d, wd, '20')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.selectCommissionGroupDropDown(d, wd,1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.datePickerClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.nextNavClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.dateSelectClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.selectSalesPersonDropDown(d, wd,"2")).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.addDiscountsButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['20off','$  20.00','Main Commission Group']));

    //SORTING
    pc = pc.then(() => discountsPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.sorting(d,  wd,2)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.sorting(d, wd,6)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => discountsPage.sorting(d, wd,6)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Manage Discounts by SQ-admin & Sales Person');
    return pc;
}

function sqAdminLogin(pc,wd,d) {
    pc = pc.then(() => login.EmailInput(d, wd, "SalesManager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    return pc;
}

function salesPersonLogin(pc,wd,d) {
    pc = pc.then(() => l1.EmailInput(d, wd, 'admin')).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    return pc;
}

module.exports = {discounts_by_SQ_admin};