const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const quote_NewQuote= require('./FT/afterLogin/quote/newQoutes');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const accounting = require('./FT/afterLogin/accountingActions/accountingActionFlow');
const cart = require('./FT/afterLogin/quote/cart');
const customerInfo = require('./FT/afterLogin/order/customerInfo');

let _u = undefined;

function create_a_vinyl_picture_window_quote_by_salesPersonLogin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Vinyl picture window Quote by salesPerson'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => quote_NewQuote.newQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));

    pc = pc.then(() => cf.testModuleData(d, 'Maximum Size:', 'Width 20 feet, Height 8 feet', 'Vinyl Picture Window'));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Vinyl Picture Window')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 1)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['*Vinyl Doors have a maximum width of 245 inches and a minimum width of 23 inches']));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 1)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['*Vinyl Doors have a maximum height of 98 inches and minimum height of 16 inches']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 50)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 20)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstep')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-1")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$353.65']));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-2")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$353.65']));

    // Start checking of panel ranges

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 20)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-Flush')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$495.11']));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-2")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$495.11']));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-1")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$495.11']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 20)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstep')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-1")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$707.30']));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-2")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$707.30']));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$707.30']));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-4")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$707.30']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 120)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 20)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-Flush')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-4")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$848.77']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 170)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 20)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstep')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-6")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$1,202.42']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 200)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 20)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-Flush')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-5")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$1,414.61']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 50)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 20)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstep')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-1")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$353.65','powered by','Sales Quoter','Choose Frame Type:']));
    // End checking of panel ranges

    //COLOR
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_White_null_#fcffff_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$353.65']));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_Beige_null_#F5F5DC_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$353.65']));

    // GLASS
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$353.65']));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Low-E 3')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$353.65']));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$353.65']));

    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d,['Vinyl Picture Window','Beige Vinyl','50" width x 20" height','51" width x 21" height',
        '1','None','none','Left 0 / Right 0','Aluminum Block','3/4 Upstep','None','Low-E 3','Argon',
        'Item 1 of 1','$353.65','Total $353.65']
    ));

    pc = pc.then(() => accounting.productionNotesInput(d, wd,'this is production sample notes')).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    // EDIT DOOR
    pc = pc.then(() => accounting.cartItemClick(d, wd, 1)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-Flush')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-2")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$353.65','powered by','Sales Quoter','Choose Frame Type:']));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_White_null_#fcffff_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$353.65']));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$353.65']));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d,['Vinyl Picture Window','White Vinyl','50" width x 20" height','51" width x 21" height',
        '1','None','none','Left 0 / Right 0','Aluminum Block','Flush','None','Glass Clear','Argon',
        'Item 1 of 1','$353.65','Total $353.65']
    ));

    // QTY
    pc = pc.then(() => cart.quantityInput(d, wd, 2)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d,['Vinyl Picture Window','White Vinyl','50" width x 20" height','51" width x 21" height',
        '1','None','none','Left 0 / Right 0','Aluminum Block','Flush','None','Glass Clear','Argon',
        'Item 1-2 of 2','$707.30','$353.65','Total $707.30']
    ));

    pc = pc.then(() => cart.duplicateClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d,['Vinyl Picture Window','White Vinyl','50" width x 20" height','51" width x 21" height',
        '1','None','none','Left 0 / Right 0','Aluminum Block','Flush','None','Glass Clear','Argon',
        'Item 1-2 of 4','Item 3-4 of 4','$707.30','$353.65','Total $1,414.60']
    ));
    pc = pc.then(() => cart.saveAsQuote(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['4']));

    pc = pc.then(() => customerInfo.leadSourceSelect(d,2)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => customerInfo.firstNameInput(d, wd,'firstname test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => customerInfo.lastNameInput(d, wd,'lastname test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => customerInfo.emailInput(d, wd,'ft@email.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => customerInfo.phoneInput(d, wd,'8989988')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingContactInputName(d, wd,'billing test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingEmailInputName(d, wd,'billing@email.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d, wd,'billing address 1')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress2InputName(d, wd,'billing address 2')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingCityInputName(d, wd,'billing city')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingStateInputName(d, wd,'billing state')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingZipInputName(d, wd,'89898')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => customerInfo.attachToQuoteClick(d, wd)).then(() => cf.test(d, 'viewSavedQuoteConfirmation', Config.BASE_URL + '?tab=viewSavedQuoteConfirmation&', true, _u));
    pc = pc.then(() => customerInfo.quoteViewButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));

    pc = pc.then(() => cf.testContentByArray(d,
        ['Item 1-2 of 4','Item 3-4 of 4','$1,414.60','$707.30','$353.65',
            'Vinyl Picture Window','White Vinyl','50" width x 20" height','51" width x 21" height',
            '1','None','none','Left 0 / Right 0','Aluminum Block','Flush','None','Glass Clear','Argon',
        'firstname test lastname test','ft@email.com','billing test','billing@email.com',
            'billing address 1','billing address 2','billing city','billing state','89898']
    ));

    // EDIT DOOR FROM QUOTE
    pc = pc.then(() => accounting.scroll(d, wd, 1200)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => accounting.cartItemClick(d, wd, 1)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    // pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstep')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-1")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$353.65','powered by','Sales Quoter','Choose Frame Type:']));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_Beige_null_#F5F5DC_null')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$353.65']));
    pc = pc.then(() => accounting.saveToQuoteClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d,['Vinyl Picture Window','Beige Vinyl','50" width x 20" height','51" width x 21" height',
        '1','None','none','Left 0 / Right 0','Aluminum Block','None','Glass Clear','Argon',
        'Item 1-2 of 4','Item 3-4 of 4','$353.65','$707.30','Total $1,414.60']
    ));

    pc = cf.handleOutput(d, pc, capability, 'Create a Vinyl Picture Window Quote by salesPerson');
    return pc;
}

module.exports = {create_a_vinyl_picture_window_quote_by_salesPersonLogin};