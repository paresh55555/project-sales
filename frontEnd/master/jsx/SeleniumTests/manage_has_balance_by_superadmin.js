const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const reportsPage= require('./FT/sqAdmin/reports/reportsPage');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const moment = require('moment');
let _u = undefined;

function has_balance_by_SQ_admin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let url = Config.BASE_URL + 'SQ-admin/reports/deliveredOrders';
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Manage Has Balance tab by SQ-admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => sideBar.hasBalanceClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = pc.then(() => reportsPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = pc.then(() => reportsPage.searchInput(d, wd, 'a')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.searchInput(d, wd, 'e')).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Manage Has Balance tab by SQ-admin');
    return pc;
}

module.exports = {has_balance_by_SQ_admin};