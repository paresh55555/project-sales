const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sidebar = require('./FT/sqAdmin/sideBar/sideBar');
const orderPage = require('./FT/sqAdmin/OrderReports/orderReports');
let _u = undefined;

function daily_intake_reports_by_sq_admin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US:Daily Intake Reports by SQ-admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "SalesManager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sidebar.dailyIntakeReportClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,6)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,6)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = pc.then(() => orderPage.paginationEntryClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));
    pc = pc.then(() => orderPage.paginationEntryClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = pc.then(() => orderPage.paginationClick(d, wd,'activeNext')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));
    pc = pc.then(() => orderPage.paginationClick(d, wd,'activePrevious')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = pc.then(() => orderPage.search(d, wd,'test')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = pc.then(() => orderPage.selectDateClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));
    pc = pc.then(() => orderPage.prevMonthClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));
    pc = pc.then(() => orderPage.prevMonthdayClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = pc.then(() => orderPage.selectDateClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = pc.then(() => orderPage.nextMonthClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));
    pc = pc.then(() => orderPage.nextMonthClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = pc.then(() => orderPage.salesFilterClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Sales Rep: Sales Person']));
    pc = pc.then(() => orderPage.salesFilterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Sales Rep: All']));

    pc = pc.then(() => orderPage.exportClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/dailyintakereport', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Daily Intake Reports by SQ-admin');
    return pc;
}

module.exports = {daily_intake_reports_by_sq_admin};