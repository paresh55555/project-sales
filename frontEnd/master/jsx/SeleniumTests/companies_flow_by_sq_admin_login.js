const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const companyPage = require('./FT/sqAdmin/company/companyPage');

let _u = undefined;

function companies_flow_by_sq_admin_login(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Manage company by admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => sideBar.companiesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));

    //ADD COMPANY
    pc = pc.then(() => companyPage.addCompanyClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.companyNameInput(d, wd, 'companyName')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.firstNameInput(d, wd, 'test')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.lastNameInput(d, wd, 'name')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.phoneInput(d, wd, '01236985744')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.address1Input(d, wd, 'apartments')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.address2Input(d, wd, 'street 2')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.cityInput(d, wd, 'miami')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.stateInput(d, wd, 'CA')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.zipInput(d, wd, '012653')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.saveClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['companyName','test','name','01236985744',
    'street 2','miami','CA','012653']));

    //SEARCH
    pc = pc.then(() => companyPage.searchInput(d, wd, 'companyName')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));

    //EDIT
    pc = pc.then(() => companyPage.companyRecordClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.companyNameInput(d, wd, 'companyName edit')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.firstNameInput(d, wd, 'test edit')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.lastNameInput(d, wd, 'name edit')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.phoneInput(d, wd, '01236985745')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.address1Input(d, wd, 'apartments')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.address2Input(d, wd, 'street 3')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.cityInput(d, wd, 'miami')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.stateInput(d, wd, 'AZ')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.zipInput(d, wd, '0126534')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.saveClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['companyName edit','test edit','name edit',
        '01236985745',
        'street 3','miami','AZ','0126534']));

    // DELETE
    pc = pc.then(() => companyPage.searchInput(d, wd, 'companyName')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.companyRecordClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.deleteClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.confirmClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.confirmClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));

    //SORTING
    pc = pc.then(() => companyPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,6)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,6)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,7)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,7)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,8)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));
    pc = pc.then(() => companyPage.sorting(d, wd,8)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/companies', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Manage company by admin');
    return pc;
}

module.exports = {companies_flow_by_sq_admin_login};