const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const manageCommissionGroupsPage = require('./FT/sqAdmin/commissions/manageCommissionGroupsPage');

let _u = undefined;

function manage_commission_group_by_SQ_admin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Manage Commission Group by SQ-admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "SalesManager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sideBar.commissionGroupsClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/groups', true, _u));

    pc = pc.then(() => manageCommissionGroupsPage.addCommissionGroupClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.cancelButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));

    pc = pc.then(() => manageCommissionGroupsPage.addCommissionGroupClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.commissionNameInput(d, wd, 'commission group test')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.commissionCheckBoxInput(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.commissionCheckBoxInput(d, wd, 2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.saveButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));

    pc = pc.then(() => manageCommissionGroupsPage.commissionSearchInput(d, wd, 'commission group test')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.recordClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.commissionNameInput(d, wd, 'commission group edit')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.commissionCheckBoxInput(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.commissionCheckBoxInput(d, wd, 2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.saveButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));

    pc = pc.then(() => manageCommissionGroupsPage.commissionSearchInput(d, wd, 'commission group edit')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.deleteButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.confirmCommissionClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));

    pc = pc.then(() => manageCommissionGroupsPage.changeOrderClick(d, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.changeOrderClick(d, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.changeOrderClick(d, 2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));
    pc = pc.then(() => manageCommissionGroupsPage.changeOrderClick(d, 3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/groups', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Manage Commission Group by SQ-admin');
    return pc;
}

module.exports = {manage_commission_group_by_SQ_admin};