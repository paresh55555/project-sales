const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const quote_NewQuote= require('./FT/afterLogin/quote/newQoutes');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const cart = require('./FT/afterLogin/quote/cart');
const quote_order_customerInfo = require('./FT/afterLogin/order/customerInfo');
const accounting = require('./FT/afterLogin/accountingActions/accountingActionFlow');
const quote_viewQuote= require('./FT/afterLogin/quote/viewQuotePage');


let _u = undefined;

function create_a_signature_door_quote_by_salesPersonLogin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Signature Door Quote by salesPerson'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd,)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => quote_NewQuote.newQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));

    // CHECKING INCHES
    pc = pc.then(() => cf.testModuleData(d, 'Maximum Size:', 'Width 250 feet, Height 10 feet', 'Signature Door (Aluminum)'));

    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Signature Door (Aluminum)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 1)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['*SMART Doors have a maximum width of 3001 inches and minimum width of 19 inches']));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 1)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['*SMART Doors have a maximum height of 120 inches and a minimum height of 16 inches']));

    //............................

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,998.80']));

    // Start of checking panel ranges
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 50)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-Flush')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-2")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,644.61']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,687.39']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-Flush')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-4")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,787.85']));

    // End of checking panel ranges

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-Flush')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,998.80','powered by','Sales Quoter','Choose Frame Type:']));


    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,"swingInOrOut-Outswing")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,998.80']));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Split")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,030.05']));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.signatureDoorColorClick(d, wd,'extColor_basicSpecial_all_Apollo White_null_#fcffff_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,030.05']));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalDoorColorClick(d, wd, 'extColor-ral')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalLilacDoorColorClick(d, wd, 'extColor_ral_Yellow_Brown beige_RAL 1011_#8A6642_undefined')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,976.71']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,0)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalDoorColorClick(d, wd, 'extColor-premiumSpecial')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalLilacDoorColorClick(d, wd, 'extColor_premiumSpecial_all_Clear Anodized_null_#D3D3D3_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$9,530.03']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,0)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalDoorColorClick(d, wd, 'extColor-arch')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalLilacDoorColorClick(d, wd, 'extColor_arch_all_Almond_null_#e6dcb8_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,976.71']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,0)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalDoorColorClick(d, wd, 'extColor-woodStandard')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalLilacDoorColorClick(d, wd, 'extColor_woodStandard_all_Fir_null_null_images/colors/wood/Birch121.jpg')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$10,744.91']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,0)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalDoorColorClick(d, wd, 'extColor-woodPremium')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalLilacDoorColorClick(d, wd, 'extColor_woodPremium_all_Cherry_null_null_images/colors/wood/Cherry121.jpg')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$11,287.84']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,0)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalDoorColorClick(d, wd, 'extColor-customColorBox')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.extriorCustomDoorColorInput(d, wd, 'red')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,248.54']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,0)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalDoorColorClick(d, wd, 'extColor-ral')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalLilacDoorColorClick(d, wd, 'extColor_ral_Yellow_Brown beige_RAL 1011_#8A6642_undefined')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,976.71']));

    pc = pc.then(() => quote_ColorAndFinish.interiorDoorColorClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorCustomDoorColorClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorCustomDoorColorInput(d, wd, 'red')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,976.71']));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Low-E 3')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$9,520.56']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,0)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$9,398.20']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,0)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Unglazed')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,867.93']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,0)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,999.53']));
    pc = pc.then(() => quote_Hardware.hardWareOptionSelect(d, wd, 'extras-Keyed Alike')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$9,025.85']));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Signature Door (Aluminum)','100" width x 90" height','101" width x 91" height',
    '3','31.44"','Left','Outswing','Left 2 / Right 0','Aluminum Block','Brown beige (RAL 1011)','Unglazed','Flush',
    'None','Panoramic Screen Split','$9,025.85','Total $9,025.85','Item 1 of 1']));

    pc = pc.then(() => cart.quantityInput(d, wd, "2")).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Signature Door (Aluminum)','100" width x 90" height','101" width x 91" height',
        '3','31.44"','Left','Outswing','Left 2 / Right 0','Aluminum Block','Brown beige (RAL 1011)','Unglazed',
        'None','Panoramic Screen Split','Flush',
        '$18,051.70','$9,025.85','Total $18,051.70','Item 1-2 of 2']));

    pc = pc.then(() => cart.duplicateClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Signature Door (Aluminum)','100" width x 90" height','101" width x 91" height',
        '3','31.44"','Left','Outswing','Left 2 / Right 0','Aluminum Block','Brown beige (RAL 1011)','Unglazed',
        'None','Left','Panoramic Screen Split','Flush',
        '$9,025.85','$18,051.70','Total $36,103.40','Item 1-2 of 4','Item 3-4 of 4']));
    pc = pc.then(() => cart.saveAsQuote(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Create a Signature Door Quote by salesPerson');
    return pc;
}

function complete_signature_door(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Complete flow Signature Door Quote'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd,)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => quote_NewQuote.newQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));

    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Signature Door (Aluminum)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 3000)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-Flush')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-86")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$183,697.86','powered by','Sales Quoter','Choose Frame Type:']));

    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,"swingInOrOut-Outswing")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$183,697.86']));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-No Screen  ")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$183,697.86']));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.signatureDoorColorClick(d, wd,'extColor_basicSpecial_all_Apollo White_null_#fcffff_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$183,697.86']));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Low-E 3')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$201,162.52']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,0)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$197,233.00']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,0)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Unglazed')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$180,204.93']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,0)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    // pc = pc.then(() => cf.testContentByArray(d, ['$180,336.53']));
    pc = pc.then(() => quote_Hardware.hardWareOptionSelect(d, wd, 'extras-Keyed Alike')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$180,362.85']));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Signature Door (Aluminum)','3000" width x 90" height','3001" width x 91" height',
        '86','34.59"','86.25"','Left','Outswing','Left 85 / Right 0','Aluminum Block','Apollo White','Unglazed','Flush',
        'Door Restrictor, Keyed Alike','New York Nickel',
        'None','No Screen','$180,362.85','$180,362.85','Item 1 of 1']));

    // ADD DOOR
    pc = pc.then(() => cart.addDoorOptionClick(d, wd,'home-sales')).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Signature Door (Aluminum)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 3000)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-Flush')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-86")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$183,697.86','powered by','Sales Quoter','Choose Frame Type:']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,"swingInOrOut-Outswing")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$183,697.86']));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-No Screen  ")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$183,697.86']));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureDoorColorClick(d, wd,'extColor_basicSpecial_all_Apollo White_null_#fcffff_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$183,697.86']));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Low-E 3')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$201,162.52']));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$201,294.12']));

    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Signature Door (Aluminum)','3000" width x 90" height','3001" width x 91" height',
        '86','34.59"','86.25"','Left','Outswing','Left 85 / Right 0','Aluminum Block','Apollo White','Unglazed','Flush',
        'Door Restrictor, Keyed Alike','New York Nickel',
        'None','No Screen','$180,362.85','Total $381,656.97','Item 1 of 2','Item 2 of 2',
    '201,294.12','Low-E 3']));

    pc = pc.then(() => cart.saveAsQuote(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.leadSourceSelect(d,2)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.firstNameInput(d, wd, 'fname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.lastNameInput(d, wd, 'lname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.emailInput(d, wd, 'test@gmail.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.phoneInput(d, wd, '8989898989')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingContactInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingEmailInputName(d, wd, 'test@email.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingCityInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingStateInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingZipInputName(d, wd, '898989')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));

    pc = pc.then(() => quote_order_customerInfo.attachToQuoteClick(d, wd)).then(() => cf.test(d, 'viewSavedQuoteConfirmation', Config.BASE_URL + '?tab=viewSavedQuoteConfirmation&', true, _u));
    pc = pc.then(() => quote_viewQuote.viewQuoteTabClick(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$381,656.97']));
    pc = pc.then(() => quote_viewQuote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Signature Door (Aluminum)','3000" width x 90" height','3001" width x 91" height',
        '86','34.59"','86.25"','Left','Outswing','Left 85 / Right 0','Aluminum Block','Apollo White','Unglazed','Flush',
        'Door Restrictor, Keyed Alike','New York Nickel',
        'None','No Screen','$180,362.85','Total $381,656.97','Item 1 of 2','Item 2 of 2',
        '201,294.12','Low-E 3']));

    pc = cf.handleOutput(d, pc, capability, 'Complete flow Signature Door Quote');
    return pc;
}

module.exports = { create_a_signature_door_quote_by_salesPersonLogin,
    complete_signature_door
};