const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const salesPage = require('./FT/sqAdmin/sales/salesPage');

let _u = undefined;

function sales_flow_by_SQ_admin_sales_person(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Sales flow by SQ-admin by Sales Person'));
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => sideBar.salesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/reports', true, _u));
    pc = pc.then(() => salesPage.openDatePickerClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/reports', true, _u));
    pc = pc.then(() => salesPage.selectPreviousYearClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/reports', true, _u));
    pc = pc.then(() => salesPage.searchInput(d, wd, '2019')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/reports', true, _u));
    pc = cf.handleOutput(d, pc, capability, 'Sales flow by SQ-admin by Sales Person');
    return pc;
}

module.exports = {sales_flow_by_SQ_admin_sales_person};