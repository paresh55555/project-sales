const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const frameFormulasPage = require('./FT/sqAdmin/module/frameFormulasPage');

let _u = undefined;

function manage_frame_formulas_flow_by_sqadmin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Manage Frame Formulas by sales person'));
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => sideBar.modulesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
    pc = pc.then(() => frameFormulasPage.vinylwithAluminumBlockFrameClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.frameFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.addFrameFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));

    pc = pc.then(() => frameFormulasPage.optionNameInput(d, wd, 'option test', 'name of option')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.optionNameInput(d, wd, 11, 'option type')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.optionNameInput(d, wd, 'material test', 'material')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.optionNameInput(d, wd, 100, 'cost meter')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.optionNameInput(d, wd, 5, 'cost paint meter')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.optionNameInput(d, wd, 10, 'waste percent')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.optionNameInput(d, wd, '10*20', 'number of lengths formula')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.optionNameInput(d, wd, 10, 'cut size formula')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.optionNameInput(d, wd, 20, 'burn off')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.optionNameInput(d, wd, 30, 'for cut sheet')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.optionNameInput(d, wd, 4, 'my order')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.saveFrameFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));

    pc = pc.then(() => frameFormulasPage.frameFormulasSearchInput(d, wd, 'option test')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
    pc = pc.then(() => frameFormulasPage.frameFormulasRecordClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
    pc = pc.then(() => frameFormulasPage.optionNameInput(d, wd, 'update test', 'name of option')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.optionNameInput(d, wd, 'material test', 'material')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.saveFrameFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));

    pc = pc.then(() => frameFormulasPage.frameFormulasSearchInput(d, wd, 'update test')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
    pc = pc.then(() => frameFormulasPage.frameFormulasRecordClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
    pc = pc.then(() => frameFormulasPage.deleteFrameFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.confirmFrameFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.confirmFrameFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.changeOrderClick(d, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.changeOrderClick(d, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => frameFormulasPage.changeOrderClick(d, 2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = cf.handleOutput(d, pc, capability, 'Manage Frame Formulas by sales person');
    return pc;
}

module.exports = { manage_frame_formulas_flow_by_sqadmin };