const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const quote_NewQuote= require('./FT/afterLogin/quote/newQoutes');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const cart = require('./FT/afterLogin/quote/cart');
const quote_order_customerInfo = require('./FT/afterLogin/order/customerInfo');
const view_order = require('./FT/afterLogin/adminActions/viewOrdersPage');
const view_quote = require('./FT/afterLogin/quote/viewQuotePage');
const quote_ViewOrder= require('./FT/afterLogin/order/viewOrder');

const accounting = require('./FT/afterLogin/accountingActions/accountingActionFlow');

let _u = undefined;

function delete_screen_by_sales_person(capability) {
    let d = Config.getDriver(Config.ChromeCapabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Delete Screen by Sales Person'));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));

    // Quote
    pc = pc.then(() => quote_NewQuote.newQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Absolute Door (Vinyl)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-right')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,500)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelDirectionSelection(d, wd,"swingInOrOut-Outswing")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Split")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_Beige_null_#F5F5DC_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Total $7,261.37']));
    pc = pc.then(() => cart.saveAsQuote(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));

    pc = pc.then(() => quote_order_customerInfo.leadSourceSelect(d,2)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.firstNameInput(d, wd, 'fname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.lastNameInput(d, wd, 'lname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.emailInput(d, wd, 'test@gmail.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.phoneInput(d, wd, '8989898989')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingContactInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingPhoneInputName(d, wd, '8989898')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingEmailInputName(d, wd, 'test@email.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress2InputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingCityInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingStateInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingZipInputName(d, wd, '898989')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.useShippingCheckbox(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.attachToQuoteClick(d, wd)).then(() => cf.test(d, 'viewSavedQuoteConfirmation', Config.BASE_URL + '?tab=viewSavedQuoteConfirmation&', true, _u));

    pc = pc.then(() => quote_order_customerInfo.quoteViewButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Total $7,261.37']));

    pc = pc.then(() => quote_order_customerInfo.revenueTypeSelect(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => quote_order_customerInfo.channelTypeSelect(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => accounting.dateDueInputChange(d, wd, 'viewOrderDateBox hasDatepicker')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));

    pc = pc.then(() => view_quote.convertToSale(d, wd, '-convertToSale')).then(() => cf.testAlertData(d,'Are you sure you want to convert this quote to an order?'));
    let orderNo;
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.getAlertText(d, wd).then((order) => orderNo = order ));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));

    // Order
    pc = pc.then(() => quote_ViewOrder.viewOrderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,orderNo)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.orderIdClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.revenueTypeSelect(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.channelTypeSelect(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.dateDueInputChange(d, wd,'ui-datepicker-current-day')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.paymentInfoClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting.paymentOptionClick(d, wd,0)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting.last4DigitsOfCreditCardInput(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    let balanceDue;
    pc = pc.then(() => accounting.getBalanceDue(d, wd)).then((balance) => balanceDue = balance);
    pc = pc.then(() => accounting.paymentAmountInput(d, wd, balanceDue)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    // pc = pc.then(() => accounting.paymentDateSelect(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting.closePaymentInfo(d,wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => quote_ViewOrder.sendToProduction(d, wd));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // CHANGE ORDER HOLD STATUS & DUE DATE SELECTION
    pc = pc.then(() => accounting.chooseActionClick(d, wd,2)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewAllProductionClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders', true, _u));
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd, orderNo)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders', true, _u));
    pc = pc.then(() => quote_ViewOrder.orderIdClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => accounting.onHoldSelection(d, wd,0)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => accounting.dateDueInputChange(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));

    // Accounting Login
    pc = pc.then(() => l1.salesPersonLogout(d, wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn&', true, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "PDAccounts")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=verifyDeposit', true, _u));

    pc = pc.then(() => accounting.searchInput(d, wd,orderNo)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting.paymentBox1Click(d,wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting.depositSatisfiedSelect(d, wd, 1)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting.depositSatisfiedClick(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));

    pc = pc.then(() => l1.salesPersonLogout(d,wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));

    // PRODUCTION LOGIN
    pc = pc.then(() => l1.EmailInput(d, wd, "production")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewAllProduction', true, _u));
    // pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,orderNo)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewAllProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.secondNumberStatusChanged(d, wd,1,0)).then(() => cf.testAlertData(d, 'Are you sure you want to move this order to Ready For Production as it has only been 0 days since it when to PreProduction'));
    pc = pc.then(() => cf.handleAlertCancel(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewAllProduction', true, _u));
    // pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,orderNo)).then(() => cf.test(d, 'viewAllProduction', Config.BASE_URL + '?tab=viewAllProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.secondNumberStatusChanged(d, wd,1,0)).then(() => cf.testAlertData(d, 'Are you sure you want to move this order to Ready For Production as it has only been 0 days since it when to PreProduction'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.testAlertData(d, 'This will update the attached screen, do you want to continue?'));
    pc = pc.then(() => cf.handleAlertCancel(d, wd)).then(() => cf.test(d, 'viewAllProduction', Config.BASE_URL + '?tab=viewAllProduction', true, _u));

    pc = pc.then(() => quote_ViewOrder.secondNumberStatusChanged(d, wd,1,0)).then(() => cf.testAlertData(d, 'Are you sure you want to move this order to Ready For Production as it has only been 0 days since it when to PreProduction'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.testAlertData(d, 'This will update the attached screen, do you want to continue?'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'viewAllProduction', Config.BASE_URL + '?tab=viewAllProduction', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewPreProductionClick(d, wd)).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,orderNo)).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction&', true, _u));
    pc = pc.then(() => quote_ViewOrder.secondOrderNumberClick(d, wd,orderNo)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder&', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,3)).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction&', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewPreProductionClick(d, wd)).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction&', true, _u));
    // pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,orderNo+'-S')).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction&', true, _u));
    pc = pc.then(() => quote_ViewOrder.orderNumberClick(d, wd)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder&', true, _u));
    pc = pc.then(() => l1.salesPersonLogout(d,wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));


    // SALES PERSON LOGIN
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,orderNo)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));

    // Switching into screen and doors
    pc = pc.then(() => quote_ViewOrder.orderIdClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Door',orderNo]));
    pc = pc.then(() => quote_ViewOrder.productionStatusOrderClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Screen',orderNo+'-S','Total $7,261.37']));
    // Remove Screen
    pc = pc.then(() => accounting.scroll(d, wd,1000)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.cartItemClick(d, wd,'1')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.scroll(d, wd,1000)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-No Screen  ")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.scroll(d, wd,100)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.saveToOrderClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Screen',orderNo+'-S']));
    pc = pc.then(() => cf.testContentByArray(d, ['Total $6,230.12']));
    pc = pc.then(() => quote_ViewOrder.productionStatusOrderClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Delete Screen','Door',orderNo]));

    pc = pc.then(() => accounting.chooseActionClick(d, wd,4));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,orderNo)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.orderIdClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    // CHECKING PRICE AFTER DELETING SCREEN
    pc = pc.then(() => cf.testContentByArray(d, ['Total $6,230.12']));

    pc = cf.handleOutput(d, pc, capability, 'Delete Screen by SalesPerson');
    return pc;
}


module.exports = {delete_screen_by_sales_person};