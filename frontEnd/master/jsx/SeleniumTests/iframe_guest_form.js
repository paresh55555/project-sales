const moment = require('moment');
const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const cart = require('./FT/afterLogin/quote/cart');
const leadsMode = require('./FT/afterLogin/leadsMode/leadsMode');
const cf = require('./FT/commonFunctions/cf');
const quote_viewQuote= require('./FT/afterLogin/quote/viewQuotePage');
const quote_order_customerInfo = require('./FT/afterLogin/order/customerInfo');
const accounting = require('./FT/afterLogin/accountingActions/accountingActionFlow');

let _u = undefined;

function iframe_guest(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Iframe Guest flow'));
    pc = pc.then(()=>d.get(Config.BASE_URL + '?tab=iframeGuest')).then(() => cf.delayBySecond(cf.delaySecond.ten));

    // CHECKING VALIDATION ON IFRAME GUEST FORM
    pc = pc.then(() => l1.requestAQuote(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=iframeGuest', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Please enter your name','Please enter your email','Please enter your phone',
        'Please enter your zip','Please select best way to contact','Please enter best time to call',
    'Please select timeline for project']));

    // FILLING FORM
    pc = pc.then(() => l1.nameInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=iframeGuest', _u, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Please enter your email','Please enter your phone',
        'Please enter your zip','Please select best way to contact','Please enter best time to call',
        'Please select timeline for project']));
    pc = pc.then(() => l1.EmailInput(d, wd, "FT@gmail.com")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=iframeGuest', _u, _u));
    pc = pc.then(() => l1.phoneInput(d, wd, "1234567890")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=iframeGuest', _u, _u));
    pc = pc.then(() => l1.zipInput(d, wd, "10001")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=iframeGuest', _u, _u));
    pc = pc.then(() => l1.bestWayToContact(d, wd, 0)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=iframeGuest', _u, _u));

    pc = pc.then(() => l1.timeToCallInput(d, wd, "timeToCall","9")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=iframeGuest', _u, _u));
    pc = pc.then(() => l1.timelineSelect(d, wd, 1)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=iframeGuest', _u, _u));
    pc = pc.then(() => l1.doorWidthInput(d, wd, "200")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=iframeGuest', _u, _u));
    pc = pc.then(() => l1.doorHeightInput(d, wd, "200")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=iframeGuest', _u, _u));
    pc = pc.then(() => l1.commentsInput(d, wd, "Test Comments")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=iframeGuest', _u, _u));

    pc = pc.then(() => l1.requestAQuote(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=iframeGuest', true, _u));
    let quoteNo;
    pc = pc.then(() => l1.getQuoteNo(d, wd)).then((num) => quoteNo=num);

    pc = pc.then(() => cf.testContentByArray(d, ['Congratulations','You will be contacted regarding your quote.',
    'You may also contact us at ','760-722-1300']));

    // LEADS SIDE
    pc = pc.then(() => d.get(Config.BASE_URL)).then(() => cf.delayBySecond(cf.delaySecond.ten));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "leads")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewLeads', true, _u));
    pc = pc.then(() => leadsMode.menuClick(d,wd,'viewAssignedLeads')).then(() => cf.test(d, 'viewAssignedLeads', Config.BASE_URL + '?tab=viewAssignedLeads&', true, _u));
    pc = pc.then(() => quote_viewQuote.searchInput(d, wd,quoteNo)).then(() => cf.test(d, 'viewAssignedLeads', Config.BASE_URL + '?tab=viewAssignedLeads', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [quoteNo,'test','10001','(123) 456-7890','0','$0.00',moment().format('MM/DD/YYYY')]));

    pc = pc.then(() => leadsMode.viewClick(d,wd)).then(() => cf.test(d, 'viewLeadQuote', Config.BASE_URL + '?tab=viewLeadQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [quoteNo,'test','10001','0','$0.00',moment().format('MM/DD/YYYY'),
        "FT@gmail.com",'1234567890',
        'Phone','9','Immediately','W-200 in ','H-200 in','Test Comments','Total $0.00',
    'Quote Expires on',moment().add(180,'days').format('MMMM DD,YYYY')]));

    // SALES PERSON SIDE
    pc = pc.then(() => d.get(Config.BASE_URL)).then(() => cf.delayBySecond(cf.delaySecond.ten));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    pc = pc.then(() => quote_viewQuote.searchInput(d, wd,quoteNo)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes', true, _u));

    // VIEW QUOTE AT SALES PERSON SIDE
    pc = pc.then(() => quote_viewQuote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [quoteNo,'test','10001','0','$0.00',moment().format('MM/DD/YYYY'),
        "FT@gmail.com",'1234567890',
        'Phone','9','Immediately','W-200 in ','H-200 in','Test Comments','Total $0.00',
        'Quote Expires on',moment().add(180,'days').format('MMMM DD,YYYY')]));

    pc = pc.then(() => quote_order_customerInfo.editCustomerClick(d, wd)).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.leadSourceSelect(d,1)).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.firstNameInput(d, wd, 'fname')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.lastNameInput(d, wd, 'lname')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.emailInput(d, wd, 'test@gmail.com')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.phoneInput(d, wd, '8989898989')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingNameInput(d, wd, 'test')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingAddress1Input(d, wd, 'test')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingAddress1Input(d, wd, 'test')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingCityInput(d, wd, 'test')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.saveCancelCustomerInfo(d, wd,0)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_order_customerInfo.attachCustomerClick(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.leadSourceSelect(d,1)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.firstNameInput(d,wd,'firstname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.lastNameInput(d,wd,'lastname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.emailInput(d,wd,'email@emial.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.phoneInput(d,wd,'898989')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingContactInputName(d,wd,'billing test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingEmailInputName(d,wd,'billing@email.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d,wd,'address 1')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress2InputName(d,wd,'adress 2')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingCityInputName(d,wd,'billing city')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingStateInputName(d,wd,'billing state')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingZipInputName(d,wd,'83211')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.attachToQuoteClick(d, wd, 'nextTitle')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => accounting.scroll(d,wd,0)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_viewQuote.qualitySelect(d)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    //ADD DOOR
    pc = pc.then(() => accounting.chooseActionClick(d,wd,0)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Absolute Door (Vinyl)')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, "track-3/4 Upstand")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd, "swingDirection-left")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd, "movement-left-0")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd, "swingInOrOut-Outswing")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd, "screens-Panoramic Screen Left")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_Beige_null_#F5F5DC_null')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)',
        '3', 'Left','Outswing','Left 2 / Right 0','Aluminum Block','3/4 Upstand','None','Glass Clear',
        'Argon','New York Nickel','Door Restrictor','Panoramic Screen Left']));

    // EDIT ADDED DOOR
    pc = pc.then(() => accounting.editAddedDoor(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, "track-Flush")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-4")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd, "swingDirection-right")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd, "movement-right-3")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd, "swingInOrOut-Inswing")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd, "screens-No Screen  ")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_White_null_#fcffff_null')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Low-E 3')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-Miami')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl',
        '4', 'Right','Inswing','Left 0 / Right 3','Aluminum Block','Flush','None','Low-E 3',
        'Argon','Miami','No Screen','Total $7,219.51']));
    pc = pc.then(() => cf.testContentByArray(d, [quoteNo,'test','0',moment().format('MM/DD/YYYY'),
        'Phone','9','Immediately','W-200 in ','H-200 in','Test Comments',
        'Quote Expires on',moment().add(180,'days').format('MMMM DD,YYYY')]));

    //ADD SCREEN DOOR
    pc = pc.then(() => accounting.chooseActionClick(d,wd,1)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Screens')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 50)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 50)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd, "screens-Panoramic Screen Left")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'extColor_basic_all_Apollo White_null_#fcffff_')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Add To Quote']));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,0)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl',
        '4', 'Right','Inswing','Left 0 / Right 3','Aluminum Block','Flush','None','Low-E 3',
        'Argon','Miami','No Screen',
    'Screens','50" width x 50" height','Apollo White','Panoramic Screen Left','51" width x 51" height','46.25"',
        'Left 0 / Right 0','Aluminum Block','Clear Anodized','Apollo White','Door Restrictor']));

    pc = cf.handleOutput(d, pc, capability, 'Iframe Guest flow');
    return pc;
}
module.exports = {iframe_guest};

