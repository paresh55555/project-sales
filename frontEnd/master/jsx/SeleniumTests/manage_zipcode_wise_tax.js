const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const l1 = require('./FT/l1');
const login = require('./FT/sqAdmin/auth/login');
const taxOverride = require('./FT/sqAdmin/TaxOverride/taxOverride');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const quote_NewQuote= require('./FT/afterLogin/quote/newQoutes');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const cart = require('./FT/afterLogin/quote/cart');
const quote_order_customerInfo = require('./FT/afterLogin/order/customerInfo');
const viewQuote = require('./FT/afterLogin/quote/viewQuotePage');
const accounting = require('./FT/afterLogin/accountingActions/accountingActionFlow');
let _u = undefined;

function manage_zip_code_wise_tax(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Zip code wise tax by SQ-admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "SalesManager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sideBar.taxOverride(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));

    // ADD TAX FOR `Idaho` State
    let stateName;
    pc = pc.then(() => taxOverride.addTaxClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Add Tax Override']));
    pc = pc.then(() => taxOverride.saveClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Please select state']));
    pc = pc.then(() => taxOverride.stateInput(d, wd,'Idaho')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.selectState(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.getStateName(d, wd)).then((s) => stateName = s);
    pc = pc.then(() => taxOverride.taxOverrideInput(d, wd,10)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.saveClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [stateName,'10']));

    // SALES PERSON SIDE TAX
    pc = pc.then(() => d.get(Config.BASE_URL));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => quote_NewQuote.newQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Absolute Door (Vinyl)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-right')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,500)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelDirectionSelection(d, wd,"swingInOrOut-Outswing")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Split")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_Beige_null_#F5F5DC_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_White_null_#fcffff_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => accounting.notesInput(d, wd, 'test notes')).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl',
        '100" width x 90" height','101" width x 91" height','3','31.35"','85.5"','Left','Outswing',
        'Left 2 / Right 0','Aluminum Block','3/4 Upstand','Glass Clear','Argon','New York Nickel','Door Restrictor',
        'Panoramic Screen Split','Item 1 of 1',
        'Total $6,990.16','$6,990.16']));

    pc = pc.then(() => cart.saveAsQuote(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.searchPreviousCustomerClick(d, wd,)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.searchPreviousCustomerInput(d, wd,'a')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.attachSearchCustomer(d, wd)).then(() => cf.test(d, 'viewSavedQuoteConfirmation', Config.BASE_URL + '?tab=viewSavedQuoteConfirmation&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.quoteViewButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d, ['Total $6,990.16','6,990.16','Tax (0%):']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,1000)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => accounting.shippingZipInput(d, wd,'83211')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // CHECKING TAX PERCENTAGE ACCORDING TO ZIP CODE
    pc = pc.then(() => cf.testContentByArray(d, ['Total $7,689.18','6,990.16','Tax (10.00%)','699.02']));

    // CHECKING GRAND TOTAL AND FOOTER PRICE
    pc = pc.then(() => accounting.checkGrandTotalAndFooterPrice(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));


    pc = pc.then(() => d.get(Config.BASE_URL + "SQ-admin/login"));
    pc = pc.then(() => login.EmailInput(d, wd, "SalesManager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sideBar.taxOverride(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));

    // DELETE STATE in TAX Override
    pc = pc.then(() => taxOverride.searchInput(d, wd,stateName)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.recordClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.deleteClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Are you sure you want to delete this Tax Override? ']));
    pc = pc.then(() => taxOverride.deleteConfirm(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Tax Override has been deleted.']));

    pc = cf.handleOutput(d, pc, capability, 'Manage Zip Code wise tax by SQ-admin');
    return pc;
}

module.exports = {manage_zip_code_wise_tax};