const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const reportsPage= require('./FT/sqAdmin/reports/reportsPage');
const orderPage = require('./FT/sqAdmin/OrderReports/orderReports');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const moment = require('moment');
let _u = undefined;

function reports_by_SQ_admin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let url = Config.BASE_URL + 'SQ-admin/sales/leadreport';
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Manage Leads Reports by SQ-admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "SalesManager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sideBar.reportsClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = pc.then(() => reportsPage.dateButtonClick(d)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.datePickerClick(d, wd, Number(moment().format('M'))-1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.datePickerYearClick(d, wd, 0)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    let dateFormat = moment().format('MMM')+", "+moment().format('YYYY')
    pc = pc.then(() => cf.testContentByArray(d, [dateFormat]));
    pc = pc.then(() => reportsPage.goButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = pc.then(() => orderPage.filterClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => orderPage.groupBySelect(d, wd,1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Month']));
    pc = pc.then(() => orderPage.filterClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => orderPage.groupBySelect(d, wd,2)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Year']));
    pc = pc.then(() => orderPage.filterClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => orderPage.groupBySelect(d, wd,0)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['All']));

    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => orderPage.leadSourceWiseSelect(d, wd,1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Lead Source: Incomplete Leads']));
    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => orderPage.leadSourceWiseSelect(d, wd,2)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Lead Source: Leads with a Quote']));
    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => orderPage.leadSourceWiseSelect(d, wd,3)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Lead Source: Request for Quote']));
    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => orderPage.leadSourceWiseSelect(d, wd,0)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Lead Source: All']));


    pc = pc.then(() => reportsPage.changeOrderClick(d, 0)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.changeOrderClick(d, 1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.changeOrderClick(d, 2)).then(() => cf.test(d, 'SQ Admin',url, true, _u));

    pc = pc.then(() => reportsPage.searchInput(d, wd, 'a')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.searchInput(d, wd, 'e')).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = pc.then(() => reportsPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = pc.then(() => reportsPage.paginationNextClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.paginationPreviousClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = pc.then(() => reportsPage.paginationNextClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.paginationPreviousClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => reportsPage.exportLeadsReportClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Manage Leads Reports by SQ-admin');
    return pc;
}

module.exports = {reports_by_SQ_admin};