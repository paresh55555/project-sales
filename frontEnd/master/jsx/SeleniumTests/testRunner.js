const cf = require('./FT/commonFunctions/cf');
const l1 = require('./l1');
const Config = require('./FT/config/config');
const test_quote_guestLogin = require('./guest_login_create_a_vinyl_door_quote.test');
const guest_login_depthFlow = require('./guest_depth_flow.test');
const test_absolute_door_laminate_quote_quest = require('./guest_login_absolute_door_laminate_quote.test')
const test_quote_salesPerson = require('./salesPerson_login_create_a_vinyl_door_quote.test');
const test_view_quote_salesPerson = require('./salesPerson_login_view_quote.test');
const admin_actions_salesPerson = require('./admin_tool.test');
const test_view_orders_salesPerson = require('./salesPerson_login_view_order.test');
const test_signature_door_quote_salesPerson = require('./salesPerson_login_create_a_signature_aluminum_door_quote.test');
const test_vinyl_picture_window_quote_salesPerson = require('./salesPerson_login_create_a_vinyl_picture_window_quote.test');
const test_absolute_door_laminate_quote_salesPerson = require('./salesPerson_login_create_a_absolute_door_laminate_quote.test')
const test_aluminum_picture_window_quote_salesPerson = require('./salesPerson_login_create_a_aluminum_picture_window_quote.test');
const door_for_joe_only = require('./door_for_joe_only_quote');
const test_accounting_login_depth_flow = require('./accounting_login_depth_flow.test');
const production_login_depth_flow = require('./production_login_depth_flow.test');
const manage_user_by_sq_admin_test = require('./manage_user_by_sq_admin_test');
const add_commission_by_sq_admin_test = require('./add_commission_by_sq_admin_test');
const edit_commission_by_sq_admin_test = require('./edit_commission_by_sq_admin_test');
const manage_commission_by_sq_admin_test = require('./manage_commission_flow_by_sq_admin');
const manage_commission_groups_by_sq_admin_test = require('./manage_commission_groups_flow_by_sq_admin');
const sales_flow_by_sq_admin_test = require('./sales_flow_by_sq_admin.test');
const companies_flow_by_sq_admin_login = require('./companies_flow_by_sq_admin_login');
const manage_has_balance_flow_by_superadmin_login = require('./manage_has_balance_by_superadmin');
const manage_user_by_sq_admin_sales_person_test = require('./manage_user_by_sq_admin_sales_person_test');
const manage_panel_module_flow_by_sqadmin = require('./manage_panel_module_flow_by_sqadmin');
const sales_flow_by_sq_admin_sales_person_test = require('./sales_flow_by_sq_admin_sales_person_test');
const manage_module_flow_by_sqadmin = require('./manage_module_flow_by_sqadmin');
const manage_glass_formulas_flow_by_sqadmin = require('./manage_glass_formulas_flow_by_sqadmin');
const manage_fixed_formulas_flow_by_sqadmin = require('./manage_fixed_formulas_flow_by_sqadmin');
const manage_percentage_formulas_flow_by_sqadmin = require('./manage_percentage_formulas_flow_by_sqadmin');
const min_max_sizes_flow_by_sqadmin = require('./min_max_sizes_flow_by_sqadmin');
const manage_frame_formulas_flow_by_sqadmin = require('./manage_frame_formulas_flow_by_sqadmin');
const manage_commission_rate_by_sqadmin = require('./manage_commission_rate_by_sq_admin_test');
const manage_reports_by_sqadmin = require('./manage_reports_flow_by_sq_admin_test');
const manage_discounts_by_sqadmin = require('./manage_discounts_flow_by_sq_admin');
const tax_override_by_sqadmin = require('./tax_override_sq_admin');
const zipcode_wise_tax = require('./manage_zipcode_wise_tax');
const quote_with_billing = require('./quote_flow_with_billing');
const edit_order_by_sales_person = require('./edit_order_by_sales_person');
const screen_selection_sales_person = require('./screen_selection_salesperson');
const deleteScreen = require('./delete_screen_flow');
const manage_order_reports_by_sq_admin = require('./manage_order_reports_by_sq_admin');
const manage_order_system_reports_by_sq_admin = require('./manage_order_system_reports_by_sq_admin');
const manage_quotes_reports_by_sq_admin = require('./manage_quotes_reports_by_sq_admin');
const manage_daily_intake_reports_by_sq_admin = require('./manage_daily_intake_reports_by_sq_admin');
const manage_leads_mode_flow = require('./leads_mode_flow');
const manage_customer_report_reports_by_sq_admin = require('./manage_customer_email_reports_by_sq_admin');
const manage_order_with_screen_option = require('./order_with_screen_option');
const refresh_quote = require('./refresh_quote_sales_person');
const pagination = require('./view_order_pagination_flow');
const search_flow = require('./manage_search_flow');
const iframe_guest = require('./iframe_guest_form');
const payment_flow = require('./payment_flow_view_order');

let pc;
pc = cf.delayBySecond(5);

const browsers = Object.getOwnPropertyNames(Config.Capabilities);
const chromeBrowsers = Object.getOwnPropertyNames(Config.ChromeCapabilities);

if (process.argv.length === 3) {
    switch (process.argv[2].toLowerCase()) {
        case 'salespersonlogin':
            return salesPersonLogin(pc).then(() => {
                caseSummary();
                return;
            });
        case 'guestlogin':
            return guestLogin(pc).then(() => {
                caseSummary();
                return;
            });
        case 'guestquote':
            return quote_guestLogin(pc).then(() => {
                caseSummary();
                return;
            });
        case 'guestdepthflow':
            return guestDepthFlow(pc).then(() => {
                caseSummary();
                return;
            });
        case 'guestabsolutelaminatedoorquote':
            return guestAbsoluteLaminateDoorFlow(pc).then(() => {
                caseSummary();
                return;
            });
        case 'orderwithscreenoption':
            return order_with_screen_option(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'salespersonquote':
            return quote_salesPersonLogin(pc).then(() => {
                caseSummary();
                return;
            });
        case 'quotecreationvalidation':
            return quote_validation(pc).then(() => {
                caseSummary();
                return;
            });
        case 'deleteitems':
            return delete_item(pc).then(() => {
                caseSummary();
                return;
            });
        case 'viewquotebysalesperson':
            return view_quote_by_salesPerson(pc).then(() => {
                caseSummary();
                return;
            });
        // case 'adminactions':
        //     return admin_action_method(pc).then(() => {
        //         caseSummary();
        //         return;
        //     });
        case 'salespersonvieworder':
            return quote_salesPersonViewOrder(pc).then(() => {
                caseSummary();
                return;
            });
        case 'salespersonsignaturedoorquote':
            return signature_door_quote_by_salesPersonLogin(pc).then(() => {
                caseSummary();
                return;
            });
        case 'completesignaturedoorquote':
            return complete_signature_door_quote_by_salesPersonLogin(pc).then(() => {
                caseSummary();
                return;
            });
        case 'salespersonvinylpicturewindowquote':
            return vinyl_picture_window_quote_by_salesPerson(pc).then(() => {
                caseSummary();
                return;
            });
        case 'salespersonabsolutedoorlaminatequote':
            return absolute_door_laminate_quote_by_salesPerson(pc).then(() => {
                caseSummary();
                return;
            });
        case 'salespersonaluminumpicturewindow':
            return aluminum_picture_window_quote_by_salesPerson(pc).then(() => {
                caseSummary();
                return;
            });
        case 'doorforjoeonly':
            return door_for_joe_only_quote(pc).then(() => {
                caseSummary();
                return;
            });
        case 'accountinglogin':
            return depth_flow_accounting_Login(pc).then(() => {
                caseSummary();
                return;
            });
        case 'verifieddepositaccounting':
            return verify_deposit_flow_accounting_Login(pc).then(() => {
                caseSummary();
                return;
            });
        case 'productiondepthflow':
            return production_login_depth_flow_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'manageuserbysqadmin':
            return manage_user_by_sq_admin_login_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'addcommissionbysqadmin':
            return add_commission_by_sq_admin_login_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'editcommissionbysqadmin':
            return edit_commission_by_sq_admin_login_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'managecommissionflowbysqadmin':
            return manage_commission_flow_by_sq_admin_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'managecommissiongroupsflowbysqadmin':
            return manage_commission_groups_flow_by_sq_admin_test(pc).then(() => {
                caseSummary();
                return;
            });
        // Functionality removed
        // case 'salesflowbyadmin':
        //     return sales_flow_by_admin_test(pc).then(() => {
        //         caseSummary();
        //         return;
        //     });
        case 'companiesflowbysqadmin':
            return companies_flow_by_sq_admin_login_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'manageuserbyadmininsqadmin':
            return manage_user_by_sq_admin_sales_person_login_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'managemoduleflowbysqadmin':
            return manage_module_flow_by_sqadmin_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'managehasbalanceflowbysuperadmin':
            return manage_has_balance_flow_by_superadmin_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'managepanelmoduleflowbysqadmin':
            return manage_panel_module_flow_by_sqadmin_test(pc).then(() => {
                caseSummary();
                return;
            });
        // case 'salesflowbysqadminsalesperson':
        //     return sales_flow_by_admin_sales_person_test(pc).then(() => {
        //         caseSummary();
        //         return;
        //     });
        case 'manageglassformulasflowbysqadmin':
            return manage_glass_formulas_flow_by_sqadmin_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'managefixedformulasflowbysqadmin':
            return manage_fixed_formulas_flow_by_sqadmin_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'managepercentageformulasflowbysqadmin':
            return manage_percentage_formulas_flow_by_sqadmin_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'minmaxsizesflowbysqadmin':
            return min_max_sizes_flow_by_sqadmin_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'manageframeformulasflowbysqadmin':
            return manage_frame_formulas_flow_by_sqadmin_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'managecommissionratebysqadmin':
            return manage_commission_rate_by_sqadmin_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'managediscountsbysqadmin':
            return manage_discounts_by_sqadmin_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'taxoverridebysqadmin':
            return manage_tax_override_by_sqadmin_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'zipcodewisetax':
            return manage_zip_code_wise_tax(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'quoteflowwithbilling':
            return quote_flow_with_billing(pc).then(() => {
                caseSummary();
                return;
            });
        case 'managecustomerviewquote':
            return manage_customer_view_quote(pc).then(() => {
                caseSummary();
                return;
            });
        case 'editorderbysalesperson':
            return edit_order_by_sales_person_test(pc).then(()=>{
                caseSummary();
                return;
            })
        case 'screenselectionsalesperson':
            return screen_selection_by_sales_person(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'deletescreen':
            return delete_screen(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'leadsreportsbysqadmin':
            return manage_reports_by_sqadmin_test(pc).then(() => {
                caseSummary();
                return;
            });
        case 'orderreportsbysqadmin':
            return order_reports_by_sq_admin(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'ordersystemreportsbysqadmin':
            return order_system_reports_by_sq_admin(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'quotesreportsbysqadmin':
            return quotes_reports_by_sq_admin(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'dailyintakereportsbysqadmin':
            return daily_intake_reports_by_sq_admin(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'leadsloginflow':
            return view_leads_mode_flow(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'customeremailreportsbysqadmin':
            return customer_email_reports_by_sq_admin(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'refreshquote':
            return refreshQuote(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'orderpaginationbysalesperson':
            return orderPaginationFlow(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'quotepaginationbysalesperson':
            return quotePaginationFlow(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'searchflow':
            return searchInGrid(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'iframeguest':
            return iframeGuest(pc).then(()=>{
                caseSummary();
                return;
            });
        case 'paymentflow':
            return paymentFlow(pc).then(()=>{
                caseSummary();
                return;
            });
        default: {
            console.log('\x1b[31m%s\x1b[0m', process.argv[2] + ' test not found');
        }
    }
} else {
    executeAll(pc).then(() => {
        caseSummary();
    });
}

function caseSummary() {
    let passCount = filterCase('pass');
    let failCount = filterCase('fail');
    var count = {}, status = 0, warningCount = 0;
    Config.failedList.forEach(function (i) {
        count[i] = (count[i] || 0) + 1;
    });
    Object.entries(count).forEach(entry => {
        status = status + 1;
        if (entry[1] < Object.keys(Config.Capabilities).length) {
            failCount = failCount - entry[1];
            warningCount = warningCount + entry[1];
        }
    });
    console.log('\x1b[33m%s\x1b[1m', '\n-------------TEST CASE-------------');
    console.log('\x1b[32m%s\x1b[1m', 'Pass : ' + passCount);
    console.log('\x1b[31m%s\x1b[0m', 'Fail : ' + failCount);
    console.log('\x1b[33m%s\x1b[0m', 'Warning : ' + warningCount);
    console.log('\x1b[33m%s\x1b[0m', '-----------------------------------');
    if (status > 0) {
        console.log('\x1b[31m%s\x1b[0m', Config.failedFT);
        console.log('\x1b[33m%s\x1b[0m', '-----------------------------------\n');
        process.exit(1);
    }
    console.log('\x1b[33m%s\x1b[0m', Config.failedFT);
    console.log('\x1b[33m%s\x1b[0m', '-----------------------------------\n');
    process.exit(0);
}

function filterCase(caseResult) {
    var count = 0;
    Config.isSuccess.forEach(value => {
        if (value === caseResult) {
            count = count + 1;
        }
    });
    return count;
}


function salesPersonLogin(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => l1.l1_common_step_for_salesPersonLogin(browsers[i]));
    }
    return pc;
}

function guestLogin(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => l1.l1_common_step_for_guestLogin(browsers[i]));
    }
    return pc;
}

function quote_guestLogin(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => test_quote_guestLogin.create_a_vinyl_door_quote_by_gustLogin(browsers[i]));
    }
    return pc;
}

function guestAbsoluteLaminateDoorFlow(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => test_absolute_door_laminate_quote_quest.create_a_absolute_door_laminate_quote_by_questLogin(browsers[i]));
    }
    return pc;
}

function guestDepthFlow(pc) {
    for (let i = 0; i < chromeBrowsers.length; i++) {
        pc = pc.then(() => guest_login_depthFlow.guest_login_depth_flow(chromeBrowsers[i]));
    }
    return pc;
}

function quote_salesPersonLogin(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => test_quote_salesPerson.create_a_vinyl_door_quote_by_salesPersonLogin(browsers[i]));
    }
    return pc;
}

function quote_validation(pc) {
    for (let i = 0; i < chromeBrowsers.length; i++) {
        pc = pc.then(() => test_quote_salesPerson.check_quote_validation(chromeBrowsers[i]));
    }
    return pc;
}

function delete_item(pc) {
    for (let i = 0; i < chromeBrowsers.length; i++) {
        pc = pc.then(() => test_quote_salesPerson.delete_items(chromeBrowsers[i]));
    }
    return pc;
}

function quote_salesPersonViewOrder(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => test_view_orders_salesPerson.view_order_by_salesPersonLogin(browsers[i]));
    }
    return pc;
}

function admin_action_method(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => admin_actions_salesPerson.admin_actions(browsers[i]));
    }
    return pc;
}

function view_quote_by_salesPerson(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => test_view_quote_salesPerson.view_quote_by_salesPersonLogin(browsers[i]));
    }
    return pc;
}

function signature_door_quote_by_salesPersonLogin(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => test_signature_door_quote_salesPerson.create_a_signature_door_quote_by_salesPersonLogin(browsers[i]));
    }
    return pc;
}

function complete_signature_door_quote_by_salesPersonLogin(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => test_signature_door_quote_salesPerson.complete_signature_door(browsers[i]));
    }
    return pc;
}

function vinyl_picture_window_quote_by_salesPerson(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => test_vinyl_picture_window_quote_salesPerson.create_a_vinyl_picture_window_quote_by_salesPersonLogin(browsers[i]));
    }
    return pc;
}

function aluminum_picture_window_quote_by_salesPerson(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => test_aluminum_picture_window_quote_salesPerson.create_a_aluminum_picture_window_quote_by_salesPersonLogin(browsers[i]));
    }
    return pc;
}

function door_for_joe_only_quote(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => door_for_joe_only.door_for_joe_only(browsers[i]));
    }
    return pc;
}

function absolute_door_laminate_quote_by_salesPerson(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => test_absolute_door_laminate_quote_salesPerson.create_a_absolute_door_laminate_quote_by_salesPersonLogin(browsers[i]));
    }
    return pc;
}

function depth_flow_accounting_Login(pc) {
    for (let i = 0; i < chromeBrowsers.length; i++) {
        pc = pc.then(() => test_accounting_login_depth_flow.accounting_login_depth_flow(chromeBrowsers[i]));
    }
    return pc;
}

function verify_deposit_flow_accounting_Login(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => test_accounting_login_depth_flow.verified_deposite_accounting(browsers[i]));
    }
    return pc;
}

function production_login_depth_flow_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => production_login_depth_flow.production_login_depth_flow(browsers[i]));
    }
    return pc;
}

function manage_user_by_sq_admin_login_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_user_by_sq_admin_test.add_user_by_SQ_admin(browsers[i]));
    }
    return pc;
}

function add_commission_by_sq_admin_login_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => add_commission_by_sq_admin_test.add_commission_by_SQ_admin(browsers[i]));
    }
    return pc;
}

function edit_commission_by_sq_admin_login_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => edit_commission_by_sq_admin_test.edit_commission_by_SQ_admin(browsers[i]));
    }
    return pc;
}

function manage_commission_flow_by_sq_admin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_commission_by_sq_admin_test.manage_commission_by_SQ_admin(browsers[i]));
    }
    return pc;
}

function manage_commission_groups_flow_by_sq_admin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_commission_groups_by_sq_admin_test.manage_commission_group_by_SQ_admin(browsers[i]));
    }
    return pc;
}

function sales_flow_by_admin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => sales_flow_by_sq_admin_test.sales_flow_by_SQ_admin(browsers[i]));
    }
    return pc;
}

function companies_flow_by_sq_admin_login_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => companies_flow_by_sq_admin_login.companies_flow_by_sq_admin_login(browsers[i]));
    }
    return pc;
}

function manage_user_by_sq_admin_sales_person_login_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_user_by_sq_admin_sales_person_test.add_user_by_SQ_admin_sales_person(browsers[i]));
    }
    return pc;
}

function manage_module_flow_by_sqadmin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_module_flow_by_sqadmin.manage_module_flow_by_sqadmin(browsers[i]));
    }
    return pc;
}

function manage_has_balance_flow_by_superadmin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_has_balance_flow_by_superadmin_login.has_balance_by_SQ_admin(browsers[i]));
    }
    return pc;
}

function manage_panel_module_flow_by_sqadmin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_panel_module_flow_by_sqadmin.manage_panel_module_flow_by_sqadmin(browsers[i]));
    }
    return pc;
}

function sales_flow_by_admin_sales_person_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => sales_flow_by_sq_admin_sales_person_test.sales_flow_by_SQ_admin_sales_person(browsers[i]));
    }
    return pc;
}

function manage_glass_formulas_flow_by_sqadmin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_glass_formulas_flow_by_sqadmin.manage_glass_formulas_flow_by_sqadmin(browsers[i]));
    }
    return pc;
}

function manage_fixed_formulas_flow_by_sqadmin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_fixed_formulas_flow_by_sqadmin.manage_fixed_formulas_flow_by_sqadmin(browsers[i]));
    }
    return pc;
}

function manage_percentage_formulas_flow_by_sqadmin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_percentage_formulas_flow_by_sqadmin.manage_percentage_formulas_flow_by_sqadmin(browsers[i]));
    }
    return pc;
}

function min_max_sizes_flow_by_sqadmin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => min_max_sizes_flow_by_sqadmin.min_max_sizes_flow_by_sqadmin(browsers[i]));
    }
    return pc;
}

function manage_frame_formulas_flow_by_sqadmin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_frame_formulas_flow_by_sqadmin.manage_frame_formulas_flow_by_sqadmin(browsers[i]));
    }
    return pc;
}

function manage_commission_rate_by_sqadmin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_commission_rate_by_sqadmin.commission_rate_by_SQ_admin(browsers[i]));
    }
    return pc;
}

function manage_reports_by_sqadmin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_reports_by_sqadmin.reports_by_SQ_admin(browsers[i]));
    }
    return pc;
}

function manage_discounts_by_sqadmin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_discounts_by_sqadmin.discounts_by_SQ_admin(browsers[i]));
    }
    return pc;
}

function manage_tax_override_by_sqadmin_test(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => tax_override_by_sqadmin.tax_override_by_SQ_admin(browsers[i]));
    }
    return pc;
}

function manage_zip_code_wise_tax(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => zipcode_wise_tax.manage_zip_code_wise_tax(browsers[i]));
    }
    return pc;
}

function quote_flow_with_billing(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => quote_with_billing.create_quote_flow_with_billing(browsers[i]));
    }
    return pc;
}

function manage_customer_view_quote(pc) {
    for (let i = 0; i < chromeBrowsers.length; i++) {
        pc = pc.then(() => quote_with_billing.manage_customer_view_quote(chromeBrowsers[i]));
    }
    return pc;
}

function edit_order_by_sales_person_test(pc) {
    for(let i = 0; i < browsers.length; i++){
        pc = pc.then(()=> edit_order_by_sales_person.edit_order_by_sales_person(browsers[i]))
    }
    return pc;
}

function screen_selection_by_sales_person(pc) {
    for(let i = 0; i < browsers.length; i++){
        pc = pc.then(()=> screen_selection_sales_person.screen_selection_by_sales_person(browsers[i]))
    }
    return pc;
}

function delete_screen(pc) {
    for(let i = 0; i < chromeBrowsers.length; i++){
        pc = pc.then(()=> deleteScreen.delete_screen_by_sales_person(chromeBrowsers[i]))
    }
    return pc;
}

function order_reports_by_sq_admin(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_order_reports_by_sq_admin.order_reports_by_sq_admin(browsers[i]));
    }
    return pc;
}

function quotes_reports_by_sq_admin(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_quotes_reports_by_sq_admin.quotes_reports_by_sq_admin(browsers[i]));
    }
    return pc;
}


function daily_intake_reports_by_sq_admin(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_daily_intake_reports_by_sq_admin.daily_intake_reports_by_sq_admin(browsers[i]));
    }
    return pc;
}

function order_system_reports_by_sq_admin(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_order_system_reports_by_sq_admin.order_system_reports_by_sq_admin(browsers[i]));
    }
    return pc;
}
function view_leads_mode_flow(pc) {
    for (let i = 0; i < chromeBrowsers.length; i++) {
        pc = pc.then(() => manage_leads_mode_flow.view_leads_mode_flow(chromeBrowsers[i]));
    }
    return pc;
}

function customer_email_reports_by_sq_admin(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_customer_report_reports_by_sq_admin.customer_email_reports_by_sq_admin(browsers[i]));
    }
    return pc;
}

function order_with_screen_option(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => manage_order_with_screen_option.create_order_with_screen_option(browsers[i]));
    }
    return pc;
}

function refreshQuote(pc) {
    for (let i = 0; i < chromeBrowsers.length; i++) {
        pc = pc.then(() => refresh_quote.refresh_quote(chromeBrowsers[i]));
    }
    return pc;
}

function orderPaginationFlow(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => pagination.viewOrderPaginationBySalesPerson(browsers[i]));
    }
    return pc;
}

function quotePaginationFlow(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => pagination.viewQuotePaginationBySalesPerson(browsers[i]));
    }
    return pc;
}

function searchInGrid(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => search_flow.search_flow(browsers[i]));
    }
    return pc;
}

function iframeGuest(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => iframe_guest.iframe_guest(browsers[i]));
    }
    return pc;
}

function paymentFlow(pc) {
    for (let i = 0; i < chromeBrowsers.length; i++) {
        pc = pc.then(() => payment_flow.payment_flow_steps(chromeBrowsers[i]));
    }
    return pc;
}

function executeAll(pc) {
   // pc = guestLogin(pc);
   // pc = quote_guestLogin(pc);
    //pc = guestDepthFlow(pc);
     pc = guestAbsoluteLaminateDoorFlow(pc);
    //
    // pc = order_with_screen_option(pc);
    // pc = quote_validation(pc);
    // pc = delete_item(pc);
    //
    // pc = quote_salesPersonViewOrder(pc);
    // pc = view_quote_by_salesPerson(pc);
    //
    // pc = salesPersonLogin(pc);
    // pc = quote_salesPersonLogin(pc);
    // pc = signature_door_quote_by_salesPersonLogin(pc);
    // pc = complete_signature_door_quote_by_salesPersonLogin(pc);
    // pc = vinyl_picture_window_quote_by_salesPerson(pc);
    // pc = absolute_door_laminate_quote_by_salesPerson(pc);
    // pc = aluminum_picture_window_quote_by_salesPerson(pc);
    // pc = door_for_joe_only_quote(pc);
    // pc = screen_selection_by_sales_person(pc);
    // pc = delete_screen(pc);
    // pc = depth_flow_accounting_Login(pc);
    // pc = verify_deposit_flow_accounting_Login(pc);
    //
    // pc = production_login_depth_flow_test(pc);
    //
    // pc = manage_user_by_sq_admin_login_test(pc);
    // pc = add_commission_by_sq_admin_login_test(pc);
    // pc = edit_commission_by_sq_admin_login_test(pc);
    // // Functionality removed
    // // pc = sales_flow_by_admin_test(pc);
    // pc = companies_flow_by_sq_admin_login_test(pc);
    // pc = manage_module_flow_by_sqadmin_test(pc);
    // pc = manage_has_balance_flow_by_superadmin_test(pc);
    // pc = manage_user_by_sq_admin_sales_person_login_test(pc);
    // // pc = sales_flow_by_admin_sales_person_test(pc);
    // pc = manage_panel_module_flow_by_sqadmin_test(pc);
    // pc = manage_fixed_formulas_flow_by_sqadmin_test(pc);
    // pc = manage_glass_formulas_flow_by_sqadmin_test(pc);
    // pc = manage_percentage_formulas_flow_by_sqadmin_test(pc);
    // pc = min_max_sizes_flow_by_sqadmin_test(pc);
    // pc = manage_frame_formulas_flow_by_sqadmin_test(pc);
    // pc = manage_commission_flow_by_sq_admin_test(pc);
    // pc = manage_commission_rate_by_sqadmin_test(pc);
    // pc = manage_commission_groups_flow_by_sq_admin_test(pc);
    // pc = manage_discounts_by_sqadmin_test(pc);
    // pc = manage_tax_override_by_sqadmin_test(pc);
    // pc = manage_zip_code_wise_tax(pc);
    // pc = quote_flow_with_billing(pc);
    // pc = manage_customer_view_quote(pc);
    // pc = edit_order_by_sales_person_test(pc);
    //
    // pc = manage_reports_by_sqadmin_test(pc);
    // pc = order_reports_by_sq_admin(pc);
    // pc = order_system_reports_by_sq_admin(pc);
    // pc = quotes_reports_by_sq_admin(pc);
    // pc = daily_intake_reports_by_sq_admin(pc);
    // pc = customer_email_reports_by_sq_admin(pc);
    //
    // pc = view_leads_mode_flow(pc);
    // pc = refreshQuote(pc);
    // pc = orderPaginationFlow(pc);
    // pc = quotePaginationFlow(pc);
    // // pc = searchInGrid(pc);
    //
    // pc = iframeGuest(pc);
    // pc = paymentFlow (pc);

    return pc;
}
