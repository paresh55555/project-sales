const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const quote_NewQuote= require('./FT/afterLogin/quote/newQoutes');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const cart = require('./FT/afterLogin/quote/cart');
const quote_order_customerInfo = require('./FT/afterLogin/order/customerInfo');
const viewQuote = require('./FT/afterLogin/quote/viewQuotePage');
const accounting = require('./FT/afterLogin/accountingActions/accountingActionFlow');
const moment = require('moment');
let _u = undefined;



function refresh_quote(capability) {
    let d = Config.getDriver(Config.ChromeCapabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Refresh Quote'));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => quote_NewQuote.newQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Absolute Door (Vinyl)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-right')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,500)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelDirectionSelection(d, wd,"swingInOrOut-Outswing")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Split")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_Beige_null_#F5F5DC_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_White_null_#fcffff_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => accounting.notesInput(d, wd, 'test notes')).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl',
        '100" width x 90" height','101" width x 91" height','3','31.35"','85.5"','Left','Outswing',
        'Left 2 / Right 0','Aluminum Block','3/4 Upstand','Glass Clear','Argon','New York Nickel','Door Restrictor',
    'Panoramic Screen Split','Item 1 of 1',
    'Total $6,990.16','$6,990.16']));

    pc = pc.then(() => cart.saveAsQuote(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.leadSourceSelect(d,2)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.firstNameInput(d, wd, 'fname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.lastNameInput(d, wd, 'lname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.emailInput(d, wd, 'test@gmail.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.phoneInput(d, wd, '8989898989')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingContactInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingEmailInputName(d, wd, 'test@email.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingPhoneInputName(d, wd, '8080808080')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingCityInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingStateInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingZipInputName(d, wd, '898989')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.useShippingCheckbox(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.attachToQuoteClick(d, wd)).then(() => cf.test(d, 'viewSavedQuoteConfirmation', Config.BASE_URL + '?tab=viewSavedQuoteConfirmation&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Quote Name: fname lname']));
    pc = pc.then(() => quote_order_customerInfo.quoteViewButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_order_customerInfo.revenueTypeSelect(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => quote_order_customerInfo.channelTypeSelect(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => accounting.quoteNotesInput(d, wd,'test quote notes')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // GET REVENUE AND CHANNEL
    let revenueData, channelData;
    pc = pc.then(() => quote_order_customerInfo.getRevenueTypeData(d, wd)).then((r) => revenueData = r );
    pc = pc.then(() => quote_order_customerInfo.getChannelTypeData(d, wd)).then((c) => channelData = c );

    // GET OPEN QUOTE NO
    let quoteNo
    pc = pc.then(() => accounting.getOpenOrderNo(d, wd)).then((o) => quoteNo = o);

    // CHECK ALL THE QUOTE DETAIL
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl',
        '100" width x 90" height','101" width x 91" height','3','31.35"','85.5"','Left','Outswing',
        'Left 2 / Right 0','Aluminum Block','3/4 Upstand','Glass Clear','Argon','New York Nickel','Door Restrictor',
        'Panoramic Screen Split','Item 1 of 1',
        'Total $6,990.16','$6,990.16']));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,990.16','fname lname','8989898989','test@gmail.com',
        'Client Referral','Alan Rees','Quote Expires on',
    moment().format('MM/DD/YYYY'), moment().add(180,'days').format('MMMM DD,YYYY'),
        '5,958.91','1,031.25','6,990.16','3,495.08']));
    pc = pc.then(() => cf.testContentByArray(d, ['Print Cut Sheet','Add Door','Print Quote','Email Quote',
        'Close Quote','Delete Quote','Refresh']));

    //  REFRESH QUOTE
    pc = pc.then(() => cf.testContentByArray(d, ['Print Cut Sheet','Add Door','Print Quote','Email Quote',
        'Close Quote','Delete Quote','Refresh']));

    pc = pc.then(() => viewQuote.refreshQuoteClick(d)).then(() => cf.testAlertText(d,'Are you sure you want to Refresh this quote?'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // CHECK ALL THE QUOTE DETAIL AFTER REFRESH QUOTE


    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl',
        '100" width x 90" height','101" width x 91" height','3','31.35"','85.5"','Left','Outswing',
        'Left 2 / Right 0','Aluminum Block','3/4 Upstand','Glass Clear','Argon','New York Nickel','Door Restrictor',
        'Panoramic Screen Split','Item 1 of 1',
        'Total $6,990.16','$6,990.16']));

    pc = pc.then(() => cf.testContentByArray(d, ['$6,990.16','fname lname','8989898989','test@gmail.com',
        'Client Referral','Alan Rees','Quote Expires on',
        moment().format('MM/DD/YYYY'), moment().add(180,'days').format('MMMM DD,YYYY'),
        '5,958.91','1,031.25','6,990.16','3,495.08']));

    pc = pc.then(() => cf.testContentByArray(d, ['Print Cut Sheet','Add Door','Print Quote','Email Quote',
        'Close Quote','Delete Quote','Refresh']));

    // // CHECK REVENUE AND CHANNEL DATA IN REFRESH QUOTE
    // pc = pc.then(() => quote_order_customerInfo.checkRevenueData(d, wd, revenueData).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u)));


    pc = cf.handleOutput(d, pc, capability, 'Refresh Quote by Sales Person');
    return pc;
}

module.exports = { refresh_quote };