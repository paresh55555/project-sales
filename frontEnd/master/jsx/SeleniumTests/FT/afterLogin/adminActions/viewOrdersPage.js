const cf = require('../../commonFunctions/cf');

const orderLinkClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("quoteRow")[0].firstElementChild.click()').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const adminLauncherClick = (d, wd) => {
    return d.findElement(wd.By.id("saleAdminLauncher")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const quoteActionsClick = (d, wd,index) => {
    return d.executeScript('window.scrollTo(0,0)').then(() =>d.executeScript("document.getElementsByClassName('quoteActionButtons')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.ten)));
}

const printQuoteClick = (d, wd) => {
    return d.executeScript('window.scrollTo(0,500)').then(() =>d.executeScript("document.getElementsByClassName('quoteActionButtons')[2].click()").then(() => cf.delayBySecond(cf.delaySecond.ten)));
}

module.exports = {orderLinkClick, adminLauncherClick,quoteActionsClick, printQuoteClick};