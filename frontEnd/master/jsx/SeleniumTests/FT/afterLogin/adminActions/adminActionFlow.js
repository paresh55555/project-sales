const cf = require('../../commonFunctions/cf');

const editQuoteClick = (d, wd) => {
    return d.executeScript('var elmnt = document.getElementsByClassName("quoteEditButton initButton pull-right")[0]; elmnt.scrollIntoView();')
        .then(() => d.executeScript('document.getElementsByClassName("quoteEditButton initButton pull-right")[0].click()')).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const addCostClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("quoteEditButton")[0].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const costNameInput = (d, wd, value) => {
    return d.executeScript("document.getElementById('costsList').getElementsByClassName('nameField editQuoteInput')[document.getElementById('costsList').getElementsByClassName('nameField editQuoteInput').length - 1].value = '" + value + "'");
}

const yesClick = (d, panel) => {
    return d.executeScript("document.getElementById('" + panel + "').getElementsByTagName('label')[document.getElementById('" + panel + "').getElementsByTagName('label').length - 2].click()");
}

const noClick = (d, panel) => {
    return d.executeScript("document.getElementById('" + panel + "').getElementsByTagName('label')[document.getElementById('" + panel + "').getElementsByTagName('label').length - 1].click()");
}

const costAmountInput = (d, wd, value) => {
    return d.executeScript("document.getElementById('costsList').getElementsByClassName('amountField editQuoteInput')[document.getElementById('costsList').getElementsByClassName('amountField editQuoteInput').length - 1].value = '" + value + "'");
}

const deleteCostClick = (d) => {
    return d.executeScript('document.getElementById(\'costsList\').getElementsByClassName(\'delete\')[document.getElementById(\'costsList\').getElementsByClassName(\'delete\').length - 1].click();')
        .then(() => cf.delayBySecond(cf.delaySecond.one));
}

const addDiscountClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("quoteEditButton")[1].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const deleteDiscountClick = (d, wd) => {
    return d.executeScript('document.getElementById(\'discountsList\').getElementsByClassName(\'delete\')[document.getElementById(\'discountsList\').getElementsByClassName(\'delete\').length - 1].click();')
        .then(() => cf.delayBySecond(cf.delaySecond.one));
}

const deleteAllDiscountClick = (d, wd) => {
    return d.executeScript(`var len = document.getElementById('discountsList').getElementsByClassName('delete').length;
    if(len > 0){
    for(var i = 0 ; i < len; i=i+1){ 
        document.getElementById('discountsList').getElementsByClassName('delete')[i].click()
         }
    }`)
        .then(() => cf.delayBySecond(cf.delaySecond.one));
}
const discountNameInput = (d, wd, value) => {
    return d.findElement(wd.By.id("nameDiscount-0")).clear().then(() => d.findElement(wd.By.id("nameDiscount-0")).sendKeys(value));
}

const discountAmountInput = (d, wd, value) => {
    return d.findElement(wd.By.id("discountAmount-0")).clear().then(() => d.findElement(wd.By.id("discountAmount-0")).sendKeys(value));
}

const shippingAmountInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shippingBox")).clear().then(() => d.findElement(wd.By.id("shippingBox")).sendKeys(value));
}

const installationAmountInput = (d, wd, value) => {
    return d.findElement(wd.By.id("installationBox")).clear().then(() => d.findElement(wd.By.id("installationBox")).sendKeys(value));
}

const nonTaxableNameInput = (d) => {
    return d.executeScript("document.getElementsByClassName('nonTaxPulldown')[document.getElementsByClassName('nonTaxPulldown').length -1].selectedIndex = 2");
}

const nonTaxableAmountInput = (d, wd, value) => {
    return d.executeScript("document.getElementById('nonTaxableList').getElementsByClassName('amountField editQuoteInput')[document.getElementById('nonTaxableList').getElementsByClassName('amountField editQuoteInput').length - 1].value = '" + value + "'");
}

const addExtraNonTaxableAmountClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("quoteEditButton")[2].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const deleteExtraNonTaxableAmountClick = (d, wd) => {
    return d.executeScript('document.getElementById(\'nonTaxableList\').getElementsByClassName(\'delete\')[document.getElementById(\'nonTaxableList\').getElementsByClassName(\'delete\').length - 1].click();')
        .then(() => cf.delayBySecond(cf.delaySecond.one));
}

const selectDiscountType = (d, wd, value) => {
    return d.findElement(wd.By.css('input[type="radio"][name="type-Discount0"][value="' + value + '"]')).click().then(() => cf.delayBySecond(cf.delaySecond.one));
}

const showOnQuoteSelect = (d, wd, name, value) => {
    return d.findElement(wd.By.css('input[type="radio"][name="' + name + '"][value="' + value + '"]')).click().then(() => cf.delayBySecond(cf.delaySecond.one));
}

const saveClick = (d) => {
    return d.executeScript("document.getElementsByClassName('editDone myButton selectedButton')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const shippingCityInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_city")).clear().then(() => d.findElement(wd.By.id("shipping_city")).sendKeys(value));
}

const shippingZipCodeInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_zip")).clear().then(() => d.findElement(wd.By.id("shipping_zip")).sendKeys(value));
}

const resaleNumberInput = (d, wd, value) => {
    return d.findElement(wd.By.id("resale")).clear().then(() => d.findElement(wd.By.id("resale")).sendKeys(value));
}

const quoteIdClick = (d, wd, value) => {
   return d.executeScript('document.getElementsByClassName("quoteRow")[0].firstElementChild.click()').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const quoteEditClick = (d, wd, value) => {
    return d.executeScript('window.scrollTo(0,1200)')
        .then(()=> {
            d.executeScript('document.getElementsByClassName("quoteEditButton")[0].click()')
        }).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const radioButtonSelection = (d, wd,row, value) => {
    return d.executeScript('var len = document.getElementById("'+row+'").getElementsByClassName("showOnQuote").length;document.getElementById("'+row+'").getElementsByClassName("showOnQuote")[len -1].getElementsByTagName("input")["'+value+'"].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const amount = (d, wd,row, value) => {
    return d.executeScript('var len = document.getElementById("'+row+'").getElementsByClassName("amount").length;document.getElementById("'+row+'").getElementsByClassName("amount")[len-1].getElementsByTagName("input")[0].value = "'+value+'"').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const addQuoteClick = (d, wd, value) => {
    return d.executeScript("document.getElementById('editContent').getElementsByTagName('button')["+value+"].click()").then(() => cf.delayBySecond(cf.delaySecond.five));
}

const nameInput = (d, wd, col, value) => {
    return d.executeScript('var len = document.getElementById("costsList").getElementsByClassName("row").length;document.getElementById("costsList").getElementsByClassName("row")[len-1].getElementsByTagName("input")["'+col+'"].value="'+value+'"').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const selectDiscount = (d, wd, value) => {
    return d.executeScript('var len = document.getElementById("discountsList").getElementsByTagName("select").length;document.getElementById("discountsList").getElementsByTagName("select")[len - 1].selectedIndex = 1').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const selectNonTaxable = (d, wd, value) => {
    return d.executeScript('var len = document.getElementById("nonTaxableList").getElementsByTagName("select").length;document.getElementById("nonTaxableList").getElementsByTagName("select")[len-1].selectedIndex = 1').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const resaleInput = (d, wd, value) => {
    return d.findElement(wd.By.id("resale")).clear().then(() => cf.delayBySecond(cf.delaySecond.two)).then(() => d.findElement(wd.By.id("resale")).sendKeys(value));
}

const taxableNoRadioClick = (d, wd, value) => {
    d.executeScript('window.scrollTo(0,2000)')
    return d.executeScript('document.getElementById("showOnQuote-nonTaxableTotal-no").click()').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const taxableYesRadioClick = (d, wd, value) => {
    return d.executeScript('document.getElementById("showOnQuote-nonTaxableTotal-yes").click()').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const amountInput = (d, wd, value) => {
    return d.executeScript("document.getElementById('nonTaxableList').getElementsByTagName('input')[0].value=100").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const deleteClick = (d, wd, row) => {
    return d.executeScript('var len = document.getElementById("'+row+'").getElementsByClassName("action").length;document.getElementById("'+row+'").getElementsByClassName("action")[len - 1].getElementsByTagName("span")[0].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const doneClick = (d, wd, value) => {
    return d.executeScript('document.getElementsByClassName("editDoneRow")[0].getElementsByTagName("button")[0].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const getTaxData = async (d, wd) => {
    let data = await d.executeScript(`return document.getElementById('tax_label').innerText`);
    return data
}

const scroll = (d, wd, value) => {
    return d.executeScript('window.scrollTo(0,'+value+')').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const discountSelect = (d, wd, value) => {
    return d.executeScript(`var len = document.getElementsByClassName('discountPulldown').length;
    document.getElementsByClassName('discountPulldown')[len - 1].lastElementChild.selected = true;
    document.getElementsByClassName('discountPulldown')[len - 1].onchange()
`).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const checkDiscountTypeData = async (d, wd, adminDiscountType) => {
    let promises = [];
    let data = await d.executeScript(`var len = document.getElementsByClassName('discountPulldown').length;
    return document.getElementsByClassName('discountPulldown')[len - 1].selectedOptions[0].innerText`);

    return Promise.all(promises).then(function () {
        let isTestPassed = true;
        let reasonsArray = [];
        if (data !== adminDiscountType) {
            let reason = 'expected text "' + adminDiscountType + '" is not getting matched with retrieved text "' + data + '"'
            isTestPassed = false;
            reasonsArray.push(reason);
        }
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}

module.exports = {
    addCostClick,
    costNameInput,
    costAmountInput,
    discountNameInput,
    discountAmountInput,
    addDiscountClick,
    shippingAmountInput,
    installationAmountInput,
    nonTaxableNameInput,
    nonTaxableAmountInput,
    addExtraNonTaxableAmountClick,
    deleteExtraNonTaxableAmountClick,
    selectDiscountType,
    saveClick,
    shippingCityInput,
    shippingZipCodeInput,
    resaleNumberInput,
    deleteDiscountClick,
    deleteCostClick,
    showOnQuoteSelect,
    editQuoteClick,
    yesClick,
    noClick,
    quoteIdClick,
    quoteEditClick,
    radioButtonSelection,
    addQuoteClick,
    nameInput,
    taxableNoRadioClick,
    taxableYesRadioClick,
    doneClick,
    deleteClick,
    selectDiscount,
    resaleInput,
    selectNonTaxable,
    amountInput,
    getTaxData,
    scroll,
    amount,
    discountSelect,
    checkDiscountTypeData,
    deleteAllDiscountClick
};