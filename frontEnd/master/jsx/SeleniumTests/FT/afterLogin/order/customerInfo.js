const cf = require('../../commonFunctions/cf');

const attachCustomerClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("changeCustomer myButton selectedButton")[0].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const revenueTypeSelect = (d) => {
    return d.executeScript(`var event = new Event('change');
    document.getElementById('revenueType').selectedIndex = 1;
    document.getElementById('revenueType').dispatchEvent(event, {})`).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const channelTypeSelect = (d) => {
    return d.executeScript(`var event = new Event('change');
    document.getElementById('channel').selectedIndex = 1;
    document.getElementById('channel').dispatchEvent(event, {})`).then(() => cf.delayBySecond(cf.delaySecond.two));
}


const searchPreviousCustomerClick = (d, wd) => {
    return d.findElement(wd.By.id("previousCustomer")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const searchPreviousCustomerInput = (d, wd, value) => {
    return d.findElement(wd.By.name("search")).clear().then(()=>d.findElement(wd.By.name("search")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
}

const closeAttachCustomer = (d, wd) => {
    return d.findElement(wd.By.id("close")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const attachSearchCustomer = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('attachToQuote myGreen')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const leadSourceSelect = (d, index) => {
    return d.executeScript(`document.getElementById("leadSource").selectedIndex = ${index};
    var event = new Event('change');
    document.getElementById('leadSource').dispatchEvent(event, {})
    `).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const comapanyNameInput = (d, wd, value) => {
    return d.findElement(wd.By.name("companyName")).clear().then(()=> d.findElement(wd.By.name("companyName")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));

}
const firstNameInput = (d, wd, value) => {
    return d.findElement(wd.By.name("firstName")).clear().then(()=> d.findElement(wd.By.name("firstName")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const lastNameInput = (d, wd, value) => {
    return d.findElement(wd.By.name("lastName")).clear().then(()=> d.findElement(wd.By.name("lastName")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const emailInput = (d, wd, value) => {
    return d.findElement(wd.By.name("email")).clear().then(()=> d.findElement(wd.By.name("email")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const phoneInput = (d, wd, value) => {
    return d.findElement(wd.By.name("phone")).clear().then(()=> d.findElement(wd.By.name("phone")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const billingNameInput = (d, wd, value) => {
    return d.findElement(wd.By.name("billingName")).clear().then(()=> d.findElement(wd.By.name("billingName")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const billingEmailInput = (d, wd, value) => {
    return d.findElement(wd.By.name("billingEmail")).clear().then(()=> d.findElement(wd.By.name("billingEmail")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const billingPhoneInput = (d, wd, value) => {
    return d.findElement(wd.By.name("billingPhone")).clear().then(()=> d.findElement(wd.By.name("billingPhone")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const billingAddress1Input = (d, wd, value) => {
    return d.findElement(wd.By.name("billingAddress1")).clear().then(()=> d.findElement(wd.By.name("billingAddress1")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const billingAddress2Input = (d, wd, value) => {
    return d.findElement(wd.By.name("billingAddress2")).clear().then(()=> d.findElement(wd.By.name("billingAddress2")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const billingCityInput = (d, wd, value) => {
    return d.findElement(wd.By.name("billingCity")).clear().then(()=> d.findElement(wd.By.name("billingCity")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const billingStateInput = (d, wd, value) => {
    return d.findElement(wd.By.name("billingState")).clear().then(()=> d.findElement(wd.By.name("billingState")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const billingStateSelect = (d, wd, value) => {
    return d.executeScript(`document.querySelector("select[name='billingState']").selectedIndex = ${value};
    document.querySelector("select[name='billingState']").onchange()`).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const billingZipInput = (d, wd, value) => {
    return d.findElement(wd.By.name("billingZip")).clear().then( d.findElement(wd.By.name("billingZip")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const saveCancelCustomerInfo = (d, wd, value) => {
    return d.executeScript(`document.getElementsByClassName('editButtons')[${value}].click()`).then(() => cf.delayBySecond(cf.delaySecond.five));
}

const attachToQuoteClick = (d, wd) => {
    return d.executeScript('window.scrollTo(0,window.outerHeight)').then(() => d.findElement(wd.By.className("nextTitle")).click().then(() => cf.delayBySecond(cf.delaySecond.ten)));
}

const editCustomerClick = (d, wd) => {
    return d.executeScript('window.scrollTo(0,0)').then(() => d.findElement(wd.By.className("editCustomer")).click().then(() => cf.delayBySecond(cf.delaySecond.ten)));
}

const editFirstNameInput = (d, wd, value) => {
    return d.findElement(wd.By.name("firstName")).clear().then( d.findElement(wd.By.name("firstName")).sendKeys(value));
}

const saveChangesClick = (d, wd) => {
    return d.executeScript('window.scrollTo(0,window.outerHeight)').then(() => d.executeScript("document.getElementsByClassName('editButtons')[1].click()").then(() => cf.delayBySecond(cf.delaySecond.five)));
}

const quoteViewButtonClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('quoteViewButton')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.five));
}

const dueDatevViewOrder = (d, wd,value) => {
    return d.executeScript("document.getElementsByClassName('viewOrderDateBox')[0].value='"+value+"'").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const useShippingAddress = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('customerSearchRow')[0].getElementsByTagName('input')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const searchAttach = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('attachToQuote')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const getRevenueTypeData = async (d,wd) => {
    let dt = await d.findElement(wd.By.id('revenueType')).getAttribute('value');
    return dt;
}

const getChannelTypeData = async (d, wd) => {
    let dt = await d.findElement(wd.By.id('channel')).getAttribute('value');
    return dt;
}

const checkRevenueData = async (d, wd, revenueData) => {
    let promises = [];
    let data = await d.findElement(wd.By.id('revenueType')).getAttribute('value');
    return Promise.all(promises).then(function () {
        let isTestPassed = true;
        let reasonsArray = [];
        if (data !== revenueData) {
            let reason = 'expected text "' + revenueData + '" is not getting matched with retrieved text "' + data + '"'
            isTestPassed = false;
            reasonsArray.push(reason);
        }
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}

const searchDataActionClick = (d, wd, index) => {
    return d.executeScript(`document.getElementsByClassName('customerEdit')[${index}].click()`).then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const inputValue = (d, wd,name, value) => {
    return d.findElement(wd.By.name(name)).clear().then(() => d.findElement(wd.By.name(name)).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const checkInputBoxData = async (d, wd, inputName, value) => {
    let promises = [];
    let inputBoxData = await d.executeScript(`return document.querySelector('input[name="${inputName}"]').value`);
    return Promise.all(promises).then(function () {
        let isTestPassed = true;
        let reasonsArray = [];
        if (inputBoxData !== value) {
            let reason = inputName +': expected value "' + value + '" is not getting matched with retrieved value "' + inputBoxData + '"'
            isTestPassed = false;
            reasonsArray.push(reason);
        }
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}

module.exports = {
    attachCustomerClick,
    firstNameInput,
    lastNameInput,
    emailInput,
    phoneInput,
    billingNameInput,
    billingCityInput,
    billingAddress1Input,
    billingAddress2Input,
    billingEmailInput,
    billingZipInput,
    billingStateInput,
    billingStateSelect,
    saveCancelCustomerInfo,
    searchPreviousCustomerClick,
    searchPreviousCustomerInput,
    closeAttachCustomer,
    attachSearchCustomer,
    leadSourceSelect,
    attachToQuoteClick,
    editCustomerClick,
    editFirstNameInput,
    saveChangesClick,
    revenueTypeSelect,
    channelTypeSelect,
    quoteViewButtonClick,
    dueDatevViewOrder,
    useShippingAddress,
    searchAttach,
    comapanyNameInput,
    billingPhoneInput,
    getRevenueTypeData,
    getChannelTypeData,
    checkRevenueData,
    searchDataActionClick,
    inputValue,
    checkInputBoxData
};