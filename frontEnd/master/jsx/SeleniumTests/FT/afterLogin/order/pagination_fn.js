const cf = require('../../commonFunctions/cf');

const nextButtonClick = async (d, wd) => {
    return d.executeScript(`document.getElementsByClassName('next')[0].click()`).then(() => cf.delayBySecond(cf.delaySecond.five));
}
const prevButtonClick = async (d, wd) => {
    return d.executeScript(`document.getElementsByClassName('prev')[0].click()`).then(() => cf.delayBySecond(cf.delaySecond.five));
}

const itemPerPageSelect = (d,wd,index) => {
    return d.executeScript(`document.getElementById("myLimit").selectedIndex = ${index}`)
        .then(() => d.executeScript(`document.getElementById("myLimit").onchange()`)
            .then(() => cf.delayBySecond(cf.delaySecond.ten)));
};

const getPerPageItems = async (d, wd) => {
    let noOfItems = await d.executeScript(`return document.getElementById('myLimit').value`);
    return noOfItems;
}

const getTotalPages = async (d, wd) => {
    let totalPages = await d.executeScript(`var len = document.getElementById('pagination').getElementsByTagName('li').length;
    return document.getElementById('pagination').getElementsByTagName('li')[len-2].getElementsByTagName('div')[0].innerText`);
    return totalPages;
}

const checkPerPageRecords = async (d, wd,itemsPerPage) => {
    let totalRecords = await d.executeScript(` return document.getElementsByClassName('orderRow ').length`);
    let promises = [];
    if(totalRecords <= itemsPerPage){
        promises.push(true);
    }else{
        promises.push(false);
    }
    return Promise.all(promises).then(function (values) {
        let isTestPassed = true;
        let reasonsArray = [];
        if (values[0] === false) {
            isTestPassed = false;
            reasonsArray.push(`Total records are greater than actual no of items`);
        }
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}

const checkTotalPages = async (d, wd,itemsPerPage,totalPage) => {
    let retrievedPages = await d.executeScript(`return document.getElementById('pagination').getElementsByTagName('li').length-2`);
    let expected = Math.ceil(totalPage/(itemsPerPage/10));
    let promises = [];
    if(retrievedPages === expected){
        promises.push(true);
    }else{
        promises.push(false);
    }
    return Promise.all(promises).then(function (values) {
        let isTestPassed = true;
        let reasonsArray = [];
        if (values[0] === false) {
            isTestPassed = false;
            reasonsArray.push(`expected pages ${expected} is not matched with retrieved total pages ${retrievedPages}`);
        }
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}

const checkTotalPagesInSearch = async (d, wd,expected) => {
    let retrievedPages = await d.executeScript(`return document.getElementById('pagination').getElementsByTagName('li').length-2`);
    let promises = [];
    if(retrievedPages === expected){
        promises.push(true);
    }else{
        promises.push(false);
    }
    return Promise.all(promises).then(function (values) {
        let isTestPassed = true;
        let reasonsArray = [];
        if (values[0] === false) {
            isTestPassed = false;
            reasonsArray.push(`expected pages ${expected} is not matched with retrieved total pages ${retrievedPages}`);
        }
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}

module.exports = {
    nextButtonClick,
    prevButtonClick,
    itemPerPageSelect,
    getPerPageItems,
    getTotalPages,
    checkPerPageRecords,
    checkTotalPages,
    checkTotalPagesInSearch
};