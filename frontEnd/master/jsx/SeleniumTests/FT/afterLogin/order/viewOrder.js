const cf = require('../../commonFunctions/cf');

const viewOrderClick = (d, wd) => {
    return d.findElement(wd.By.id("viewOrders")).click().then(() => cf.delayBySecond(cf.delaySecond.twenty));
}

const searchOrderInput = (d, wd, value) => {
    return d.findElement(wd.By.id("search")).clear().then(() => d.findElement(wd.By.id("search")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.fifty)));
}

const sortOrderByIdClick = (d, wd) => {
    return d.findElement(wd.By.id("id")).click().then(() => cf.delayBySecond(cf.delaySecond.five));
}

const orderIdClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("orderRow")[0].firstElementChild.click()').then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const itemPerPageSelect = (d) => {
    return d.executeScript('document.getElementById("myLimit").selectedIndex = 2').then(()=>d.executeScript('document.getElementById("myLimit").onchange()')).then(() => cf.delayBySecond(cf.delaySecond.twenty));
}

const viewOrdersNeedsPaymentClick = (d, wd) => {
    return d.findElement(wd.By.id("needsPayment")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const viewOrdersHoldClick = (d, wd) => {
    return d.findElement(wd.By.id("holdOrder")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const viewOrdersAccountingClick = (d, wd) => {
    return d.findElement(wd.By.id("accounting")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const viewOrdersPreProductionClick = (d, wd) => {
    return d.findElement(wd.By.id("preProduction")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const viewOrdersProductionClick = (d, wd) => {
    return d.findElement(wd.By.id("production")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const viewOrdersCompletedClick = (d, wd) => {
    return d.findElement(wd.By.id("completed")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const viewOrdersDeliveredClick = (d, wd) => {
    return d.findElement(wd.By.id("delivered")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const viewPreProductionClick = (d, wd) => {
    return d.executeScript("document.getElementById('preMainContent').children[1].click()").then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const viewProductionClick = (d, wd) => {
    return d.executeScript("document.getElementById('preMainContent').children[2].click()").then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const viewCompletedClick = (d, wd) => {
    return d.executeScript("document.getElementById('preMainContent').children[3].click()").then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const viewDeliveredClick = (d, wd) => {
    return d.executeScript("document.getElementById('preMainContent').children[4].click()").then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const viewHoldClick = (d, wd) => {
    return d.executeScript("document.getElementById('preMainContent').children[5].click()").then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const viewAllProductionClick = (d, wd) => {
    return d.executeScript("document.getElementById('preMainContent').children[0].click()").then(() => cf.delayBySecond(cf.delaySecond.twenty));
}

const productionMenuClick = (d, wd) => {
    return d.findElement(wd.By.id("headerRightMenu")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const productionMenuItemClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('menuText')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.thirty));
}

const orderNumberClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("accountRow")[0].firstElementChild.click()').then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const secondOrderNumberClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("accountRow")[1].firstElementChild.click()').then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const secondNumberStatusChanged = (d, wd,index, value) => {
    return d.executeScript(`document.getElementsByClassName('accountRow')[${index}].getElementsByTagName('select')[0].selectedIndex = ${value};
    document.getElementsByClassName('accountRow')[${index}].getElementsByTagName('select')[0].onchange()`).then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const sendToProduction = (d, wd) => {
    return d.findElement(wd.By.id("sendToProduction")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const productionStatusOrderClick = (d, wd) => {
    return d.findElement(wd.By.id("linkedQuote")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const sendToProductionClick = (d, wd) => {
    return d.findElement(wd.By.id("sendToProduction")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const getFirstRowOrderNo = async (d, wd, rowClass) => {
    let data = await d.executeScript(`return document.getElementsByClassName('${rowClass}')[0].firstElementChild.innerText`);
    return data;
}

const checkedSearchedData =  async(d, wd,rowClass, expectedData) => {
    let data = await d.executeScript(`return document.getElementsByClassName('${rowClass}')[0].firstElementChild.innerText`);

    let promises = [];
    if (data !== expectedData) {
        let reason = 'expected data "' + expectedData + '" is not getting matched with retrived searched "' + data + '"'
        promises.push({isTestPassed: false, reason});
    }
    return Promise.all(promises).then(function (values) {
        let isTestPassed = true;
        let reasonsArray = [];
        values.forEach(function (e, i) {
            if (!e.isTestPassed) {
                isTestPassed = false;
                reasonsArray.push(e.reason);
            }
        });
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}

const deleteAllPayment = (d, wd) => {
    return d.executeScript(`var len = document.getElementsByClassName('paymentBoxRight')[0].getElementsByClassName('deletePayment').length;
    for(var i = 0;i < len; i++){
    document.getElementsByClassName('paymentBoxRight')[0].getElementsByClassName('deletePayment')[0].click()
    }
`).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const testPaymentDate = async (d, wd, expectedDate) => {
    let innerData = await d.executeScript(`var len = document.getElementsByClassName('paymentBoxRight')[0].getElementsByClassName('paymentsGroupBox').length;
    return document.getElementsByClassName('paymentBoxRight')[0].getElementsByClassName('paymentsGroupBox')[len - 1].firstElementChild.children[1].innerText
`);
    let promises = [];
    if(innerData === expectedDate){
        promises.push(true);
    }else{
        promises.push(false);
    }
    return Promise.all(promises).then(function (values) {
        let isTestPassed = true;
        let reasonsArray = [];
        if (values[0] === false) {
            isTestPassed = false;
            reasonsArray.push(`expected payment date ${expectedDate} is not matched with retrieved payment date ${innerData}`);
        }
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
};

testBalanceDue = async (d, wd, value)  => {
    let promises = [];
    let balance = await d.executeScript(`return document.getElementsByClassName('paymentsAmountDue')[2].getElementsByTagName('span')[0].innerText`);
    if(balance === value){
        promises.push(true);
    }else{
        promises.push(false);
    }
    return  Promise.all(promises).then(function (values) {
        let isTestPassed = true;
        let reasonsArray = [];
        if(values[0] === false){
            isTestPassed = false;
            reasonsArray.push(value + " Balance due not matched");
        }
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}

const getGrandTotal = async (d, wd) => {
    let total= await d.executeScript(`return document.getElementById('grandTotalAmount').innerText;`);

    return total;
};

const testGrandTotal$ = async (d, wd)  => {
    let promises = [];
    let retrievedTotal = await getGrandTotal(d,wd);

    if(retrievedTotal.indexOf('$')===-1){
        promises.push(true);
    }else{
        promises.push(false);
    }
    return  Promise.all(promises).then(function (values) {
        let isTestPassed = true;
        let reasonsArray = [];
        if(values[0] === false){
            isTestPassed = false;
            reasonsArray.push(`In Payment block Grand Total ${retrievedTotal} contains $`);
        }
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}

module.exports = {
    viewOrderClick,
    searchOrderInput,
    sortOrderByIdClick,
    orderIdClick,
    secondOrderNumberClick,
    itemPerPageSelect,
    viewOrdersNeedsPaymentClick,
    viewOrdersHoldClick,
    viewOrdersAccountingClick,
    viewOrdersPreProductionClick,
    viewOrdersProductionClick,
    viewOrdersCompletedClick,
    viewOrdersDeliveredClick,
    viewPreProductionClick,
    viewProductionClick,
    viewCompletedClick,
    viewDeliveredClick,
    viewHoldClick,
    viewAllProductionClick,
    productionMenuClick,
    productionMenuItemClick,
    orderNumberClick,
    sendToProduction,
    productionStatusOrderClick,
    sendToProductionClick,
    getFirstRowOrderNo,
    checkedSearchedData,
    secondNumberStatusChanged,
    deleteAllPayment,
    testPaymentDate,
    testBalanceDue,
    getGrandTotal,
    testGrandTotal$

};