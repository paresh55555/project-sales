const cf = require('../../commonFunctions/cf');

const menuClick = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.twenty));
};

const itemsPerPageSelect = (d,wd, index) => {
    return d.executeScript('document.getElementById("myLimit").selectedIndex = '+index).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const nextButtonClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("next")[0].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
};

const prevButtonClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("prev")[0].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
};

const transferToClick = (d, wd, index) => {
    return d.executeScript("document.getElementById('leadSalesPersons').getElementsByTagName('select')[0].selectedIndex ="+index).then(()=>d.executeScript("document.getElementById('leadSalesPersons').getElementsByTagName('select')[0].onchange()")).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const transferClick = (d, wd, index) => {
    return d.executeScript("document.getElementById('transferTo').children[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const checkBoxClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('leadsLastColumn')[1].getElementsByTagName('input')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const searchInput = (d, wd, value) => {
    return d.findElement(wd.By.id('search')).clear().then(() => d.findElement(wd.By.id('search')).sendKeys(value)).then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const orderIdClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("leadQuoteCell")[1].click()').then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const viewClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('assignedleadIcons')[0].getElementsByTagName('img')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const deleteClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('assignedleadIcons')[0].getElementsByTagName('img')[1].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const revokeClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('assignedleadIcons')[0].getElementsByTagName('img')[2].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const getFirstRowQuoteNo = async(d, wd) => {
    return await d.executeScript("return document.getElementsByClassName('leadRow')[0].children[1].innerText");
};

const getReferenceQuoteText = async (d, wd) => {
    let quoteText = await d.findElement(wd.By.id('referenceQuote')).getText();
    let quoteID = quoteText.substring(20);
    return quoteID;
}

const startOverClick = (d, wd, id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};


module.exports = {
    menuClick,
    itemsPerPageSelect,
    nextButtonClick,
    prevButtonClick,
    transferToClick,
    transferClick,
    checkBoxClick,
    searchInput,
    orderIdClick,
    viewClick,
    deleteClick,
    revokeClick,
    getFirstRowQuoteNo,
    getReferenceQuoteText,
    startOverClick,

};