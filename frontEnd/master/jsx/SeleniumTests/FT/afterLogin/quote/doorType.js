const cf = require('../../commonFunctions/cf');


const doorTypeClick = (d, wd, door) => {
    return d.executeScript(`document.querySelector('img[title="${door}"]').click()`).then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const vinylDoorTypeClick = (d, wd) => {
    return d.findElement(wd.By.id("Absolute Door (Vinyl)-39")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const absoluteDoorVinylTypeClick = (d, wd) => {
    return d.findElement(wd.By.id("Absolute Door (Vinyl)-39")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const signatureDoorTypeClick = (d, wd) => {
    return d.executeScript("document.getElementById('Signature Door (Aluminum)-41').click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const absoluteLaminateDoorTypeClick = (d, wd) => {
    return d.findElement(wd.By.id("Absolute + Door (Laminate)-40")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    doorTypeClick,
    vinylDoorTypeClick, signatureDoorTypeClick, absoluteLaminateDoorTypeClick, absoluteDoorVinylTypeClick};