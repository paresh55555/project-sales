const cf = require('../../commonFunctions/cf');

const vinylPictureWindowTypeClick = (d, wd) => {
    return d.findElement(wd.By.id("Vinyl Picture Window-26")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const aluminumPictureWindowTypeClick = (d, wd) => {
    return d.findElement(wd.By.id("Aluminum Picture Window-25")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}
const screenSelection = (d, wd) => {
    return d.findElement(wd.By.id("Screens-38")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

module.exports = {vinylPictureWindowTypeClick, aluminumPictureWindowTypeClick,screenSelection};