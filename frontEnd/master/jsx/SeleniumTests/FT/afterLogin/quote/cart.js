const cf = require('../../commonFunctions/cf');

const quantityInput = (d, wd, value) => {
    return d.findElement(wd.By.className("cartQuantity")).clear().then(() => d.findElement(wd.By.className("cartQuantity")).sendKeys(value));
}

const addQuoteClick = (d, wd) => {
    return d.executeScript('window.scrollTo(0,0)').then(() =>d.findElement(wd.By.id("home")).click().then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const printQuoteClick = (d, wd,id) => {
    return d.findElement(wd.By.id("printGuestQuote")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const duplicateClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("cartIcons")[2].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const emailQuoteClick = (d, wd) => {
    return d.executeScript('window.scrollTo(0,0)').then(() =>d.findElement(wd.By.id("emailGuestQuote")).click().then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const cancelEmailQuoteClick = (d, wd) => {
    return d.findElement(wd.By.id("-emailCancel")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const cancelEmail = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('emailButtons')[1].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const printGuestQuote = (d, wd) => {
    return d.findElement(wd.By.id("printGuestQuote")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const emailAddressesInput = (d, wd, value) => {
    return d.findElement(wd.By.id("emailAddresses")).clear().then(() => d.findElement(wd.By.id("emailAddresses")).sendKeys(value));
}

const emailMessageInput = (d, wd, value) => {
    return d.findElement(wd.By.id("emailMessage")).clear().then(() => d.findElement(wd.By.id("emailMessage")).sendKeys(value));
}

const sendEmailClick = (d, wd) => {
    return d.findElement(wd.By.id("-emailSend")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}
const guestEditButtonClick = (d, wd) => {
    return d.executeScript('window.scrollTo(0,200)').then(() =>d.executeScript('document.getElementsByClassName("guestEdit")[0].click()').then(() => cf.delayBySecond(cf.delaySecond.five)));
}

const saveAsQuote = (d, wd, value) => {
    return d.findElement(wd.By.id("saveAsQuote")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const closeWithoutSaving = (d, wd) => {
    return d.findElement(wd.By.id("viewQuotes-sales")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const addDoorClick = (d, wd, id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const addDoorOptionClick = (d, wd, id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const cartClick = (d, wd, id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deleteAddedDoor = (d, wd) => {
    return d.executeScript("var len = document.getElementsByClassName('cartRow').length;document.getElementsByClassName('cartRow')[len - 1].getElementsByClassName('cartIcons')[3].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const startOverClick = (d, wd, id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const PdfTab = (d) => {
    return d.getAllWindowHandles().then(function (handles) {
        d.switchTo().window(handles[1]);
        d.close();
    });
}

const SalesPersonMenuClick = (d, wd) => {
    return d.findElement(wd.By.id("headerRightMenu")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const menuItemClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('menuText')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const getReferenceQuoteText = async (d, wd, index) => {
    let quoteText = await d.findElement(wd.By.id('referenceQuote')).getText();
    let quoteID = quoteText.substring(20);
    return quoteID;
}

const getReferenceQuoteFullText = async (d, wd, index) => {
    let quoteText = await d.findElement(wd.By.id('referenceQuote')).getText();
    let quoteID = quoteText.substring(17);
    return quoteID;
}

const greenAddDoor = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('myGreen')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

module.exports = {
    quantityInput,
    printQuoteClick,
    duplicateClick,
    emailQuoteClick,
    cancelEmailQuoteClick,
    cancelEmail,
    sendEmailClick,
    printGuestQuote,
    emailAddressesInput,
    emailMessageInput,
    guestEditButtonClick,
    saveAsQuote,
    closeWithoutSaving,
    addDoorOptionClick,
    addQuoteClick,
    addDoorClick,
    cartClick,
    deleteAddedDoor,
    startOverClick,
    PdfTab,
    SalesPersonMenuClick,
    menuItemClick,
    getReferenceQuoteText,
    greenAddDoor,
    getReferenceQuoteFullText
};