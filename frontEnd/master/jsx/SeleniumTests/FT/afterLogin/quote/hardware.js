const cf = require('../../commonFunctions/cf');

const hardWareSelect = (d, wd, value) => {
    return d.findElement(wd.By.id(value)).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const hardWareOptionSelect = (d, wd, value) => {
    return d.findElement((wd.By.id(value))).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const addToQuoteClick = (d, wd) => {
    return d.findElement(wd.By.id("nextHardwareTitle")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

module.exports = {hardWareSelect,hardWareOptionSelect, addToQuoteClick};

