const cf = require('../../commonFunctions/cf');

const glassTypeSelect = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.five));
}

const eecoreglassTypeSelect = (d, wd) => {
        return d.findElement(wd.By.id("glass-Low-E 3")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const glassOptionSelect = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.five));
}

const nextButtonClick = (d, wd) => {
    return d.executeScript('(document.getElementsByClassName("nextButton"))[0].click()').then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const previousButtonClick = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

module.exports = {glassTypeSelect, nextButtonClick, eecoreglassTypeSelect,
    previousButtonClick, glassOptionSelect};