const cf = require('../../commonFunctions/cf');
const moment = require('moment');

const searchInput = (d, wd, value) => {
    return d.findElement(wd.By.id('search')).clear().then(() => d.findElement(wd.By.id('search')).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.ten)))
}

const followupInput = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).clear().then(() => d.findElement(wd.By.id(id)).sendKeys(moment().add(1,'days').format('MM/DD/YYYY')).then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const itemsPerPageSelect = (d) => {
    return d.executeScript('document.getElementById("myLimit").selectedIndex = 2').then(()=>d.executeScript('document.getElementById("myLimit").onchange()')).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const sortItems = (d, wd, value) => {
    return d.findElement(wd.By.id(value)).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const newButtonClick = (d, wd) => {
    return d.executeScript("document.getElementById('new').click()").then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const hotButtonClick = (d, wd) => {
    return d.findElement(wd.By.id("hot")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const mediumButtonClick = (d, wd) => {
    return d.findElement(wd.By.id("medium")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const coldButtonClick = (d, wd) => {
    return d.findElement(wd.By.id("cold")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const holdButtonClick = (d, wd) => {
    return d.findElement(wd.By.id("hold")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const archivedButtonClick = (d, wd) => {
    return d.findElement(wd.By.id("archived")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const viewAllButtonClick = (d, wd) => {
    return d.findElement(wd.By.id("viewAll")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const qualitySelect = (d) => {
    return d.executeScript('document.getElementsByClassName("leadStatusPullDown")[0].selectedIndex = 2').then(()=>d.executeScript('document.getElementsByClassName("leadStatusPullDown")[0].onchange()')).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const contactedClick = (d, wd, id, value) => {
    return d.executeScript(`document.getElementById('${id}').value='${value}';document.getElementById('followUp-0').onchange()`).then(()=>d.executeScript('document.getElementsByClassName("leadStatusPullDown")[0].onchange()')).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const quoteIdClick = (d) => {
    return d.executeScript('document.getElementsByClassName("quoteRow")[0].firstElementChild.click()').then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const convertToSale = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const refreshQuoteClick = (d) => {
    return d.executeScript(`document.querySelectorAll('div[title="Refresh Quote"]')[0].click()`).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const viewQuoteTabClick = (d, wd) => {
    return d.findElement(wd.By.id('viewQuotes')).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const getTotalPrice = (d, wd) =>{
    return d.executeScript(`return document.getElementById('footerPrice').innerText.substr(6)`);
}

const dateInputById = (d, wd, id) => {
    return d.executeScript(`document.getElementById('${id}').value= "${moment().format('MM/DD/YYYY')}";
    document.getElementById('${id}').onchange()`).then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const checkDateValues =  async(d, wd , id, expectedDate) => {
    let dateValue = await d.executeScript(`return document.getElementById('${id}').value`);
    let promises = [];
    if (dateValue !== expectedDate) {
        let reason = `${id}: expected date "' + expectedDate + '" is not getting matched with retrived date "' + dateValue + '"`
        promises.push({isTestPassed: false, reason});
    }
    return Promise.all(promises).then(function (values) {
        let isTestPassed = true;
        let reasonsArray = [];
        values.forEach(function (e, i) {
            if (!e.isTestPassed) {
                isTestPassed = false;
                reasonsArray.push(e.reason);
            }
        });
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}

module.exports = { newButtonClick,followupInput, searchInput, sortItems, itemsPerPageSelect, hotButtonClick, mediumButtonClick, coldButtonClick, holdButtonClick, archivedButtonClick, viewAllButtonClick, qualitySelect, quoteIdClick,
    convertToSale,
    refreshQuoteClick,
    contactedClick,
    viewQuoteTabClick,
    getTotalPrice,
    dateInputById,
    checkDateValues
};