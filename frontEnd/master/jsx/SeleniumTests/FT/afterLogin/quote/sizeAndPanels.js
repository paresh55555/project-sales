const cf = require('../../commonFunctions/cf');

const widthInput = (d, wd, value) => {
    return d.findElement(wd.By.id("width")).clear().then(() => d.findElement(wd.By.id("width")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
}
const heightInput = (d, wd, value) => {
    return d.findElement(wd.By.id("height")).clear().then(() => d.findElement(wd.By.id("height")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.ten)));
}

const clearInput = (d, wd, id) => {
    return d.findElement(wd.By.id(id)).clear().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const panelSelection = (d, wd, value) => {
    return d.executeScript("document.getElementById('" + value +"').click()").then(() => cf.delayBySecond(cf.delaySecond.ten));
}
const swingDirectionSelection = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.five));
}
const swingDirectionRightSelection = (d, wd) => {
    return d.findElement(wd.By.id("swingDirection-right")).click().then(() => cf.delayBySecond(cf.delaySecond.five));
}
const nextButtonClick = (d, wd) => {
    return d.executeScript('(document.getElementsByClassName("nextButton"))[0].click()').then(() => cf.delayBySecond(cf.delaySecond.ten));
}
const trackTypeSelection = (d, wd, id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.five));
    // return d.executeScript("document.getElementById('chooseTrackSection').getElementsByClassName('extendedHalfButton')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.five));
}
const panelMovementSelection = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.five));
}
const panelDirectionSelection = (d, wd,id) => {
    // return d.findElement(wd.By.id("swingDirection-left")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.five));
}
const screenSelection = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const chooseTrackType = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('extendedHalfButtonImage')[1].click()").then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const scrollBottom = (d, wd,value) => {
    return d.executeScript("window.scrollTo(0,"+value+")").then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const checkNoOfFrameType = async(d, wd, expectedNum) => {
    let retrivedNum = await d.executeScript(`return document.querySelectorAll('div[id="frame-Aluminum Block"]').length`);
    let promises = [];
    if(expectedNum !== retrivedNum){
        let reason = 'expected frame length "' + expectedNum + '" is not getting matched with retrieved frame length "' + retrivedNum + '"'
        promises.push({isTestPassed: false, reason});
    }
    return Promise.all(promises).then(function (values) {
        let isTestPassed = true;
        let reasonsArray = [];
        values.forEach(function (e, i) {
            if (!e.isTestPassed) {
                isTestPassed = false;
                reasonsArray.push(e.reason);
            }
        });
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}

const checkNoOfTrackType = async(d, wd, expectedNum, id) => {
    let retrivedNum = await d.executeScript(`return document.querySelectorAll('div[id="${id}"]').length`);
    let promises = [];
    if(expectedNum !== retrivedNum){
        let reason = 'expected track type length "' + expectedNum + '" is not getting matched with retrieved track length "' + retrivedNum + '"'
        promises.push({isTestPassed: false, reason});
    }
    return Promise.all(promises).then(function (values) {
        let isTestPassed = true;
        let reasonsArray = [];
        values.forEach(function (e, i) {
            if (!e.isTestPassed) {
                isTestPassed = false;
                reasonsArray.push(e.reason);
            }
        });
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}
module.exports = {
    widthInput,
    heightInput,
    clearInput,
    panelSelection,
    swingDirectionSelection,
    swingDirectionRightSelection,
    nextButtonClick,
    trackTypeSelection,
    panelMovementSelection,
    panelDirectionSelection,
    screenSelection,
    chooseTrackType,
    scrollBottom,
    checkNoOfFrameType,
    checkNoOfTrackType
};