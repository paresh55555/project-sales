const cf = require('../../commonFunctions/cf');

const tabClick = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const doorColorClick = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const exteriorLaminateClick = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const interiorLaminateClick = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const signatureDoorColorClick = (d, wd, value) => {
    return d.findElement(wd.By.id(value)).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const signatureRalDoorColorClick = (d, wd, value) => {
    return d.findElement(wd.By.id(value)).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const signatureRalLilacDoorColorClick = (d, wd, value) => {
    return d.findElement(wd.By.id(value)).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const interiorDoorColorClick = (d, wd) => {
    return  d.executeScript('window.scrollTo(0,1500)').then(() => d.findElement(wd.By.id("no")).click().then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const interiorCustomDoorColorClick = (d, wd) => {
    return  d.executeScript('window.scrollTo(0,2000)').then(() => d.findElement(wd.By.id("intColor-custom")).click().then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const interiorCustomDoorColorInput = (d, wd, value) => {
    return  d.executeScript('window.scrollTo(0,window.outerHeight)').then(() => d.findElement(wd.By.id("intColor-customColor")).sendKeys(value));
}

const extriorCustomDoorColorInput = (d, wd, value) => {
    return  d.executeScript('window.scrollTo(0,window.outerHeight)').then(() => d.findElement(wd.By.id("extColor-customColor")).sendKeys(value));
}

const interiorWoodStandardDoorColorClick  = (d, wd) => {
    return  d.executeScript('window.scrollTo(0,window.outerHeight)').then(() => d.findElement(wd.By.id("intColor_basic_all_White_null_#FFFFFF_null")).click().then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const interiorWoodStandardDoorColorSelect = (d, wd, value) => {
    return  d.executeScript('window.scrollTo(0,window.outerHeight)').then(() => d.findElement(wd.By.id(value)).click().then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const premiumDoorColorClick = (d, wd) => {
    return d.findElement(wd.By.id("extColor-premium")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const premiumSubDoorColorClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('optionsItemImage')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const nextButtonClick = (d, wd) => {
    return d.executeScript('(document.getElementsByClassName("nextButton"))[0].click()').then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const interiorButtonClick = (d, wd, id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const interiorColorClick = (d, wd, id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.five));
}

const colorGroupButtonClick = (d, wd, id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const scroll = (d, wd,value) => {
    return d.executeScript('window.scrollTo(0,'+value+')').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const colorClick = (d, wd, index) => {
    return d.executeScript(`document.getElementById('vinylOptions').children[${index}].click()`).then(() => cf.delayBySecond(cf.delaySecond.two));
}

module.exports = {exteriorLaminateClick,extriorCustomDoorColorInput,interiorLaminateClick,
    tabClick,doorColorClick, nextButtonClick, signatureDoorColorClick, signatureRalDoorColorClick, signatureRalLilacDoorColorClick, interiorDoorColorClick, interiorCustomDoorColorClick, interiorCustomDoorColorInput, premiumDoorColorClick, premiumSubDoorColorClick, interiorWoodStandardDoorColorClick, interiorWoodStandardDoorColorSelect,
interiorButtonClick,interiorColorClick,colorGroupButtonClick,scroll, colorClick};