const cf = require('../../commonFunctions/cf');

const newQuoteClick = (d, wd) => {
    return d.findElement(wd.By.id("newQuote")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
}

module.exports = {newQuoteClick};