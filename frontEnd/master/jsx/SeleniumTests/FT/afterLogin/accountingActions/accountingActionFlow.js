var webElement = require('selenium-webdriver').WebElement;
const moment = require('moment');
const cf = require('../../commonFunctions/cf');

const accountingAllClick = (d, wd) => {
    return d.findElement(wd.By.id('accountingAll')).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const verifyDepositClick = (d, wd) => {
    return d.findElement(wd.By.id('verifyDeposit')).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const accountingPreProductionClick = (d, wd) => {
    return d.findElement(wd.By.id('accountingPreProduction')).click().then(() => cf.delayBySecond(cf.delaySecond.five));
};

const accountingProductionClick = (d, wd) => {
    return d.findElement(wd.By.id('accountingProduction')).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const accountingCompletedClick = (d, wd) => {
    return d.findElement(wd.By.id('accountingCompleted')).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const balanceDueClick = (d, wd) => {
    return d.findElement(wd.By.id('balanceDue')).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const accountingDeliveredClick = (d, wd) => {
    return d.findElement(wd.By.id('accountingDelivered')).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const accountingHoldClick = (d, wd) => {
    return d.findElement(wd.By.id('accountingHold')).click().then(() => cf.delayBySecond(cf.delaySecond.twenty));
};

const searchInput = (d, wd, value) => {
    return d.findElement(wd.By.id('search')).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.twenty));
};

const itemsPerPageSelect = (d,wd,index) => {
        return d.executeScript('document.getElementById("myLimit").selectedIndex ='+ index).then(() => d.executeScript('document.getElementById("myLimit").onchange()').then(() =>cf.delayBySecond(cf.delaySecond.two)));
};

const paginationButtonClick = (d,wd,name) => {
    return d.executeScript("document.getElementsByClassName('"+name+"')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const depositSatisfiedSelect = (d, wd, index) => {
    return d.executeScript('document.getElementsByClassName("ordersPullDown")[0].selectedIndex = ' + index).then(() => d.executeScript('document.getElementsByClassName("ordersPullDown")[0].onchange()').then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const depositSatisfiedClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("accountAction")[0].click()').then(() => cf.delayBySecond(cf.delaySecond.twenty));
};

const paidFullSelect = (d, wd, index) => {
    return d.executeScript('document.getElementsByClassName("ordersPullDown")[0].selectedIndex = ' + index).then(() => d.executeScript('document.getElementsByClassName("ordersPullDown")[0].onchange()').then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const paidFullClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("accountAction")[0].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
};

const revertDepositSatisfied = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("accountAction")[0].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
};

const orderIdClick = (d) => {
    return d.executeScript('document.getElementsByClassName("accountRow")[0].firstElementChild.click()').then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const previousOrderIDClick = (d, wd, searchText) => {
    return d.executeScript(`var aTags = document.getElementsByClassName("quoteCell1");
    var searchText = ${searchText};
    var found;
    for (var i = 0; i < aTags.length; i++) {
        if (aTags[i].textContent === searchText) {
            found = aTags[i];
            break;
        };
    };
found.click()`).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const changeProductionStatus = (d, wd, index) => {
    return d.executeScript(`document.getElementsByClassName("accountRow")[0].getElementsByTagName('select')[0].selectedIndex = ${index};`)
        .then(()=>d.executeScript(`document.getElementsByClassName("accountRow")[0].getElementsByTagName('select')[0].onchange()`))
        .then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const paymentInfoClick = (d, wd) => {
    let script = `if(document.getElementsByClassName('paymentsGroupBox').length < 6) 
    {document.getElementById('enterPaymentInfo').click();}
    else{ document.getElementsByClassName('paymentsLeft deletePayment')[0].click();
    document.getElementById('enterPaymentInfo').click();}`;
    return d.executeScript('window.scrollTo(0,0)').then(() => d.executeScript(script).then(() => cf.delayBySecond(cf.delaySecond.ten)));
};

const paymentOptionClick = (d,wd,index) => {
    return d.executeScript('document.getElementsByClassName("paymentOptionsBox")['+index+'].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
};

const last4DigitsOfCreditCardInput = (d, wd, value) => {
    return d.findElement(wd.By.id('lastFour')).clear().then(() => d.findElement(wd.By.id('lastFour')).sendKeys(value));
};

const paymentAmountInput = (d, wd, value) => {
    return d.findElement(wd.By.id('paymentAmount')).clear().then(() => d.findElement(wd.By.id('paymentAmount')).sendKeys(value));
};

const getBalanceDue =  async(d, wd) => {
    let balanceDue = await d.executeScript("var len = document.getElementsByClassName('paymentsAmountDue').length;return document.getElementsByClassName('paymentsAmountDue')[len-1].innerText");
    return balanceDue.replace('$','').replace(',','')
}

const addPaymentClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('addPaymentButton myGreen')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.five));
};

const deletePaymentClick = (d, wd) => {
    return d.executeScript("if(document.getElementsByClassName('paymentsLeft deletePayment').length > 0){document.getElementsByClassName('paymentsLeft deletePayment')[0].click()}").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const paymentOrderNotesInput = (d, wd, value) => {
    return d.findElement(wd.By.id('paymentOrderNotes')).clear().then(() => d.findElement(wd.By.id('paymentOrderNotes')).sendKeys(value));
};

const closePaymentInfo = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('customerViewButtons')[0].getElementsByTagName('div')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.five));
};

const paymentDateInput = (d, wd) => {
    // let selectedDate = new Date();
    // selectedDate.setMonth(selectedDate.getMonth() + 1);
    // return d.findElement(wd.By.id('paymentDate')).sendKeys(moment(selectedDate).format('MM/DD/YYY')).then(() => cf.delayBySecond(cf.delaySecond.two));
    return d.executeScript(`document.getElementById('paymentDate').value='${moment().format('MM/DD/YYYY')}'`)
        .then(() => d.executeScript(`var event = new Event('change');document.getElementById('paymentDate').dispatchEvent(event, {})`))
        .then(() => cf.delayBySecond(cf.delaySecond.five));

};

const paymentDateSelect = (d, wd) => {
    return d.executeScript("document.getElementById('ui-datepicker-div').getElementsByTagName('a')[4].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const PDAccountsClick = (d, wd) => d.findElement(wd.By.id('headerRightMenu')).click().then(() => cf.delayBySecond(cf.delaySecond.two));

const allSelect = (d) => {
    return d.executeScript("document.getElementsByClassName('menuRows')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const verifyDepositSelect = (d) => {
    return d.executeScript("document.getElementsByClassName('menuRows')[1].click()").then(() => cf.delayBySecond(cf.delaySecond.five));
};

const preProductionSelect = (d) => {
    return d.executeScript("document.getElementsByClassName('menuRows')[2].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const productionSelect = (d) => {
    return d.executeScript("document.getElementsByClassName('menuRows')[3].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const completedSelect = (d) => {
    return d.executeScript("document.getElementsByClassName('menuRows')[4].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const verifyFinalPaymentSelect = (d) => {
    return d.executeScript("document.getElementsByClassName('menuRows')[5].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deliveredSelect = (d) => {
    return d.executeScript("document.getElementsByClassName('menuRows')[6].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const holdSelect = (d) => {
    return d.executeScript("document.getElementsByClassName('menuRows')[7].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const signOutSelect = (d) => {
    return d.executeScript("document.getElementsByClassName('menuRowBottom')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.five));
};

const onHoldSelection = (d, wd, index) => {
    return d.executeScript("document.getElementById('onHold').selectedIndex = " + index).then(()=>d.executeScript("document.getElementById('onHold').onchange()")).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const dateDueInput = async (d,wd,className) => {
    let dateInput = moment().add(1,'days').format('MM/DD/YYYY').toString();
    webElement = d.findElement(wd.By.className('viewOrderDateBox hasDatepicker'));
    let id = await webElement.getAttribute('id');
    return d.findElement(wd.By.id(id.toString())).click().then(() => cf.delayBySecond(cf.delaySecond.two)).then(() =>d.executeScript("document.getElementsByClassName('"+className+"')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const dateDueInputChange = async (d,wd) => {
    let dateInput = moment().format('MM/DD/YYYY').toString();
    // webElement = d.findElement(wd.By.className('viewOrderDateBox hasDatepicker'));
    // let id = await webElement.getAttribute('id');
    return d.executeScript(`document.getElementsByClassName('viewOrderDateBox hasDatepicker')[0].value ='${dateInput}'`).then(()=>d.executeScript(`document.getElementsByClassName('viewOrderDateBox hasDatepicker')[0].onchange()`)).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const quoteNotesInput = (d, wd, value) => {
    return d.findElement(wd.By.id("quoteNotes")).clear().then(() =>d.findElement(wd.By.id("quoteNotes")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const quoteNotesInputName = (d, wd, value) => {
    return d.findElement(wd.By.className("quoteNotes")).clear().then(() =>d.findElement(wd.By.className("quoteNotes")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const poInput = (d, wd, value) => {
    return d.findElement(wd.By.id("po")).clear().then(() =>d.findElement(wd.By.id("po")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const commentInput = (d, wd, value) => {
    return d.findElement(wd.By.name("productionNotes")).clear().then(() =>d.findElement(wd.By.name("productionNotes")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const productionNotesInput = (d, wd, value) => {
    return d.findElement(wd.By.className("productionNotes")).clear().then(() =>d.findElement(wd.By.className("productionNotes")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const notesInput = (d, wd, value) => {
    return d.findElement(wd.By.className("quoteNotes")).clear().then(() =>d.findElement(wd.By.className("quoteNotes")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const pickupDeliverySelection = (d, wd, index) => {
    return d.executeScript("window.scrollTo(0,600)").then(() => d.executeScript("document.getElementById('pickupDelivery').selectedIndex = " + index).then(()=>d.executeScript("document.getElementById('pickupDelivery').onchange()")).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const productionDetailNotesInput = (d, wd, value) => {
    return d.findElement(wd.By.id("productionDetailNotes88")).clear().then(() =>d.findElement(wd.By.id("productionDetailNotes88")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const followUpNotesInput = (d, wd, value) => {
    return d.findElement(wd.By.className("orderDetailNotes")).clear().then(() =>d.findElement(wd.By.className("orderDetailNotes")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingContactInput = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_contact")).clear().then(() =>d.findElement(wd.By.id("billing_contact")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingAddress1Input = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_address1")).clear().then(() =>d.findElement(wd.By.id("billing_address1")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingAddress2Input = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_address2")).clear().then(() =>d.findElement(wd.By.id("billing_address2")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingCityInput = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_city")).clear().then(() =>d.findElement(wd.By.id("billing_city")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingStateInput = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_state")).clear().then(() =>d.findElement(wd.By.id("billing_state")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingZipInput = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_zip")).clear().then(() =>d.findElement(wd.By.id("billing_zip")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingPhoneInput = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_phone")).clear().then(() =>d.findElement(wd.By.id("billing_phone")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingEmailInput = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_email")).clear().then(() =>d.findElement(wd.By.id("billing_email")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingContactInputName = (d, wd, value) => {
    return d.findElement(wd.By.name("billing_contact")).clear().then(() =>d.findElement(wd.By.name("billing_contact")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingAddress1InputName = (d, wd, value) => {
    return d.findElement(wd.By.name("billing_address1")).clear().then(() =>d.findElement(wd.By.name("billing_address1")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingAddress2InputName = (d, wd, value) => {
    return d.findElement(wd.By.name("billing_address2")).clear().then(() =>d.findElement(wd.By.name("billing_address2")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingCityInputName = (d, wd, value) => {
    return d.findElement(wd.By.name("billing_city")).clear().then(() =>d.findElement(wd.By.name("billing_city")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingStateInputName = (d, wd, value) => {
    return d.findElement(wd.By.name("billing_state")).clear().then(() =>d.findElement(wd.By.name("billing_state")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingZipInputName = (d, wd, value) => {
    return d.findElement(wd.By.name("billing_zip")).clear().then(() =>d.findElement(wd.By.name("billing_zip")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingPhoneInputName = (d, wd, value) => {
    return d.findElement(wd.By.name("billing_phone")).clear().then(() =>d.findElement(wd.By.name("billing_phone")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingEmailInputName = (d, wd, value) => {
    return d.findElement(wd.By.name("billing_email")).clear().then(() =>d.findElement(wd.By.name("billing_email")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const shippingContactInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_contact")).clear().then(() =>d.findElement(wd.By.id("shipping_contact")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const shippingAddress1Input = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_address1")).clear().then(() =>d.findElement(wd.By.id("shipping_address1")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const shippingAddress2Input = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_address2")).clear().then(() =>d.findElement(wd.By.id("shipping_address2")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const shippingCityInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_city")).clear().then(() =>d.findElement(wd.By.id("shipping_city")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const shippingStateInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_state")).clear().then(() =>d.findElement(wd.By.id("shipping_state")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const shippingZipInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_zip")).clear().then(() =>d.findElement(wd.By.id("shipping_zip")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const shippingPhoneInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_phone")).clear().then(() =>d.findElement(wd.By.id("shipping_phone")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const shippingEmailInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_email")).clear().then(() =>d.findElement(wd.By.id("shipping_email")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const useShippingCheckbox = (d, wd) => {
    return d.executeScript(`document.getElementsByName('useShippingAddress')[0].click()`).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const scrollDown = (d, wd, value) => {
    return d.executeScript('window.scrollTo(0,' + value + ')').then(() => cf.delayBySecond(cf.delaySecond.two));
};

const quantityInput = (d, wd, value) => {
    return d.findElement(wd.By.className("cartQuantity")).clear().then(() => d.findElement(wd.By.className("cartQuantity")).sendKeys(value));
};

const cartItemClick = (d, wd, value) => {
    return d.executeScript("document.getElementsByClassName('cartIcons')[" + value + "].click()").then(() => cf.delayBySecond(cf.delaySecond.five));
};

const nextButtonClick = (d, wd) => {
    return d.executeScript('(document.getElementsByClassName("nextButton"))[0].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
};

const editQuoteClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('quoteEditButton')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const scroll = (d, wd, value) => {
    return d.executeScript("window.scrollTo(0,"+value+")").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const saveToOrderClick = (d, wd) => {
    return d.findElement(wd.By.css("div[title='Save To Order']")).click().then(() => cf.delayBySecond(cf.delaySecond.five));
};

const saveToQuoteClick = (d, wd) => {
    return d.findElement(wd.By.css("div[title='Save To Quote']")).click().then(() => cf.delayBySecond(cf.delaySecond.five));
};

const chooseActionClick = (d, wd,index) => {
    return d.executeScript("document.getElementsByClassName('quoteActionButtons')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.twenty));

};

const generateProductionReport = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.five));
};

const closeOrder = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('quoteActionButtons')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const exportClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('accountCell11 borderLeftRightTop export')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const selectDiscountReason = (d, wd,id,value) => {
    return d.executeScript("document.getElementById('"+id+"').selectedIndex = "+value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const discountMessage = (d, wd, value) => {
    return d.findElement(wd.By.id("newRequestText")).clear().then(() =>d.findElement(wd.By.id("newRequestText")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const makeRequestButtonClick = (d, wd) => {
    return d.findElement(wd.By.id("makeRequest")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deleteRequest = (d, wd) => {
    return d.executeScript("var len = document.getElementsByClassName('deleteRequest').length;document.getElementsByClassName('deleteRequest')[len -1].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const lastFollowUp = (d, wd, value) => {
    return d.findElement(wd.By.id("followUp-0")).clear().then(() =>d.findElement(wd.By.id("followUp-0")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const nextFollowUp = (d, wd, value) => {
    return d.findElement(wd.By.id("secondFollowUp-0")).clear().then(() =>d.findElement(wd.By.id("secondFollowUp-0")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const paymentBoxClick = (d,wd) => {
    return d.executeScript("document.getElementsByClassName('verified')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const paymentBox1Click = (d,wd) => {
    return d.executeScript(`var length = document.getElementsByClassName('ukPaymentSpecial borderLeftTop ')[1].getElementsByClassName('accountCellUKPayment borderLeftTop').length;
    for (i=length-1;i>=0;i--) {
	if(document.getElementsByClassName('ukPaymentSpecial borderLeftTop ')[1].getElementsByClassName('accountCellUKPayment borderLeftTop')[i].children.length) {
		document.getElementsByClassName('ukPaymentSpecial borderLeftTop ')[1].getElementsByClassName('accountCellUKPayment borderLeftTop')[i].getElementsByClassName('accountMultipleRows verifiedPaymentLink')[0].click();
		break;
    }
}`).then(() => cf.delayBySecond(cf.delaySecond.two));
};


const getOpenOrderNo =  async(d, wd) => {
    let orderText = await d.findElement(wd.By.id('workingOnTitle')).getText();
    // let orderText = document.getElementById('workingOnTitle').innerText;
    let orderID = orderText.substring(orderText.lastIndexOf(' ')+1,orderText.length);
    return orderID;
}

const checkGrandTotalAndFooterPrice =  async(d, wd) => {
    let grandTotal = await d.findElement(wd.By.id('grandTotalAmount')).getText();
    let footerPrice = await d.findElement(wd.By.id('footerPrice')).getText();
    let newFooterPrice = footerPrice.substr(7);
    let promises = [];
    if (grandTotal !== newFooterPrice) {
        let reason = 'expected Grand Total "' + grandTotal + '" is not getting matched with footer price "' + newFooterPrice + '"'
        promises.push({isTestPassed: false, reason});
    }
    return Promise.all(promises).then(function (values) {
        let isTestPassed = true;
        let reasonsArray = [];
        values.forEach(function (e, i) {
            if (!e.isTestPassed) {
                isTestPassed = false;
                reasonsArray.push(e.reason);
            }
        });
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}

const checkEnterPaymentInfoLink = async(d,wd) => {
    let booData = await d.executeScript(`var a;
    if(document.getElementById('enterPaymentInfo')){a=true}
    else{a=false};
    return a;`);
    return booData;
};

const editAddedDoor = (d, wd) => {
    return d.executeScript("var len = document.getElementsByClassName('cartIcons').length;document.getElementsByClassName('cartIcons')[len-3].click()").then(() => cf.delayBySecond(cf.delaySecond.twenty));
};

const testEmptyOrderNotMovedToProduction =  async(d, wd, text) => {
    let isSendToProduction = await d.executeScript(`if(document.getElementsByClassName('quoteDetailsColumn')[2].children[2])
     {

         return document.getElementsByClassName('quoteDetailsColumn')[2].children[2].innerText
     }
     else{
     return undefined;
     }`);

    let promises = [];
    if (isSendToProduction !== text) {
        let reason = `Send To production button is present on empty order`;
        promises.push({isTestPassed: false, reason});
    }
    return Promise.all(promises).then(function (values) {
        let isTestPassed = true;
        let reasonsArray = [];
        values.forEach(function (e, i) {
            if (!e.isTestPassed) {
                isTestPassed = false;
                reasonsArray.push(e.reason);
            }
        });
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}

module.exports = {
    commentInput,
    poInput,
    accountingAllClick,
    verifyDepositClick,
    accountingPreProductionClick,
    accountingProductionClick,
    accountingCompletedClick,
    balanceDueClick,
    accountingDeliveredClick,
    accountingHoldClick,
    searchInput,
    itemsPerPageSelect,
    depositSatisfiedSelect,
    paidFullSelect,
    paidFullClick,
    revertDepositSatisfied,
    orderIdClick,
    PDAccountsClick,
    allSelect,
    verifyDepositSelect,
    preProductionSelect,
    productionSelect,
    completedSelect,
    verifyFinalPaymentSelect,
    deliveredSelect,
    holdSelect,
    signOutSelect,
    paymentInfoClick,
    last4DigitsOfCreditCardInput,
    paymentAmountInput,
    getBalanceDue,
    paymentDateInput,
    paymentOrderNotesInput,
    paymentOptionClick,
    addPaymentClick,
    deletePaymentClick,
    onHoldSelection,
    quoteNotesInput,
    quoteNotesInputName,
    productionNotesInput,
    notesInput,
    pickupDeliverySelection,
    productionDetailNotesInput,
    billingContactInput,
    billingAddress1Input,
    billingAddress2Input,
    billingCityInput,
    billingStateInput,
    billingZipInput,
    billingPhoneInput,
    billingEmailInput,
    billingContactInputName,
    billingAddress1InputName,
    billingAddress2InputName,
    billingCityInputName,
    billingStateInputName,
    billingZipInputName,
    billingPhoneInputName,
    billingEmailInputName,
    shippingContactInput,
    shippingAddress1Input,
    shippingAddress2Input,
    shippingCityInput,
    shippingStateInput,
    shippingZipInput,
    shippingPhoneInput,
    shippingEmailInput,
    useShippingCheckbox,
    paymentDateSelect,
    scrollDown,
    quantityInput,
    cartItemClick,
    nextButtonClick,
    editQuoteClick,
    closePaymentInfo,
    followUpNotesInput,
    scroll,
    saveToOrderClick,
    chooseActionClick,
    generateProductionReport,
    saveToQuoteClick,
    closeOrder,
    paginationButtonClick,
    depositSatisfiedClick,
    exportClick,
    selectDiscountReason,
    discountMessage,
    makeRequestButtonClick,
    deleteRequest,
    lastFollowUp,
    nextFollowUp,
    dateDueInput,
    paymentBoxClick,
    paymentBox1Click,
    getOpenOrderNo,
    previousOrderIDClick,
    checkGrandTotalAndFooterPrice,
    dateDueInputChange,
    changeProductionStatus,
    checkEnterPaymentInfoLink,
    editAddedDoor,
    testEmptyOrderNotMovedToProduction
};