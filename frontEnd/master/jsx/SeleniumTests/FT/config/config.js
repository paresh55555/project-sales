const webdriver = require('selenium-webdriver');
const user = 'frackafuture2';
const secret = 'hBpqeZsuy5HW2Yosw4pU';
const BASE_URL = 'https://test-us.rioft.com/';
const SERVER_URL = 'http://hub.browserstack.com/wd/hub';
const isSuccess = [];
const failedList = [];
let failedFT = '';

const Capabilities = {
    "mac_mojave_safari_1920_1080": {
        'browserName': 'Safari',
        'browser_version': '12.0',
        'os': 'OS X',
        'os_version': 'Mojave',
        'resolution': '1920x1080',
        'browserstack.user': user,
        'browserstack.key': secret
    },
};

const ChromeCapabilities = {
    "mac_mojave_chrome_1920_1080": {
        'browserName': 'Chrome',
        'browser_version': '75.0',
        'os': 'OS X',
        'os_version': 'Mojave',
        'resolution': '1920x1080',
        'chromeOptions': { 'args': ['start-fullscreen'] },
        'browserstack.user': user,
        'browserstack.key': secret
    },
};


function getDriver(Capability) {
    return new webdriver.Builder().usingServer(SERVER_URL).withCapabilities(Capability).build();
};

module.exports = {user, secret, Capabilities, BASE_URL, SERVER_URL, getDriver, isSuccess, failedFT, failedList,ChromeCapabilities};
