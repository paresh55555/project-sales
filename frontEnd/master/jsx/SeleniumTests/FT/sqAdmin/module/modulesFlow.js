const cf = require('../../commonFunctions/cf');

const selectModuleClick = (d, index) => {
    return d.executeScript("document.getElementsByClassName('panel-content')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const selectFrameRandomClick = (d) => {
    return d.executeScript("var frame = (Math.floor(Math.random() *  document.getElementsByClassName('panel widget').length)); document.getElementsByClassName('panel widget')[frame].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    selectModuleClick,
    selectFrameRandomClick
};