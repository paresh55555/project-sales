const cf = require('../../commonFunctions/cf');

const vinylwithAluminumBlockFrameClick = (d) => {
    return d.executeScript("document.getElementsByClassName('panel-content')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const minMaxSizeClick = (d) => {
    return d.executeScript("document.getElementsByClassName('panel widget')[5].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const optionNameInput = (d, wd, value, inputName, index) => {
    return d.findElement(wd.By.css("input[placeholder='" + inputName + "']")).sendKeys('').then(() => cf.delayBySecond(cf.delaySecond.two));
};

const saveMinMaxSizeClick = (d) => {
    return d.executeScript("document.getElementsByClassName('btn btn-sm btn-primary')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const cancelMinMaxSizeClick = (d) => {
    return d.executeScript("document.getElementsByClassName('mh btn btn-sm btn-default')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    vinylwithAluminumBlockFrameClick,
    minMaxSizeClick,
    optionNameInput,
    saveMinMaxSizeClick,
    cancelMinMaxSizeClick
};