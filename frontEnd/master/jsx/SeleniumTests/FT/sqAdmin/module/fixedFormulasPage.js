const cf = require('../../commonFunctions/cf');

const vinylwithAluminumBlockFrameClick = (d) => {
    return d.executeScript("document.getElementsByClassName('panel-content')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const fixedFormulasClick = (d) => {
    return d.executeScript("document.getElementsByClassName('panel widget')[2].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const addFixedFormulasClick = (d) => {
    return d.executeScript("document.getElementsByClassName('mb mv btn btn-labeled btn-info mr pull-right')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const optionNameInput = (d, wd, value, inputName) => {
    return d.findElement(wd.By.css("input[placeholder='" + inputName + "']")).clear().then(() => d.findElement(wd.By.css("input[placeholder='" + inputName + "']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const saveFixedFormulasClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('btn btn-sm btn-primary')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const glassFormulasSearchInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const fixedFormulasRecordClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByTagName('tr')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deleteFixedFormulasClick = (d, wd) => {
    return d.findElement(wd.By.className("pull-right btn btn-sm btn-danger")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const confirmFixedFormulasClick = (d, wd) => {
    return d.findElement(wd.By.className("confirm")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const changeOrderClick = (d, index) => {
    return d.executeScript("document.getElementsByTagName('th')["+ index +"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const searchInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    vinylwithAluminumBlockFrameClick,
    fixedFormulasClick,
    addFixedFormulasClick,
    optionNameInput,
    saveFixedFormulasClick,
    glassFormulasSearchInput,
    fixedFormulasRecordClick,
    deleteFixedFormulasClick,
    confirmFixedFormulasClick,
    changeOrderClick,
    searchInput
};