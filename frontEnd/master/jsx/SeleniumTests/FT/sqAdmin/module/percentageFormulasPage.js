const cf = require('../../commonFunctions/cf');

const vinylwithAluminumBlockFrameClick = (d) => {
    return d.executeScript("document.getElementsByClassName('panel-content')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const percentageFormulasClick = (d) => {
    return d.executeScript("document.getElementsByClassName('panel widget')[4].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const addPercentageFormulasClick = (d) => {
    return d.executeScript("document.getElementsByClassName('mb mv btn btn-labeled btn-info mr pull-right')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const optionNameInput = (d, wd, value, inputName) => {
    return d.findElement(wd.By.css("input[placeholder='" + inputName + "']")).clear().then(() => d.findElement(wd.By.css("input[placeholder='" + inputName + "']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const savePercentageFormulasClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-sm btn-primary")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const percentageFormulasSearchInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const percentageFormulasRecordClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByTagName('tr')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deletePercentageFormulasClick = (d, wd) => {
    return d.findElement(wd.By.className("pull-right btn btn-sm btn-danger")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const confirmPercentageFormulasClick = (d, wd) => {
    return d.findElement(wd.By.className("confirm")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const changeOrderClick = (d, index) => {
    return d.executeScript("document.getElementsByTagName('th')["+ index +"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    vinylwithAluminumBlockFrameClick,
    percentageFormulasClick,
    addPercentageFormulasClick,
    optionNameInput,
    savePercentageFormulasClick,
    percentageFormulasSearchInput,
    percentageFormulasRecordClick,
    deletePercentageFormulasClick,
    confirmPercentageFormulasClick,
    changeOrderClick
};