const cf = require('../../commonFunctions/cf');

const vinylwithAluminumBlockFrameClick = (d) => {
    return d.executeScript("document.getElementsByClassName('panel-content')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const panelRangesClick = (d) => {
    return d.executeScript("document.getElementsByClassName('panel widget')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const addPanelRangesClick = (d) => {
    return d.executeScript("document.getElementsByClassName('btn-labeled')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const panelInput = (d, wd) => {
    return d.findElement(wd.By.css("input[placeholder='panels']")).sendKeys(parseInt(Math.random()*1000)).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const lowInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[placeholder='low']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const highInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[placeholder='high']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const savePanelRangesClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-sm btn-primary")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const panelRangesSearchInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const panelRangesRecordClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByTagName('tr')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deletePanelRangesClick = (d, wd) => {
    return d.findElement(wd.By.className("pull-right btn btn-sm btn-danger")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const confirmPanelRangesClick = (d, wd) => {
    return d.findElement(wd.By.className("confirm")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const changeOrderClick = (d, index) => {
    return d.executeScript("document.getElementsByTagName('th')["+ index +"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    vinylwithAluminumBlockFrameClick,
    panelRangesClick,
    addPanelRangesClick,
    panelInput,
    lowInput,
    highInput,
    savePanelRangesClick,
    panelRangesSearchInput,
    panelRangesRecordClick,
    deletePanelRangesClick,
    confirmPanelRangesClick,
    changeOrderClick
};