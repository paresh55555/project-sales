const cf = require('../../commonFunctions/cf');

const vinylwithAluminumBlockFrameClick = (d) => {
    return d.executeScript("document.getElementsByClassName('panel-content')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const frameFormulasClick = (d) => {
    return d.executeScript("document.getElementsByClassName('panel widget')[3].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const addFrameFormulasClick = (d) => {
    return d.executeScript("document.getElementsByClassName('mb mv btn btn-labeled btn-info mr pull-right')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const optionNameInput = (d, wd, value, inputName) => {
    return d.findElement(wd.By.css("input[placeholder='" + inputName + "']")).clear().then(()=> d.findElement(wd.By.css("input[placeholder='" + inputName + "']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const setInputByIndex = (d, wd, value, index) => {
    return d.executeScript("document.getElementsByClassName('form-control form-control')[" + index + "].value = ''").then(d.executeScript("document.getElementsByClassName('form-control form-control')[" + index + "].value = '" + value + "'").then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const saveFrameFormulasClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-sm btn-primary")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const frameFormulasSearchInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const frameFormulasRecordClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByTagName('tr')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deleteFrameFormulasClick = (d, wd) => {
    return d.findElement(wd.By.className("pull-right btn btn-sm btn-danger")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const confirmFrameFormulasClick = (d, wd) => {
    return d.findElement(wd.By.className("confirm")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const changeOrderClick = (d, index) => {
    return d.executeScript("document.getElementsByTagName('th')["+ index +"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    vinylwithAluminumBlockFrameClick,
    frameFormulasClick,
    addFrameFormulasClick,
    optionNameInput,
    saveFrameFormulasClick,
    frameFormulasSearchInput,
    frameFormulasRecordClick,
    deleteFrameFormulasClick,
    confirmFrameFormulasClick,
    changeOrderClick,
    setInputByIndex
};