const cf = require('../../commonFunctions/cf');

const vinylwithAluminumBlockFrameClick = (d) => {
    return d.executeScript("document.getElementsByClassName('panel-content')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const glassFormulasClick = (d) => {
    return d.executeScript("document.getElementsByClassName('panel widget')[1].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const addGlassFormulasClick = (d) => {
    return d.executeScript("document.getElementsByClassName('mb mv btn btn-labeled btn-info mr pull-right')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const optionNameInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[placeholder='name of option']")).clear().then(d.findElement(wd.By.css("input[placeholder='name of option']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const costPerPanelInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[placeholder='cost per panel']")).clear().then(d.findElement(wd.By.css("input[placeholder='cost per panel']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const costSquareMeterInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[placeholder='cost square meter']")).clear().then(d.findElement(wd.By.css("input[placeholder='cost square meter']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const heightCutSizeFormulaInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[placeholder='height cut size formula']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const widthCutSizeFormulaInput= (d, wd, value) => {
    return d.findElement(wd.By.css("input[placeholder='width cut size formula']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const descriptionInput= (d, wd, value) => {
    return d.findElement(wd.By.css("input[placeholder='description']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const saveGlassFormulasClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-sm btn-primary")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const glassFormulasSearchInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const glassFormulasRecordClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByTagName('tr')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deleteGlassFormulasClick = (d, wd) => {
    return d.findElement(wd.By.className("pull-right btn btn-sm btn-danger")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const confirmGlassFormulasClick = (d, wd) => {
    return d.findElement(wd.By.className("confirm")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const changeOrderClick = (d, index) => {
    return d.executeScript("document.getElementsByTagName('th')["+ index +"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    vinylwithAluminumBlockFrameClick,
    glassFormulasClick,
    addGlassFormulasClick,
    optionNameInput,
    costPerPanelInput,
    costSquareMeterInput,
    heightCutSizeFormulaInput,
    widthCutSizeFormulaInput,
    descriptionInput,
    saveGlassFormulasClick,
    glassFormulasSearchInput,
    glassFormulasRecordClick,
    deleteGlassFormulasClick,
    confirmGlassFormulasClick,
    changeOrderClick
};