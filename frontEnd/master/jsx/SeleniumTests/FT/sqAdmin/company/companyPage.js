const cf = require('../../commonFunctions/cf');

const addCompanyClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-labeled btn-info")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const companyNameInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='Company Name']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='Company Name']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const firstNameInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='First Name']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='First Name']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const lastNameInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='Last Name']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='Last Name']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const phoneInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='Phone']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='Phone']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const address1Input = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='Address 1']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='Address 1']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const address2Input = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='Address 2']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='Address 2']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const cityInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='City']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='City']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const stateInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='State']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='State']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const zipInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='Zip']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='Zip']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const saveClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-primary")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const searchInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='search']")).clear().then(() => d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
};

const companyRecordClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByTagName('tr')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.four));
};

const deleteClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-danger")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const confirmClick = (d, wd) => {
    return d.findElement(wd.By.className("confirm")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const sorting = (d, wd, index) => {
    return d.executeScript("document.getElementsByTagName('tr')[0].getElementsByTagName('th')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.four));
};

module.exports = {
    addCompanyClick,
    companyNameInput,
    firstNameInput,
    lastNameInput,
    phoneInput,
    address1Input,
    address2Input,
    cityInput,
    stateInput,
    zipInput,
    saveClick,
    searchInput,
    companyRecordClick,
    confirmClick,
    deleteClick,
    sorting
};