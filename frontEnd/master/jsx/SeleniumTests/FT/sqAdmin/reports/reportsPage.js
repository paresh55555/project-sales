const cf = require('../../commonFunctions/cf');

const searchInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='search']")).clear().then(() => d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
};

const changeOrderClick = (d, index) => {
    return d.executeScript("document.getElementsByTagName('th')["+ index +"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const dateSelectClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('react-datepicker__week')[1].getElementsByClassName('react-datepicker__day react-datepicker__day--mon')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const goButtonClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('filterButton')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const datePickerClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('toDatePicker')[0].getElementsByTagName('button')[0].click()").then(() => d.executeScript("document.getElementsByClassName('toDatePicker')[0].getElementsByTagName('a')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const datePickerYearClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('fromDatePicker')[0].getElementsByTagName('button')[0].click()").then(() => d.executeScript("document.getElementsByClassName('fromDatePicker')[0].getElementsByTagName('a')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const previousNavClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('react-datepicker__navigation react-datepicker__navigation--previous')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const dateButtonClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('monthYearFilter')[0].getElementsByTagName('button')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const paginationNextClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('pagination')[0].getElementsByClassName('activeNext')[0].getElementsByTagName('a')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const paginationPreviousClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('pagination')[0].getElementsByClassName('activePrevious')[0].getElementsByTagName('a')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const exportLeadsReportClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('export-button')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const sorting = (d, wd,index) => {
    return d.executeScript("document.getElementsByTagName('table')[0].getElementsByTagName('thead')[0].getElementsByTagName('th')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

module.exports = {
    searchInput,
    changeOrderClick,
    dateSelectClick,
    datePickerClick,
    datePickerYearClick,
    goButtonClick,
    previousNavClick,
    dateButtonClick,
    paginationNextClick,
    paginationPreviousClick,
    exportLeadsReportClick,
    sorting
};