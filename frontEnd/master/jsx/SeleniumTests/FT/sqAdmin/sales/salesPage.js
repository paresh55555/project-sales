const cf = require('../../commonFunctions/cf');

const openDatePickerClick = (d) => {
    return d.executeScript("document.getElementById('dropdown-basic').click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const dateWidgetClick = (d, wd) => {
    return d.executeScript("document.getElementById('dropdown-basic').click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const changeMonthClick = (d) => {
    for(var i = 0; i < 10 ; i++ ){
        d.executeScript("document.getElementsByClassName('react-datepicker__navigation react-datepicker__navigation--previous')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
    }
    return d.executeScript("document.getElementsByClassName('react-datepicker__navigation react-datepicker__navigation--previous')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const startDateClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('react-datepicker-wrapper')[0].getElementsByTagName('input')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const startDateSelect = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('react-datepicker-popper')[0].getElementsByClassName('react-datepicker__day')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const filterSelect = (d) => {
    return d.executeScript("document.querySelectorAll(\"input[type='checkbox']\")[1].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const goButtonClick = (d, wd) => {
    return d.findElement(wd.By.className("filterButton")).click().then(() => cf.delayBySecond(cf.delaySecond.twenty));
};

const selectPreviousYearClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('listFilters')[0].getElementsByTagName('li')[1].click()").then(() => cf.delayBySecond(cf.delaySecond.twenty));
}

const searchInput = (d, wd, value) => {
    return d.executeScript("document.getElementsByClassName('form-control input-sm')[0].value = '" + value + "'").then(() => cf.delayBySecond(cf.delaySecond.five));
};

const ascOrderJobNumberClick = (d, wd) => {
    return d.findElement(wd.By.css('th[class="job_number sorting"]')).click().then(() => cf.delayBySecond(cf.delaySecond.six));
};

const descOrderJobNumberClick = (d, wd) => {
    return d.findElement(wd.By.css('th[class="job_number sorting_asc"]')).click().then(() => cf.delayBySecond(cf.delaySecond.six));
};

const ascOrderTotalClick = (d, wd) => {
    return d.findElement(wd.By.css('th[class="total sorting"]')).click().then(() => cf.delayBySecond(cf.delaySecond.six));
};

const descOrderTotalClick = (d, wd) => {
    return d.findElement(wd.By.css('th[class="total sorting_asc"]')).click().then(() => cf.delayBySecond(cf.delaySecond.six));
};

const ascOrderNumberOfPanelsClick = (d, wd) => {
    return d.findElement(wd.By.css('th[class="number_of_panels sorting"]')).click().then(() => cf.delayBySecond(cf.delaySecond.six));
};

const descOrderNumberOfPanelsClick = (d, wd) => {
    return d.findElement(wd.By.css('th[class="number_of_panels sorting_asc"]')).click().then(() => cf.delayBySecond(cf.delaySecond.six));
};

const ascOrderQuotedClick = (d, wd) => {
    return d.findElement(wd.By.css('th[class="quoted sorting"]')).click().then(() => cf.delayBySecond(cf.delaySecond.six));
};

const descOrderQuotedClick = (d, wd) => {
    return d.findElement(wd.By.css('th[class="quoted sorting_asc"]')).click().then(() => cf.delayBySecond(cf.delaySecond.six));
};

module.exports = {
    startDateClick,
    startDateSelect,
    goButtonClick,
    filterSelect,
    searchInput,
    ascOrderJobNumberClick,
    descOrderJobNumberClick,
    ascOrderTotalClick,
    descOrderTotalClick,
    ascOrderNumberOfPanelsClick,
    descOrderNumberOfPanelsClick,
    ascOrderQuotedClick,
    descOrderQuotedClick,
    changeMonthClick,
    openDatePickerClick,
    selectPreviousYearClick,
    dateWidgetClick
};