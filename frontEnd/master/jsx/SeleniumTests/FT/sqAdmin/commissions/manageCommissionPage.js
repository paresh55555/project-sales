const cf = require('../../commonFunctions/cf');

const addCommissionClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-labeled btn-info")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const dateButtonClick = (d, wd) => {
    return d.executeScript("document.getElementById('dropdown-basic').click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const orderInput = (d, wd, value) => {
    return d.findElement(wd.By.className("rbt-input-main form-control rbt-input")).clear().then(() => d.findElement(wd.By.className("rbt-input-main form-control rbt-input")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const commissionInput = (d, wd, inputName, value) => {
    return d.findElement(wd.By.css("input[placeholder='" + inputName + "']")).clear().then(() => d.findElement(wd.By.css("input[placeholder='" + inputName + "']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.twenty)));
};

const selectOrderValueClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('rbt-menu dropdown-menu show')[0].getElementsByTagName('div')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const dateSelectClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('react-datepicker__week')["+index+"].getElementsByClassName('react-datepicker__day react-datepicker__day--mon')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const currentDateSelect = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('react-datepicker__day--today')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const goButtonClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('filterButton')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const datePickerClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('react-datepicker__input-container')["+index+"].getElementsByTagName('input')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const previousNavClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('react-datepicker__navigation react-datepicker__navigation--previous')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const amountInput = (d, wd, value) => {
    return d.findElement(wd.By.id("amount")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const correctionInput = (d, wd, value) => {
    return d.findElement(wd.By.id("correction")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const saveClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-primary")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const commissionSearchInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const commissionRecordClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByTagName('tr')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const editButtonClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('pull-right btn btn-default')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const editCorrectionInput = (d, wd, value) => {
    return d.executeScript("document.getElementsByClassName('list-row')[0].getElementsByClassName('correction')[0].value = " + value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const startDateClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('report-datepicker')[0].getElementsByTagName('input')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const startDateSelect = (d, wd) => {
    return d.executeScript("document.querySelectorAll(\"div[aria-label='day-1']\")[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deleteClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('deleteButton btn btn-default')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const newCommissionEditAmountInput = (d, wd, index, value) => {
    return d.findElement(wd.By.id("new_commission_amount_"+index)).clear().then(() => d.findElement(wd.By.id("new_commission_amount_"+index)).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.six)));
};

const newCommissionEditCorrectionInput = (d, wd, index, value) => {
    return d.findElement(wd.By.id("new_commission_correction_"+index)).clear().then(() => d.findElement(wd.By.id("new_commission_correction_"+index)).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.six)));
};

const addCommissionBoxInput = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('addCommissionBtn')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const  confirmClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('confirm')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const changeOrderClick = (d, index) => {
    return d.executeScript("document.getElementsByTagName('th')["+ index +"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    addCommissionClick,
    orderInput,
    commissionInput,
    selectOrderValueClick,
    dateSelectClick,
    amountInput,
    correctionInput,
    saveClick,
    goButtonClick,
    commissionRecordClick,
    editButtonClick,
    editCorrectionInput,
    startDateClick,
    startDateSelect,
    deleteClick,
    dateButtonClick,
    datePickerClick,
    previousNavClick,
    commissionSearchInput,
    addCommissionBoxInput,
    newCommissionEditAmountInput,
    newCommissionEditCorrectionInput,
    confirmClick,
    changeOrderClick,
    currentDateSelect
};