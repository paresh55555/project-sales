const cf = require('../../commonFunctions/cf');

const addCommissionRateClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-labeled btn-info")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const commissionRateInput = (d, wd, el, value) => {
    return d.findElement(wd.By.id(el)).clear().then(()=>d.findElement(wd.By.id(el)).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const selectType = (d, wd) => {
    return d.findElement(wd.By.id('inlineradio2')).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const addCommissionRateButtonClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-primary")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const searchInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='search']")).clear().then(() => d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
};

const recordClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByTagName('tr')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deleteCommissionClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('pull-right btn btn-default')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const confirmClick = (d, wd) => {
    return d.findElement(wd.By.className("confirm")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const changeOrderClick = (d, index) => {
    return d.executeScript("document.getElementsByTagName('th')["+ index +"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const commissionGroupSelect = (d, wd) => {
    return d.findElement(wd.By.css('#commission_group>option[value="2"]')).click();
};


module.exports = {
    addCommissionRateClick,
    commissionRateInput,
    addCommissionRateButtonClick,
    searchInput,
    selectType,
    recordClick,
    deleteCommissionClick,
    confirmClick,
    changeOrderClick,
    commissionGroupSelect
};