const cf = require('../../commonFunctions/cf');

const addCommissionClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-labeled btn-info")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const orderInput = (d, wd, value) => {
    return d.findElement(wd.By.className("rbt-input-main form-control rbt-input")).clear().then(() => d.findElement(wd.By.className("rbt-input-main form-control rbt-input")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
};

const orderIdSelect = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('dropdown-item')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const dateInputClick = (d, wd) => {
    return d.executeScript("document.querySelectorAll(\"input[type='text'][class='form-control']\")[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const dateSelectClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('react-datepicker-popper')[0].getElementsByClassName('react-datepicker__day')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const amountInput = (d, wd, value) => {
    return d.findElement(wd.By.id('amount')).clear().then(()=>d.findElement(wd.By.id('amount')).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const correctionInput = (d, wd, value) => {
    return d.findElement(wd.By.id('correction')).clear().then(()=>d.findElement(wd.By.id('correction')).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};


const saveClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-primary")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const goButtonClick = (d, wd) => {
    return d.findElement(wd.By.className("filterButton")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const commissionRecordClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByTagName('tr')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const addCommisionButtonClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('addCommissionBtn')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const editButtonClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('pull-right btn btn-default')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const editCorrectionInput = (d, wd, value) => {
    return d.executeScript("document.getElementsByClassName('table-List')[0].getElementsByClassName('commissionTextBox')[0].value = " + value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const dateWidgetClick = (d, wd) => {
    return d.executeScript("document.getElementById('dropdown-basic').click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const startDateClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('react-datepicker-wrapper')[0].getElementsByTagName('input')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const startDateSelect = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('react-datepicker-popper')[0].getElementsByClassName('react-datepicker__day')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const paymentAmountInput = (d, wd,value) => {
    return d.executeScript("var len = document.getElementsByClassName('table-List').length;document.getElementsByClassName('table-List')[len-1].getElementsByTagName('input')[1].value="+value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const correctionAmountInput = (d, wd,value) => {
    return d.executeScript("var len = document.getElementsByClassName('table-List').length;document.getElementsByClassName('table-List')[len-1].getElementsByTagName('input')[2].value="+value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deleteClick = (d, wd) => {
    return d.executeScript("var len = document.getElementsByClassName('deleteButton btn btn-default').length;document.getElementsByClassName('deleteButton btn btn-default')[len -1].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deleteConfirm = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('confirm')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const addCommissionStartDateClick = (d, wd) => {
    return d.executeScript("var len = document.getElementsByClassName('table-List').length;document.getElementsByClassName('table-List')[len-1].getElementsByClassName('react-datepicker-wrapper')[0].getElementsByTagName('input')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};
module.exports = {
    addCommissionClick,
    orderInput,
    orderIdSelect,
    dateInputClick,
    dateSelectClick,
    amountInput,
    correctionInput,
    saveClick,
    goButtonClick,
    commissionRecordClick,
    editButtonClick,
    editCorrectionInput,
    startDateClick,
    startDateSelect,
    deleteClick,
    dateWidgetClick,
    paymentAmountInput,
    correctionAmountInput,
    addCommisionButtonClick,
    deleteConfirm,
    addCommissionStartDateClick
};