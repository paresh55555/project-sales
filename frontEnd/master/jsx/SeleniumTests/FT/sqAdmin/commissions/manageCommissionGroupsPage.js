const cf = require('../../commonFunctions/cf');

const addCommissionGroupClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-labeled btn-info")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const cancelButtonClick = (d, wd) => {
    return d.findElement(wd.By.className("cancel btn btn-default")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const commissionNameInput = (d, wd, value) => {
    return d.findElement(wd.By.id("commissionName")).clear().then(()=>d.findElement(wd.By.id("commissionName")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const commissionCheckBoxInput = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('form-group')["+index+"].getElementsByTagName('input')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const saveButtonClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-primary")).click().then(() => cf.delayBySecond(cf.delaySecond.five));
};

const changeOrderClick = (d, index) => {
    return d.executeScript("document.getElementsByTagName('th')["+ index +"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deleteButtonClick = (d, wd) => {
    return d.findElement(wd.By.className("pull-right btn btn-default")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const commissionSearchInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const confirmCommissionClick = (d, wd) => {
    return d.findElement(wd.By.className("confirm")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const recordClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByTagName('tr')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    addCommissionGroupClick,
    cancelButtonClick,
    commissionNameInput,
    commissionCheckBoxInput,
    changeOrderClick,
    saveButtonClick,
    deleteButtonClick,
    commissionSearchInput,
    confirmCommissionClick,
    recordClick
};