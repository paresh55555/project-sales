const cf = require('../../commonFunctions/cf');

const addUserClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-labeled btn-info")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const salesPersonTypeClick = (d, wd) => {
    return d.findElement(wd.By.id("inlineradio1")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const salesManagerTypeClick = (d, wd) => {
    return d.findElement(wd.By.id("inlineradio2")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const prefixInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[placeholder='Prefix']")).clear().then(() => d.findElement(wd.By.css("input[placeholder='Prefix']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const commissionGroupSelect = (d, wd) => {
    return d.findElement(wd.By.css('#commissionGroupSelect>option[value="2"]')).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const companyNameSelect = (d, wd) => {
    return d.executeScript("document.querySelectorAll('select[name=\"type\"]')[0].selectedIndex = document.getElementsByTagName(\"option\").length - (document.getElementsByTagName(\"option\").length )").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const nameInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='Name']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='Name']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const userNameInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='Email / Username']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='Email / Username']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const discountInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='Discount']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='Discount']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const passwordInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='Password']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='Password']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const phoneInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='Phone']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='Phone']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const sendMessage = (d, wd) => {
    return d.executeScript("document.querySelector('input[type=\"checkbox\"][name=\"sendMessage\"]').click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const anytimeQuoteEditsSelect = (d, wd) => {
    return d.executeScript("document.querySelector('input[type=\"checkbox\"][name=\"anytimeQuoteEdits\"]').click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const viewAllQuoteSelect = (d, wd) => {
    return d.executeScript("document.querySelector('input[type=\"checkbox\"][name=\"superSales\"]').click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const sendEmailMessageSelect = (d, wd) => {
    return d.executeScript("document.querySelector('input[type=\"checkbox\"][name=\"sendMessage\"]').click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const emailSubjectInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='text'][placeholder='Email Subject']")).clear().then(() => d.findElement(wd.By.css("input[type='text'][placeholder='Email Subject']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const emailMessageInput = (d, wd, value) => {
    return d.findElement(wd.By.css("textarea[placeholder='Email Message']")).clear().then(() => d.findElement(wd.By.css("textarea[placeholder='Email Message']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const addUserButtonClick = (d, wd) => {
    return d.findElement(wd.By.className("btn-sm btn-primary")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const searchInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='search']")).clear().then(() => d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
};

const selectUserClick = (d, wd) => {
    return d.executeScript("document.querySelectorAll(\"tr[role='row']\")[1].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deleteUserClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('btn-danger')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const confirmClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('confirm')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const sorting = (d, wd,index) => {
    return d.executeScript("document.getElementsByTagName('table')[0].getElementsByTagName('thead')[0].getElementsByTagName('th')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    addUserClick,
    salesPersonTypeClick,
    salesManagerTypeClick,
    prefixInput,
    companyNameSelect,
    nameInput,
    userNameInput,
    passwordInput,
    phoneInput,
    sendMessage,
    anytimeQuoteEditsSelect,
    viewAllQuoteSelect,
    sendEmailMessageSelect,
    emailSubjectInput,
    emailMessageInput,
    addUserButtonClick,
    searchInput,
    selectUserClick,
    deleteUserClick,
    confirmClick,
    commissionGroupSelect,
    discountInput,
    sorting
};