const cf = require('../../commonFunctions/cf');

const commissionClick = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Commissions']")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const commissionGroupsClick = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Commission Groups']")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const salesClick = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Sales']")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const companiesClick = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Companies']")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const modulesClick = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Modules']")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const commissionRateClick = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Commission Rate']")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const discountsClick = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Discounts']")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const reportsClick = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Lead Report']")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const customerEmailReportClick = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Customer Emails Report']")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const ordersReportClick = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Orders Report']")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const quotesReportClick = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Open Quotes Report']")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const dailyIntakeReportClick = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Daily Intake Report']")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const orderSystemReportClick = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Order Systems Report']")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const hasBalanceClick = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Has Balance']")).click().then(() => cf.delayBySecond(cf.delaySecond.twenty));
};

const taxOverride = (d, wd) => {
    return d.findElement(wd.By.css("a[title='Tax Override']")).click().then(() => cf.delayBySecond(cf.delaySecond.twenty));
};

const toggleSidebar = (d, wd) => {
    return d.findElement(wd.By.css("a[class='hidden-xs']")).click().then(() => cf.delayBySecond(cf.delaySecond.twenty));
};

module.exports = {
    commissionClick,
    salesClick,
    companiesClick,
    modulesClick,
    commissionGroupsClick,
    commissionRateClick,
    discountsClick,
    reportsClick,
    customerEmailReportClick,
    ordersReportClick,
    quotesReportClick,
    dailyIntakeReportClick,
    orderSystemReportClick,
    hasBalanceClick,
    taxOverride,
    toggleSidebar
};