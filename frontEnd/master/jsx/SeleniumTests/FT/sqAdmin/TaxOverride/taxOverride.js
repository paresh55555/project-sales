var webElement = require('selenium-webdriver').WebElement;
const cf = require('../../commonFunctions/cf');

const addTaxClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-labeled btn-info")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const taxOverrideInput = (d, wd, value) => {
    return d.findElement(wd.By.id('taxOverride')).clear().then(()=>d.findElement(wd.By.id('taxOverride')).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const stateInput = (d, wd, value) => {
    return d.findElement(wd.By.css('input[placeholder="Search State"]')).clear().then(()=>d.findElement(wd.By.css('input[placeholder="Search State"]')).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const selectState = (d, wd,index) => {
    return d.executeScript("document.getElementsByClassName('locationItem')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const saveClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-primary")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const recordClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByTagName('tr')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deleteClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('btn-danger')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deleteConfirm = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('confirm')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const getStateName =  async(d, wd) => {
    webElement = d.findElement(wd.By.css('input[placeholder="Search State"]'));
    let value = await webElement.getAttribute('value');
    return value
}

const searchInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='search']")).clear().then(() => d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
};

const sorting = (d, wd,index) => {
    return d.executeScript("document.getElementsByTagName('table')[0].getElementsByTagName('thead')[0].getElementsByTagName('th')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

module.exports = {
    addTaxClick,
    taxOverrideInput,
    stateInput,
    selectState,
    saveClick,
    recordClick,
    deleteClick,
    deleteConfirm,
    getStateName,
    searchInput,
    sorting
};