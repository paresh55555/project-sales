const cf = require('../../commonFunctions/cf');

const addDiscountsClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-labeled btn-info")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const discountNameInput = (d, wd, value) => {
    return d.findElement(wd.By.id('name')).clear().then(() => d.findElement(wd.By.id('name')).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
};

const discountAmountInput = (d, wd, value) => {
    return d.findElement(wd.By.id('amount')).clear().then(() => d.findElement(wd.By.id('amount')).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
};

const selectType = (d, wd,id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const addDiscountsButtonClick = (d, wd) => {
    return d.findElement(wd.By.className("btn btn-primary")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const searchInput = (d, wd, value) => {
    return d.findElement(wd.By.css("input[type='search']")).clear().then(() => d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
};

const deleteDiscountClick = (d, wd) => {
    return d.findElement(wd.By.className("pull-right btn btn-default")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const confirmClick = (d, wd) => {
    return d.findElement(wd.By.className("confirm")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const nextNavClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('react-datepicker__navigation react-datepicker__navigation--next')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const dateSelectClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('react-datepicker__week')["+index+"].getElementsByClassName('react-datepicker__day react-datepicker__day--mon')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const datePickerClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('react-datepicker__input-container')["+index+"].getElementsByTagName('input')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const selectCommissionGroupDropDown = (d, wd, value) => {
    return d.findElement(wd.By.css('#commission_group>option[value="'+value+'"]')).click();
};

const selectSalesPersonDropDown = (d, wd, value) => {
    return d.findElement(wd.By.css(`#sales_person>option[value="${value}"]`)).click();
};

const sorting = (d, wd,index) => {
    return d.executeScript("document.getElementsByTagName('table')[0].getElementsByTagName('thead')[0].getElementsByTagName('th')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const discountRowClick = (d, wd,index) => {
    return d.executeScript("document.getElementsByTagName('tr')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const noEndClick = (d, wd) => {
    return d.executeScript(`document.querySelector('input[type="checkbox"]').click()`).then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    addDiscountsClick,
    discountNameInput,
    discountAmountInput,
    addDiscountsButtonClick,
    searchInput,
    selectType,
    deleteDiscountClick,
    confirmClick,
    nextNavClick,
    dateSelectClick,
    datePickerClick,
    selectCommissionGroupDropDown,
    selectSalesPersonDropDown,
    sorting,
    discountRowClick,
    noEndClick
};