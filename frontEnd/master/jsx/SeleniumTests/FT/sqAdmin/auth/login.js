const cf = require('../../commonFunctions/cf');

const logIn = (d, wd) => {
    return d.findElement(wd.By.className("btn-primary")).click().then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const EmailInput = (d, wd, value) => {
    return d.findElement(wd.By.id("exampleInputEmail1")).clear().then(() => d.findElement(wd.By.id("exampleInputEmail1")).sendKeys(value)).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const PasswordInput = (d, wd, value) => {
    return d.findElement(wd.By.id("exampleInputPassword1")).clear().then(() => d.findElement(wd.By.id("exampleInputPassword1")).sendKeys(value)).then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    EmailInput,
    PasswordInput,
    logIn
};