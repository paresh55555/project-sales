const cf = require('../../commonFunctions/cf');
const moment = require('moment');

const filterClick = (d, wd,index) => {
    return d.executeScript("document.getElementsByClassName('reportFilterInnerCon')["+index+"].getElementsByTagName('button')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const dateFilterItemSelect = (d, wd,index) => {
    return d.executeScript("document.getElementsByClassName('listFilters')[0].getElementsByTagName('li')["+index+"].click();").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const searchSalesPersonInput= (d, wd,value) => {
    return d.findElement(wd.By.css("input[placeholder='Search']")).clear().then( d.findElement(wd.By.css("input[placeholder='Search']")).sendKeys(value));
};

const checkboxClick = (d, wd,row,index) => {
    return d.executeScript("document.getElementsByClassName('searchResultCon')[0].getElementsByTagName('ul')["+row+"].getElementsByTagName('input')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const clearButtonClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('searchBarCon')[0].getElementsByTagName('button')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const closeIconClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('filterCloseIcon')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const filterButtonClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('salesPersonFilterCon')[0].getElementsByTagName('button')[1].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const clearIconClick = (d, wd,filterName,index) => {
    return d.executeScript("document.getElementsByClassName('"+filterName+"')["+index+"].getElementsByTagName('i')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const searchCityInput = (d, wd,value) => {
        return d.findElement(wd.By.css("input[placeholder='Search City']")).clear().then(()=>d.findElement(wd.By.css("input[placeholder='Search City']")).sendKeys(value).then(()=>cf.delayBySecond(cf.delaySecond.five)));
}

const searchStateInput = (d, wd,value) => {
    return d.findElement(wd.By.css("input[placeholder='Search State']")).clear().then(()=>d.findElement(wd.By.css("input[placeholder='Search State']")).sendKeys(value).then(()=>cf.delayBySecond(cf.delaySecond.five)));
}

const selectCity= (d, wd) => {
    return d.executeScript("document.getElementsByClassName('locationItem')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const filterCityClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('locationFilterCon')[0].getElementsByTagName('button')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const radioClick = (d, wd,index) => {
    return d.executeScript("document.getElementsByClassName('radio-inline c-radio')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const searchZipInput = (d, wd,value) => {
    return d.findElement(wd.By.css("input[placeholder='Zip Code']")).clear().then( d.findElement(wd.By.css("input[placeholder='Zip Code']")).sendKeys(value).then(()=>cf.delayBySecond(cf.delaySecond.two)));
}

const filterZipClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('locationFilterCon')[0].getElementsByTagName('button')[1].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const selectOrderStatus = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('orderFilterCon')[0].getElementsByTagName('li')["+index+"].getElementsByTagName('input')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const filterSelectOrderStatusClick= (d, wd) => {
    return d.executeScript("document.getElementsByClassName('orderFilterCon')[0].getElementsByTagName('button')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const orderStatusFilterClick= (d, wd) => {
    return d.executeScript("document.getElementsByClassName('pull-right')[0].getElementsByTagName('button')[0].click();").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const orderStatusFilterItemClick = (d, wd,index) => {
    return d.executeScript("document.getElementsByClassName('checkboxFilterList')[0].getElementsByTagName('input')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const paginationEntryClick = (d, wd,index) => {
    return d.executeScript("document.getElementsByClassName('tableHeader')[0].getElementsByTagName('select')[0].selectedIndex="+index).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const paginationClick = (d, wd,buttonName) => {
    return d.executeScript("document.getElementsByClassName('"+buttonName+"')[0].getElementsByTagName('a')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const sorting = (d, wd,index) => {
    return d.executeScript("document.getElementsByTagName('table')[0].getElementsByTagName('thead')[0].getElementsByTagName('th')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const search = (d, wd,value) => {
    return d.findElement(wd.By.css("input[type='search']")).clear().then(()=>d.findElement(wd.By.css("input[type='search']")).sendKeys(value).then(()=>cf.delayBySecond(cf.delaySecond.five)));
}

const exportClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('exportBtn')[0].getElementsByTagName('button')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const dateInput = (d, wd,name,value) => {
    return d.executeScript("document.getElementsByClassName('"+name+"')[0].getElementsByTagName('input')[0].value='"+value+"'").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const goButton = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('filterButton')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const selectDateClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('date singleDateFilterCon')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const prevMonthClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('reportFilterInnerCon')[0].getElementsByClassName('react-datepicker__navigation react-datepicker__navigation--previous')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const nextMonthClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('reportFilterInnerCon')[0].getElementsByClassName('react-datepicker__navigation react-datepicker__navigation--next')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const prevMonthdayClick = (d, wd) => {
    var day = moment().subtract(1,'month');
    var week = Math.ceil(day.date() / 7) - 1;

    let dayName =moment().format('ddd').toLowerCase();
    return d.executeScript("document.getElementsByClassName('reportFilterInnerCon')[0].getElementsByClassName('react-datepicker__week')["+week+"].getElementsByClassName('react-datepicker__day--"+dayName+"')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const nextMonthdayClick = (d, wd) => {
    var day = moment();
    var week = Math.ceil(day.date() / 7) - 1;

    let dayName =moment().format('ddd').toLowerCase();
    return d.executeScript("document.getElementsByClassName('reportFilterInnerCon')[0].getElementsByClassName('react-datepicker__week')["+week+"].getElementsByClassName('react-datepicker__day--"+dayName+"')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const salesFilterClick = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('checkboxFilterList')[0].getElementsByTagName('li')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const datePickerClick = (d, wd,name) => {
    return d.executeScript("document.getElementsByClassName('"+name+"')[0].getElementsByTagName('input')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const dayClick = (d, wd,name) => {
    return d.executeScript("document.getElementsByClassName('"+name+"')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const locationFilterTabClick = (d, wd,index) => {
    return d.executeScript("document.getElementsByClassName('locationTabs')[0].getElementsByTagName('li')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const groupBySelect = (d, wd,index) => {
    return d.executeScript("document.getElementsByClassName('groupByFilter')[0].getElementsByTagName('li')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const leadSourceWiseSelect = (d, wd,index) => {
    return d.executeScript("document.getElementsByClassName('groupByFilter')[1].getElementsByTagName('li')["+index+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const scrollLeft = (d, wd,value) => {
    return d.executeScript("document.getElementsByClassName('haveScroll')[0].scrollLeft ="+value).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const filterDataColumnClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('salesPersonIcon')[1].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const defaultButtonClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('defaultButton ')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const selectAllButtonClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('selectAllBtn')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const filterDataColumnsButtonClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('filterButton')[3].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const dataColumnScroll = (d, wd,value) => {
    return d.executeScript("document.getElementsByClassName('searchResultCon')[0].scrollTo(0,"+value+")").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const checkboxesClick = (d, wd,row,checkbox) => {
    return d.executeScript("document.getElementsByClassName('columnFilter')["+row+"].getElementsByTagName('input')["+checkbox+"].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
}

module.exports = {
    filterClick,
    dateFilterItemSelect,
    searchSalesPersonInput,
    checkboxClick,
    clearButtonClick,
    filterButtonClick,
    clearIconClick,
    searchCityInput,
    selectCity,
    filterCityClick,
    radioClick,
    searchZipInput,
    filterZipClick,
    selectOrderStatus,
    filterSelectOrderStatusClick,
    orderStatusFilterClick,
    orderStatusFilterItemClick,
    paginationEntryClick,
    paginationClick,
    sorting,
    search,
    exportClick,
    dateInput,
    goButton,
    selectDateClick,
    prevMonthClick,
    nextMonthClick,
    prevMonthdayClick,
    nextMonthdayClick,
    salesFilterClick,
    datePickerClick,
    dayClick,
    locationFilterTabClick,
    searchStateInput,
    groupBySelect,
    leadSourceWiseSelect,
    scrollLeft,
    filterDataColumnClick,
    defaultButtonClick,
    selectAllButtonClick,
    filterDataColumnsButtonClick,
    dataColumnScroll,
    checkboxesClick,
    closeIconClick
};