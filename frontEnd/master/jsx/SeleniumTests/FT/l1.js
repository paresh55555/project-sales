const cf = require('./commonFunctions/cf');

const signIn = (d, wd) => {
    return d.findElement(wd.By.id("signInButton")).click().then(() => cf.delayBySecond(cf.delaySecond.thirty));
}

const EmailInput = (d, wd, value) => {
    return d.findElement(wd.By.id("email")).sendKeys(value);
}

const PasswordInput = (d, wd, value) => {
    return d.findElement(wd.By.id("password")).sendKeys(value);
}

const nameInput = (d, wd, value) => {
    return d.findElement(wd.By.id("name")).sendKeys(value);
}

const zipInput = (d, wd, value) => {
    return d.findElement(wd.By.id("zip")).sendKeys(value);
}

const phoneInput = (d, wd, value) => {
    return d.findElement(wd.By.id("phone")).sendKeys(value);
}

const leadTypeRadio = (d) => {
    return d.executeScript('(document.getElementsByName(\'leadType\')[0]).click()');
}

const bestWayToContact = (d, wd,index) => {
    return d.executeScript(`document.querySelectorAll('input[name="contactType"]')[${index}].click()`).then(() => cf.delayBySecond(cf.delaySecond.five));
}

const timeToCallInput = (d, wd,id,value) => {
    return d.findElement(wd.By.id(id)).clear().then(() => d.findElement(wd.By.id(id)).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
}

const timelineSelect = (d, wd,index) => {
    return d.executeScript(`var event = new Event('change');
    document.getElementById('timeline').selectedIndex = ${index};
    document.getElementById('timeline').dispatchEvent(event, {})`).then(() => cf.delayBySecond(cf.delaySecond.two));
}

const doorWidthInput = (d, wd,value) => {
    return d.findElement(wd.By.id('doorWidth')).clear().then(() => d.findElement(wd.By.id('doorWidth')).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
}

const doorHeightInput = (d, wd,value) => {
    return d.findElement(wd.By.id('doorHeight')).clear().then(() => d.findElement(wd.By.id('doorHeight')).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
}

const commentsInput = (d, wd,value) => {
    return d.findElement(wd.By.id('customerComment')).clear().then(() => d.findElement(wd.By.id('customerComment')).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
}

const requestAQuote = (d, wd) => {
    return d.findElement(wd.By.id("requestaQuote")).click().then(() => cf.delayBySecond(cf.delaySecond.twenty));
}

const getQuoteNo = async (d, wd) => {
    let no = await d.executeScript(`return document.getElementsByClassName('guestSuccess')[0].getElementsByTagName('span')[0].innerText`);
    return no;
}

const proceedToQuoteBtn = (d, wd) => {
    return d.findElement(wd.By.id("proceedToQuote")).click().then(() => cf.delayBySecond(cf.delaySecond.five));
}

const checkTotal = (d, wd) => {
    return d.findElement(wd.By.id("proceedToQuote")).click().then(() => cf.delayBySecond(cf.delaySecond.five));
}

const salesPersonLogout = (d, wd) => {
    return d.executeScript("document.getElementById('headerRightMenu').click();document.getElementsByClassName('menuRowBottom')[0].click();").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const loginLinkClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('guestNoteLogin')[0].getElementsByTagName('a')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.ten));
}

const ambassadorNumber = (d, wd,id,value) => {
    return d.findElement(wd.By.id(id)).clear().then(() => d.findElement(wd.By.id(id)).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.five)));
}

const adminLogout = (d, wd) => {
    return d.executeScript("document.getElementById('dropdown-no-caret').click();document.getElementsByClassName('dropdown-menu')[0].getElementsByTagName('a')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.thirty));
}

module.exports = {EmailInput, PasswordInput, signIn, nameInput, zipInput, phoneInput, leadTypeRadio, proceedToQuoteBtn, checkTotal,
    salesPersonLogout,
    loginLinkClick,
    ambassadorNumber,
    adminLogout,
    bestWayToContact,
    timeToCallInput,
    timelineSelect,
    doorWidthInput,
    doorHeightInput,
    commentsInput,
    requestAQuote,
    getQuoteNo
};