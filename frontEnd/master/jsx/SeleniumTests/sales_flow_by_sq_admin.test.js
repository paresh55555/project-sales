const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const salesPage = require('./FT/sqAdmin/sales/salesPage');

let _u = undefined;

function sales_flow_by_SQ_admin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Sales flow by SQ-admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "SalesManager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sideBar.salesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/salesReports', true, _u));
    pc = pc.then(() => salesPage.dateWidgetClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/salesReports', true, _u));
    pc = pc.then(() => salesPage.startDateClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/salesReports', true, _u));
    pc = pc.then(() => salesPage.startDateSelect(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/salesReports', true, _u));
    pc = pc.then(() => salesPage.filterSelect(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/salesReports', true, _u));
    pc = pc.then(() => salesPage.goButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/salesReports', true, _u));
    pc = pc.then(() => salesPage.searchInput(d, wd, '2019')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/salesReports', true, _u));
    pc = cf.handleOutput(d, pc, capability, 'Sales flow by SQ-admin');
    return pc;
}

module.exports = {sales_flow_by_SQ_admin};