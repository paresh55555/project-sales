const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const panelRangesPage = require('./FT/sqAdmin/module/panelRangesPage');

let _u = undefined;

function manage_panel_module_flow_by_sqadmin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Manage Panel Ranges by sales person'));
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => sideBar.modulesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
    pc = pc.then(() => panelRangesPage.vinylwithAluminumBlockFrameClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.panelRangesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.addPanelRangesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.panelInput(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.lowInput(d, wd, 10)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.highInput(d, wd, 1000)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.savePanelRangesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.panelRangesSearchInput(d, wd, 1000)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.panelRangesRecordClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
    pc = pc.then(() => panelRangesPage.lowInput(d, wd, 20)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.highInput(d, wd, 1000)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.savePanelRangesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));

    pc = pc.then(() => panelRangesPage.panelRangesSearchInput(d, wd, 1000)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.panelRangesRecordClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
    pc = pc.then(() => panelRangesPage.deletePanelRangesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.confirmPanelRangesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.confirmPanelRangesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.changeOrderClick(d, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.changeOrderClick(d, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => panelRangesPage.changeOrderClick(d, 2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Manage Panel Ranges module by sales person');
    return pc;
}

module.exports = {manage_panel_module_flow_by_sqadmin};