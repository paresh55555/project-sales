const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const quote_NewQuote= require('./FT/afterLogin/quote/newQoutes');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const cart = require('./FT/afterLogin/quote/cart');

let _u = undefined;

function door_for_joe_only(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Door for Joe only'));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => quote_NewQuote.newQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));

    // CHECKING INCHES VALIDATION
    pc = pc.then(() => cf.testModuleData(d, 'Maximum Size:', 'Width 19 feet, Height 8 feet', 'Door for Joe Only'));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Door for Joe Only')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 1)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['*Vinyl Doors have a maximum width of 236 inches and a minimum width of 20 inches']));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 1)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['*Vinyl Doors have a maximum height of 96 inches and minimum height of 16 inches']));
    // .......

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 110)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 80)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd, 200)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,744.07']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd, "swingDirection-left")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd, "movement-left-0")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd, "swingInOrOut-Outswing")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,744.07']));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd, "screens-Panoramic Screen Left")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,752.40']));

    // COLOR
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_White_null_#fcffff_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,752.40']));

    // GLASS
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,111.11']));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Low-E 3')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,216.01']));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));

    // HARDWARE
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd,'hardware-Miami')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,216.01']));

    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart', true, _u));

    pc = pc.then(() => cf.testContentByArray(d,
        ['Door for Joe Only','White Vinyl','110" width x 80" height','111" width x 81" height',
            '3','34.69"','75.5"','Left','Outswing','Left 2 / Right 0','Aluminum Block','3/4 Upstand',
        'None','Low-E 3','Miami','Door Restrictor','Panoramic Screen Left','White','Operation ','left',
        'Item 1 of 1','$6,216.01','Total $6,216.01']
    ));
    pc = pc.then(() => cart.quantityInput(d, wd, 2)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d,
        ['Door for Joe Only','White Vinyl','110" width x 80" height','111" width x 81" height',
            '3','34.69"','75.5"','Left','Outswing','Left 2 / Right 0','Aluminum Block','3/4 Upstand',
            'None','Low-E 3','Miami','Door Restrictor','Panoramic Screen Left','White','Operation ','left',
            'Item 1-2 of 2','$6,216.01','Total $12,432.02','$12,432.02']
    ));
    pc = pc.then(() => cart.duplicateClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d,
        ['Door for Joe Only','White Vinyl','110" width x 80" height','111" width x 81" height',
            '3','34.69"','75.5"','Left','Outswing','Left 2 / Right 0','Aluminum Block','3/4 Upstand',
            'None','Low-E 3','Miami','Door Restrictor','Panoramic Screen Left','White','Operation ','left',
            'Item 3-4 of 4','Item 3-4 of 4','$6,216.01','Total $24,864.04','$12,432.02']
    ));
    pc = pc.then(() => cart.saveAsQuote(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['4']));

    pc = cf.handleOutput(d, pc, capability, 'Door for Joe only');
    return pc;
}

module.exports = {door_for_joe_only};