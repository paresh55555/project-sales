const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const accounting_action = require('./FT/afterLogin/accountingActions/accountingActionFlow');
const quote_ViewOrder = require('./FT/afterLogin/order/viewOrder');

let _u = undefined;

function accounting_login_depth_flow(capability) {
    let d = Config.getDriver(Config.ChromeCapabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Accounting login depth flow'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "PDAccounts")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=verifyDeposit', true, _u));

    pc = pc.then(() => accounting_action.accountingAllClick(d, wd)).then(() => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));
    // pc = pc.then(() => accounting_action.verifyDepositClick(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.accountingPreProductionClick(d, wd)).then(() => cf.test(d, 'accountingPreProduction', Config.BASE_URL + '?tab=accountingPreProduction', true, _u));
    pc = pc.then(() => accounting_action.accountingProductionClick(d, wd)).then(() => cf.test(d, 'accountingProduction', Config.BASE_URL + '?tab=accountingProduction', true, _u));
    pc = pc.then(() => accounting_action.accountingCompletedClick(d, wd)).then(() => cf.test(d, 'accountingCompleted', Config.BASE_URL + '?tab=accountingCompleted', true, _u));
    pc = pc.then(() => accounting_action.balanceDueClick(d, wd)).then(() => cf.test(d, 'balanceDue', Config.BASE_URL + '?tab=balanceDue', true, _u));
    pc = pc.then(() => accounting_action.accountingDeliveredClick(d, wd)).then(() => cf.test(d, 'accountingDelivered', Config.BASE_URL + '?tab=accountingDelivered', true, _u));
    pc = pc.then(() => accounting_action.accountingHoldClick(d, wd)).then(() => cf.test(d, 'accountingHold', Config.BASE_URL + '?tab=accountingHold', true, _u));
    pc = pc.then(() => accounting_action.searchInput(d, wd, "a")).then(() => cf.test(d, 'accountingHold', Config.BASE_URL + '?tab=accountingHold', true, _u));
    pc = pc.then(() => accounting_action.accountingAllClick(d, wd)).then(() => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));

    pc = pc.then(() => accounting_action.itemsPerPageSelect(d, wd, 2)).then((value) => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));
    pc = pc.then(() => accounting_action.itemsPerPageSelect(d, wd, 1)).then((value) => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['mainContent']));
    pc = pc.then(() => accounting_action.paginationButtonClick(d, wd, 'page-link next')).then((value) => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));
    pc = pc.then(() => accounting_action.paginationButtonClick(d, wd, 'page-link prev')).then((value) => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));

    pc = pc.then(() => accounting_action.verifyDepositClick(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.orderIdClick(d, wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    // ENTERING PAYMENT
    pc = pc.then(() => accounting_action.paymentInfoClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentOptionClick(d, wd,0)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.last4DigitsOfCreditCardInput(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentAmountInput(d, wd, '42.5788')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$42.58','Check 1111']));
    pc = pc.then(() => accounting_action.closePaymentInfo(d,wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.closeOrder(d,wd)).then(() => cf.test(d, 'confirmOrders', Config.BASE_URL + '?tab=confirmOrders', true, _u));
    // VERIFYING DEPOSITE
    pc = pc.then(() => accounting_action.verifyDepositClick(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.paymentBox1Click(d,wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.depositSatisfiedSelect(d, wd, 1)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.depositSatisfiedClick(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.verifyDepositClick(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));

    // CHECKING PAYMENT OPTIONS
    pc = pc.then(() => accounting_action.verifyDepositClick(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.orderIdClick(d, wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    let orderID;
    pc = pc.then(() => accounting_action.getOpenOrderNo(d, wd)).then((o)=>orderID=o);

    pc = pc.then(() => accounting_action.paymentInfoClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentOptionClick(d, wd,1)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.last4DigitsOfCreditCardInput(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentAmountInput(d, wd, '55.522')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$55.52','Visa 1111']));
    pc = pc.then(() => accounting_action.deletePaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo',  Config.BASE_URL + '?tab=paymentInfo', true, _u));

    pc = pc.then(() => accounting_action.paymentOptionClick(d, wd,2)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.last4DigitsOfCreditCardInput(d, wd, '1000')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentAmountInput(d, wd, '22')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$22.00','MasterCard 1000']));
    pc = pc.then(() => accounting_action.deletePaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo',  Config.BASE_URL + '?tab=paymentInfo', true, _u));

    pc = pc.then(() => accounting_action.paymentOptionClick(d, wd,3)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.last4DigitsOfCreditCardInput(d, wd, '9999')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentAmountInput(d, wd, '2222')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    // pc = pc.then(() => accounting_action.paymentDateSelect(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$2,222.00','Amex 9999']));
    pc = pc.then(() => accounting_action.deletePaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo',  Config.BASE_URL + '?tab=paymentInfo', true, _u));

    pc = pc.then(() => accounting_action.paymentOptionClick(d, wd,4)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.last4DigitsOfCreditCardInput(d, wd, '3333')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentAmountInput(d, wd, '20')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$20.00','Cash 3333']));
    pc = pc.then(() => accounting_action.deletePaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo',  Config.BASE_URL + '?tab=paymentInfo', true, _u));

    pc = pc.then(() => accounting_action.paymentOptionClick(d, wd,5)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.last4DigitsOfCreditCardInput(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentAmountInput(d, wd, '30')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$30.00','Wire 1111']));
    pc = pc.then(() => accounting_action.deletePaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo',  Config.BASE_URL + '?tab=paymentInfo', true, _u));

    // pc = pc.then(() => accounting_action.paymentOrderNotesInput(d, wd, 'Payment Order Notes test')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.closePaymentInfo(d,wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));

    // pc = pc.then(() => accounting_action.quoteNotesInput(d, wd, "Some notes")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.pickupDeliverySelection(d, wd, 2)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.pickupDeliverySelection(d, wd, 1)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.pickupDeliverySelection(d, wd, 0)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.commentInput(d, wd,'comments here')).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    // pc = pc.then(() => accounting_action.poInput(d, wd, "Po test here")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));

    // pc = pc.then(() => accounting_action.productionDetailNotesInput(d, wd, "Some note")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.billingContactInput(d, wd, "test name")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.billingAddress1Input(d, wd, "some address")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.billingAddress2Input(d, wd, "test")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.billingCityInput(d, wd, "San Mateo")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.billingStateInput(d, wd, "CA")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.billingZipInput(d, wd, "10001")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.billingPhoneInput(d, wd, "012-345-6789")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.billingEmailInput(d, wd, "test@test.com")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.shippingContactInput(d, wd, "test name")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.shippingAddress1Input(d, wd, "some address")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.shippingAddress2Input(d, wd, "test")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.shippingCityInput(d, wd, "San Mateo")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.shippingStateInput(d, wd, "CA")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.shippingZipInput(d, wd, "10001")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.shippingPhoneInput(d, wd, "012-345-6799")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.shippingEmailInput(d, wd, "testy@test.com")).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));

    pc = pc.then(() => accounting_action.scrollDown(d, wd, 2000)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    // pc = pc.then(() => accounting_action.quantityInput(d, wd, 4)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.cartItemClick(d, wd, 2)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.closeOrder(d, wd)).then(() => cf.test(d, 'confirmOrders', Config.BASE_URL + '?tab=confirmOrders', true, _u));

    // CHECK DATA
    pc = pc.then(() => accounting_action.accountingAllClick(d, wd)).then(() => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));
    pc = pc.then(() => accounting_action.searchInput(d, wd,orderID)).then(() => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));
    pc = pc.then(() => accounting_action.orderIdClick(d, wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));

    pc = pc.then(() => cf.testContentByArray(d, [
        'some address','test','San Mateo','CA','some address',
    'testy@test.com','test@test.com']));

    pc = pc.then(() => accounting_action.closeOrder(d, wd)).then(() => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));
    pc = pc.then(() => accounting_action.allSelect(d, wd)).then(() => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));
    pc = pc.then(() => accounting_action.PDAccountsClick(d, wd)).then(() => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));
    pc = pc.then(() => accounting_action.verifyDepositSelect(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.PDAccountsClick(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.preProductionSelect(d, wd)).then(() => cf.test(d, 'accountingPreProduction', Config.BASE_URL + '?tab=accountingPreProduction', true, _u));
    pc = pc.then(() => accounting_action.revertDepositSatisfied(d, wd));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'accountingPreProduction', Config.BASE_URL + '?tab=accountingPreProduction', true, _u));
    pc = pc.then(() => accounting_action.PDAccountsClick(d, wd)).then(() => cf.test(d, 'accountingPreProduction', Config.BASE_URL + '?tab=accountingPreProduction', true, _u));
    pc = pc.then(() => accounting_action.productionSelect(d, wd)).then(() => cf.test(d, 'accountingProduction', Config.BASE_URL + '?tab=accountingProduction', true, _u));
    pc = pc.then(() => accounting_action.PDAccountsClick(d, wd)).then(() => cf.test(d, 'accountingProduction', Config.BASE_URL + '?tab=accountingProduction', true, _u));
    pc = pc.then(() => accounting_action.completedSelect(d, wd)).then(() => cf.test(d, 'accountingCompleted', Config.BASE_URL + '?tab=accountingCompleted', true, _u));
    pc = pc.then(() => accounting_action.PDAccountsClick(d, wd)).then(() => cf.test(d, 'accountingCompleted', Config.BASE_URL + '?tab=accountingCompleted', true, _u));
    pc = pc.then(() => accounting_action.verifyFinalPaymentSelect(d, wd)).then(() => cf.test(d, 'balanceDue', Config.BASE_URL + '?tab=balanceDue', true, _u));
    pc = pc.then(() => accounting_action.PDAccountsClick(d, wd)).then(() => cf.test(d, 'balanceDue', Config.BASE_URL + '?tab=balanceDue', true, _u));
    pc = pc.then(() => accounting_action.deliveredSelect(d, wd)).then(() => cf.test(d, 'accountingDelivered', Config.BASE_URL + '?tab=accountingDelivered', true, _u));
    pc = pc.then(() => accounting_action.PDAccountsClick(d, wd)).then(() => cf.test(d, 'accountingDelivered', Config.BASE_URL + '?tab=accountingDelivered', true, _u));
    pc = pc.then(() => accounting_action.holdSelect(d, wd)).then(() => cf.test(d, 'accountingHold', Config.BASE_URL + '?tab=accountingHold', true, _u));
    pc = pc.then(() => accounting_action.exportClick(d, wd)).then(() => cf.test(d, 'accountingHold', Config.BASE_URL + '?tab=accountingHold', true, _u));
    pc = pc.then(() => accounting_action.PDAccountsClick(d, wd)).then(() => cf.test(d, 'accountingHold', Config.BASE_URL + '?tab=accountingHold', true, _u));
    pc = pc.then(() => accounting_action.signOutSelect(d, wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Accounting login depth flow');
    return pc;
}


function verified_deposite_accounting(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Accounting Verified Deposit flow'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "PDAccounts")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=verifyDeposit', true, _u));

    // START TEST RAIL STEP = C1286
    // GETTING ORDER NO
    pc = pc.then(() => accounting_action.verifyDepositClick(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.orderIdClick(d, wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));

    let orderID;
    pc = pc.then(() => accounting_action.getOpenOrderNo(d, wd)).then((o)=>orderID=o);

    // ENTERING PAYMENT
    pc = pc.then(() => accounting_action.paymentInfoClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentOptionClick(d, wd,0)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.deletePaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));

    pc = pc.then(() => accounting_action.last4DigitsOfCreditCardInput(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    let balanceAmount;
    pc = pc.then(() => accounting_action.getBalanceDue(d, wd)).then((b) => balanceAmount=b);
    pc = pc.then(() => accounting_action.paymentAmountInput(d, wd, '10')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    // pc = pc.then(() => accounting_action.paymentDateSelect(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$'+'10'.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
        'Check 1111']));
    pc = pc.then(() => accounting_action.closePaymentInfo(d,wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.closeOrder(d,wd)).then(() => cf.test(d, 'confirmOrders', Config.BASE_URL + '?tab=confirmOrders', true, _u));

    // VERIFYING DEPOSITE
    pc = pc.then(() => accounting_action.verifyDepositClick(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.searchInput(d, wd,orderID)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    // pc = pc.then(() => accounting_action.paymentBoxClick(d,wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.paymentBox1Click(d,wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.depositSatisfiedSelect(d, wd, 1)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.depositSatisfiedClick(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting_action.verifyDepositClick(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));

    pc = pc.then(() => accounting_action.balanceDueClick(d, wd)).then(() => cf.test(d, 'balanceDue', Config.BASE_URL + '?tab=balanceDue', true, _u));
    pc = pc.then(() => accounting_action.searchInput(d, wd,orderID)).then(() => cf.test(d, 'balanceDue', Config.BASE_URL + '?tab=balanceDue', true, _u));
    pc = pc.then(() => accounting_action.paidFullSelect(d, wd, 1)).then(() => cf.test(d, 'balanceDue', Config.BASE_URL + '?tab=balanceDue', true, _u));
    // pc = pc.then(() => accounting_action.paidFullClick(d, wd)).then(() => cf.test(d, 'balanceDue', Config.BASE_URL + '?tab=balanceDue', true, _u));

    pc = pc.then(() => accounting_action.accountingHoldClick(d, wd)).then(() => cf.test(d, 'accountingHold', Config.BASE_URL + '?tab=accountingHold', true, _u));
    // pc = pc.then(() => accounting_action.searchInput(d, wd,orderID)).then(() => cf.test(d, 'accountingHold', Config.BASE_URL + '?tab=accountingHold', true, _u));
    pc = pc.then(() => accounting_action.orderIdClick(d, wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.PDAccountsClick(d, wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting_action.signOutSelect(d, wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));

    pc = pc.then(() => l1.EmailInput(d, wd, "production")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewAllProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewAllProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuItemClick(d, wd, 4)).then(() => cf.test(d, 'viewHold', Config.BASE_URL + '?tab=viewHold', true, _u));
    // pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd, orderID)).then(() => cf.test(d, 'viewHold', Config.BASE_URL + '?tab=viewHold', true, _u));
    pc = pc.then(() => accounting_action.orderIdClick(d, wd)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuClick(d, wd)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuItemClick(d, wd, 5)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));
    // END TEST RAIL STEP = C1286

    // START TEST RAILS STEP = C1288
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrdersHoldClick(d, wd)).then(() => cf.test(d, 'viewOrdersHold', Config.BASE_URL + '?tab=viewOrdersHold&', true, _u));
    // pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,orderID)).then(() => cf.test(d, 'viewOrdersHold', Config.BASE_URL + '?tab=viewOrdersHold&', true, _u));
    // pc = pc.then(() => quote_ViewOrder.orderIdClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    // pc = pc.then(() => accounting_action.onHoldSelection(d, wd,0)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // END TEST RAILS STEP = C1288
    pc = cf.handleOutput(d, pc, capability, 'Accounting Verified Deposit flow');
    return pc;
}

module.exports = {accounting_login_depth_flow,verified_deposite_accounting};