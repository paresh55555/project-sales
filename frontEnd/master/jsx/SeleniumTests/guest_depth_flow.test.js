const moment = require('moment');
const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const cart = require('./FT/afterLogin/quote/cart');
const leadsMode = require('./FT/afterLogin/leadsMode/leadsMode');
const cf = require('./FT/commonFunctions/cf');
const quote_viewQuote= require('./FT/afterLogin/quote/viewQuotePage');

let _u = undefined;

function guest_login_depth_flow(capability) {
    let d = Config.getDriver(Config.ChromeCapabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'UK: Guest Quote Aluminum'));
    pc = pc.then(() => l1.nameInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=guest', _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "FT" + moment() + "@gmail.com")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=guest', _u, _u));
    pc = pc.then(() => l1.zipInput(d, wd, "000000")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=guest', _u, _u));
    pc = pc.then(() => l1.phoneInput(d, wd, "1234567890")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=guest', _u, _u));
    pc = pc.then(() => l1.leadTypeRadio(d)).then(() => cf.test(d, 'Sales Quoter', _u, Config.BASE_URL + '?tab=guest', _u));
    // pc = pc.then(() => l1.ambassadorNumber(d,wd,'ambassador',12390)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.proceedToQuoteBtn(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Signature Door (Aluminum)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['T.B.D.','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,998.80','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,998.80','powered by','Sales Quoter']));

    // COLOR
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureDoorColorClick(d, wd,'extColor_basicSpecial_all_Apollo White_null_#fcffff_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,998.80','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.signatureDoorColorClick(d, wd,'extColor_basicSpecial_all_Statuary Bronze_null_#352B20_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,998.80','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalDoorColorClick(d, wd, 'extColor-premiumSpecial')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalLilacDoorColorClick(d, wd, 'extColor_premiumSpecial_all_Clear Anodized_null_#D3D3D3_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,498.78','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalDoorColorClick(d, wd, 'extColor-arch')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalLilacDoorColorClick(d, wd, 'extColor_arch_all_Almond_null_#e6dcb8_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,945.46','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalDoorColorClick(d, wd, 'extColor-custom')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.extriorCustomDoorColorInput(d, wd, 'red')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,217.29','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd, 0)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.signatureRalDoorColorClick(d, wd, 'extColor-ral')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalLilacDoorColorClick(d, wd, 'extColor_ral_Yellow_Beige_RAL 1001_#C2B078_undefined')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,945.46','powered by','Sales Quoter']));

    pc = pc.then(() => quote_ColorAndFinish.interiorDoorColorClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.interiorColorClick(d, wd,'intColor_basicSpecial_all_Apollo White_null_#fcffff_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,726.98','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,2000)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorButtonClick(d, wd,'intColor-premiumSpecial')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorColorClick(d, wd,'intColor_premiumSpecial_all_Clear Anodized_null_#D3D3D3_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,476.97','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.interiorButtonClick(d, wd,'intColor-arch')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorColorClick(d, wd,'intColor_arch_all_ANSI 61 Gray_null_#787d7d_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,819.25','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.interiorButtonClick(d, wd,'intColor-ral')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd,2000)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorColorClick(d, wd,'intColor_ral_Yellow_Beige_RAL 1001_#C2B078_undefined')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,945.46','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.interiorButtonClick(d, wd,'intColor-woodStandard')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorColorClick(d, wd,'intColor_woodStandard_all_Oak_null_null_images/colors/wood/WhiteOak121.jpg')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$9,084.38','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.interiorButtonClick(d, wd,'intColor-woodPremium')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorColorClick(d, wd,'intColor_woodPremium_all_Mahogany_null_null_images/colors/wood/Mohagony121.jpg')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$9,355.90','powered by','Sales Quoter']));

    pc = pc.then(() => quote_ColorAndFinish.interiorCustomDoorColorClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorCustomDoorColorInput(d, wd, 'black')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,945.46','powered by','Sales Quoter']));

    // GLASS
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Low-E 3')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,489.31','powered by','Sales Quoter']));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Glass.previousButtonClick(d, wd,'glass','powered by','Sales Quoter')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,366.95','powered by','Sales Quoter']));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Glass.previousButtonClick(d, wd,'glass')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,535.56','powered by','Sales Quoter']));


    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-Miami')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,535.56','powered by','Sales Quoter']));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-Windows White')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,535.56','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d, wd, 0)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));

    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,667.16','powered by','Sales Quoter']));

    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d, ['Signature Door (Aluminum)','100" width x 90" height',
        '101" width x 91" height','3','Left','Left 2 / Right 0','Beige (RAL 1001)','black (custom)',
        'Glass Clear','Argon','New York Nickel','Door Restrictor',
        'Item 1 of 1','$8,667.16','Total $8,667.16']
    )).then(() => cf.delayBySecond(cf.delaySecond.two));


    // ADD DOOR
    pc = pc.then(() => cart.addQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Signature Door (Aluminum)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 80)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,861.94']));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.signatureDoorColorClick(d, wd,'extColor_basicSpecial_all_Apollo White_null_#fcffff_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,861.94']));

    pc = pc.then(() => quote_ColorAndFinish.signatureRalDoorColorClick(d, wd, 'extColor-ral')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureRalLilacDoorColorClick(d, wd, 'extColor_ral_Yellow_Brown beige_RAL 1011_#8A6642_undefined')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,769.30']));

    pc = pc.then(() => quote_ColorAndFinish.interiorDoorColorClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorCustomDoorColorClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorCustomDoorColorInput(d, wd, 'White')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,769.30']));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,139.12']));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,287.04']));

    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,418.64']));

    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d, ['Signature Door (Aluminum)','100" width x 90" height',
        '101" width x 91" height','3','Left','Left 2 / Right 0','Brown beige (RAL 1011)','black (custom)',
        'Glass Clear','Argon','New York Nickel','Door Restrictor',
        'Item 1 of 2','Item 2 of 2','$8,667.16','Total $17,085.80',
        'Signature Door (Aluminum)','100" width x 80" height','101" width x 81" height','3','Left','Left 2 / Right 0',
        'Beige (RAL 1001)','White (custom)','Glass Clear','Argon','New York Nickel','Door Restrictor']
    )).then(() => cf.delayBySecond(cf.delaySecond.two));


    //EMAIL QUOTE
    pc = pc.then(() => cart.emailQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cart.emailAddressesInput(d, wd, 'test@test.com')).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cart.emailMessageInput(d, wd, 'Some test message')).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    // pc = pc.then(() => cart.cancelEmailQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cart.sendEmailClick(d, wd)).then(() => cf.testAlertData(d, 'Error Sending the Email'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Send']));

    pc = pc.then(() => cart.quantityInput(d, wd, 2).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u)));
    pc = pc.then(() => cf.testContentByArray(d, ['Signature Door (Aluminum)','100" width x 90" height',
        '101" width x 91" height','3','Left','Left 2 / Right 0','Brown beige (RAL 1011)','black (custom)',
        'Glass Clear','Argon','New York Nickel','Door Restrictor',
        'Item 1-2 of 3','Item 3-3 of 3','$8,667.16','$17,334.32','Total $25,752.96',
        'Signature Door (Aluminum)','100" width x 80" height','101" width x 81" height','3','Left','Left 2 / Right 0',
        'Brown beige (RAL 1011)','White (custom)','Glass Clear','Argon','New York Nickel','Door Restrictor']
    )).then(() => cf.delayBySecond(cf.delaySecond.two));

    let quoteId, quote;
    pc = pc.then(() => cart.getReferenceQuoteText(d,wd)).then((no) => quoteId=no);
    // pc = pc.then(() => cart.getReferenceQuoteFullText(d,wd)).then((no) => quote=no);
    pc = pc.then(() => cart.startOverClick(d,wd,'startOver-guest'));
    pc = pc.then(() => cf.handleAlertOK(d,wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));

    pc = pc.then(() => cart.SalesPersonMenuClick(d,wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => cart.menuItemClick(d,wd,0)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn&', true, _u));

    pc = pc.then(() => l1.EmailInput(d, wd, "Leads")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewLeads', true, _u));
    pc = pc.then(() => leadsMode.searchInput(d, wd, quoteId)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewLeads', _u, _u));
    pc = pc.then(() => cf.testContentByArray(d, [moment().format('MM/DD/YYYY'),quoteId]));
    //pc = pc.then(() => leadsMode.menuClick(d,wd,'viewLeads')).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.searchInput(d,wd,quoteId)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.checkBoxClick(d, wd)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.transferToClick(d, wd,2)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.transferClick(d, wd)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));

    pc = pc.then(() => leadsMode.orderIdClick(d, wd)).then(() => cf.test(d, 'viewLeadQuote', Config.BASE_URL + '?tab=viewLeadQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Signature Door (Aluminum)','100" width x 90" height',
        '101" width x 91" height','3','Left','Left 2 / Right 0','Brown beige (RAL 1011)','black (custom)',
        'Glass Clear','Argon','New York Nickel','Door Restrictor']
    )).then(() => cf.delayBySecond(cf.delaySecond.two));

    pc = pc.then(() => cart.SalesPersonMenuClick(d,wd)).then(() => cf.test(d, 'viewLeadQuote', Config.BASE_URL + '?tab=viewLeadQuote', true, _u));
    pc = pc.then(() => cart.menuItemClick(d,wd,2)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn&', true, _u));

    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    pc = pc.then(() => quote_viewQuote.searchInput(d, wd,quoteId)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    pc = pc.then(() => quote_viewQuote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Quote Expires on',
        moment().format('MM/DD/YYYY'), moment().add(180,'days').format('MMMM DD,YYYY')]));

    pc = cf.handleOutput(d, pc, capability, 'Guest Quote for Signature Door (Aluminum)');
    return pc;
}

module.exports = {guest_login_depth_flow};
