const moment = require('moment');
const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');

let _u = undefined;

function l1_common_step_for_salesPersonLogin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: SalesPerson Login'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = cf.handleOutput(d, pc, capability, 'salesperson Login');
    return pc;
}

function l1_common_step_for_guestLogin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Guest Login'));
    pc = pc.then(() => l1.nameInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "FT" + moment() + "@gmail.com")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.zipInput(d, wd, "000000")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.phoneInput(d, wd, "1234567890")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.leadTypeRadio(d)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    // pc = pc.then(() => l1.ambassadorNumber(d,wd,'ambassador',12390)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.proceedToQuoteBtn(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = cf.handleOutput(d, pc, capability, 'Guest Login');
    return pc;
}

module.exports = {l1_common_step_for_salesPersonLogin, l1_common_step_for_guestLogin};