const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const commissionsPage = require('./FT/sqAdmin/commissions/commissionsPage');

let _u = undefined;

function edit_commission_by_SQ_admin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Edit commission by SQ-admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "salesmanager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sideBar.commissionClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.dateWidgetClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.startDateClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.startDateSelect(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.goButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));

    // EDIT COMMISSION
    pc = pc.then(() => commissionsPage.commissionRecordClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.editButtonClick(d, wd, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.addCommisionButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.addCommissionStartDateClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.startDateSelect(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.paymentAmountInput(d, wd,10)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.correctionAmountInput(d, wd,5.50)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));

    pc = pc.then(() => commissionsPage.saveClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));

    pc = pc.then(() => commissionsPage.editButtonClick(d, wd, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.deleteClick(d, wd, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.deleteConfirm(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.saveClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Edit commission by SQ-admin');
    return pc;
}

module.exports = {edit_commission_by_SQ_admin};