const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const quote_NewQuote= require('./FT/afterLogin/quote/newQoutes');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const cart = require('./FT/afterLogin/quote/cart');
const moment  = require('moment');

let _u = undefined;

function create_a_absolute_door_laminate_quote_by_questLogin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US:Guest Quote for Absolute Door Laminate'));
    pc = pc.then(() => l1.nameInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=guest', _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "FT" + moment() + "@gmail.com")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=guest', _u, _u));
    pc = pc.then(() => l1.zipInput(d, wd, "000000")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=guest', _u, _u));
    pc = pc.then(() => l1.phoneInput(d, wd, "1234567890")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=guest', _u, _u));
    pc = pc.then(() => l1.leadTypeRadio(d)).then(() => cf.test(d, 'Sales Quoter', _u, Config.BASE_URL + '?tab=guest', _u));
    // pc = pc.then(() => l1.ambassadorNumber(d,wd,'ambassador',12390)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.proceedToQuoteBtn(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Absolute + Door (Laminate)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 200)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['T.B.D.','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-6")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$9,749.26','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$9,749.26','powered by','Sales Quoter']));

    // Start of checking panel ranges

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 50)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-2")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$3,379.24','powered by','Sales Quoter']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,866.93','powered by','Sales Quoter']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-4")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,394.48','powered by','Sales Quoter']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 120)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-5")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,882.14','powered by','Sales Quoter']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 170)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-6")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$9,629.55','powered by','Sales Quoter']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 200)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-6")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$9,749.26','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$9,749.26','powered by','Sales Quoter']));
    // End of checking panel ranges

    // COLOR
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureDoorColorClick(d, wd,'extColor_basic_all_White_null_#FFFFFF_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$9,749.26','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.exteriorLaminateClick(d, wd,'extColor-lam').then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u)));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'extColor_lam_all_Bronze Laminated_null_#534238_null').then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u)));
    pc = pc.then(() => cf.testContentByArray(d, ['$11,475.44','powered by','Sales Quoter']));

    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,100)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.interiorDoorColorClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorWoodStandardDoorColorClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$10,901.73','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,200)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorLaminateClick(d, wd,'intColor-lam')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorColorClick(d, wd,'intColor_lam_all_Bronze Laminated_null_#534238_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$11,475.44','powered by','Sales Quoter']));

    // GLASS
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$12,091.24','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.tabClick(d, wd,'glass')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Low-E 3')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$12,273.01','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.tabClick(d, wd,'glass')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$12,520.26','powered by','Sales Quoter']));

    // HARDWARE
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-London Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$12,642.76','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,0)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));

    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Bronze')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$12,642.76','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,0)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));

    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$12,642.76','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,0)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));

    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-Miami')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$12,520.26','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,0)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));

    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-London Bronze')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$12,642.76','powered by','Sales Quoter']));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d,['Absolute + Door (Laminate)','Vinyl','200" width x 70" height',
        '201" width x 71" height','6','Left','Left 5 / Right 0','Bronze Laminated','Low-E 3',
        'Argon','London Bronze','Door Restrictor',
        'Total $12,642.76','$12,642.76','Item 1 of 1']
    ));

    //EDIT DOOR
    pc = pc.then(() => cart.guestEditButtonClick(d, wd)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,675.62','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-right')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,675.62','powered by','Sales Quoter']));

    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'extColor_lam_all_Black Laminated_null_#000000_null').then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u)));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,675.62','powered by','Sales Quoter']));

    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,587.50','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,200)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));

    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-Windows White')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,465.00','powered by','Sales Quoter']));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d,
        ['Absolute + Door (Laminate)','Vinyl','100" width x 70" height','101" width x 71" height',
        '3','Right','Left 0 / Right 2','Black Laminated','Bronze Laminated','Glass Clear','Argon','Windows White',
        'Door Restrictor','$6,465.00','Total $6,465.00','Item 1 of 1']
    ));

    // ADD DOOR
    pc = pc.then(() => cart.addDoorClick(d, wd,'home')).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Signature Door (Aluminum)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,725.08']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,725.08']));

    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureDoorColorClick(d, wd,'extColor_basicSpecial_all_Statuary Bronze_null_#352B20_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,725.08']));

    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,043.26']));

    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-London Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    //pc = pc.then(() => cf.testContentByArray(d, ['$7,267.22']));

    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute + Door (Laminate)','Vinyl','100" width x 70" height','101" width x 71" height',
        '3','Right','Left 0 / Right 2','Black Laminated','Bronze Laminated','Glass Clear','Argon','Windows White',
        'Door Restrictor','Item 1 of 2','Item 2 of 2',
        'Signature Door (Aluminum)','100" width x 70" height','101" width x 71" height',
        '6','Left','Left 2 / Right 0','Statuary Bronze','Glass Clear','None','London Nickel','Door Restrictor'
    ]));

    //EMAIL QUOTE
    pc = pc.then(() => cart.emailQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cart.emailAddressesInput(d, wd,'yyz@email.com')).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cart.emailMessageInput(d, wd,'testing quote')).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cart.cancelEmailQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cart.quantityInput(d, wd,2)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d,['Absolute + Door (Laminate)','Vinyl','100" width x 70" height',
        '101" width x 71" height','6','Left','Left 0 / Right 2','Bronze Laminated','Glass Clear',
        'Argon','Windows White','Door Restrictor',
        'Item 1-2 of 3','Item 3-3 of 3']
    ));
    pc = pc.then(() => cart.printGuestQuote(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Guest Quote for Absolute Door Laminate');
    return pc;
}

module.exports = {create_a_absolute_door_laminate_quote_by_questLogin};