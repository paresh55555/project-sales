const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const commissionRatePage = require('./FT/sqAdmin/commissions/commissionsRatePage');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
let _u = undefined;

function commission_rate_by_SQ_admin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let url = Config.BASE_URL + 'SQ-admin/sales/rates';
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Manage commission rate by SQ-admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "SalesManager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sideBar.commissionRateClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = pc.then(() => commissionRatePage.addCommissionRateClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = pc.then(() => commissionRatePage.commissionRateInput(d, wd, 'name', 'commission rate test')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.commissionRateInput(d, wd, 'low', 1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.commissionRateInput(d, wd, 'high', 100)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.selectType(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.commissionRateInput(d, wd, 'amount', 200)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.commissionGroupSelect(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.addCommissionRateButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = pc.then(() => commissionRatePage.changeOrderClick(d, 0)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.changeOrderClick(d, 1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.changeOrderClick(d, 2)).then(() => cf.test(d, 'SQ Admin',url, true, _u));
    pc = pc.then(() => commissionRatePage.changeOrderClick(d, 3)).then(() => cf.test(d, 'SQ Admin',url, true, _u));
    pc = pc.then(() => commissionRatePage.changeOrderClick(d, 4)).then(() => cf.test(d, 'SQ Admin',url, true, _u));
    pc = pc.then(() => commissionRatePage.changeOrderClick(d, 5)).then(() => cf.test(d, 'SQ Admin',url, true, _u));
    pc = pc.then(() => commissionRatePage.changeOrderClick(d, 6)).then(() => cf.test(d, 'SQ Admin',url, true, _u));

    pc = pc.then(() => commissionRatePage.searchInput(d, wd, 'commission rate test')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.recordClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = pc.then(() => commissionRatePage.commissionRateInput(d, wd, 'name', 'commission rate edit')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.commissionRateInput(d, wd, 'low', 1)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.commissionRateInput(d, wd, 'high', 50)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.selectType(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.commissionRateInput(d, wd, 'amount', 100)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.commissionGroupSelect(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.addCommissionRateButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = pc.then(() => commissionRatePage.searchInput(d, wd, 'commission rate edit')).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.deleteCommissionClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));
    pc = pc.then(() => commissionRatePage.confirmClick(d, wd)).then(() => cf.test(d, 'SQ Admin', url, true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Manage commission rate by SQ-admin');
    return pc;
}

module.exports = {commission_rate_by_SQ_admin};