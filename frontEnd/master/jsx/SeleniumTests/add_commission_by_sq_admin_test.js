const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const commissionsPage = require('./FT/sqAdmin/commissions/commissionsPage');

let _u = undefined;

function add_commission_by_SQ_admin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Add commission by SQ-admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "SalesManager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sideBar.commissionClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.addCommissionClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.orderInput(d, wd, "US")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.orderIdSelect(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.dateInputClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.dateSelectClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.amountInput(d, wd, 50)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => commissionsPage.correctionInput(d, wd, 10)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$  50.00','$  10.00']));
    pc = pc.then(() => commissionsPage.saveClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = cf.handleOutput(d, pc, capability, 'Add commission by SQ-admin');
    return pc;
}

module.exports = {add_commission_by_SQ_admin};