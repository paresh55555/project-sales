const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/l1');
const accounting_action = require('./FT/afterLogin/accountingActions/accountingActionFlow');
const quote_ViewOrder = require('./FT/afterLogin/order/viewOrder');

const moment = require('moment');

let _u = undefined;

function payment_flow_steps(capability) {
    let d = Config.getDriver(Config.ChromeCapabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Payment Flow by Sales Person'));

    // SALES PERSON LOGIN
    pc = pc.then(() => login.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrdersNeedsPaymentClick(d, wd)).then(() => cf.test(d, 'viewOrdersNeedsPayment', Config.BASE_URL + '?tab=viewOrdersNeedsPayment&', true, _u));
    pc = pc.then(() => quote_ViewOrder.orderIdClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // ENTERING PAYMENT INFO
    pc = pc.then(() => quote_ViewOrder.deleteAllPayment(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting_action.paymentInfoClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));

    pc = pc.then(() => accounting_action.paymentOptionClick(d, wd,0)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.last4DigitsOfCreditCardInput(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentAmountInput(d, wd, '42.5788')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));

    // CHECKING ENTERED DATE AND AMOUNT ON *PAYMENT PAGE*
    pc = pc.then(() => cf.testContentByArray(d, ['$42.58','Check 1111']));
    pc = pc.then(() => quote_ViewOrder.testPaymentDate(d, wd, moment().format('MM/DD/YYYY')));

    pc = pc.then(() => accounting_action.paymentOrderNotesInput(d, wd, 'Payment Order Notes test')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.closePaymentInfo(d, wd)).then(() => cf.test(d, 'viewOrder',  Config.BASE_URL + '?tab=viewOrder', true, _u));

    // CHECKING ENTERED DATE AND AMOUNT ON *ORDER PAGE*
    pc = pc.then(() => cf.testContentByArray(d, ['$42.58','Check 1111']));
    pc = pc.then(() => quote_ViewOrder.testPaymentDate(d, wd, moment().format('MM/DD/YYYY')));

    // CHECKING BALANCE DUE
    let balanceDue;
    pc = pc.then(() => quote_ViewOrder.deleteAllPayment(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting_action.paymentInfoClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo&', true, _u));
    pc = pc.then(() => accounting_action.getBalanceDue(d, wd)).then((b) => balanceDue = b );
    pc = pc.then(() => accounting_action.paymentOptionClick(d, wd,0)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.last4DigitsOfCreditCardInput(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentAmountInput(d, wd, balanceDue)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting_action.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));

    pc = pc.then(() => accounting_action.closePaymentInfo(d, wd)).then(() => cf.test(d, 'viewOrder',  Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => quote_ViewOrder.testPaymentDate(d, wd, moment().format('MM/DD/YYYY')));

    pc = pc.then(() => cf.testContentByArray(d, ['$0.00']));
    pc = pc.then(() => quote_ViewOrder.testBalanceDue(d,wd, '$0.00'));

    // CHECKING MAXIMUM LIMIT OF PAYMENT
    pc = pc.then(() => accounting_action.paymentInfoClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo&', true, _u));
    pc = pc.then(() => quote_ViewOrder.deleteAllPayment(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo&', true, _u));

    let optionsArray = ['Check','Visa','MasterCard','Amex','Cash','Wire'];

    for(let i = 0; i < optionsArray.length; i++)
    {
        if(i<5) {
            pc = pc.then(() => accounting_action.paymentOptionClick(d, wd,i)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
            pc = pc.then(() => accounting_action.last4DigitsOfCreditCardInput(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
            pc = pc.then(() => accounting_action.paymentAmountInput(d, wd, '10')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
            pc = pc.then(() => accounting_action.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
            pc = pc.then(() => accounting_action.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
            pc = pc.then(() => cf.testContentByArray(d, [moment().format('MM/DD/YYYY'),'$10.00',`${optionsArray[i]} 1111`]));
        }
        if(i===5) {
            pc = pc.then(() => accounting_action.getBalanceDue(d, wd)).then((b) => balanceDue = b );
            pc = pc.then(() => accounting_action.paymentOptionClick(d, wd,i)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
            pc = pc.then(() => accounting_action.last4DigitsOfCreditCardInput(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
            pc = pc.then(() => accounting_action.paymentAmountInput(d, wd, balanceDue)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
            pc = pc.then(() => accounting_action.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
            pc = pc.then(() => accounting_action.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
            pc = pc.then(() => cf.testContentByArray(d, [moment().format('MM/DD/YYYY'),'$10.00',`${optionsArray[i]} 1111`]));

        }
    }
    pc = pc.then(() => cf.testContentByArray(d, ['$0.00']));
    pc = pc.then(() => quote_ViewOrder.testBalanceDue(d,wd, '$0.00'));

    pc = pc.then(() => accounting_action.addPaymentClick(d, wd)).then(() => cf.testAlertData(d, 'Sorry you can only add a maximum of 6 payments'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));

    pc = pc.then(() => accounting_action.closePaymentInfo(d, wd)).then(() => cf.test(d, 'viewOrder',  Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => quote_ViewOrder.testPaymentDate(d, wd, moment().format('MM/DD/YYYY')));

    pc = pc.then(() => cf.testContentByArray(d, ['$0.00']));
    pc = pc.then(() => quote_ViewOrder.testBalanceDue(d,wd, '$0.00'));
    pc = pc.then(() => quote_ViewOrder.deleteAllPayment(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Payment Flow by Sales Person');
    return pc;
}

module.exports = {payment_flow_steps};