const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const minMaxSizesPage = require('./FT/sqAdmin/module/minMaxSizesPage');

let _u = undefined;

function min_max_sizes_flow_by_sqadmin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Min Max Sizes by sales person'));
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));

    pc = pc.then(() => sideBar.modulesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
    pc = pc.then(() => minMaxSizesPage.vinylwithAluminumBlockFrameClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => minMaxSizesPage.minMaxSizeClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/module/', true, _u));
    pc = pc.then(() => minMaxSizesPage.cancelMinMaxSizeClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => minMaxSizesPage.minMaxSizeClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/module/', true, _u));
    pc = pc.then(() => minMaxSizesPage.optionNameInput(d, wd, 23, 'Min Width', 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/module/', true, _u));
    pc = pc.then(() => minMaxSizesPage.optionNameInput(d, wd, 245, 'Max Width', 1 )).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/module/', true, _u));
    pc = pc.then(() => minMaxSizesPage.optionNameInput(d, wd, 16, 'Min Height', 2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/module/', true, _u));
    pc = pc.then(() => minMaxSizesPage.optionNameInput(d, wd, 98, 'Max Height', 3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/module/', true, _u));
    pc = pc.then(() => minMaxSizesPage.saveMinMaxSizeClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Min Max Sizes by sales person');
    return pc;
}

module.exports = { min_max_sizes_flow_by_sqadmin };