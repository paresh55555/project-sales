const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sidebar = require('./FT/sqAdmin/sideBar/sideBar');
const orderPage = require('./FT/sqAdmin/OrderReports/orderReports');
const moment = require('moment');
let _u = undefined;

function order_system_reports_by_sq_admin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US:Order System Reports by SQ-admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "SalesManager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sidebar.orderSystemReportClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    // DATE WISE FIlTER
    let currentMonthDate1 = moment().date(1).format('MM/DD/YYYY');
    let currentMonthDate2 = moment().endOf('month').format('MM/DD/YYYY');
    let previousMonthDate1 = moment().subtract(1, 'months').date(1).format('MM/DD/YYYY');
    let previousMonthDate2 = moment().subtract(1,'months').endOf('month').format('MM/DD/YYYY');
    let currentYearDate1 = moment().startOf('year').format('MM/DD/YYYY');
    let currentYearDate2 = moment().endOf('year').format('MM/DD/YYYY');
    let previousYearDate1 = moment().startOf('year').subtract(1,'year').format('MM/DD/YYYY');
    let previousYearDate2 = moment().endOf('year').subtract(1,'year').format('MM/DD/YYYY');

    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [previousMonthDate1, previousMonthDate2]));
    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [currentMonthDate1, currentMonthDate2]));
    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [currentYearDate1, currentYearDate2]));
    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [previousYearDate1, previousYearDate2]));

    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.datePickerClick(d, wd,'toDatePicker')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.dayClick(d, wd,'react-datepicker__day--tue')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.datePickerClick(d, wd,'fromDatePicker')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.dayClick(d, wd,'react-datepicker__day--tue')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.goButton(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [currentYearDate1, currentYearDate2]));

    // SALES PERSON WISE FIlTER
    pc = pc.then(() => orderPage.filterClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.searchSalesPersonInput(d, wd,'a')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,0,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,0,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,0,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.clearButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.closeIconClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Filter by Sales Person']));
    pc = pc.then(() => orderPage.filterClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.searchSalesPersonInput(d, wd,'a')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,0,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Sales Person - ']));
    pc = pc.then(() => orderPage.clearIconClick(d, wd,'reportFilterInnerCon',1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    // LOCATION WISE FIlTER

    // CUSTOMER LOCATION
    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.searchCityInput(d, wd,'a')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.selectCity(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterCityClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['city - ']));
    pc = pc.then(() => orderPage.clearIconClick(d, wd,'locationFilterIcon',0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Filter by Location']));

    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.radioClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.searchStateInput(d, wd,'a')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.selectCity(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterCityClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['state - ']));
    pc = pc.then(() => orderPage.clearIconClick(d, wd,'locationFilterIcon',0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Filter by Location']));

    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.radioClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.searchZipInput(d, wd,'30307')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterCityClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Zipcode - 30307']));
    pc = pc.then(() => orderPage.clearIconClick(d, wd,'locationFilterIcon',0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Filter by Location']));


    // BILLING LOCATION
    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.locationFilterTabClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.radioClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.searchCityInput(d, wd,'a')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.selectCity(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterCityClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['city - ']));
    pc = pc.then(() => orderPage.clearIconClick(d, wd,'locationFilterIcon',0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Filter by Location']));

    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.locationFilterTabClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.radioClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.searchStateInput(d, wd,'a')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.selectCity(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterCityClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['state - ']));
    pc = pc.then(() => orderPage.clearIconClick(d, wd,'locationFilterIcon',0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Filter by Location']));

    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.locationFilterTabClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.radioClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.searchZipInput(d, wd,30307)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterCityClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Zipcode - 30307']));
    pc = pc.then(() => orderPage.clearIconClick(d, wd,'locationFilterIcon',0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Filter by Location']));

    // SHIPPING LOCATION
    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.locationFilterTabClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.radioClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.searchCityInput(d, wd,'a')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.selectCity(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterCityClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['city - ']));
    pc = pc.then(() => orderPage.clearIconClick(d, wd,'locationFilterIcon',0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Filter by Location']));

    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.locationFilterTabClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.radioClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.searchStateInput(d, wd,'a')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.selectCity(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterCityClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['state - ']));
    pc = pc.then(() => orderPage.clearIconClick(d, wd,'locationFilterIcon',0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Filter by Location']));

    pc = pc.then(() => orderPage.filterClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.locationFilterTabClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.radioClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.searchZipInput(d, wd,30307)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterCityClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Zipcode - 30307']));
    pc = pc.then(() => orderPage.clearIconClick(d, wd,'locationFilterIcon',0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Filter by Location']));

    // ORDER STATUS WISE FILTER
    pc = pc.then(() => orderPage.filterClick(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.selectOrderStatus(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.selectOrderStatus(d, wd, 2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterSelectOrderStatusClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Order Status - order,Confirming Payment']));
    pc = pc.then(() => orderPage.clearIconClick(d, wd,'orderFilterIcon',0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterClick(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.selectOrderStatus(d, wd, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterSelectOrderStatusClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Order Status - order,Confirming Payment,Ready For Production,In Production,Completed,Delivered']));
    pc = pc.then(() => orderPage.clearIconClick(d, wd,'orderFilterIcon',0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterClick(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.selectOrderStatus(d, wd, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.clearIconClick(d, wd,'orderFilterIcon',0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Filter by Order Status']));

    //SORTING
    pc = pc.then(() => orderPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,6)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,6)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,7)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,7)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.scrollLeft(d, wd,900)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,8)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,8)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,9)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,9)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,10)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,10)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,11)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,11)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,12)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,12)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,13)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,13)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,14)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,14)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,15)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,15)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,16)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,16)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,17)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,17)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.scrollLeft(d, wd,1800)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,18)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,18)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    // DATA COLUMN FILTER
    pc = pc.then(() => orderPage.filterDataColumnClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    // SHIPPING COLUMN
    pc = pc.then(() => orderPage.dataColumnScroll(d, wd,400)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,1,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,1,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,1,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,1,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,1,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,1,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,1,6)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,1,7)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterDataColumnsButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    // CHECK FILTER SHIPPING COLUMN USING SORTING
    pc = pc.then(() => orderPage.scrollLeft(d, wd,1800)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,19)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,19)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,20)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,20)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,21)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,21)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,22)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,22)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,23)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,23)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,24)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,24)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,25)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,25)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,26)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,26)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    // BILLING COLUMN
    pc = pc.then(() => orderPage.filterDataColumnClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.dataColumnScroll(d, wd,600)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,2,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,2,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,2,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,2,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,2,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,2,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,2,6)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,2,7)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterDataColumnsButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    // CHECK FILTER BILLING COLUMN USING SORTING
    pc = pc.then(() => orderPage.scrollLeft(d, wd,2500)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,27)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,27)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,28)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,28)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,29)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,29)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,30)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,30)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,31)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,31)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,32)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,32)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,33)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,33)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,34)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,34)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    // CUSTOMER COLUMN
    pc = pc.then(() => orderPage.filterDataColumnClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.dataColumnScroll(d, wd,400)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,3,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,3,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,3,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,3,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,3,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,3,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,3,6)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.checkboxClick(d, wd,3,7)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterDataColumnsButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    // CHECK FILTER CUSTOMER COLUMN USING SORTING
    pc = pc.then(() => orderPage.scrollLeft(d, wd,1800)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,35)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,35)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,36)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,36)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,37)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,37)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,38)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,38)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,39)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,39)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,40)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,40)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,41)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,41)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,42)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,42)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    // SELECT DEFAULT DATA COLUMNS
    pc = pc.then(() => orderPage.filterDataColumnClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.defaultButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.filterDataColumnsButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    // SELECT ALL COLUMN
    // pc = pc.then(() => orderPage.selectAllButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    // pc = pc.then(() => orderPage.filterDataColumnsButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    // GROUP BY WISE FILTER
    pc = pc.then(() => orderPage.filterClick(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.groupBySelect(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Sales Person']));
    pc = pc.then(() => orderPage.filterClick(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.groupBySelect(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Shipping State']));
    pc = pc.then(() => orderPage.filterClick(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.groupBySelect(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Order Status']));
    pc = pc.then(() => orderPage.filterClick(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.groupBySelect(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Group by']));

    //PAGINATION
    pc = pc.then(() => orderPage.paginationEntryClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.paginationEntryClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.paginationClick(d, wd,'activeNext')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));
    pc = pc.then(() => orderPage.paginationClick(d, wd,'activePrevious')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = pc.then(() => orderPage.exportClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/ordersystemsreport', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Order System Reports by SQ-admin');
    return pc;
}

module.exports = {order_system_reports_by_sq_admin};