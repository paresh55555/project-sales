const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const taxOverride = require('./FT/sqAdmin/TaxOverride/taxOverride');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');

let _u = undefined;

function tax_override_by_SQ_admin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Tax Override by SQ-admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "SalesManager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sideBar.taxOverride(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));

    let stateName;
    // ADD TAX OVERRIDE
    pc = pc.then(() => taxOverride.addTaxClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Add Tax Override']));
    pc = pc.then(() => taxOverride.saveClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Please select state']));
    pc = pc.then(() => taxOverride.stateInput(d, wd,'a')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.selectState(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.getStateName(d, wd)).then((s) => stateName = s);
    pc = pc.then(() => taxOverride.taxOverrideInput(d, wd,10)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.saveClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [stateName,'10']));

    // SEARCH ADDED STATE
    pc = pc.then(() => taxOverride.searchInput(d, wd,stateName)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.recordClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));

    // EDIT TAX OVERRIDE
    pc = pc.then(() => cf.testContentByArray(d, ['Edit Tax Override','Update Override','Delete']));
    pc = pc.then(() => taxOverride.taxOverrideInput(d, wd,20)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.saveClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [stateName,'20']));

    // SEARCH EDITED STATE
    pc = pc.then(() => taxOverride.searchInput(d, wd,stateName)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.recordClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));

    // SORTING
    pc = pc.then(() => taxOverride.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));

    //DELETE QUOTE
    pc = pc.then(() => taxOverride.deleteClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));
    pc = pc.then(() => taxOverride.deleteConfirm(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/taxOverride', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Tax Override by SQ-admin');
    return pc;
}

module.exports = {tax_override_by_SQ_admin};