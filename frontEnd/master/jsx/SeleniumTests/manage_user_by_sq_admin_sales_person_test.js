const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const usersPage = require('./FT/sqAdmin/users/usersPage');

let _u = undefined;

function add_user_by_SQ_admin_sales_person(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Manage user by SQ-admin admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));

    //ADD USER
    pc = pc.then(() => usersPage.addUserClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => usersPage.prefixInput(d, wd, 'user')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => usersPage.commissionGroupSelect(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => usersPage.nameInput(d, wd, 'user name')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => usersPage.userNameInput(d, wd, 'user.name' + Number(new Date()))).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => usersPage.passwordInput(d, wd, 'somePassword')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => usersPage.addUserButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));

    //SEARCH
    pc = pc.then(() => usersPage.searchInput(d, wd, 'user.name')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));

    //EDIT
    pc = pc.then(() => usersPage.selectUserClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => usersPage.commissionGroupSelect(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => usersPage.nameInput(d, wd, 'user.name edit')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => usersPage.discountInput(d, wd, 10)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => usersPage.addUserButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));

    //DELETE
    pc = pc.then(() => usersPage.searchInput(d, wd, 'user.name edit')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => usersPage.selectUserClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => usersPage.deleteUserClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => usersPage.confirmClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL, true, _u));
    pc = pc.then(() => usersPage.confirmClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL, true, _u));

    //SORTING
    pc = pc.then(() => usersPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL, true, _u));
    pc = pc.then(() => usersPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL, true, _u));
    pc = pc.then(() => usersPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL, true, _u));
    pc = pc.then(() => usersPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL, true, _u));
    pc = pc.then(() => usersPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL, true, _u));
    pc = pc.then(() => usersPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL, true, _u));
    pc = pc.then(() => usersPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL, true, _u));
    pc = pc.then(() => usersPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL, true, _u));
    pc = pc.then(() => usersPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL, true, _u));
    pc = pc.then(() => usersPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL, true, _u));
    pc = pc.then(() => usersPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL, true, _u));
    pc = pc.then(() => usersPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL, true, _u));


    pc = cf.handleOutput(d, pc, capability, 'Manage user by SQ-admin admin');
    return pc;
}

module.exports = {add_user_by_SQ_admin_sales_person};