const moment = require('moment');
const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_NewQuote= require('./FT/afterLogin/quote/newQoutes');
const quote_WindowType= require('./FT/afterLogin/quote/windowType');
const accounting = require('./FT/afterLogin/accountingActions/accountingActionFlow');
const cart = require('./FT/afterLogin/quote/cart');

const cf = require('./FT/commonFunctions/cf');
let _u = undefined;

function screen_selection_by_sales_person(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Screen selection by sales Person'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => quote_NewQuote.newQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));

    pc = pc.then(() => cf.testModuleData(d, 'Maximum Size:', 'Width 32 feet, Height 10 feet', 'Screens'));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Screens')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 1)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [`*SMART Doors have a maximum width of ${32*12} inches and minimum width of 24 inches`]));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 1)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [`*SMART Doors have a maximum height of ${10*12} inches and a minimum height of 24 inches`]));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 50)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 30)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Left")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$171.88']));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Right")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$171.88']));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Split")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$171.88']));

    //COLOR
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'extColor_basic_all_Apollo White_null_#fcffff_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$171.88']));

    pc = pc.then(() => quote_ColorAndFinish.premiumDoorColorClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.premiumSubDoorColorClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$171.88']));

    pc = pc.then(() => quote_ColorAndFinish.colorGroupButtonClick(d, wd,'extColor-arch')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'extColor_arch_all_Almond_null_#e6dcb8_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$171.88']));

    pc = pc.then(() => quote_ColorAndFinish.colorGroupButtonClick(d, wd,'extColor-ral')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'extColor_ral_Yellow_Green beige_RAL 1000_#BEBD7F_undefined')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$171.88']));

    pc = pc.then(() => quote_ColorAndFinish.colorGroupButtonClick(d, wd,'extColor-custom')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.extriorCustomDoorColorInput(d, wd,'red')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$171.88']));

    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d,['Screens','50" width x 30" height','red (custom)','Panoramic Screen Split',
        '50" width x 30" height','51" width x 31" height','1','0.00"','26.25"','None','none','Left 0 / Right 0','Aluminum Block','red (custom)',
        'None','Door Restrictor','Item 1 of 1','$171.88','Total $171.88']
    ));

    pc = pc.then(() => accounting.quoteNotesInputName(d, wd,'this is sample notes')).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => accounting.productionNotesInput(d, wd,'this is production sample notes')).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => accounting.scroll(d, wd,0)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cart.quantityInput(d, wd, 2)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d,['Screens','50" width x 30" height','red (custom)','Panoramic Screen Split',
        '50" width x 30" height','51" width x 31" height','1','0.00"','26.25"','None','none','Left 0 / Right 0','Aluminum Block','red (custom)',
        'None','Door Restrictor','Item 1-2 of 2','$343.76','$171.88','Total $343.76']
    ));
    pc = pc.then(() => cart.duplicateClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d,['Screens','50" width x 30" height','red (custom)','Panoramic Screen Split',
        '50" width x 30" height','51" width x 31" height','1','0.00"','26.25"','None','none','Left 0 / Right 0','Aluminum Block','red (custom)',
        'None','Door Restrictor','Item 1-2 of 4','Item 3-4 of 4','$343.76','$171.88','Total $687.52']
    ));

    // ADD DOOR
    pc = pc.then(() => cart.addDoorClick(d, wd,'home-sales')).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Signature Door (Aluminum)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, "track-3/4 Upstand")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,725.08']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,725.08']));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,725.08']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingInOrOut-Outswing')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,725.08']));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,'screens-Panoramic Screen Split')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,527.16']));

    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureDoorColorClick(d, wd,'extColor_basicSpecial_all_Statuary Bronze_null_#352B20_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,527.16']));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,845.34']));

    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,972.60']));

    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-London Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,104.20']));

    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d,['Screens','50" width x 30" height','red (custom)','Panoramic Screen Split',
        '50" width x 30" height','51" width x 31" height','1','0.00"','26.25"','None','none','Left 0 / Right 0','Aluminum Block','red (custom)',
        'None','Door Restrictor','Item 1-2 of 5','Item 3-4 of 5','Item 5 of 5','$343.76','$171.88','Total $8,791.72','$8,104.20',
        'Signature Door (Aluminum)','100" width x 70" height','101" width x 71" height','3','31.44"','65.5"','Left','Outswing','Left 2 / Right 0',
        'Aluminum Block','3/4 Upstand','Statuary Bronze','Statuary Bronze','Glass Clear','Argon','London Nickel','Door Restrictor','Panoramic Screen Split']
    ));
    pc = pc.then(() => cart.saveAsQuote(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['5']));

    pc = cf.handleOutput(d, pc, capability, 'Screen Selection by salesPerson');
    return pc;
}

module.exports = {screen_selection_by_sales_person};
