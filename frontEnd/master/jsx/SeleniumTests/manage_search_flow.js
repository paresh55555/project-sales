const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const cart = require('./FT/afterLogin/quote/cart');
const view_quote = require('./FT/afterLogin/quote/viewQuotePage');
const accounting = require('./FT/afterLogin/accountingActions/accountingActionFlow');
const quote_ViewOrder= require('./FT/afterLogin/order/viewOrder');
const leadsMode = require('./FT/afterLogin/leadsMode/leadsMode');

const moment = require('moment');
let _u = undefined;

function search_flow(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Search flow in Grids'));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));

    // SALES PERSON SEARCH
    pc = salespersonSearchFlow(pc, d, wd);

    // PRODUCTION SEARCH
    pc = pc.then(() => l1.salesPersonLogout(d,wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));
    pc = productionSearchFlow(pc, d, wd);

    // ACCOUNTING SEARCH
    pc = pc.then(() => l1.salesPersonLogout(d,wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));
    pc = accountingSearchFlow(pc, d, wd);

    // LEADS SEARCH
    pc = pc.then(() => l1.salesPersonLogout(d,wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));
    pc = leadsSearchFlow(pc, d, wd);

    pc = cf.handleOutput(d, pc, capability, 'Search flow in Grids');
    return pc;
}

function salespersonSearchFlow(pc, d, wd) {

    // SEARCH AT SALES PERSON SITE
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    // VIEW QUOTE SEARCHING
    let quoteNo;
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'quoteRow ')).then((quote) => quoteNo = quote );
    pc = pc.then(() => view_quote.searchInput(d, wd, quoteNo)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    pc = pc.then(() => view_quote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,4)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));

    pc = pc.then(() => view_quote.newButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesNew', Config.BASE_URL + '?tab=viewQuotesNew&', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'quoteRow ')).then((quote) => quoteNo = quote );
    pc = pc.then(() => view_quote.searchInput(d, wd, quoteNo)).then(() => cf.test(d, 'viewQuotesNew', Config.BASE_URL + '?tab=viewQuotesNew&', true, _u));
    pc = pc.then(() => view_quote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,4)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));

    pc = pc.then(() => view_quote.hotButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesHot', Config.BASE_URL + '?tab=viewQuotesHot&', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'quoteRow ')).then((quote) => quoteNo = quote );
    pc = pc.then(() => view_quote.searchInput(d, wd, quoteNo)).then(() => cf.test(d, 'viewQuotesHot', Config.BASE_URL + '?tab=viewQuotesHot', true, _u));
    pc = pc.then(() => view_quote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,4)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));

    pc = pc.then(() => view_quote.mediumButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'quoteRow ')).then((quote) => quoteNo = quote );
    pc = pc.then(() => view_quote.searchInput(d, wd, quoteNo)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    pc = pc.then(() => view_quote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,4)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));

    pc = pc.then(() => view_quote.coldButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesCold', Config.BASE_URL + '?tab=viewQuotesCold&', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'quoteRow ')).then((quote) => quoteNo = quote );
    pc = pc.then(() => view_quote.searchInput(d, wd, quoteNo)).then(() => cf.test(d, 'viewQuotesCold', Config.BASE_URL + '?tab=viewQuotesCold', true, _u));
    pc = pc.then(() => view_quote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,4)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));

    pc = pc.then(() => view_quote.holdButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesHold', Config.BASE_URL + '?tab=viewQuotesHold&', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'quoteRow ')).then((quote) => quoteNo = quote );
    pc = pc.then(() => view_quote.searchInput(d, wd, quoteNo)).then(() => cf.test(d, 'viewQuotesHold', Config.BASE_URL + '?tab=viewQuotesHold', true, _u));
    pc = pc.then(() => view_quote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,4)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));

    pc = pc.then(() => view_quote.archivedButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesArchived', Config.BASE_URL + '?tab=viewQuotesArchived&', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'quoteRow ')).then((quote) => quoteNo = quote );
    pc = pc.then(() => view_quote.searchInput(d, wd, quoteNo)).then(() => cf.test(d, 'viewQuotesArchived', Config.BASE_URL + '?tab=viewQuotesArchived', true, _u));
    pc = pc.then(() => view_quote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,4)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));

    // VIEW ORDER SEARCHING
    let orderNo;
    pc = pc.then(() => quote_ViewOrder.viewOrderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'orderRow ')).then((order) => orderNo = order);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd, orderNo)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.checkedSearchedData(d, wd, 'orderRow ', orderNo)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewOrdersNeedsPaymentClick(d, wd)).then(() => cf.test(d, 'viewOrdersNeedsPayment', Config.BASE_URL + '?tab=viewOrdersNeedsPayment&', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'orderRow ')).then((order) => orderNo = order);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd, orderNo)).then(() => cf.test(d, 'viewOrdersNeedsPayment', Config.BASE_URL + '?tab=viewOrdersNeedsPayment&', true, _u));
    pc = pc.then(() => quote_ViewOrder.checkedSearchedData(d, wd, 'orderRow ', orderNo)).then(() => cf.test(d, 'viewOrdersNeedsPayment', Config.BASE_URL + '?tab=viewOrdersNeedsPayment&', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewOrdersHoldClick(d, wd)).then(() => cf.test(d, 'viewOrdersHold', Config.BASE_URL + '?tab=viewOrdersHold&', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'orderRow ')).then((order) => orderNo = order);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd, orderNo)).then(() => cf.test(d, 'viewOrdersHold', Config.BASE_URL + '?tab=viewOrdersHold&', true, _u));
    pc = pc.then(() => quote_ViewOrder.checkedSearchedData(d, wd, 'orderRow ', orderNo)).then(() => cf.test(d, 'viewOrdersHold', Config.BASE_URL + '?tab=viewOrdersHold&', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewOrdersAccountingClick(d, wd)).then(() => cf.test(d, 'viewOrdersAccounting', Config.BASE_URL + '?tab=viewOrdersAccounting&', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'orderRow ')).then((order) => orderNo = order);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd, orderNo)).then(() => cf.test(d, 'viewOrdersAccounting', Config.BASE_URL + '?tab=viewOrdersAccounting&', true, _u));
    pc = pc.then(() => quote_ViewOrder.checkedSearchedData(d, wd, 'orderRow ', orderNo)).then(() => cf.test(d, 'viewOrdersAccounting', Config.BASE_URL + '?tab=viewOrdersAccounting&', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewOrdersPreProductionClick(d, wd)).then(() => cf.test(d, 'viewOrdersPreProduction', Config.BASE_URL + '?tab=viewOrdersPreProduction&', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'orderRow ')).then((order) => orderNo = order);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd, orderNo)).then(() => cf.test(d, 'viewOrdersPreProduction', Config.BASE_URL + '?tab=viewOrdersPreProduction&', true, _u));
    pc = pc.then(() => quote_ViewOrder.checkedSearchedData(d, wd, 'orderRow ', orderNo)).then(() => cf.test(d, 'viewOrdersPreProduction', Config.BASE_URL + '?tab=viewOrdersPreProduction&', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewOrdersProductionClick(d, wd)).then(() => cf.test(d, 'viewOrdersProduction', Config.BASE_URL + '?tab=viewOrdersProduction&', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'orderRow ')).then((order) => orderNo = order);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd, orderNo)).then(() => cf.test(d, 'viewOrdersProduction', Config.BASE_URL + '?tab=viewOrdersProduction&', true, _u));
    pc = pc.then(() => quote_ViewOrder.checkedSearchedData(d, wd, 'orderRow ', orderNo)).then(() => cf.test(d, 'viewOrdersProduction', Config.BASE_URL + '?tab=viewOrdersProduction&', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewOrdersCompletedClick(d, wd)).then(() => cf.test(d, 'viewOrdersCompleted', Config.BASE_URL + '?tab=viewOrdersCompleted&', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'orderRow ')).then((order) => orderNo = order);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd, orderNo)).then(() => cf.test(d, 'viewOrdersCompleted', Config.BASE_URL + '?tab=viewOrdersCompleted&', true, _u));
    pc = pc.then(() => quote_ViewOrder.checkedSearchedData(d, wd, 'orderRow ', orderNo)).then(() => cf.test(d, 'viewOrdersCompleted', Config.BASE_URL + '?tab=viewOrdersCompleted&', true, _u));

    // pc = pc.then(() => quote_ViewOrder.viewOrdersDeliveredClick(d, wd)).then(() => cf.test(d, 'viewOrdersDelivered', Config.BASE_URL + '?tab=viewOrdersDelivered&', true, _u));
    // pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'orderRow ')).then((order) => orderNo = order);
    // pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd, orderNo)).then(() => cf.test(d, 'viewOrdersDelivered', Config.BASE_URL + '?tab=viewOrdersDelivered&', true, _u));
    // pc = pc.then(() => quote_ViewOrder.checkedSearchedData(d, wd, 'orderRow ', orderNo)).then(() => cf.test(d, 'viewOrdersDelivered', Config.BASE_URL + '?tab=viewOrdersDelivered&', true, _u));

    return pc;
}

function productionSearchFlow(pc,d,wd){

    // SEARCH AT PRODUCTION SITE
    pc = pc.then(() => l1.EmailInput(d, wd, "production")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewAllProduction', true, _u));
    //SEARCH IN ALL THE TABS
    let porderNo;
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => porderNo = o);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,porderNo)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewAllProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.checkedSearchedData(d, wd,'accountRow',porderNo)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewAllProduction', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewPreProductionClick(d, wd)).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => porderNo = o);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,porderNo)).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.checkedSearchedData(d, wd,'accountRow',porderNo)).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.orderNumberClick(d, wd)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,3)).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewProductionClick(d, wd)).then(() => cf.test(d, 'viewProduction', Config.BASE_URL + '?tab=viewProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => porderNo = o);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,porderNo)).then(() => cf.test(d, 'viewProduction', Config.BASE_URL + '?tab=viewProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.checkedSearchedData(d, wd,'accountRow',porderNo)).then(() => cf.test(d, 'viewProduction', Config.BASE_URL + '?tab=viewProduction', true, _u));
    // pc = pc.then(() => quote_ViewOrder.orderNumberClick(d, wd)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    // pc = pc.then(() => accounting.chooseActionClick(d, wd,3)).then(() => cf.test(d, 'viewProduction', Config.BASE_URL + '?tab=viewProduction', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewCompletedClick(d, wd)).then(() => cf.test(d, 'viewCompleted', Config.BASE_URL + '?tab=viewCompleted', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => porderNo = o);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,porderNo)).then(() => cf.test(d, 'viewCompleted', Config.BASE_URL + '?tab=viewCompleted', true, _u));
    pc = pc.then(() => quote_ViewOrder.checkedSearchedData(d, wd,'accountRow',porderNo)).then(() => cf.test(d, 'viewCompleted', Config.BASE_URL + '?tab=viewCompleted', true, _u));
    pc = pc.then(() => quote_ViewOrder.orderNumberClick(d, wd)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,3)).then(() => cf.test(d, 'viewCompleted', Config.BASE_URL + '?tab=viewCompleted', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewDeliveredClick(d, wd)).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => porderNo = o);
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,porderNo)).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));
    pc = pc.then(() => quote_ViewOrder.checkedSearchedData(d, wd,'accountRow',porderNo)).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));
    pc = pc.then(() => quote_ViewOrder.orderNumberClick(d, wd)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,3)).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));

    // pc = pc.then(() => quote_ViewOrder.viewHoldClick(d, wd)).then(() => cf.test(d, 'viewHold', Config.BASE_URL + '?tab=viewHold', true, _u));
    // pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => porderNo = o);
    // pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,porderNo)).then(() => cf.test(d, 'viewHold', Config.BASE_URL + '?tab=viewHold', true, _u));
    // pc = pc.then(() => quote_ViewOrder.checkedSearchedData(d, wd,'accountRow',porderNo)).then(() => cf.test(d, 'viewHold', Config.BASE_URL + '?tab=viewHold', true, _u));
    // pc = pc.then(() => quote_ViewOrder.orderNumberClick(d, wd)).then(() => cf.test(d, 'viewProductionOrder', Config.BASE_URL + '?tab=viewProductionOrder', true, _u));
    // pc = pc.then(() => accounting.chooseActionClick(d, wd,3)).then(() => cf.test(d, 'viewHold', Config.BASE_URL + '?tab=viewHold', true, _u));
    return pc;
}

function accountingSearchFlow(pc,d,wd) {

    // SEARCH AT ACCOUNTING SITE
    pc = pc.then(() => l1.EmailInput(d, wd, "PDAccounts")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    // SEARCH IN ALL THE TABS
    let porderNo;
    pc = pc.then(() => accounting.accountingAllClick(d, wd)).then(() => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => porderNo = o);
    pc = pc.then(() => accounting.searchInput(d, wd,porderNo)).then(() => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));
    pc = pc.then(() => accounting.orderIdClick(d, wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,0)).then(() => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));

    pc = pc.then(() => accounting.verifyDepositClick(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => porderNo = o);
    pc = pc.then(() => accounting.searchInput(d, wd,porderNo)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting.orderIdClick(d, wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,0)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));

    pc = pc.then(() => accounting.accountingPreProductionClick(d, wd)).then(() => cf.test(d, 'accountingPreProduction', Config.BASE_URL + '?tab=accountingPreProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => porderNo = o);
    pc = pc.then(() => accounting.searchInput(d, wd,porderNo)).then(() => cf.test(d, 'accountingPreProduction', Config.BASE_URL + '?tab=accountingPreProduction', true, _u));
    pc = pc.then(() => accounting.orderIdClick(d, wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,0)).then(() => cf.test(d, 'accountingPreProduction', Config.BASE_URL + '?tab=accountingPreProduction', true, _u));

    pc = pc.then(() => accounting.accountingProductionClick(d, wd)).then(() => cf.test(d, 'accountingProduction', Config.BASE_URL + '?tab=accountingProduction', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => porderNo = o);
    pc = pc.then(() => accounting.searchInput(d, wd,porderNo)).then(() => cf.test(d, 'accountingProduction', Config.BASE_URL + '?tab=accountingProduction', true, _u));
    pc = pc.then(() => accounting.orderIdClick(d, wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,0)).then(() => cf.test(d, 'accountingProduction', Config.BASE_URL + '?tab=accountingProduction', true, _u));

    pc = pc.then(() => accounting.accountingCompletedClick(d, wd)).then(() => cf.test(d, 'accountingCompleted', Config.BASE_URL + '?tab=accountingCompleted', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => porderNo = o);
    pc = pc.then(() => accounting.searchInput(d, wd,porderNo)).then(() => cf.test(d, 'accountingCompleted', Config.BASE_URL + '?tab=accountingCompleted', true, _u));
    pc = pc.then(() => accounting.orderIdClick(d, wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,0)).then(() => cf.test(d, 'accountingCompleted', Config.BASE_URL + '?tab=accountingCompleted', true, _u));

    pc = pc.then(() => accounting.balanceDueClick(d, wd)).then(() => cf.test(d, 'balanceDue', Config.BASE_URL + '?tab=balanceDue', true, _u));
    pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => porderNo = o);
    pc = pc.then(() => accounting.searchInput(d, wd,porderNo)).then(() => cf.test(d, 'balanceDue', Config.BASE_URL + '?tab=balanceDue', true, _u));
    pc = pc.then(() => accounting.orderIdClick(d, wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd,0)).then(() => cf.test(d, 'balanceDue', Config.BASE_URL + '?tab=balanceDue', true, _u));

    // pc = pc.then(() => accounting.accountingDeliveredClick(d, wd)).then(() => cf.test(d, 'accountingDelivered', Config.BASE_URL + '?tab=accountingDelivered', true, _u));
    // pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => porderNo = o);
    // pc = pc.then(() => accounting.searchInput(d, wd,porderNo)).then(() => cf.test(d, 'accountingDelivered', Config.BASE_URL + '?tab=accountingDelivered', true, _u));
    // pc = pc.then(() => accounting.orderIdClick(d, wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    // pc = pc.then(() => accounting.chooseActionClick(d, wd,0)).then(() => cf.test(d, 'accountingDelivered', Config.BASE_URL + '?tab=accountingDelivered', true, _u));

    // pc = pc.then(() => accounting.accountingHoldClick(d, wd)).then(() => cf.test(d, 'accountingHold', Config.BASE_URL + '?tab=accountingHold', true, _u));
    // pc = pc.then(() => quote_ViewOrder.getFirstRowOrderNo(d, wd,'accountRow')).then((o) => porderNo = o);
    // pc = pc.then(() => accounting.searchInput(d, wd,porderNo)).then(() => cf.test(d, 'accountingHold', Config.BASE_URL + '?tab=accountingHold', true, _u));
    // pc = pc.then(() => accounting.orderIdClick(d, wd)).then(() => cf.test(d, 'viewAccountingOrder', Config.BASE_URL + '?tab=viewAccountingOrder', true, _u));
    // pc = pc.then(() => accounting.chooseActionClick(d, wd,0)).then(() => cf.test(d, 'accountingHold', Config.BASE_URL + '?tab=accountingHold', true, _u));
    return pc;

}

function leadsSearchFlow(pc, d, wd) {

    //SEARCH LEADS SITE
    pc = pc.then(() => l1.EmailInput(d, wd, "Leads")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewLeads', true, _u));
    // SEARCH IN ALL THE TABS
    let orderNo;
    pc = pc.then(() => leadsMode.menuClick(d,wd,'viewLeads')).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.getFirstRowQuoteNo(d,wd)).then((order) => orderNo = order);
    pc = pc.then(() => leadsMode.searchInput(d,wd,orderNo)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.orderIdClick(d,wd)).then(() => cf.test(d, 'viewLeadQuote', Config.BASE_URL + '?tab=viewLeadQuote&', true, _u));

    pc = pc.then(() => leadsMode.menuClick(d,wd,'viewAssignedLeads')).then(() => cf.test(d, 'viewAssignedLeads', Config.BASE_URL + '?tab=viewAssignedLeads&', true, _u));
    pc = pc.then(() => leadsMode.getFirstRowQuoteNo(d,wd,)).then((order) => orderNo = order);
    pc = pc.then(() => leadsMode.searchInput(d,wd,orderNo)).then(() => cf.test(d, 'viewAssignedLeads', Config.BASE_URL + '?tab=viewAssignedLeads&', true, _u));
    pc = pc.then(() => leadsMode.orderIdClick(d,wd)).then(() => cf.test(d, 'viewAssignedLeads', Config.BASE_URL + '?tab=viewAssignedLeads&', true, _u));

    return pc;
}

module.exports = {
    search_flow
};