const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const leadsMode = require('./FT/afterLogin/leadsMode/leadsMode');
const accounting_action = require('./FT/afterLogin/accountingActions/accountingActionFlow');
const quote_ViewOrder = require('./FT/afterLogin/order/viewOrder');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const cart = require('./FT/afterLogin/quote/cart');
const quote_viewQuote= require('./FT/afterLogin/quote/viewQuotePage');

const moment = require('moment');
let _u = undefined;
let guestQuoteNo;
function view_leads_mode_flow(capability) {
    let d = Config.getDriver(Config.ChromeCapabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Leads Mode Login flow'));

    // Guset Quote
    pc = guestFlowForLeads(pc,d);

    // LEAD LOGIN
    pc = pc.then(() => l1.EmailInput(d, wd, "Leads")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewLeads', true, _u));

    pc = pc.then(() => cf.testContentByArray(d, ['Leads',' New ',' Assigned ']));

    // SEARCH GUEST QUOTE IN LEADS TAB
    pc = pc.then(() => leadsMode.menuClick(d,wd,'viewLeads')).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.searchInput(d,wd,guestQuoteNo.substr(guestQuoteNo.length-6))).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.orderIdClick(d, wd)).then(() => cf.test(d, 'viewLeadQuote', Config.BASE_URL + '?tab=viewLeadQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl','100" width x 90" height',
        '101" width x 91" height','3','Right','Left 0 / Right 2','None','Glass Clear','Argon','New York Nickel','Door Restrictor',
        'Item 1 of 1','$5,958.91']));

    pc = pc.then(() => leadsMode.menuClick(d,wd,'viewLeads')).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.searchInput(d,wd,guestQuoteNo)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.checkBoxClick(d, wd)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.transferToClick(d, wd,2)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.transferClick(d, wd)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));

    // VIEW GUEST QUOTE IN ASSIGNED LEADS TAB
    pc = pc.then(() => leadsMode.menuClick(d,wd,'viewAssignedLeads')).then(() => cf.test(d, 'viewAssignedLeads', Config.BASE_URL + '?tab=viewAssignedLeads&', true, _u));
    pc = pc.then(() => leadsMode.searchInput(d,wd,guestQuoteNo.substr(guestQuoteNo.length-6))).then(() => cf.test(d, 'viewAssignedLeads', Config.BASE_URL + '?tab=viewAssignedLeads&', true, _u));
    pc = pc.then(() => leadsMode.viewClick(d,wd)).then(() => cf.test(d, 'viewLeadQuote', Config.BASE_URL + '?tab=viewLeadQuote&', true, _u));
    // Verify that user should be able to view quote details
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl','100" width x 90" height',
        '101" width x 91" height','3','Right','Left 0 / Right 2','None','Glass Clear','Argon','New York Nickel','Door Restrictor',
        'Item 1 of 1','Factory Direct Price','$5,958.91','Item Sub-total:','1','White','New York Nickel']));

    // REVOKE GUEST QUOTE IN ASSIGNED LEADS TAB
    pc = pc.then(() => leadsMode.menuClick(d,wd,'viewAssignedLeads')).then(() => cf.test(d, 'viewAssignedLeads', Config.BASE_URL + '?tab=viewAssignedLeads&', true, _u));
    pc = pc.then(() => leadsMode.revokeClick(d,wd)).then(() => cf.testAlertData(d,'Are you sure you want to revoke this lead?'));
    pc = pc.then(() => cf.handleAlertOK(d,wd)).then(() => cf.test(d, 'viewAssignedLeads', Config.BASE_URL + '?tab=viewAssignedLeads&', true, _u));

    // AGAIN VIEW GUEST QUOTE IN LEADS TAB
    pc = pc.then(() => leadsMode.menuClick(d,wd,'viewLeads')).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.searchInput(d,wd,guestQuoteNo.substr(guestQuoteNo.length-6))).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.checkBoxClick(d, wd)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.transferToClick(d, wd,1)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.transferClick(d, wd)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));

    // VIEW QUOTE IN SALES PERSON LOGIN
    pc = pc.then(() => quote_ViewOrder.productionMenuClick(d, wd)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuItemClick(d, wd, 2)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));

    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    pc = pc.then(() => quote_viewQuote.searchInput(d, wd,guestQuoteNo.substr(guestQuoteNo.length-6))).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    pc = pc.then(() => quote_viewQuote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl','100" width x 90" height',
        '101" width x 91" height','3','Right','Left 0 / Right 2','None','Glass Clear','Argon','New York Nickel','Door Restrictor',
        'Item 1 of 1','Factory Direct Price','$5,958.91','Item Sub-total:','1','White','New York Nickel']));

    // CHECKING CUSTOMER INFO
    pc = pc.then(() => cf.testContentByArray(d, ['test','1234567890',"FT" + moment().format('MM/DD/YYYY') + "@gmail.com"]));

    pc = pc.then(() => accounting_action.PDAccountsClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => accounting_action.signOutSelect(d, wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "Leads")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewLeads', true, _u));

    // DELETE GUEST QUOTE IN ASSIGNED LEADS TAB
    pc = pc.then(() => leadsMode.menuClick(d,wd,'viewAssignedLeads')).then(() => cf.test(d, 'viewAssignedLeads', Config.BASE_URL + '?tab=viewAssignedLeads&', true, _u));
    pc = pc.then(() => leadsMode.deleteClick(d,wd)).then(() => cf.testAlertData(d,'Are you sure you want to delete this quote?'));
    pc = pc.then(() => cf.handleAlertOK(d,wd)).then(() => cf.test(d, 'viewAssignedLeads', Config.BASE_URL + '?tab=viewAssignedLeads&', true, _u));

    // TAB VIEW
    pc = pc.then(() => leadsMode.menuClick(d,wd,'viewAssignedLeads')).then(() => cf.test(d, 'viewAssignedLeads', Config.BASE_URL + '?tab=viewAssignedLeads&', true, _u));
    pc = pc.then(() => leadsMode.menuClick(d,wd,'viewLeads')).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));

    // PAGINATION
    pc = pc.then(() => accounting_action.itemsPerPageSelect(d, wd,1)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => accounting_action.itemsPerPageSelect(d, wd,2)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => accounting_action.itemsPerPageSelect(d, wd,3)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => accounting_action.itemsPerPageSelect(d, wd,4)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => accounting_action.itemsPerPageSelect(d, wd,5)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => accounting_action.itemsPerPageSelect(d, wd,1)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));
    pc = pc.then(() => leadsMode.prevButtonClick(d, wd)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads&', true, _u));

    // RIGHT SIDE MENU
    pc = pc.then(() => quote_ViewOrder.productionMenuClick(d, wd)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuItemClick(d, wd, 0)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuClick(d, wd)).then(() => cf.test(d, 'viewLeads', Config.BASE_URL + '?tab=viewLeads', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuItemClick(d, wd, 1)).then(() => cf.test(d, 'viewAssignedLeads', Config.BASE_URL + '?tab=viewAssignedLeads', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuClick(d, wd)).then(() => cf.test(d, 'viewAssignedLeads', Config.BASE_URL + '?tab=viewAssignedLeads', true, _u));
    pc = pc.then(() => quote_ViewOrder.productionMenuItemClick(d, wd, 2)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Leads Mode Login flow');
    return pc;
}

function guestFlowForLeads(pc, d) {

    pc = pc.then(() => l1.nameInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=guest', _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "FT" + moment().format('MM/DD/YYYY') + "@gmail.com")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=guest', _u, _u));
    pc = pc.then(() => l1.zipInput(d, wd, "000000")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=guest', _u, _u));
    pc = pc.then(() => l1.phoneInput(d, wd, "1234567890")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=guest', _u, _u));
    pc = pc.then(() => l1.leadTypeRadio(d)).then(() => cf.test(d, 'Sales Quoter', _u, Config.BASE_URL + '?tab=guest', _u));
    // pc = pc.then(() => l1.ambassadorNumber(d,wd,'ambassador',12390)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.proceedToQuoteBtn(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Absolute Door (Vinyl)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,275.02']));

    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-right')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_White_null_#fcffff_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,958.91']));

    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl','100" width x 90" height',
        '101" width x 91" height','3','Right','Left 0 / Right 2','None','Glass Clear','Argon','New York Nickel','Door Restrictor',
        'Item 1 of 1','$5,958.91','Total $5,958.91']));

    pc = pc.then(() => leadsMode.getReferenceQuoteText(d,wd)).then((no) => guestQuoteNo=no);

    pc = pc.then(() => leadsMode.startOverClick(d,wd,'startOver-guest'));
    pc = pc.then(() => cf.handleAlertOK(d,wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));

    pc = pc.then(() => accounting_action.PDAccountsClick(d,wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => cart.menuItemClick(d,wd,0)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn&', true, _u));

    return pc;
}
module.exports = {view_leads_mode_flow};