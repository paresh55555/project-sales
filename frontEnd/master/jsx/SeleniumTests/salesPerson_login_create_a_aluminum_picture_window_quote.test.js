const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const quote_NewQuote= require('./FT/afterLogin/quote/newQoutes');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const cart = require('./FT/afterLogin/quote/cart');
const view_quote = require('./FT/afterLogin/quote/viewQuotePage');


let _u = undefined;

function create_a_aluminum_picture_window_quote_by_salesPersonLogin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Aluminum Picture Window by sales person'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => quote_NewQuote.newQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));

    // CHECKING INCHES
    pc = pc.then(() => cf.testModuleData(d, 'Maximum Size:', 'Width 68 feet, Height 12 feet', 'Aluminum Picture Window'));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Aluminum Picture Window')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 1)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['*SMART Doors have a maximum width of 816 inches and minimum width of 12 inches']));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 1)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['*SMART Doors have a maximum height of 144 inches and a minimum height of 12 inches']));
    // .......

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 110)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 80)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd, 200)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-2")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,045.78','Choose Frame Type:']));

    // Start of checking  panel ranges

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 50)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 80)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd, 200)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-Flush')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-2")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$1,838.99','Choose Frame Type:']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 80)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd, 200)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$2,574.58','Choose Frame Type:']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 80)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd, 200)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-Flush')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-4")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$3,677.98','Choose Frame Type:']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 120)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 80)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd, 200)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-5")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,413.57','Choose Frame Type:']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 170)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 80)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd, 200)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-Flush')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-6")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,252.57','Choose Frame Type:']));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 200)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 80)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd, 200)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-6")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,355.96','Choose Frame Type:']));

    // End of checking panel ranges

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 110)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 80)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = checkingTrackLength(pc,d);
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd, 200)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-2")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,045.78','Choose Frame Type:']));

    // COLOR
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'extColor_basic_all_Apollo White_null_#fcffff_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,045.78']));

    pc = pc.then(() => quote_ColorAndFinish.premiumDoorColorClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.premiumSubDoorColorClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,450.36']));

    pc = pc.then(() => quote_ColorAndFinish.colorGroupButtonClick(d, wd,'extColor-arch')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'extColor_arch_all_Almond_null_#e6dcb8_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,045.78']));

    pc = pc.then(() => quote_ColorAndFinish.colorGroupButtonClick(d, wd,'extColor-ral')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'extColor_ral_Yellow_Green beige_RAL 1000_#BEBD7F_undefined')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,045.78']));

    pc = pc.then(() => quote_ColorAndFinish.colorGroupButtonClick(d, wd,'extColor-woodStandard')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'extColor_woodStandard_all_Alder_null_null_images/colors/wood/Alder121.jpg')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,045.78']));

    pc = pc.then(() => quote_ColorAndFinish.colorGroupButtonClick(d, wd,'extColor-custom')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.extriorCustomDoorColorInput(d, wd,'red')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,045.78']));

    pc = pc.then(() => quote_ColorAndFinish.interiorDoorColorClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'intColor_basic_all_Apollo White_null_#fcffff_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,045.78']));
    pc = pc.then(() => quote_ColorAndFinish.scroll(d,wd,500)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.colorGroupButtonClick(d, wd,'intColor-premium')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorColorClick(d, wd,'intColor_premium_all_Matte Black C_null_#000000_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,652.64']));

    pc = pc.then(() => quote_ColorAndFinish.colorGroupButtonClick(d, wd,'intColor-arch')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorColorClick(d, wd,'intColor_arch_all_Anodized Silver (Powder Coat)_null__images/colors/architecturalColors/anodized-121.png')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,045.78']));

    pc = pc.then(() => quote_ColorAndFinish.colorGroupButtonClick(d, wd,'intColor-ral')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorColorClick(d, wd,'intColor_ral_Yellow_Beige_RAL 1001_#C2B078_undefined')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,045.78']));

    pc = pc.then(() => quote_ColorAndFinish.colorGroupButtonClick(d, wd,'intColor-woodStandard')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorColorClick(d, wd,'intColor_woodStandard_all_Fir_null_null_images/colors/wood/Birch121.jpg')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,979.43']));

    pc = pc.then(() => quote_ColorAndFinish.colorGroupButtonClick(d, wd,'intColor-woodPremium')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorColorClick(d, wd,'intColor_woodPremium_all_Walnut_null_null_images/colors/wood/Walnut121.jpg')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,979.43']));

    pc = pc.then(() => quote_ColorAndFinish.interiorCustomDoorColorClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.interiorCustomDoorColorInput(d, wd, 'black')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,045.78']));

    // GLASS
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,045.78']));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Low-E 3')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,045.78']));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Unglazed')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$4,045.78']));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d,
        ['Item 1 of 1','$4,045.78', '1', 'Aluminum Picture Window', '110" width x 80" height',
            '110" width x 80" height','111" width x 81" height','2','52.28"','75.5"','None','Left 0 / Right 0',
            'Aluminum Block','3/4 Upstand','red (custom)','black (custom)','Unglazed',
            'Left 0 / Right 0', 'Clear Anodized', 'black (custom)']
    ));
    pc = pc.then(() => cart.quantityInput(d, wd, 2)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d,
        ['Item 1-2 of 2','$8,091.56','$4,045.78', '1', 'Aluminum Picture Window', '110" width x 80" height',
            '110" width x 80" height','111" width x 81" height','2','52.28"','75.5"','None','Left 0 / Right 0',
            'Aluminum Block','3/4 Upstand','red (custom)','black (custom)','Unglazed',
            'Left 0 / Right 0', 'Clear Anodized', 'black (custom)']
    ));
    pc = pc.then(() => cart.duplicateClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d,
        ['Item 1-2 of 4','Item 3-4 of 4','$16,183.12','$8,091.56','$4,045.78', '1', 'Aluminum Picture Window', '110" width x 80" height',
            '110" width x 80" height','111" width x 81" height','2','52.28"','75.5"','None','Left 0 / Right 0',
            'Aluminum Block','3/4 Upstand','red (custom)','black (custom)','Unglazed',
            'Left 0 / Right 0', 'Clear Anodized', 'black (custom)']
    ));
    pc = pc.then(() => cart.saveAsQuote(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['4']));

    // CHECKING BACK BUTTON ISSUE
    pc = pc.then(() => view_quote.viewQuoteTabClick(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    pc = pc.then(() => cf.backButtonClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Create a Aluminum Picture Window Quote by salesPerson');
    return pc;
}

function checkingTrackLength (pc,d) {
    // CHECKING NO OF FRAME TYPES AND TRACK TYPE
    pc = pc.then(() => quote_SizeAndPanels.checkNoOfFrameType(d, wd, 1));
    pc = pc.then(() => quote_SizeAndPanels.checkNoOfTrackType(d, wd, 1,'track-3/4 Upstand'));
    pc = pc.then(() => quote_SizeAndPanels.checkNoOfFrameType(d, wd, 1,'track-Flush'));
    return pc;
}

module.exports = {create_a_aluminum_picture_window_quote_by_salesPersonLogin};