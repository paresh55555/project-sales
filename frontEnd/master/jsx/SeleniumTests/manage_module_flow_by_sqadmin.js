const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const modulesFlow = require('./FT/sqAdmin/module/modulesFlow');

let _u = undefined;

function manage_module_flow_by_sqadmin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let total_frame = 3;
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Manage module flow by sales person'));
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));

    for(let i = 0; i < total_frame; i++) {
        pc = pc.then(() => sideBar.modulesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
        pc = pc.then(() => modulesFlow.selectModuleClick(d, i)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/module', true, _u));
        pc = pc.then(() => modulesFlow.selectFrameRandomClick(d)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/module', true, _u));
    }

    pc = cf.handleOutput(d, pc, capability, 'Manage module flow by sales person');
    return pc;
}

module.exports = {manage_module_flow_by_sqadmin};