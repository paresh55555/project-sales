const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const quote_viewQuote= require('./FT/afterLogin/quote/viewQuotePage');
const quote_order_customerInfo = require('./FT/afterLogin/order/customerInfo');
const accounting = require('./FT/afterLogin/accountingActions/accountingActionFlow');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const view_cart = require('./FT/afterLogin/quote/cart');
const admin_action_flow = require('./FT/afterLogin/adminActions/adminActionFlow');
const moment = require('moment');
let _u = undefined;

function view_quote_by_salesPersonLogin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Sales Person view Quote flow'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    // pc = pc.then(() => quote_viewQuote.searchInput(d, wd, 'a')).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    pc = pc.then(() => quote_viewQuote.sortItems(d, wd, 'total')).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    pc = pc.then(() => quote_viewQuote.itemsPerPageSelect(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    // pc = pc.then(() => quote_viewQuote.newButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesNew', Config.BASE_URL + '?tab=viewQuotesNew&', true, _u));
    // pc = pc.then(() => quote_viewQuote.hotButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesHot', Config.BASE_URL + '?tab=viewQuotesHot&', true, _u));
    // pc = pc.then(() => quote_viewQuote.mediumButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    // pc = pc.then(() => quote_viewQuote.coldButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesCold', Config.BASE_URL + '?tab=viewQuotesCold&', true, _u));
    // pc = pc.then(() => quote_viewQuote.holdButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesHold', Config.BASE_URL + '?tab=viewQuotesHold&', true, _u));
    // pc = pc.then(() => quote_viewQuote.archivedButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesArchived', Config.BASE_URL + '?tab=viewQuotesArchived&', true, _u));
    // pc = pc.then(() => quote_viewQuote.viewAllButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));

    pc = pc.then(() => quote_viewQuote.qualitySelect(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    // pc = pc.then(() => quote_viewQuote.contactedClick(d, wd, 'followUp-0', moment().format('DD/MM/YYYY') )).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    // pc = pc.then(() => quote_viewQuote.contactedClick(d, wd, 'secondFollowUp-0', moment().add(1,'day').format('DD/MM/YYYY') )).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));

    // View Quote
    pc = pc.then(() => quote_viewQuote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => quote_order_customerInfo.revenueTypeSelect(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => quote_order_customerInfo.channelTypeSelect(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => accounting.onHoldSelection(d, wd,0)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => accounting.onHoldSelection(d, wd,1)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));

    pc = pc.then(() => quote_order_customerInfo.editCustomerClick(d, wd)).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.leadSourceSelect(d,2)).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.firstNameInput(d, wd, 'fname')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.lastNameInput(d, wd, 'lname')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.emailInput(d, wd, 'test@gmail.com')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.phoneInput(d, wd, '8989898989')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingNameInput(d, wd, 'test')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingAddress1Input(d, wd, 'test')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingAddress1Input(d, wd, 'test')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingCityInput(d, wd, 'test')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => accounting.scroll(d, wd, 1000)).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.saveCancelCustomerInfo(d, wd,1)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_order_customerInfo.attachCustomerClick(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.leadSourceSelect(d,2)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.firstNameInput(d,wd,'firstname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.lastNameInput(d,wd,'lastname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.emailInput(d,wd,'email@emial.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.phoneInput(d,wd,'898989')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingContactInputName(d,wd,'billing test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingEmailInputName(d,wd,'billing@email.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d,wd,'address 1')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress2InputName(d,wd,'adress 2')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingCityInputName(d,wd,'billing city')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingStateInputName(d,wd,'billing state')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingZipInputName(d,wd,'83211')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.attachToQuoteClick(d, wd, 'nextTitle')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => accounting.scroll(d,wd,0)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => accounting.followUpNotesInput(d,wd, "This is follow up notes")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_viewQuote.qualitySelect(d)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    // pc = pc.then(() => quote_viewQuote.followupInput(d,wd,'followUp-0')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    // pc = pc.then(() => quote_viewQuote.followupInput(d,wd,'secondFollowUp-0')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    //ADD DOOR
    pc = pc.then(() => accounting.chooseActionClick(d,wd,1)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Absolute Door (Vinyl)')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, "track-3/4 Upstand")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd, "swingDirection-left")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd, "movement-left-0")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd, "swingInOrOut-Outswing")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd, "screens-Panoramic Screen Left")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_Beige_null_#F5F5DC_null')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)',
        '3', 'Left','Outswing','Left 2 / Right 0','Aluminum Block','3/4 Upstand','None','Glass Clear',
    'Argon','New York Nickel','Door Restrictor','Panoramic Screen Left']));

    // CHECKING DATE IN EDIT TIME
    pc = pc.then(() => quote_viewQuote.dateInputById(d, wd,'followUp-0')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_viewQuote.dateInputById(d, wd,'secondFollowUp-0')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // EDIT ADDED DOOR
    pc = pc.then(() => accounting.editAddedDoor(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // CHECKING ENTERED DATE
    pc = pc.then(() => quote_viewQuote.checkDateValues(d, wd,'followUp-0',moment().format('MM/DD/YYYY'))).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_viewQuote.checkDateValues(d, wd,'secondFollowUp-0',moment().format('MM/DD/YYYY'))).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, "track-Flush")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-4")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd, "swingDirection-right")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd, "movement-right-3")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd, "swingInOrOut-Inswing")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd, "screens-No Screen  ")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_White_null_#fcffff_null')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Low-E 3')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-Miami')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl',
        '4', 'Right','Inswing','Left 0 / Right 3','Aluminum Block','Flush','None','Low-E 3',
        'Argon','Miami','No Screen']));

    // CANCEL EDIT QUOTE
    pc = pc.then(() => accounting.editAddedDoor(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd, 1)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl',
        '4', 'Right','Inswing','Left 0 / Right 3','Aluminum Block','Flush','None','Low-E 3',
        'Argon','Miami','No Screen']));


    // SAVE QUOTE FORM ANY STEP
    pc = pc.then(() => accounting.editAddedDoor(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd, "screens-Panoramic Screen Split")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_Beige_null_#F5F5DC_null')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd, 0)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','Beige','Beige Vinyl',
        '4', 'Right','Inswing','Left 0 / Right 3','Aluminum Block','Flush','None','Low-E 3',
        'Argon','Miami','Panoramic Screen Split']));

    // QTY input and total check and grand total
    pc = pc.then(() => view_cart.quantityInput(d, wd, 2)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => accounting.scroll(d, wd, 100)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => accounting.chooseActionClick(d, wd, 0)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    let price;
    pc = pc.then(() => quote_viewQuote.getTotalPrice(d, wd)).then((p) => price = p );
    pc = pc.then(() => cf.testContentByArray(d, [price]));

    // EMAIL QUOTE
    pc = pc.then(() => accounting.chooseActionClick(d, wd,3)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => view_cart.emailAddressesInput(d, wd,'test@test.com')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => view_cart.emailMessageInput(d, wd,'testing')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => view_cart.cancelEmail(d,wd )).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => accounting.scroll(d,wd,0)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => accounting.pickupDeliverySelection(d,wd,1)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => accounting.pickupDeliverySelection(d,wd,2)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => accounting.poInput(d,wd,'po input')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => accounting.commentInput(d,wd,'This is comment')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = checkTaxValue(pc,d,wd);

    // EDIT PROJECT INFO
    pc = pc.then(() => admin_action_flow.editQuoteClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.addCostClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.deleteCostClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // Cost
    pc = pc.then(() => admin_action_flow.addCostClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.costNameInput(d, wd, 'TestName')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.costAmountInput(d, wd, 100)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.yesClick(d, 'costsList')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.noClick(d, 'costsList')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // Discount
    pc = pc.then(() => admin_action_flow.addDiscountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.deleteDiscountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.addDiscountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.yesClick(d, 'discountsList')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.noClick(d, 'discountsList')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // taxInfo
    pc = pc.then(() => admin_action_flow.resaleNumberInput(d, wd, 'testNumber')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.yesClick(d, 'taxInfo')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.noClick(d, 'taxInfo')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // Non Taxable
    pc = pc.then(() => admin_action_flow.addExtraNonTaxableAmountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.deleteExtraNonTaxableAmountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.addExtraNonTaxableAmountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.nonTaxableNameInput(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.nonTaxableAmountInput(d, wd, 120)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.yesClick(d, 'taxInfo')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.noClick(d, 'taxInfo')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => admin_action_flow.saveClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => accounting.chooseActionClick(d,wd,2)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Sales Person view Quote flow');
    return pc;
}

function checkTaxValue(pc, d, wd) {
    // Checking TAX value
    let taxData;
    pc = pc.then(() => admin_action_flow.getTaxData(d, wd)).then((data) => taxData = data );
    pc = pc.then(() => admin_action_flow.editQuoteClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.saveClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [taxData]));
    return pc;
}
module.exports = {view_quote_by_salesPersonLogin};