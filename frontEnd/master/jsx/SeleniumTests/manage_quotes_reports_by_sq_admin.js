const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sidebar = require('./FT/sqAdmin/sideBar/sideBar');
const orderPage = require('./FT/sqAdmin/OrderReports/orderReports');
const moment = require('moment');
let _u = undefined;

function quotes_reports_by_sq_admin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Open Quotes Reports by SQ-admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "SalesManager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sidebar.quotesReportClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));

    let currentMonthDate1 = moment().date(1).format('MM/DD/YYYY');
    let currentMonthDate2 = moment().endOf('month').format('MM/DD/YYYY');
    let previousMonthDate1 = moment().subtract(1, 'months').date(1).format('MM/DD/YYYY');
    let previousMonthDate2 = moment().subtract(1,'months').endOf('month').format('MM/DD/YYYY');;
    let currentYearDate1 = moment().startOf('year').format('MM/DD/YYYY');
    let currentYearDate2 = moment().endOf('year').format('MM/DD/YYYY');
    let previousYearDate1 = moment().startOf('year').subtract(1,'year').format('MM/DD/YYYY');
    let previousYearDate2 = moment().endOf('year').subtract(1,'year').format('MM/DD/YYYY');
    let previous90DaysDate1 = moment().subtract(90,'day').format('MM/DD/YYYY');
    let currentDate = moment().format('MM/DD/YYYY');
    let previous3MonthDate1 = moment().subtract(2, 'months').date(1).format('MM/DD/YYYY');

    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [previousMonthDate1, previousMonthDate2]));
    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [previous3MonthDate1, currentMonthDate2]));
    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [previous90DaysDate1, currentDate]));
    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [currentYearDate1, currentYearDate2]));
    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [previousYearDate1, previousYearDate2]));
    pc = pc.then(() => orderPage.filterClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [currentMonthDate1, currentMonthDate2]));

    pc = pc.then(() => orderPage.dateInput(d, wd,'toDatePicker',currentMonthDate1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.dateInput(d, wd,'fromDatePicker',currentMonthDate2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.goButton(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [currentMonthDate1, currentMonthDate2]));

    pc = pc.then(() => orderPage.paginationEntryClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.paginationEntryClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));

    pc = pc.then(() => orderPage.paginationClick(d, wd,'activeNext')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.paginationClick(d, wd,'activePrevious')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));

    pc = pc.then(() => orderPage.search(d, wd,'test')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));
    pc = pc.then(() => orderPage.search(d, wd,'')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));

    pc = pc.then(() => orderPage.exportClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/openquotesreport', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Open Quotes Reports by SQ-admin');
    return pc;
}

module.exports = {quotes_reports_by_sq_admin};