const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const admin_action_flow = require('./FT/afterLogin/adminActions/adminActionFlow');
const view_order_actions = require('./FT/afterLogin/adminActions/viewOrdersPage');

let _u = undefined;

function admin_actions(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Admin actions'));
    pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => view_order_actions.orderLinkClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => admin_action_flow.editQuoteClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.addCostClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.deleteCostClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // Cost
    pc = pc.then(() => admin_action_flow.addCostClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.costNameInput(d, wd, 'TestName')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.costAmountInput(d, wd, 100)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.yesClick(d, 'costsList')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.noClick(d, 'costsList')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // Discount
    pc = pc.then(() => admin_action_flow.addDiscountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.deleteDiscountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.addDiscountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.yesClick(d, 'discountsList')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.noClick(d, 'discountsList')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // taxInfo
    pc = pc.then(() => admin_action_flow.resaleNumberInput(d, wd, 'testNumber')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.yesClick(d, 'taxInfo')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.noClick(d, 'taxInfo')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // Non Taxable
    pc = pc.then(() => admin_action_flow.addExtraNonTaxableAmountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.deleteExtraNonTaxableAmountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.addExtraNonTaxableAmountClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.nonTaxableNameInput(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.nonTaxableAmountInput(d, wd, 120)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.yesClick(d, 'taxInfo')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => admin_action_flow.noClick(d, 'taxInfo')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => admin_action_flow.saveClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Admin action flow');
    return pc;
}

module.exports = {admin_actions};