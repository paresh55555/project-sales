const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const quote_NewQuote= require('./FT/afterLogin/quote/newQoutes');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const cart = require('./FT/afterLogin/quote/cart');
const quote_order_customerInfo = require('./FT/afterLogin/order/customerInfo');
const view_order = require('./FT/afterLogin/adminActions/viewOrdersPage');
const view_quote = require('./FT/afterLogin/quote/viewQuotePage');
const accounting = require('./FT/afterLogin/accountingActions/accountingActionFlow');
const quote_ViewOrder= require('./FT/afterLogin/order/viewOrder');

const moment = require('moment');
let _u = undefined;

function create_a_vinyl_door_quote_by_salesPersonLogin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: SalesPerson Quote'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => quote_NewQuote.newQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => cf.testModuleData(d, 'Maximum Size:', 'Width 19 feet, Height 8 feet', 'Absolute Door (Vinyl)'));

    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Absolute Door (Vinyl)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 1)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['*Vinyl Doors have a maximum width of 236 inches and a minimum width of 20 inches']));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 1)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['*Vinyl Doors have a maximum height of 96 inches and minimum height of 16 inches']));

    pc = pc.then(() => cf.testContentByArray(d, ['T.B.D.','powered by','Sales Quoter']));

    pc = checkingPricePanels(pc, wd, d);

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['powered by','Sales Quoter','Choose Frame Type:']));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,275.02']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-right')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,275.02']));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,500)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,275.02','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,275.02','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.panelDirectionSelection(d, wd,"swingInOrOut-Outswing")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,275.02','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Split")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,306.27','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_Beige_null_#F5F5DC_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,577.48','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_White_null_#fcffff_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,306.27','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,707.09','powered by','Sales Quoter']));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,867.66','powered by','Sales Quoter']));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));

    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,990.16','powered by','Sales Quoter']));

    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl','100" width x 90" height',
        '101" width x 91" height','3','31.35"','85.5"','Left','Outswing','Left 2 / Right 0','Aluminum Block','3/4 Upstand',
        'None','Glass Clear','Argon','New York Nickel','Door Restrictor','Panoramic Screen Split','Item 1 of 1',
        '$6,990.16','Total $6,990.16']
    ));

    pc = pc.then(() => cart.quantityInput(d, wd,2)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d,['Absolute Door (Vinyl)','White Vinyl','100" width x 90" height',
        '101" width x 91" height','3','31.35"','85.5"','Left','Outswing','Left 2 / Right 0','Aluminum Block','3/4 Upstand',
        'None','Glass Clear','Argon','New York Nickel','Door Restrictor','Panoramic Screen Split',
        'Item 1-2 of 2',
        '$6,990.16','$13,980.32','Total $13,980.32']));
    pc = pc.then(() => cart.duplicateClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d,['Absolute Door (Vinyl)','White Vinyl','100" width x 90" height',
        '101" width x 91" height','3','31.35"','85.5"','Left','Outswing','Left 2 / Right 0','Aluminum Block','3/4 Upstand',
        'None','Glass Clear','Argon','New York Nickel','Door Restrictor','Panoramic Screen Split',
        'Item 1-2 of 4','Item 3-4 of 4',
        '$6,990.16','$13,980.32','Total $27,960.64']));

    // ADD DOOR
    pc = pc.then(() => cart.addDoorOptionClick(d, wd,'home-sales')).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Signature Door (Aluminum)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3",'powered by','Sales Quoter','Choose Frame Type:')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,998.80']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,998.80','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,998.80','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.panelDirectionSelection(d, wd,"swingInOrOut-Outswing")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,998.80','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Split")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,030.05','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'extColor_basicSpecial_all_Apollo White_null_#fcffff_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,030.05','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Low-E 3')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,573.90','powered by','Sales Quoter']));

    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,742.51','powered by','Sales Quoter']));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));

    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-Miami')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,742.51','powered by','Sales Quoter']));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d,['Absolute Door (Vinyl)','White Vinyl','100" width x 90" height',
        '101" width x 91" height','3','31.35"','85.5"','Left','Outswing','Left 2 / Right 0','Aluminum Block','3/4 Upstand',
        'None','Glass Clear','Argon','New York Nickel','Door Restrictor','Panoramic Screen Split',
        'Item 1-2 of 5','Item 3-4 of 5','Item 5 of 5',
        '$6,990.16','$13,980.32','Total $36,703.15','$6,990.16',
        'Signature Door (Aluminum)','100" width x 90" height','101" width x 91" height','3','31.44"','85.5"','Left',
        'Outswing','Left 2 / Right 0','Aluminum Block','3/4 Upstand','Apollo White','Low-E 3','Argon','Miami',
        'Door Restrictor','Panoramic Screen Split']));
    pc = pc.then(() => cart.quantityInput(d, wd,3)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cart.saveAsQuote(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Cart (','6',')']));
    pc = pc.then(() => quote_order_customerInfo.leadSourceSelect(d,2)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.firstNameInput(d, wd, 'fname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.lastNameInput(d, wd, 'lname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.emailInput(d, wd, 'test@gmail.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.phoneInput(d, wd, '8989898989')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingContactInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingEmailInputName(d, wd, 'test@email.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingCityInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingStateInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingZipInputName(d, wd, '898989')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));

    pc = pc.then(() => quote_order_customerInfo.attachToQuoteClick(d, wd)).then(() => cf.test(d, 'viewSavedQuoteConfirmation', Config.BASE_URL + '?tab=viewSavedQuoteConfirmation&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Quote Name: fname lname']));
    pc = pc.then(() => quote_order_customerInfo.quoteViewButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => quote_order_customerInfo.attachCustomerClick(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.searchPreviousCustomerClick(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.searchPreviousCustomerInput(d, wd,'a')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.attachSearchCustomer(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // Checking Expiry Date

    pc = pc.then(() => cf.testContentByArray(d, ['Quote Expires on',
        moment().format('MM/DD/YYYY'), moment().add(180,'days').format('MMMM DD,YYYY')]));
    pc = pc.then(() => view_order.quoteActionsClick(d, wd,3)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Create a Vinyl Door Quote by salesPerson');
    return pc;
}

function check_quote_validation(capability) {
    let d = Config.getDriver(Config.ChromeCapabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Check Quote validation in Sales Person'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => quote_NewQuote.newQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Absolute Door (Vinyl)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    // CHECKING NEXT STEP IF WE CLEAR INPUT BOX
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 25550)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 5666)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    // pc = pc.then(() => quote_ColorAndFinish.tabClick(d, wd,'color')).then(()=>cf.testAlertData(d,'You have an invalid width You have an invalid height'));
    pc = pc.then(() => quote_ColorAndFinish.tabClick(d, wd,'color'));
    pc = pc.then(() => cf.handleAlertOK(d, wd));
    pc = pc.then(() => quote_SizeAndPanels.clearInput(d, wd, 'width')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.clearInput(d, wd, 'height')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.tabClick(d, wd,'color')).then(()=>cf.testAlertData(d,'Please enter a valid width'));
    // START CHECKING OF REQUIRED VALIDATION
    pc = pc.then(() => quote_ColorAndFinish.tabClick(d, wd,'color')).then(()=>cf.testAlertData(d,'Please enter a valid width'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, '0000520')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, ' +200')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(()=>cf.testAlertData(d,'Please enter a valid height'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(()=>cf.testAlertData(d,'You must complete all the selections on the current page to continue'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-Flush')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(()=>cf.testAlertData(d,'You must complete all the selections on the current page to continue'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(()=>cf.testAlertData(d,'You must complete all the selections on the current page to continue'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(()=>cf.testAlertData(d,'You must complete all the selections on the current page to continue'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.panelDirectionSelection(d, wd,"swingInOrOut-Outswing")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(()=>cf.testAlertData(d,'You must complete all the selections on the current page to continue'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Split")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(()=>cf.testAlertData(d,'Please select a vinyl color'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureDoorColorClick(d, wd,'vinylColor_vinyl_all_White_null_#fcffff_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(()=>cf.testAlertData(d,'Please select a type of Glass'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));

    pc = pc.then(() => cart.cartClick(d, wd,'cartButton')).then(()=>cf.testAlertData(d,'Currently there are no items in your cart'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    //END CHECKING OF REQUIRED VALIDATION

    // START C1285
    pc = pc.then(() => cart.addDoorOptionClick(d, wd,'home-sales')).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Signature Door (Aluminum)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 70)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, "track-3/4 Upstand")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingInOrOut-Outswing')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,'screens-Panoramic Screen Split')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.tabClick(d, wd,'size')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-right')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(()=>cf.testAlertData(d,'You must complete all the selections on the current page to continue'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-right-2')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    // END C1285
    pc = pc.then(() => quote_ColorAndFinish.signatureDoorColorClick(d, wd,'extColor_basicSpecial_all_Statuary Bronze_null_#352B20_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-London Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    //DELETE ADDED DOOR
    pc = pc.then(() => cart.deleteAddedDoor(d, wd)).then(()=>cf.testAlertData(d,'Are you sure you want to delete this door?'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    // START OVER
    pc = pc.then(() => cart.startOverClick(d, wd,'startOver-sales')).then(()=>cf.testAlertData(d,'Are you sure you want to delete everything in your cart and start over?'));
    pc = pc.then(() => cf.handleAlertCancel(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cart.startOverClick(d, wd,'startOver-sales')).then(()=>cf.testAlertData(d,'Are you sure you want to delete everything in your cart and start over?'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Check Quote validation in Sales Person');
    return pc;
}

function checkingPricePanels(pc, wd, d) {
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,275.02']));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 80)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-4")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,483.88']));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,275.02']));
    return pc;
}

function delete_items(capability) {
    let d = Config.getDriver(Config.ChromeCapabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Delete Item by Sales Person'));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => quote_NewQuote.newQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));

    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Absolute Door (Vinyl)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['T.B.D.','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-right')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,500)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelDirectionSelection(d, wd,"swingInOrOut-Outswing")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Split")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_Beige_null_#F5F5DC_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_White_null_#fcffff_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    // Delete Item From a Cart
    pc = pc.then(() => accounting.cartItemClick(d, wd,'3')).then(() => cf.testAlertData(d, 'Are you sure you want to delete this door?'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    // ADD DOOR
    pc = pc.then(() => cart.addDoorOptionClick(d, wd,'home-sales')).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Signature Door (Aluminum)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3",'powered by','Sales Quoter','Choose Frame Type:')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelDirectionSelection(d, wd,"swingInOrOut-Outswing")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Split")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'extColor_basicSpecial_all_Apollo White_null_#fcffff_')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Low-E 3')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-Miami')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cart.saveAsQuote(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.leadSourceSelect(d,2)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.firstNameInput(d, wd, 'fname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.lastNameInput(d, wd, 'lname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.emailInput(d, wd, 'test@gmail.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.phoneInput(d, wd, '8989898989')).then(() => cf.test(d, 'customer', Config .BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingContactInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingEmailInputName(d, wd, 'test@email.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingCityInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingStateInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingZipInputName(d, wd, '898989')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.attachToQuoteClick(d, wd)).then(() => cf.test(d, 'viewSavedQuoteConfirmation', Config.BASE_URL + '?tab=viewSavedQuoteConfirmation&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.quoteViewButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // Delete Item From a quote
    pc = pc.then(() => accounting.cartItemClick(d, wd,'3')).then(() => cf.testAlertData(d, 'Are you sure you want to delete this door?'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    //ADD DOOR
    pc = pc.then(() => accounting.chooseActionClick(d,wd,0)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Absolute Door (Vinyl)')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, "track-3/4 Upstand")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd, "swingDirection-left")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd, "movement-left-0")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd, "swingInOrOut-Outswing")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd, "screens-Panoramic Screen Left")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.colorClick(d, wd,'1')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.revenueTypeSelect(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.channelTypeSelect(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => accounting.dateDueInputChange(d, wd,'viewOrderDateBox hasDatepicker')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => accounting.scroll(d, wd,0)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => view_quote.convertToSale(d, wd,'-convertToSale')).then(() => cf.testAlertData(d, 'Are you sure you want to convert this quote to an order?'));
    let orderNo;
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.getAlertText(d, wd).then((data)=> orderNo = data));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));

    // View Order delete item
    pc = pc.then(() => quote_ViewOrder.viewOrderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,orderNo)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.orderIdClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // SEND TO PRODUCTION BUTTON SHOULD BE AVAILABLE FOR ORDER
    pc = pc.then(() => accounting.testEmptyOrderNotMovedToProduction(d, wd,'Send To Production'));

    // Delete Item From a order
    pc = pc.then(() => accounting.cartItemClick(d, wd,'3')).then(() => cf.testAlertData(d, 'Are you sure you want to delete this door?'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // SEND TO PRODUCTION BUTTON SHOULD NOT BE AVAILABLE FOR EMPTY ORDER
    pc = pc.then(() => accounting.testEmptyOrderNotMovedToProduction(d, wd,null));

    pc = cf.handleOutput(d, pc, capability, 'Delete Item');
    return pc;
}

module.exports = {
    create_a_vinyl_door_quote_by_salesPersonLogin,check_quote_validation,
    delete_items
};