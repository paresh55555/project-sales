const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const fixedFormulasPage = require('./FT/sqAdmin/module/fixedFormulasPage');

let _u = undefined;

function manage_fixed_formulas_flow_by_sqadmin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Manage Fixed Formulas by sales person'));
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => sideBar.modulesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
    pc = pc.then(() => fixedFormulasPage.vinylwithAluminumBlockFrameClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.fixedFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.addFixedFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.optionNameInput(d, wd, 'panel', 'name of option')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.optionNameInput(d, wd, 1, 'part number')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.optionNameInput(d, wd, 100, 'cost item')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.optionNameInput(d, wd, 'swings - 1 - 1000', 'quantity formula')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.optionNameInput(d, wd, 0, 'for cut sheet')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.optionNameInput(d, wd, 4, 'my order')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.saveFixedFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.searchInput(d, wd, 'swings - 1 - 1000')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.fixedFormulasRecordClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
    pc = pc.then(() => fixedFormulasPage.deleteFixedFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.confirmFixedFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.confirmFixedFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.changeOrderClick(d, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.changeOrderClick(d, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => fixedFormulasPage.changeOrderClick(d, 2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = cf.handleOutput(d, pc, capability, 'Manage Fixed Formulas by sales person');
    return pc;
}

module.exports = { manage_fixed_formulas_flow_by_sqadmin };