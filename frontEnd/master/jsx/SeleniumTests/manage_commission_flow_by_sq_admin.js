const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const manageCommissionPage = require('./FT/sqAdmin/commissions/manageCommissionPage');
const orderPage = require('./FT/sqAdmin/OrderReports/orderReports');

const moment = require('moment');
let _u = undefined;

function manage_commission_by_SQ_admin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Manage commission Flow by SQ-admin'));
    pc = pc.then(() => login.EmailInput(d, wd, "SalesManager")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/users', true, _u));
    pc = pc.then(() => sideBar.commissionClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));

    pc = pc.then(() => orderPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));


    let currentMonthDate1 = moment().date(1).format('MM/DD/YYYY');
    let currentMonthDate2 = moment().endOf('month').format('MM/DD/YYYY');
    let previousMonthDate1 = moment().subtract(1, 'months').date(1).format('MM/DD/YYYY');
    let previousMonthDate2 = moment().subtract(1,'months').endOf('month').format('MM/DD/YYYY');
    let currentYearDate1 = moment().startOf('year').format('MM/DD/YYYY');
    let currentYearDate2 = moment().endOf('year').format('MM/DD/YYYY');
    let previousYearDate1 = moment().startOf('year').subtract(1,'year').format('MM/DD/YYYY');
    let previousYearDate2 = moment().endOf('year').subtract(1,'year').format('MM/DD/YYYY');

    // DATE FILTER
    pc = pc.then(() => manageCommissionPage.dateButtonClick(d)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [previousMonthDate1, previousMonthDate2]));
    pc = pc.then(() => manageCommissionPage.dateButtonClick(d)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [currentMonthDate1, currentMonthDate2]));
    pc = pc.then(() => manageCommissionPage.dateButtonClick(d)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [currentYearDate1, currentYearDate2]));
    pc = pc.then(() => manageCommissionPage.dateButtonClick(d)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [previousYearDate1, previousYearDate2]));

    pc = pc.then(() => manageCommissionPage.dateButtonClick(d)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.datePickerClick(d, wd, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.previousNavClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.previousNavClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.dateSelectClick(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.datePickerClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.dateSelectClick(d, wd, 4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.goButtonClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.dateButtonClick(d)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.dateFilterItemSelect(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, [currentYearDate1, currentYearDate2]));
    pc = pc.then(() => manageCommissionPage.commissionRecordClick(d, wd, 2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));

    pc = pc.then(() => manageCommissionPage.addCommissionClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.commissionInput(d, wd, 'Search order...', 'AC')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.selectOrderValueClick(d, wd)).then(() =>cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.datePickerClick(d, wd, 2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.currentDateSelect(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.amountInput(d, wd, 8999)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.correctionInput(d, wd, 899)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$  8,999.00','$  899.00']));
    pc = pc.then(() => manageCommissionPage.saveClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));

    pc = pc.then(() => manageCommissionPage.commissionRecordClick(d, wd, 2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.editButtonClick(d, wd, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.addCommissionBoxInput(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.newCommissionEditAmountInput(d, wd, 0, '10000')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.newCommissionEditCorrectionInput(d, wd, 0, '10.00')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$  10,010.00']));
    pc = pc.then(() => manageCommissionPage.saveClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));

    pc = pc.then(() => manageCommissionPage.commissionSearchInput(d, wd, 'A')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.commissionRecordClick(d, wd, 2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));

    pc = pc.then(() => manageCommissionPage.editButtonClick(d, wd, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.deleteClick(d, wd, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.confirmClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));

    pc = pc.then(() => manageCommissionPage.confirmClick(d, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.confirmClick(d, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.confirmClick(d, 2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.confirmClick(d, 3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => manageCommissionPage.confirmClick(d, 4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL+'SQ-admin/sales/commissions', true, _u));

    // SORTING
    pc = pc.then(() => orderPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,3)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,4)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,5)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,6)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,6)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,7)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.sorting(d, wd,7)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));

    //PAGINATION
    pc = pc.then(() => orderPage.paginationEntryClick(d, wd,2)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.paginationEntryClick(d, wd,0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));

    pc = pc.then(() => orderPage.paginationClick(d, wd,'activeNext')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));
    pc = pc.then(() => orderPage.paginationClick(d, wd,'activePrevious')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/sales/commissions', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Manage commission Flow by SQ-admin');
    return pc;
}

module.exports = {manage_commission_by_SQ_admin};