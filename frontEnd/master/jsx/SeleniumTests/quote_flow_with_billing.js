const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const quote_NewQuote= require('./FT/afterLogin/quote/newQoutes');
const quote_BillingQuote = require('./FT/afterLogin/quote/cart');
const customer_Info = require('./FT/afterLogin/order/customerInfo');
const billing_account = require('./FT/afterLogin/accountingActions/accountingActionFlow');
const cart = require('./FT/afterLogin/quote/cart');
const viewQuote= require('./FT/afterLogin/quote/viewQuotePage');

const cf = require('./FT/commonFunctions/cf');
let _u = undefined;
function create_quote_flow_with_billing(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Quote Flow with billing'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=signIn', _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=signIn', _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=signIn', _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => quote_NewQuote.newQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Absolute + Door (Laminate)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd,'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,288.69']));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-4")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,596.89']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,596.89']));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,1000)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-right')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,596.89']));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-right-3')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,596.89']));
    pc = pc.then(() => quote_SizeAndPanels.panelDirectionSelection(d, wd,"swingInOrOut-Outswing")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,596.89']));

    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-No Screen  ")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,596.89']));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Left")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,628.14']));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Right")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,628.14']));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Split")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,628.14']));

    // COLOR
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'extColor_basic_all_White_null_#FFFFFF_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,628.14']));

    //GLASS
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,997.51']));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Low-E 3')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,106.26']));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,254.49']));

    //HARDWARE
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,376.99']));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,0)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-London Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,376.99']));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,0)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Bronze')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,376.99']));

    pc = pc.then(() => quote_Hardware.hardWareOptionSelect(d, wd, 'extras-Keyed Alike')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,401.49']));

    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d, ['Absolute + Door (Laminate)','Vinyl','100" width x 90" height','101" width x 91" height',
    '4','23.12"','85.5"','Right','Outswing','Left 0 / Right 3','Aluminum Block','3/4 Upstand','White','Low-E 3','Argon',
        'New York Bronze','Keyed Alike','Panoramic Screen Split',
    'Item 1 of 1','$8,401.49','Total $8,401.49']));
    pc = pc.then(() => billing_account.notesInput(d, wd, 'test notes here')).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => billing_account.productionNotesInput(d, wd, 'Production notes here')).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cart.quantityInput(d, wd,2)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute + Door (Laminate)','Vinyl','100" width x 90" height','101" width x 91" height',
        '4','23.12"','85.5"','Right','Outswing','Left 0 / Right 3','Aluminum Block','3/4 Upstand','White','Low-E 3','Argon',
        'New York Bronze','Keyed Alike','Panoramic Screen Split',
        'Item 1-2 of 2','$8,401.49','$16,802.98','Total $16,802.98']));

    pc = pc.then(() => cart.duplicateClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute + Door (Laminate)','Vinyl','100" width x 90" height','101" width x 91" height',
        '4','23.12"','85.5"','Right','Outswing','Left 0 / Right 3','Aluminum Block','3/4 Upstand','White','Low-E 3','Argon',
        'New York Bronze','Keyed Alike','Panoramic Screen Split',
        'Item 1-2 of 4','Item 3-4 of 4','$8,401.49','$16,802.98','Total $33,605.96']));

    pc = pc.then(() => quote_BillingQuote.saveAsQuote(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));

    // SEARCH CUSTOMER
    pc = pc.then(() => customer_Info.searchPreviousCustomerClick(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => customer_Info.searchPreviousCustomerInput(d, wd,'a')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => customer_Info.useShippingAddress(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));

    pc = pc.then(() => customer_Info.searchAttach(d, wd)).then(() => cf.test(d, 'viewSavedQuoteConfirmation', Config.BASE_URL + '?tab=viewSavedQuoteConfirmation&', true, _u));

    // VIEW QUOTE
    pc = pc.then(() => customer_Info.quoteViewButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => customer_Info.revenueTypeSelect(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => customer_Info.channelTypeSelect(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    // pc = pc.then(() => customer_Info.dueDatevViewOrder(d, wd, '04/29/2019')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    // pc = pc.then(() => billing_account.onHoldSelection(d, wd, 1)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    // pc = pc.then(() => billing_account.onHoldSelection(d, wd, 2)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => billing_account.followUpNotesInput(d, wd, 'test follow up')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => viewQuote.qualitySelect(d)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => billing_account.pickupDeliverySelection(d,wd,1)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => billing_account.poInput(d,wd,90909)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => billing_account.commentInput(d,wd,'test comment')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));

    // CUSTOMER INFO
    pc = pc.then(() => billing_account.billingContactInput(d, wd, '123456987')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => billing_account.billingEmailInput(d, wd, 'test_billing@gmail.com')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => billing_account.billingPhoneInput(d, wd, '714-715-6279')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => billing_account.billingAddress1Input(d, wd, 'Florida (FL)')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => billing_account.billingAddress2Input(d, wd, '6747 Everglades ')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => billing_account.billingCityInput(d, wd, 'Mesa')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => billing_account.billingStateInput(d, wd, 'AZ')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => billing_account.billingZipInput(d, wd, '85213')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => billing_account.shippingContactInput(d, wd, '123456987')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => billing_account.shippingEmailInput(d, wd, 'test_billing@gmail.com')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => billing_account.shippingPhoneInput(d, wd, '714-715-6279')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => billing_account.shippingAddress1Input(d, wd, 'Florida (FL)')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => billing_account.shippingAddress2Input(d, wd, '6747 Everglades ')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => billing_account.shippingCityInput(d, wd, 'Mesa')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => billing_account.shippingStateInput(d, wd, 'AZ')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => billing_account.shippingZipInput(d, wd, '85213')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Quote Flow with billing');
    return pc;
}

function manage_customer_view_quote(capability) {
    let d = Config.getDriver(Config.ChromeCapabilities[capability]);
    d.manage().window().maximize();
    let pc;
    let title1='customerView', title2='customer';

    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Manage Customer in View Quote'));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=signIn', _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=signIn', _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=signIn', _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));

    // MANAGE CUSTOMER IN QUOTE
    pc = pc.then(() => viewQuote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));

    // EDIT CUSTOMER CANCEL
    pc = pc.then(() => customer_Info.editCustomerClick(d, wd)).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+ title1, true, _u));
    pc = pc.then(() => customer_Info.saveCancelCustomerInfo(d, wd,2)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // EDIT CUSTOMER SAVE
    pc = pc.then(() => customer_Info.editCustomerClick(d, wd)).then(() => cf.test(d,title1, Config.BASE_URL + '?tab=' + title1, true, _u));
    pc = pc.then(() => customer_Info.leadSourceSelect(d,0)).then(() => cf.test(d, title1, Config.BASE_URL + '?tab=' + title1, true, _u));
    pc = pc.then(() => customer_Info.saveCancelCustomerInfo(d, wd,1)).then(() => cf.test(d, title1, Config.BASE_URL + '?tab=' + title1, true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Please enter the lead Source']));

    pc = pc.then(() => customer_Info.leadSourceSelect(d,1)).then(() => cf.test(d,title1, Config.BASE_URL + '?tab='+title1, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'companyName', 'cxxx')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab=' + title1, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'firstName', 'fname')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+title1, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'lastName', 'lname')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+title1, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'email', 'test@gmail.com')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+title1, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'phone', '8989898989')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+title1, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billingName', 'test')).then(() => cf.test(d, title1, Config.BASE_URL +'?tab='+title1, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billingEmail', 'test@gmail.com')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+title1, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billingPhone', '89787878')).then(() => cf.test(d, title1, Config.BASE_URL +'?tab='+title1, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billingAddress1', 'test address 1')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+title1, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billingAddress2', 'test address 2')).then(() => cf.test(d, title1, Config.BASE_URL +'?tab='+title1, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billingCity', 'Mumbai')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+title1, true, _u));
    pc = pc.then(() => customer_Info.billingStateSelect(d, wd,3)).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+title1, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billingZip', '10001')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+title1, true, _u));
    pc = pc.then(() => billing_account.useShippingCheckbox(d, wd)).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+title1, true, _u));
    pc = pc.then(() => customer_Info.saveCancelCustomerInfo(d, wd,1)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));

    //CHECKING ENTERED DATA IN FORM
    pc = pc.then(() => cf.testContentByArray(d, ['fname lname','8989898989','test@gmail.com']));
    pc = pc.then(() => customer_Info.editCustomerClick(d, wd)).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+ title1, true, _u));

    pc = pc.then(() => customer_Info.checkInputBoxData(d, wd,'firstName','fname')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+ title1, true, _u));
    pc = pc.then(() => customer_Info.checkInputBoxData(d, wd,'lastName','lname')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+ title1, true, _u));
    pc = pc.then(() => customer_Info.checkInputBoxData(d, wd,'email','test@gmail.com')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+ title1, true, _u));
    pc = pc.then(() => customer_Info.checkInputBoxData(d, wd,'phone','8989898989')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+ title1, true, _u));
    pc = pc.then(() => customer_Info.checkInputBoxData(d, wd,'billingName','test')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+ title1, true, _u));
    pc = pc.then(() => customer_Info.checkInputBoxData(d, wd,'billingEmail','test@gmail.com')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+ title1, true, _u));
    pc = pc.then(() => customer_Info.checkInputBoxData(d, wd,'billingPhone','89787878')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+ title1, true, _u));
    pc = pc.then(() => customer_Info.checkInputBoxData(d, wd,'billingAddress1','test address 1')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+ title1, true, _u));
    pc = pc.then(() => customer_Info.checkInputBoxData(d, wd,'billingAddress2','test address 2')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+ title1, true, _u));
    pc = pc.then(() => customer_Info.checkInputBoxData(d, wd,'billingCity','Mumbai')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+ title1, true, _u));
    pc = pc.then(() => customer_Info.checkInputBoxData(d, wd,'billingZip','10001')).then(() => cf.test(d, title1, Config.BASE_URL + '?tab='+ title1, true, _u));

    pc = pc.then(() => customer_Info.saveCancelCustomerInfo(d, wd,2)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));


    // ATTACH DIFFERENT CUSTOMER
    pc = pc.then(() => customer_Info.attachCustomerClick(d, wd)).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.attachToQuoteClick(d, wd)).then(() => cf.testAlertData(d, 'Please enter your lead source'));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));

    // NEW CUSOTMER
    pc = pc.then(() => customer_Info.leadSourceSelect(d,1)).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'firstName', 'fname')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'lastName', 'lname')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'email', 'test@gmail.com')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'phone', '8989898989')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billing_contact', 'test')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billing_email', 'test@gmail.com')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billing_phone', '89787878')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billing_address1', 'test address 1')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billing_address2', 'test address 2')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billing_city', 'Mumbai')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billing_state', 'Arizona')).then(() => cf.test(d, title2, Config.BASE_URL +'?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billing_zip', '10001')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => billing_account.useShippingCheckbox(d, wd)).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.attachToQuoteClick(d, wd,1)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));

    // SEARCH CUSTOMER
    pc = pc.then(() => customer_Info.attachCustomerClick(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer', true, _u));
    pc = pc.then(() => customer_Info.searchPreviousCustomerClick(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer', true, _u));

    // EDIT SEARCH CUSTOMER
    pc = pc.then(() => customer_Info.searchPreviousCustomerInput(d, wd,'fname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer', true, _u));
    pc = pc.then(() => customer_Info.searchDataActionClick(d, wd,0)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer', true, _u));;

    pc = pc.then(() => customer_Info.leadSourceSelect(d,1)).then(() => cf.test(d, title2, Config.BASE_URL + '?tab=' + title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'firstName', 'fname')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'lastName', 'lname')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'email', 'test@gmail.com')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'phone', '8989898989')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billingName', 'test')).then(() => cf.test(d, title2, Config.BASE_URL +'?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billingEmail', 'test@gmail.com')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billingPhone', '89787878')).then(() => cf.test(d, title2, Config.BASE_URL +'?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billingAddress1', 'test address 1')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billingAddress2', 'test address 2')).then(() => cf.test(d, title2, Config.BASE_URL +'?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billingCity', 'Mumbai')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.billingStateSelect(d, wd,3)).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.inputValue(d, wd,'billingZip', '10001')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.saveCancelCustomerInfo(d, wd,0)).then(() => cf.test(d, title2, Config.BASE_URL + '?tab=' + title2, true, _u));

    // DELETE SEARCH CUSTOMER
    pc = pc.then(() => customer_Info.searchPreviousCustomerInput(d, wd,'fname ')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer', true, _u));
    pc = pc.then(() => customer_Info.searchDataActionClick(d, wd,1));
    pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer', true, _u));

    // ATTACH SEARCH CUSTOMER
    pc = pc.then(() => customer_Info.searchPreviousCustomerInput(d, wd,'a ')).then(() => cf.test(d, title2, Config.BASE_URL + '?tab='+title2, true, _u));
    pc = pc.then(() => customer_Info.useShippingAddress(d, wd)).then(() => cf.test(d, title2, Config.BASE_URL + '?tab=' +title2, true, _u));
    pc = pc.then(() => customer_Info.searchAttach(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));

    // CANCEL ATTACH CUSTOMER
    pc = pc.then(() => customer_Info.attachCustomerClick(d, wd)).then(() => cf.test(d, title2, Config.BASE_URL + '?tab=' +title2, true, _u));
    pc = pc.then(() => customer_Info.closeAttachCustomer(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Manage Customer in View Quote');
    return pc;
}

module.exports = {
    create_quote_flow_with_billing,
    manage_customer_view_quote
};
