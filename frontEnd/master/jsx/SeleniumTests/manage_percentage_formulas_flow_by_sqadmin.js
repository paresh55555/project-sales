const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/sqAdmin/auth/login');
const sideBar = require('./FT/sqAdmin/sideBar/sideBar');
const percentageFormulasPage = require('./FT/sqAdmin/module/percentageFormulasPage');

let _u = undefined;

function manage_percentage_formulas_flow_by_sqadmin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL + "SQ-admin/login").then(() => cf.changeSessionTestName(d, 'US: Manage Percentage Formulas by sales person'));
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/login', _u, _u));
    pc = pc.then(() => login.logIn(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/users', true, _u));
    pc = pc.then(() => sideBar.modulesClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
    pc = pc.then(() => percentageFormulasPage.vinylwithAluminumBlockFrameClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.percentageFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.addPercentageFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.optionNameInput(d, wd, 'panel test', 'name of option')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.optionNameInput(d, wd, 80, 'percentage')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.savePercentageFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));

    pc = pc.then(() => percentageFormulasPage.percentageFormulasSearchInput(d, wd, 'panel test')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.percentageFormulasRecordClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
    pc = pc.then(() => percentageFormulasPage.optionNameInput(d, wd, 'panel test', 'name of option', 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.optionNameInput(d, wd, 80, 'percentage', 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.optionNameInput(d, wd, 'test', 'percentage', 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.savePercentageFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['This value seems to be invalid.']));
    pc = pc.then(() => percentageFormulasPage.optionNameInput(d, wd, 80, 'percentage', 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.savePercentageFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));

    pc = pc.then(() => percentageFormulasPage.percentageFormulasSearchInput(d, wd, 'panel test')).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.percentageFormulasRecordClick(d, wd, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules', true, _u));
    pc = pc.then(() => percentageFormulasPage.deletePercentageFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.confirmPercentageFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.confirmPercentageFormulasClick(d, wd)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.changeOrderClick(d, 0)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));
    pc = pc.then(() => percentageFormulasPage.changeOrderClick(d, 1)).then(() => cf.test(d, 'SQ Admin', Config.BASE_URL + 'SQ-admin/modules/', true, _u));

    pc = cf.handleOutput(d, pc, capability, 'Manage Percentage Formulas by sales person');
    return pc;
}

module.exports = { manage_percentage_formulas_flow_by_sqadmin };