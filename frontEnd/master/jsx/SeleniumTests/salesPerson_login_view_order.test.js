const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const quote_ViewOrder= require('./FT/afterLogin/order/viewOrder');
const quote_order_customerInfo = require('./FT/afterLogin/order/customerInfo');
const accounting = require('./FT/afterLogin/accountingActions/accountingActionFlow');
const customerInfo = require('./FT/afterLogin/order/customerInfo');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const cart = require('./FT/afterLogin/quote/cart');

const moment = require('moment');
let _u = undefined;

function view_order_by_salesPersonLogin(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: SalesPerson view Order flow'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrdersNeedsPaymentClick(d, wd)).then(() => cf.test(d, 'viewOrdersNeedsPayment', Config.BASE_URL + '?tab=viewOrdersNeedsPayment&', true, _u));

    pc = pc.then(() => quote_ViewOrder.orderIdClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // ADD DOOR
    pc = pc.then(() => accounting.chooseActionClick(d, wd,1)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Signature Door (Aluminum)')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 70)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, "track-3/4 Upstand")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,725.08']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,725.08']));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,725.08']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingInOrOut-Outswing')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,725.08']));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,'screens-Panoramic Screen Split')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,527.16']));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => quote_ColorAndFinish.signatureDoorColorClick(d, wd,'extColor_basicSpecial_all_Statuary Bronze_null_#352B20_')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,527.16']));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,845.34']));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$7,972.60']));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, "hardware-London Nickel")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$8,104.20']));
    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d,
        ['Signature Door (Aluminum)','100" width x 70" height','101" width x 71" height','3','31.44"','65.5"','Left','Outswing','Left 2 / Right 0',
            'Aluminum Block','3/4 Upstand','Statuary Bronze','Statuary Bronze','Glass Clear','Argon','London Nickel','Door Restrictor','Panoramic Screen Split']
    ));
    pc = pc.then(() => accounting.scroll(d, wd,0)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    pc = pc.then(() => quote_order_customerInfo.revenueTypeSelect(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.channelTypeSelect(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    // It is depend on screen order or not
    // pc = pc.then(() => accounting.onHoldSelection(d, wd,0)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    // pc = pc.then(() => accounting.dateDueInput(d, wd,'ui-datepicker-current-day')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.followUpNotesInput(d, wd,'This is follow up notes')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // EDIT CUSTOMER
    pc = pc.then(() => quote_order_customerInfo.editCustomerClick(d, wd)).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.leadSourceSelect(d,2)).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.comapanyNameInput(d,wd,'cxxx')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.firstNameInput(d, wd, 'fname')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.lastNameInput(d, wd, 'lname')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.emailInput(d, wd, 'test@gmail.com')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.phoneInput(d, wd, '8989898989')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingNameInput(d, wd, 'test')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingEmailInput(d, wd, 'billingtest@email.com')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingPhoneInput(d, wd, '89787878')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingAddress1Input(d, wd, 'test address 1')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingAddress2Input(d, wd, 'test address 2')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingCityInput(d, wd, 'testing City')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.billingZipInput(d, wd, '10001')).then(() => cf.test(d, 'customerView', Config.BASE_URL + '?tab=customerView&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.saveCancelCustomerInfo(d, wd,1)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // ATTACH CUSTOMER
    pc = pc.then(() => quote_order_customerInfo.attachCustomerClick(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.leadSourceSelect(d,2)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.firstNameInput(d, wd, 'fname test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.lastNameInput(d, wd, 'lname test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.emailInput(d, wd, 'testcust@gmail.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.phoneInput(d, wd, '8888888')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingContactInputName(d, wd, 'contact test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d, wd, 'address1')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d, wd, 'address2')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingCityInputName(d, wd, 'billing city')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingStateInputName(d, wd, 'billing state')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingZipInputName(d, wd, '303999')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.attachToQuoteClick(d, wd, 'nextTitle')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // PROJECT INFO SECTION
    pc = pc.then(() => accounting.scroll(d, wd,700)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.pickupDeliverySelection(d, wd,1)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.poInput(d, wd,9090)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.commentInput(d, wd,'This is a comment')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['fname test','lname test','testcust@gmail.com',
        '8888888','This is follow up notes','contact test','address1','address2',
    'billing city','billing state','303999','This is follow up notes']));

    pc = pc.then(() => accounting.scroll(d, wd,0)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // READ GRAND TOTAL
    let gTotal
    pc = pc.then(() => quote_ViewOrder.getGrandTotal(d, wd)).then((total) => gTotal = total);

    // BILLING INFO
    pc = pc.then(() => accounting.scroll(d, wd, 1000)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.billingContactInput(d, wd, 'b contact')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.billingAddress1Input(d, wd, 'b address1')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.billingAddress1Input(d, wd, 'b address2')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.billingCityInput(d, wd, 'b city')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.billingStateInput(d, wd, 'b state')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.billingZipInput(d, wd, '10001')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.billingEmailInput(d, wd, 'bemail@email.com')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.billingPhoneInput(d, wd, '999999')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // SHIPPING INFO
    pc = pc.then(() => accounting.shippingContactInput(d, wd, 's contact')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.shippingAddress1Input(d, wd, 's address1')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.shippingAddress2Input(d, wd, 's address2')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.shippingCityInput(d, wd, 's city')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.shippingStateInput(d, wd, 's state')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.shippingZipInput(d, wd, '787878')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.shippingEmailInput(d, wd, 'semail@gmail.com')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.shippingPhoneInput(d, wd, '888888')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // CHECKING GRAND TOTAL
    pc = pc.then(() => quote_ViewOrder.testGrandTotal$(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // MANAGE DISCOUNT
    pc = pc.then(() => accounting.chooseActionClick(d, wd,2)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrdersCompletedClick(d, wd)).then(() => cf.test(d, 'viewOrdersCompleted', Config.BASE_URL + '?tab=viewOrdersCompleted&', true, _u));
    pc = pc.then(() => quote_ViewOrder.orderIdClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    pc = pc.then(() => accounting.scroll(d, wd,1800)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.selectDiscountReason(d, wd, 'newReason',1)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.discountMessage(d, wd, 'testing discount message PM')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.makeRequestButtonClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Production - Mistake','testing discount message PM']));
    pc = pc.then(() => accounting.deleteRequest(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.selectDiscountReason(d, wd, 'newReason',2)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.discountMessage(d, wd, 'testing discount message SD')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.makeRequestButtonClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Shipping - Delay','testing discount message SD']));
    pc = pc.then(() => accounting.deleteRequest(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.selectDiscountReason(d, wd, 'newReason',3)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.discountMessage(d, wd, 'testing discount message SM')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.makeRequestButtonClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Shipping - Mistake','testing discount message SM']));
    pc = pc.then(() => accounting.deleteRequest(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.selectDiscountReason(d, wd, 'newReason',0)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.discountMessage(d, wd, 'testing discount message PD')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => accounting.makeRequestButtonClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Production - Delay','testing discount message PD']));
    pc = pc.then(() => accounting.deleteRequest(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // pc = pc.then(() => accounting.chooseActionClick(d, wd,2)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    // // Delete Item
    // pc = pc.then(() => accounting.scroll(d, wd,1800)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    // pc = pc.then(() => accounting.cartItemClick(d, wd,3))
    // pc = pc.then(() => cf.handleAlertCancel(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    // pc = pc.then(() => accounting.cartItemClick(d, wd,3))
    // pc = pc.then(() => cf.handleAlertOK(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // ORDER STATUS CLICK
    pc = pc.then(() => quote_ViewOrder.viewOrderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrdersNeedsPaymentClick(d, wd)).then(() => cf.test(d, 'viewOrdersNeedsPayment', Config.BASE_URL + '?tab=viewOrdersNeedsPayment&', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrdersHoldClick(d, wd)).then(() => cf.test(d, 'viewOrdersHold', Config.BASE_URL + '?tab=viewOrdersHold&', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrdersAccountingClick(d, wd)).then(() => cf.test(d, 'viewOrdersAccounting', Config.BASE_URL + '?tab=viewOrdersAccounting&', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrdersPreProductionClick(d, wd)).then(() => cf.test(d, 'viewOrdersPreProduction', Config.BASE_URL + '?tab=viewOrdersPreProduction&', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrdersProductionClick(d, wd)).then(() => cf.test(d, 'viewOrdersProduction', Config.BASE_URL + '?tab=viewOrdersProduction&', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrdersCompletedClick(d, wd)).then(() => cf.test(d, 'viewOrdersCompleted', Config.BASE_URL + '?tab=viewOrdersCompleted&', true, _u));
    pc = pc.then(() => quote_ViewOrder.viewOrdersDeliveredClick(d, wd)).then(() => cf.test(d, 'viewOrdersDelivered', Config.BASE_URL + '?tab=viewOrdersDelivered&', true, _u));
    pc = cf.handleOutput(d, pc, capability, 'SalesPerson view Order flow');
    return pc;
}

module.exports = {view_order_by_salesPersonLogin};