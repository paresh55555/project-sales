const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const l1 = require('./FT/l1');
const cf = require('./FT/commonFunctions/cf');
const quote_NewQuote= require('./FT/afterLogin/quote/newQoutes');
const quote_DoorType = require('./FT/afterLogin/quote/doorType');
const quote_SizeAndPanels = require('./FT/afterLogin/quote/sizeAndPanels');
const quote_ColorAndFinish = require('./FT/afterLogin/quote/colorAndFinish');
const quote_Glass = require('./FT/afterLogin/quote/glass');
const quote_Hardware = require('./FT/afterLogin/quote/hardware');
const cart = require('./FT/afterLogin/quote/cart');
const quote_order_customerInfo = require('./FT/afterLogin/order/customerInfo');
const accounting = require('./FT/afterLogin/accountingActions/accountingActionFlow');
const quote_viewQuote= require('./FT/afterLogin/quote/viewQuotePage');
const quote_ViewOrder= require('./FT/afterLogin/order/viewOrder');
const moment = require('moment');
let _u = undefined;

function create_order_with_screen_option(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'US: Create a order with screen option'));
    // pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));
    pc = pc.then(() => l1.loginLinkClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));
    pc = pc.then(() => quote_NewQuote.newQuoteClick(d, wd)).then(() => cf.test(d, 'home', Config.BASE_URL + '?tab=home&', true, _u));
    pc = pc.then(() => quote_DoorType.doorTypeClick(d, wd,'Absolute Door (Vinyl)')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['T.B.D.','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.widthInput(d, wd, 100)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => quote_SizeAndPanels.heightInput(d, wd, 90)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['T.B.D.','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.trackTypeSelection(d, wd, 'track-3/4 Upstand')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['T.B.D.','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.panelSelection(d, wd, "panels-3")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,275.02']));
    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-right')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,275.02']));
    pc = pc.then(() => quote_SizeAndPanels.scrollBottom(d, wd,500)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => quote_SizeAndPanels.swingDirectionSelection(d, wd,'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,275.02','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.panelMovementSelection(d, wd,'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,275.02','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.panelDirectionSelection(d, wd,"swingInOrOut-Outswing")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$5,275.02','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.screenSelection(d, wd,"screens-Panoramic Screen Split")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,306.27','powered by','Sales Quoter']));
    pc = pc.then(() => quote_SizeAndPanels.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));

    pc = pc.then(() => quote_ColorAndFinish.doorColorClick(d, wd,'vinylColor_vinyl_all_White_null_#fcffff_null')).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,306.27','powered by','Sales Quoter']));
    pc = pc.then(() => quote_ColorAndFinish.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => quote_Glass.glassTypeSelect(d, wd,'glass-Glass Clear')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,707.09','powered by','Sales Quoter']));
    pc = pc.then(() => quote_Glass.glassOptionSelect(d, wd,'glass-Argon')).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,867.66','powered by','Sales Quoter']));
    pc = pc.then(() => quote_Glass.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));

    pc = pc.then(() => quote_Hardware.hardWareSelect(d, wd, 'hardware-New York Nickel')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,990.16','powered by','Sales Quoter']));

    pc = pc.then(() => quote_Hardware.addToQuoteClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl','100" width x 90" height',
        '101" width x 91" height','3','31.35"','85.5"','Left','Outswing','Left 2 / Right 0','Aluminum Block','3/4 Upstand',
        'None','Glass Clear','Argon','New York Nickel','Door Restrictor','Panoramic Screen Split','Item 1 of 1',
        '$6,990.16','Total $6,990.16']
    ));
    pc = pc.then(() => cart.saveAsQuote(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));

    // CUSTOMER INFO
    pc = pc.then(() => quote_order_customerInfo.leadSourceSelect(d,2)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.firstNameInput(d, wd, 'fname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.lastNameInput(d, wd, 'lname')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.emailInput(d, wd, 'test@gmail.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.phoneInput(d, wd, '8989898989')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingContactInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingEmailInputName(d, wd, 'test@email.com')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingPhoneInputName(d, wd, '898989')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress1InputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingAddress2InputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingCityInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingStateInputName(d, wd, 'test')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.billingZipInputName(d, wd, '898989')).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => accounting.useShippingCheckbox(d, wd)).then(() => cf.test(d, 'customer', Config.BASE_URL + '?tab=customer&', true, _u));
    pc = pc.then(() => quote_order_customerInfo.attachToQuoteClick(d, wd)).then(() => cf.test(d, 'viewSavedQuoteConfirmation', Config.BASE_URL + '?tab=viewSavedQuoteConfirmation&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Quote Name: fname lname']));
    pc = pc.then(() => quote_order_customerInfo.quoteViewButtonClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    // CONVERT QOUTE TO ORDER
    // pc = pc.then(() => quote_viewQuote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => quote_order_customerInfo.revenueTypeSelect(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => quote_order_customerInfo.channelTypeSelect(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));
    pc = pc.then(() => accounting.dateDueInput(d,wd,' ui-datepicker-days-cell-over  ui-datepicker-current-day ui-datepicker-today')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => accounting.followUpNotesInput(d,wd, "This is follow up notes")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_viewQuote.qualitySelect(d)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    // pc = pc.then(() => quote_viewQuote.followupInput(d,wd,'followUp-0')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    // pc = pc.then(() => quote_viewQuote.followupInput(d,wd,'secondFollowUp-0')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => quote_viewQuote.convertToSale(d, wd, '-convertToSale'));

    pc = pc.then(() => cf.handleAlertOK(d));
    // let order
    // pc = pc.then(() => cf.getAlertText(d)).then((o)=>order=o);
    pc = pc.then(() => cf.handleAlertOK(d));

    // VIEW ORDER
    pc = pc.then(() => quote_ViewOrder.viewOrderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    // pc = pc.then(() => quote_ViewOrder.searchOrderInput(d, wd,order)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => quote_ViewOrder.orderIdClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl','100" width x 90" height',
        '101" width x 91" height','3','31.35"','85.5"','Left','Outswing','Left 2 / Right 0','Aluminum Block','3/4 Upstand',
        'None','Glass Clear','Argon','New York Nickel','Door Restrictor','Panoramic Screen Split','Item 1 of 1',
        '$6,990.16','Total $6,990.16',
        'Revenue Type:','Channel:','Date Ordered:','Date Due:','On Hold:']
    ));
    // ENTER PAYMENT INFO
    pc = pc.then(() => accounting.paymentInfoClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting.paymentOptionClick(d, wd,0)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting.last4DigitsOfCreditCardInput(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting.paymentAmountInput(d, wd, '6990.16')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    // pc = pc.then(() => accounting.paymentDateSelect(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => accounting.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['$6,990.16','Check 1111']));
    pc = pc.then(() => accounting.closePaymentInfo(d,wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => quote_ViewOrder.sendToProduction(d, wd));
    pc = pc.then(() => cf.handleAlertOK(d));
    pc = pc.then(() => l1.salesPersonLogout(d,wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));

    // ACCOUNTING LOGIN

    pc = pc.then(() => l1.EmailInput(d, wd, "PDAccounts")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=verifyDeposit', true, _u));

    pc = pc.then(() => accounting.paymentBox1Click(d,wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting.depositSatisfiedSelect(d, wd, 1)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => accounting.depositSatisfiedClick(d, wd)).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => l1.salesPersonLogout(d,wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL + '?tab=signIn', true, _u));

    // SALES PERSON LOGIN
    pc = pc.then(() => l1.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', _u, _u, _u));
    pc = pc.then(() => l1.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewQuotes&r=', true, _u));

    pc = pc.then(() => quote_ViewOrder.viewOrderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));

    pc = pc.then(() => quote_ViewOrder.orderIdClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Screens','None','Panoramic Screen Split',
        'White Vinyl','3','31.35"','Left','Outswing','Left 2 / Right 0','Aluminum Block','3/4 Upstand',
        'Glass Clear','Argon','New York Nickel','Door Restrictor','Item 1 of 1',]
    ));
    pc = pc.then(() => cf.testContentByArray(d, [
        'Revenue Type:','Channel:','Date Ordered:','Date Due:','On Hold:',
        moment().format('MM/DD/YYYY')]
    ));

    pc = pc.then(() => quote_ViewOrder.productionStatusOrderClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => cf.testContentByArray(d, ['Absolute Door (Vinyl)','White Vinyl','3','31.35"','85.5"',
        'Left','Outswing','Left 2 / Right 0','Aluminum Block','3/4 Upstand','None','Argon','New York Nickel','Door Restrictor',
        'Panoramic Screen Split','Total $6,990.16','$6,990.16','Item 1 of 1','Document Upload']
    ));

    pc = cf.handleOutput(d, pc, capability, 'Create a order with screen option');
    return pc;
}

module.exports = {create_order_with_screen_option};