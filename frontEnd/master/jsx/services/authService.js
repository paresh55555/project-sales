import axios from 'axios';
import * as constant from '../constant.js'
import { replace,push } from 'react-router-redux';

axios.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
  }, function (error) {
    // Do something with response error
    if(error.status==401){
    	return replace('/' )
    }
    return Promise.reject(error);
  });
export function loggedIn(){
	if(!!sessionStorage.frontend_token){
		axios.defaults.headers.common['Authorization'] = 'Bearer '+ sessionStorage.frontend_token;
	}
	return !!sessionStorage.frontend_token
}
export function logout(){
	delete sessionStorage.frontend_token;
	location.href = '/' ;
}
 
