/* global __API__ */
// @flow
import request from 'axios'
import axios from 'axios'
import when from 'when'
import {API_URL} from './ApiServerConfigs.js'
const root: string = API_URL

export function getHeaders (): Object {
  const headers = {'Authorization':'Bearer '+ sessionStorage.frontend_token};
  return headers
}
export function hasToken (): boolean {
  return !!getHeaders()
}
export function get (path: string, params: Object = {}): Promise {
  return when(request.get(root + path, {
    headers: getHeaders(),
    params: params
  }).catch((error) => {
    console.error('GET ERROR', error)
    console.error(error.stack)
  }))
}
export function post (path: string, body: Object): Promise {
  const headers = getHeaders()
  return when(request.post(root + path, body, {headers: {...headers, 'Content-Type': 'application/json'}})
  .catch((e) => {
    console.error('POST ERROR', e)
    throw e
  }))
}
export function put (path: string, body: Object): Promise {
  return when(request.put(root + path, body, {headers: getHeaders()}))
  .catch((e) => {
    console.error('PUT ERROR', e)
    throw e
  })
}
export function del (path: string, data: Object): Promise {
  const headers = getHeaders()
  return when(request.delete(root + path, {data, headers: {...headers, 'Content-Type': 'application/json'}}))
  .catch((e) => {
    console.error('DEL ERROR', e)
    throw e
  })
}