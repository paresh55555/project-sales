import * as types from '../actions/actionTypes';

export function cartReducer(state = {cartItems:{}}, action = null){
    switch(action.type) {
    		case types.SET_CART_ITEMS:
    			sessionStorage.setItem('cartItems', JSON.stringify(action.data))
    			return Object.assign({}, state, {cartItems:action.data});
        default:
            return state;
    }
};