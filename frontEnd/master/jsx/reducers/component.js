import * as types from '../actions/actionTypes';

export function componentReducer(state = {
    isLoading: false,
    error: false,
    data: [],
    cart: {
        width: 0,
        height: 0,
        swingDoorDirection: '',
        numberLeft: null
    }}
    , action = null) {
    switch(action.type) {
        case types.GET_PANEL_RANGE:
            return Object.assign({}, state, {isLoading: true, error: false});
        case types.GET_PANEL_RANGE_SUCCESS:
            return Object.assign({}, state, {isLoading: false, error: false, data: action.data});
        case types.GET_PANEL_RANGE_ERROR:
            return Object.assign({}, state, {isLoading: false, error: true, data: action.data});
        case types.SET_COMPONENT_SIZE:
            return Object.assign({}, state, {cart: Object.assign({}, state.cart, {width: action.width, height: action.height})});
        case types.SET_SWINGDOORDIRECTION:
            return Object.assign({}, state, {cart: Object.assign({}, state.cart, {swingDoorDirection: action.direction})});
        case types.SET_PANEL_MOVEMENT:
            return Object.assign({}, state, {cart: Object.assign({}, state.cart, {numberLeft: action.numberLeft})});
        case types.SET_PANELS_AMOUNT:
            return Object.assign({}, state, {cart: Object.assign({}, state.cart, {panels: action.amount})});
        default:
            return state;
    }
};