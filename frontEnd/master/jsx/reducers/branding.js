import{SET_SITE_BRANDING} from '../actions/actionTypes';

export function brandingReducer(state = {
	isLoading: true,
	siteBranding: {}
}
, action = null) {
	switch(action.type) {
		case SET_SITE_BRANDING:
			return {...state,siteBranding:action.payload,isLoading:false};
		default:
			return state;
	}
};