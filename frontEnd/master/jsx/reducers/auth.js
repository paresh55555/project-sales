import * as types from '../actions/actionTypes';

export function authReducer(state = {
	isLoading: false,
	data: [],
	error: false}
, action = null) {
	switch(action.type) {
		 
		case types.LOGIN_ERROR:
			return Object.assign({}, state, {isLoading: false, data: action.data, error: true});
		case types.LOGIN_SUCCESS:
			return Object.assign({}, state, {isLoading: false, data: action.data, error: false });
		case types.LOGIN:
			return Object.assign({}, state, {isLoading: true, error: false });
		default:
			return state;
	}
};