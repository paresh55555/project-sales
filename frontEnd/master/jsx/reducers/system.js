import{SET_SYSTEMGROUPS,SET_SYSTEMCOMPONENTS} from '../actions/actionTypes';

export function systemReducer(state = {
	isLoading: false,
	system_groups: [],
    systemComponents: [],
	error: false}
, action = null) {
	switch(action.type) {
		 
		case SET_SYSTEMGROUPS:
			return {...state,system_groups:action.payload};
        case SET_SYSTEMCOMPONENTS:
            return {...state,systemComponents:action.payload};
		default:
			return state;
	}
};