import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';
import {authReducer} from './auth';
import {systemReducer} from './system'
import {brandingReducer} from './branding'
import {componentReducer} from './component'
import {cartReducer} from './cart'

const rootReducer = combineReducers({
	routing: routerReducer,
	auth:authReducer,
	system:systemReducer,
	branding: brandingReducer,
	component: componentReducer,
	cart: cartReducer
});

export default rootReducer;