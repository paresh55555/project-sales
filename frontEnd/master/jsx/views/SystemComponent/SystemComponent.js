import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Tabs, Tab, Col, Panel, Button, Form, ControlLabel, FormControl, Radio, FormGroup } from 'react-bootstrap';
import ComponentLayout from '../../components/ComponentLayout/ComponentLayout'
import Dimensions from '../../components/Dimensions/Dimensions'
import Color from '../../components/Color/Color'
import SwingDirection from '../../components/SwingDirection/SwingDirection'
import DoorPanels from '../../components/Door/DoorPanels'
import PanelMovement from '../../components/PanelMovement/PanelMovement'
import WindowBuilder from '../../components/WindowBuilder/WindowBuilder'
import Cart from '../../components/Cart'
import {systemAction, componentAction, cartAction} from '../../actions';
import NextTab from "../../components/NextTab/NextTab";

class SystemComponent extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            selectedMenu: null,
            menuIDs: []
        };
    }
    componentWillMount(){
        // this.props.dispatch(actions.getSystemComponent(this.props.params.id));
        this.props.getSystemComponent(this.props.params.id);
    }
    componentDidMount() {
        const { route } = this.props;
        const { router } = this.context;
        router.setRouteLeaveHook(route, this.routerWillLeave)
    }
    routerWillLeave(nextLocation) {
        localStorage.removeItem('cart')
    }
    componentWillReceiveProps(nextProps){
        var menu = nextProps.system.systemComponents && nextProps.system.systemComponents.length && nextProps.system.systemComponents.filter((component)=>component.name ==='Menu');
        if(!this.state.selectedMenu && menu && menu.length){
            var menuIDs = []
            menu.forEach((item)=>{ menuIDs.push(item.id) })
            this.setState({selectedMenu:menu[0].id, menuIDs: menuIDs})
        }
    }
    getComponentsByMenu(menuID){
        let components = null, content = null, self = this;
        const {systemComponents} = this.props.system
        components = systemComponents && systemComponents.length && systemComponents.filter((component)=>component.grouped_under === this.state.selectedMenu)
        content = !!components.length && components.map((component)=>{
            return self.getComponentContent(component)
        })
        return content;
    }
    checkVisibility(component) {
        let visible = component.traits.find((trait)=>(trait.name ==='visible' ? trait : null));
        const guestsAllow = component.traits.find(trait => trait.name === 'guestsAllow' ? trait : null);
        const isGuest = sessionStorage.getItem('guest');
        if(visible && !visible.value || guestsAllow && !guestsAllow.value && isGuest) {
            return false;
        }
        return true;
    }
    getComponentContent(component){
        let content = null;
        const cart = this.props.component.cart
        if(!this.checkVisibility(component)){
            return null
        }
        switch (component.name){
            case 'Dimensions':
                content = <Dimensions key={component.id} systemID={parseInt(this.props.params.id)} component={component} />;
                break;
            case 'Track':
            case 'Frame':
                content = <ComponentLayout key={component.id} type={'halfline'} systemID={parseInt(this.props.params.id)} component={component} /> ;
                break;
            case 'Glass':
            case 'GlassOptions':
            case 'HardwareOptions':
                content = <ComponentLayout key={component.id} systemID={parseInt(this.props.params.id)} type={'fullline'} component={component} /> ;
                break;
            case 'Hardware':
                content = <ComponentLayout key={component.id} systemID={parseInt(this.props.params.id)} type={'thumb'} component={component} /> ;
                break;
            case 'SwingDirection':
                content =!!cart.panels ? <SwingDirection key={component.id} component={component} /> : null;
                break;
            case 'PanelMovement':
                content = !!cart.swingDoorDirection ? <PanelMovement key={component.id} component={component} /> : null;
                break;
            case 'Interior Color':
            case 'Exterior Color':
                const {systemComponents} = this.props.system
                var groups = []
                if(systemComponents && systemComponents.length){
                    groups = systemComponents.filter((cmp)=>cmp.grouped_under === component.id && cmp.name === 'ColorGroup')
                    groups && groups.forEach((group, i)=>{
                        groups[i]['subGroups'] = systemComponents.filter((cmp)=>cmp.grouped_under === group.id && cmp.name === 'ColorSubGroup')
                    })
                }
                content = <Color key={component.id} component={component} groups={groups} /> ;
                break;
            case 'DoorPanels':
                content =cart.width && cart.height ? <DoorPanels key={component.id} component={component} /> : null;
                break;
            case 'Windows':
                content = <WindowBuilder key={component.id} component={component} /> ;
                break;
            default:
                content = '';
        }
        return content;
    }
    handleSelect(id){
        this.setState({selectedMenu:id});
    }
    nextTab = () => {
        var currentIndex = this.state.menuIDs.indexOf(this.state.selectedMenu)
        if(this.state.menuIDs[currentIndex+1]){
            this.setState({selectedMenu:this.state.menuIDs[currentIndex+1]});
        }else{
            this.lastMenuFunction()
        }
    }
    lastMenuFunction = () => {
        alert('Not implemented')
    }
    render(){
        var self = this;
        const {systemComponents} = this.props.system;
        const menu = systemComponents && systemComponents.length && systemComponents.filter((component)=>component.name ==='Menu');
        const nextTabComponent = systemComponents && systemComponents.length && systemComponents.find((component)=>component.name ==='NextTab' && component.grouped_under === self.state.selectedMenu)
        return(
            <div className="content-wrapper">
                <Row>
                    <div className="panel-default panel mainBox">
                        <div className="panel-body">
                            <div className="menuBox">
                            {!!menu.length && menu.map((menuItem)=>{
                                let title = menuItem.traits && menuItem.traits.filter((trait)=>trait.name ==='title');
                                return <div key={menuItem.id} onClick={self.handleSelect.bind(self,menuItem.id)} className={"optionMenu" + (self.state.selectedMenu === menuItem.id ? " optionMenuChoosen" : "")}>
                                    <span className="optionMenuText">{title && title[0] ? title[0].value : '< No title >'}</span>
                                </div>
                            })}
                            </div>
                            {self.getComponentsByMenu()}
                            {nextTabComponent ? <NextTab component={nextTabComponent} onNextTab={this.nextTab} /> : null}
                        </div>
                    </div>
                </Row>
                <Cart />
            </div>
        )
    }
}
SystemComponent.contextTypes = {
    router: React.PropTypes.object.isRequired
};
export default connect(state => ({
    system: state.system,
    component: state.component,
    cart: state.cart,
}), Object.assign({},systemAction, componentAction, cartAction))(SystemComponent);