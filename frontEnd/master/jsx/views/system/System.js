import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col, Panel, Button, Form, ControlLabel, FormControl, Radio, FormGroup } from 'react-bootstrap';
import SystemGroup from '../../components/System/SystemGroup'
import {systemAction as actions } from '../../actions';

@connect(state => (state))
export default class Guest extends React.Component {
	componentWillMount(){
		this.props.dispatch(actions.getSystemGroup());
	}
	render(){
		const {system_groups} = this.props.system
		return(
			<div className="content-wrapper">
				<Row>
					<Col sm={12} >
						{!!system_groups.length && system_groups.map((group)=>{
							return (<SystemGroup key={group.id} systemGroup={group} />)
						})}
					</Col>
				</Row>
			</div>
		)
	}
}