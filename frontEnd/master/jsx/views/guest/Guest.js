import React, { Component, PropTypes } from 'react';
import { push } from 'react-router-redux'
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { Link} from 'react-router'
import { Grid, Row, Col, Panel, Button, Form, ControlLabel, FormControl, Radio, FormGroup } from 'react-bootstrap';
import FieldGroup from '../../components/Common/FieldGroup'
import {authAction} from '../../actions';

@connect(state => (state))
export default class Guest extends React.Component {
		constructor(props){
			super(props)
			this.onSave = this.onSave.bind(this);
		}
    onSave(){
    	let guest = {}
    	if($('form[data-parsley-validate]').parsley().validate({group: 'block-' + 0})){
			$('#guestForm').serializeArray().map((data)=>{
				guest[data.name] = data.value;
			})
			this.props.dispatch(authAction.Authenticate({username:'guest',password:'guest'})).then((res)=>{
				this.props.dispatch(authAction.ProceedGuest(guest))
			.then((res)=>{
				this.props.dispatch(push('/systemGroups'))
			})
			})
     	}

    }
    render() {
        return (
            <div className="block-center mt-xl wd-xl">
                { /* START panel */ }
                <div className="panel panel-dark panel-flat">
                    <div className="panel-heading text-center">
                        <a href="#">
                            Sales Quoter  
                        </a>
                    </div>
                    <div className="panel-body">
                        <p className="text-center pv">Please fill out the form below so that we can know where to send your quote. Thank you!</p>
                        <Form id="guestForm" data-parsley-validate="" data-parsley-group="block-0" noValidate>
                        	<FieldGroup
											      id="formControlsText"
											      type="text"
											      label="Name *"
											      placeholder="Enter Name"
											      name="name"
											      data-parsley-pattern="[a-zA-Z]+(\\s+[a-zA-Z]+)*"
											      required
												  />
												  <FieldGroup
											      id="formControlsText"
											      type="email"
											      label="Email *"
											      placeholder="Enter Email"
											      name="email"
											      data-parsley-pattern="^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$"
											      required
												  />
												  <FieldGroup
											      id="formControlsText"
											      type="text"
											      label="Zip Code *"
											      name="zip"
											      placeholder="Enter Zip Code"
											      data-parsley-pattern="[0-9.]+"
											      required
												  />
												  <FieldGroup
											      id="formControlsText"
											      type="text"
											      label="Phone *"
											      name="phone"
											      placeholder="Enter Phone"
											      data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$"
											      required
												  />
												  <FormGroup>
												  	<ControlLabel>What best describes you?</ControlLabel>
												  	<Radio value="Home Owner" name="leadType" required className="required-field">Home Owner</Radio>
												  	<Radio value="Contractor" name="leadType">Contractor</Radio>
												  	<Radio value="Architect/Designer" name="leadType">Architect/Designer</Radio>
												  	<Radio value="Builder" name="leadType">Builder</Radio>
												  </FormGroup>
                        </Form>
                        <button onClick={this.onSave} className="btn btn-block btn-primary mt-lg">Proceed To Quote</button>
                        <p>Account <Link to='/login'>Login</Link></p>
                       
                    </div>
                </div>
                { /* END panel */ }
            </div>
            );
    }

}
