/* @flow */
import {
  getSystemGroup as getSystemGroupAPI,
  getSystemComponent as getSystemComponentAPI
} from '../api/system-api'
// ------------------------------------
// Constants
// ------------------------------------
import {
  SET_SYSTEMGROUPS,
  SET_SYSTEMCOMPONENTS
} from './actionTypes';

export const setSystemGroups = (payload): Action => ({
  // employee: string, loginTime: number, company: string
  type: SET_SYSTEMGROUPS,
  payload: payload
})

export const setSystemComponents = (payload): Action => ({
    type: SET_SYSTEMCOMPONENTS,
    payload: payload
})

export const getSystemGroup = (): Function => {
  return (dispatch: Function): Promise => {
    return getSystemGroupAPI()
    .then((systemgroups) => {
      dispatch(setSystemGroups(systemgroups.response_data))
    })
  }
}

export const getSystemComponent = (id): Function => {
    return (dispatch: Function): Promise => {
        return getSystemComponentAPI(id)
            .then((systemComponents) => {
                dispatch(setSystemComponents(systemComponents.response_data))
            })
    }
}