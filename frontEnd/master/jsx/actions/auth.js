import * as types from './actionTypes';
import axios from 'axios';
import * as constant from '../constant.js';
import { replace,push } from 'react-router-redux';
import * as apiUtils from '../api/api-utils'


function requestData() {
	return {type: types.LOGIN}
};

function receiveData(json) {
	return{
		type: types.LOGIN_SUCCESS,
		data: json
	}
};

function receiveError(json) {
	return {
		type: types.LOGIN_ERROR,
		data: json
	}
};

export function Authenticate({username,password}) {
		var authOptions = {
	    method: 'POST',
	    url: constant.AUTH_URL,
	    headers: {
	        'Authorization': 'Basic ' + (new Buffer(username + ':' + password).toString('base64')),
	        'Content-Type': 'application/x-www-form-urlencoded'
	    },
	    json: true
	};
	return function(dispatch) {
		dispatch(requestData());
		return axios(authOptions)
			.then(function(response) {
				if(response.data.token){
					sessionStorage.setItem("frontend_token",response.data.token)
					dispatch(receiveData(response.data));
					return response;
				}else{
                    dispatch(receiveError(response.data));
				}
			})
			.catch(function(response){
				dispatch(receiveError(response.data));
				//dispatch(push(null,'/error'));
			})
	}
};
export function ProceedGuest(data) {
	return (dispatch: Function): Promise => {
        return apiUtils.post('/guest',data)
            .then((res) => {
            	sessionStorage.setItem("guest",JSON.stringify(data))
            	return res
                //dispatch(setSystemComponents(systemComponents.response_data))
            })
    }
}