import * as types from './actionTypes';
import axios from 'axios';
import * as constant from '../constant.js';

export function setSize(id, width, height) {
    return{
        type: types.SET_COMPONENT_SIZE,
        id,
        width,
        height
    }
};

export function setPanelsAmount(amount) {
    return{
        type: types.SET_PANELS_AMOUNT,
        amount
    }
};

function requestPanelRanges() {
    return {type: types.GET_PANEL_RANGE}
};

function receivePanelRangesSuccess(data) {
    return{
        type: types.GET_PANEL_RANGE_SUCCESS,
        data
    }
};

function receivePanelRangesError(data) {
    return {
        type: types.GET_PANEL_RANGE_ERROR,
        data
    }
};

export function getPanelRanges(id, width, height) {
    return (dispatch) => {
        dispatch(requestPanelRanges());
        return axios.get(constant.SYSTEM_LIST_URL+'/'+id+'/panelRanges/width/'+width+'/height/'+height)
            .then((r) => {
                dispatch(receivePanelRangesSuccess(r.data.response_data));
            }, (r)=>{
                dispatch(receivePanelRangesError(r.data.message))
            })
    }
};

export function setSwingDoorDirection(direction) {
    return{
        type: types.SET_SWINGDOORDIRECTION,
        direction: direction
    }
};

export function setPanelMovement(num) {
    return{
        type: types.SET_PANEL_MOVEMENT,
        numberLeft: num
    }
};