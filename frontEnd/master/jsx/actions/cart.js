/* @flow */
import {
  getSiteBranding as getSiteBrandingAPI
} from '../api/branding-api'
// ------------------------------------
// Constants
// ------------------------------------
import {
  SET_CART_ITEMS
} from './actionTypes';

export const setCartItem = (payload): Action => ({
  type: SET_CART_ITEMS,
  data: payload
})

export function getCartItems(){
	return (dispatch)=>{
		let cartItems = sessionStorage.cartItems;
		console.log(cartItems)
		if(cartItems){
			dispatch(setCartItem(JSON.parse(cartItems)))
		}
	}
}
