var gulp = require('gulp'),
    gutil = require('gulp-util'),
    $ = require('gulp-load-plugins')(),
    gulpsync = $.sync(gulp),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    historyApiFallback = require('connect-history-api-fallback'),
    PluginError = $.util.PluginError,
    webpack = require('webpack'),
    WebpackDevServer = require("webpack-dev-server");

var requireDir  = require( 'require-dir' );
var configs = require('./gulp/configs');

requireDir( './gulp/', { recurse: true } );

//---------------
// TASKS
//---------------

// Builds a dev version of the site with the JS code running inside 
// of webpack-dev-server for fast compiling of changes
gulp.task('startLocal',  gulpsync.sync([
    'cleanProd',
    'vendor', 
    'assets', 
    'copyLocalEnv', 
    'webpack-dev-server'
    ]), done);


gulp.task('build',  gulpsync.sync([
  'cleanBuild',
  'cleanDist',
  'webpack:build',
  'vendor',
  'assets'
]));

gulp.task('buildLocal',  gulpsync.sync([
    'build'
    ]), done);

gulp.task('buildDev',  gulpsync.sync([
    'build'
    ]), done);

gulp.task('buildQa',  gulpsync.sync([
    'build'
    ]), done);

gulp.task('buildProd',  gulpsync.sync([
    'build'
    ]), done);


// Packaging up all server assets 
gulp.task('assets', gulpsync.sync([
    'styles:app',
    'styles:app:rtl',
    'styles:themes',
    'webpack:styles',
    'fonts',
    'images',
    'templates:index',
    'server-assets',
    'scripts:app',
    'html',
    'css'
]) );

gulp.task("setProduction", function(callback) {
   
    isProduction = true;
    callback();
});


gulp.task("webpack-dev-server", function(callback) {
    // Start a webpack-dev-server
    var compiler = webpack(require('./webpack.config_dev2.js'));

  gutil.log("[webpack-dev-server]", "http://localhost:8080/index.html");

  new WebpackDevServer(compiler, {
        // server and middleware options
        historyApiFallback: true,
        proxy: {
          '/server': 'http://localhost:8080/dist',
          '/img': 'http://localhost:8080/dist'
        }

    }).listen(8080, "localhost", function(err) {
        if(err) throw new gutil.PluginError("SQ-admin", err);
        // Server listening
        gutil.log("[webpack-dev-server]", "http://localhost:8080/SQ-admin/index.html");

        // keep the server alive or continue?
        // callback();
    });
});


// Build the Javascript Code 
gulp.task('webpack:build', function (callback) {
    webpack(configs.webpackConfig, function (err, stats) {
        if (err)
            throw new gutil.PluginError('webpack:build', err);
        gutil.log('[webpack:build] Completed\n' + stats.toString({
            assets: true,
            chunks: false,
            chunkModules: false,
            colors: true,
            hash: false,
            timings: false,
            version: false
        }));
        callback();
    });
});

/////////////////////

function done() {
    configs.log('************');
    configs.log('* All Done * ');
    configs.log('************');
}

//////////////
/// Un-used packages
//////////////
    // webpackDevMiddleware = require('webpack-dev-middleware'),
    // webpackHotMiddleware = require('webpack-hot-middleware'),
    // webpack_stream = require('webpack-stream'),
    // webpackStream = require('webpack-stream'),
    // var args = require('yargs').argv,
    // path = require('path'),
    // fs = require('fs'),
    // del = require('del'),
    