// html.js

var gulp    = require('gulp');
var configs = require('./configs');
var $ = require('gulp-load-plugins')();

gulp.task('html', function() {

    configs.log('Copying html..');
    // Minify and copy html (except vendor scripts)
    return gulp.src('./build/*.html')
        .pipe(gulp.dest(configs.paths.dist))
        ;
});


gulp.task('css', function() {

    configs.log('Copying css..');
    // Minify and copy all css (except vendor scripts)
    return gulp.src('./build/*.css')
        .pipe(gulp.dest(configs.build.styles))
        ;
});
