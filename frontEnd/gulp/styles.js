// styles.js
var gulp    = require('gulp');
var $ = require('gulp-load-plugins')();
var configs = require('./configs');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('styles:app:rtl', function() {
    configs.log('Building application RTL styles..');
    return gulp.src(configs.source.styles.app)
        .pipe($.if(configs.useSourceMaps, $.sourcemaps.init()))
        .pipe(configs.useSass ? $.sass() : $.less())
        .on("error", configs.handleError)
        .pipe($.rtlcss())
        .pipe($.if(configs.isProduction, $.cssnano(configs.cssnanoOpts)))
        .pipe($.if(configs.useSourceMaps, $.sourcemaps.write()))
        .pipe($.rename(function(path) {
            path.basename += "-rtl";
            return path;
        }))
        .pipe(gulp.dest(configs.build.styles))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('webpack:styles', function() {
    configs.log('Building Webpack styles..');
    return gulp.src(configs.source.styles.webpack)
        .pipe(configs.useSass ? $.sass() : $.less())
        .on("error", configs.handleError)
        .pipe(gulp.dest(configs.build.styles))
        .pipe(reload({
            stream: true
        }));
});


gulp.task('styles:themes', function() {
    configs.log('Building application theme styles..');
    return gulp.src(configs.source.styles.themes)
        .pipe(configs.useSass ? $.sass() : $.less())
        .on("error", configs.handleError)
        .pipe(gulp.dest(configs.build.styles))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('styles:app', function() {
    configs.log('Building application styles..');
    return gulp.src(configs.source.styles.app)
        .pipe($.if(configs.useSourceMaps, $.sourcemaps.init()))
        .pipe(configs.useSass ? $.sass() : $.less())
        .on("error", configs.handleError)
        .pipe($.if(configs.isProduction, $.cssnano(configs.cssnanoOpts)))
        .pipe($.if(configs.useSourceMaps, $.sourcemaps.write()))
        .pipe(gulp.dest(configs.build.styles))
        .pipe(reload({
            stream: true
        }));
});



