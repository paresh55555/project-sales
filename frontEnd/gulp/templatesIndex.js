// templatesIndex.js

var gulp    = require('gulp');
var configs = require('./configs');
var $ = require('gulp-load-plugins')();

gulp.task('templates:index', function() {
    return gulp.src(configs.source.templates.index)
        .pipe(gulp.dest(configs.paths.dist))
})
