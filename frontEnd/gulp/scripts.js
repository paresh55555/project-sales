// scripts.js


var gulp    = require('gulp');
var configs = require('./configs');
var $ = require('gulp-load-plugins')();

gulp.task('scripts:app', function() {
    configs.log('Copying js scripts..');
    // Minify and copy all JavaScript (except vendor scripts)
    return gulp.src('./build/*.js')
        .pipe(gulp.dest(configs.build.scripts));
});
