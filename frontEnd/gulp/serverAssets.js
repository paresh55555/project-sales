// serverAssets.js

var gulp    = require('gulp');
var configs = require('./configs');
var $ = require('gulp-load-plugins')();

gulp.task('server-assets', function() {
    return gulp.src(configs.source.serverAssets)
        .pipe(gulp.dest(configs.build.serverAssets))
})