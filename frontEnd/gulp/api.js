var gulp    = require('gulp');
var gutil   = require('gulp-util');
var configs = require('./configs');

// Remove all files from dist folder
gulp.task('setApiLocation', function(done) {
    configs.log('Setting API Location');
    var pkg = require('../package.json')

    var apiConfigFileContents = "export const API_URL = '"+configs.api+"'";
    return string_src("ApiServerConfigs.js", apiConfigFileContents).pipe(gulp.dest('master/jsx/api/'))
});


function string_src(filename, string) {
  var src = require('stream').Readable({ objectMode: true })
  src._read = function () {
    this.push(new gutil.File({
      cwd: "",
      base: "",
      path: filename,
      contents: new Buffer(string)
    }))
    this.push(null)
  }
  return src
}
