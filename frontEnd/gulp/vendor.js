// vendor.js

var gulp    = require('gulp');
var configs = require('./configs');
var fs = require('fs');
var $ = require('gulp-load-plugins')();


// VENDOR CONFIG
var vendor = {
    source: './vendor.json',
    dist: configs.paths.dist + 'vendor',
    bundle: {
        js: 'vendor.bundle.js',
        css: 'vendor.bundle.css'
    }
};
// VENDOR BUILD
// copy file from bower folder into the app vendor folder
gulp.task('vendor', function() {
    configs.log('Copying vendor assets..');

    var jsFilter = $.filter('**/*.js', {
        restore: true
    });
    var cssFilter = $.filter('**/*.css', {
        restore: true
    });
    var imgFilter = $.filter('**/*.{png,jpg}', {
        restore: true
    });
    var fontsFilter = $.filter('**/*.{ttf,woff,woff2,eof,svg}', {
        restore: true
    });
    var vendorSrc = JSON.parse(fs.readFileSync(vendor.source, 'utf8'));

    return gulp.src(vendorSrc, {
        base: 'bower_components'
    })
    .pipe($.expectFile(vendorSrc))
    .pipe(jsFilter)
    .pipe($.if(configs.isProduction, $.uglify(configs.vendorUglifyOpts)))
    .pipe($.concat(vendor.bundle.js))
    .pipe(gulp.dest(configs.build.scripts))
    .pipe(jsFilter.restore())
    .pipe(cssFilter)
    .pipe($.if(configs.isProduction, $.cssnano(configs.cssnanoOpts)))
    .pipe($.concat(vendor.bundle.css))
    .pipe(gulp.dest(configs.build.styles))
    .pipe(cssFilter.restore())
    .pipe(imgFilter)
    .pipe($.flatten())
    .pipe(gulp.dest(configs.build.images))
    .pipe(imgFilter.restore())
    .pipe(fontsFilter)
    .pipe($.flatten())
    .pipe(gulp.dest(configs.build.fonts));

});
