// webpackDevServer.js
// myTest.js

var WebpackDevServer = require("webpack-dev-server");
var gulp    = require('gulp');
var webpack = require('webpack');
var configs = require('./configs');
var gutil   = require('gulp-util');


gulp.task("webpack-dev-server_2", function(callback) {
    // Start a webpack-dev-server
    var compiler = webpack(configs.webpackConfig);

    new WebpackDevServer(compiler, {
        // server and middleware options
        historyApiFallback: true
    }).listen(8080, "localhost", function(err) {
        if(err) throw new gutil.PluginError("SQ-admin", err);
        // Server listening
        gutil.log("[webpack-dev-server]", "http://localhost:8080/SQ-admin/index.html");

        // keep the server alive or continue?
        // callback();
    });
});



