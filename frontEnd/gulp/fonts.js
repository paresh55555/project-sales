// fonts.js

var gulp    = require('gulp');
var configs = require('./configs');
var $ = require('gulp-load-plugins')();

gulp.task('fonts', function() {
    return gulp.src(configs.source.fonts)
        .pipe($.flatten())
        .pipe(gulp.dest(configs.build.fonts))
})